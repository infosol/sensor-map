#!/bin/bash

 set -e

 CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
 cd $CURRENT_PATH

 ###########################
 ## Load Configuration   ###
 
 source ./sensor-conf.sh
 
 ###########################
 ###########################

 ###########################
 # ALL VARIABLES LOADED  ### 
 # FROM sensor-conf.sh   ###
 ###########################

 PROTOCOL="http" 
 
 if [ "$USE_SSL" = "true" ]; then
 
    WITH_SSL="ssl "
    PROTOCOL="https"
    ## Gen Certificate 
    bash ./utils/cert-generator.sh $CA_CERTIFICATE_DIR
    
 fi

 export WITH_SSL=${WITH_SSL}
 export SENSOR_MAP_PORT=${SENSOR_MAP_PORT} 
 export SENSOR_FROST_PROXY_PORT=${SENSOR_FROST_PROXY_PORT}
 export SENSOR_BOARD_PORT=${SENSOR_BOARD_PORT} 
 export DOMAIN_NAME=${DOMAIN_NAME}
 
 export SENSORTHINGS_ROOT_CONTEXT=${SENSORTHINGS_ROOT_CONTEXT}
 export SENSORTHINGS_SERVER_VERSION=${SENSORTHINGS_SERVER_VERSION}

 export sensorThingsServiceRootUrl="$PROTOCOL://$DOMAIN_NAME:$SENSOR_FROST_PROXY_PORT/$SENSORTHINGS_ROOT_CONTEXT"

 export REWRITE_SENSORTHINGS_PROXY_URL_FOR_DOCKER=${REWRITE_SENSORTHINGS_PROXY_URL_FOR_DOCKER}
 
 if [[ $REWRITE_SENSORTHINGS_PROXY_URL_FOR_DOCKER == "true" ]]; then
     export INTERNAL_SENSORTHINGS_PROXY_URL="$PROTOCOL://proxy:$NGINX_PORT/$SENSORTHINGS_ROOT_CONTEXT/$SENSORTHINGS_SERVER_VERSION"
 fi

 export SENSORTHINGS_DB_USERNAME=${SENSORTHINGS_DB_USERNAME}
 export SENSORTHINGS_DB_PASSWORD=${SENSORTHINGS_DB_PASSWORD}
 
 export ENABLE_DELETE_ALL_ENTITIES=${ENABLE_DELETE_ALL_ENTITIES}

 export AUTH_TYPE=${AUTH_TYPE}
 
 echo
 
 cd sensorThings && docker compose down && docker compose up -d
 
 cd $CURRENT_PATH  
 
 cd sensor-map   && docker compose down && docker compose up --build -d 

 echo  
 echo " ==================================================================================== " 
 echo 
 echo " Servers :                                                                            "
 echo "  -  Sensor-Map-Server ( EntryPoint ) :  $PROTOCOL://$DOMAIN_NAME:$SENSOR_MAP_PORT    "
 echo "  -  SensorBoard                      :  $PROTOCOL://$DOMAIN_NAME:$SENSOR_BOARD_PORT  "
 echo "  -  FROST PROXY ( API )              :  $sensorThingsServiceRootUrl                  "
 echo 
 echo " ==================================================================================== " 
 echo
 
 ## Remove Exited containers ( roles_initializer after 15s ) 
 ( sleep 30 && docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs docker rm > /dev/null 2>&1 ) &
 
