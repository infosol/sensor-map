#!/bin/bash

 SSL_DIR=${1:-"./ca_certificate"} 

 KEY_FILE="./$SSL_DIR/ssl-key.key"
 CERT_FILE="./$SSL_DIR/ssl-cert.crt"

 if [ ! -d "$SSL_DIR" ]; then
    mkdir -p $SSL_DIR
 fi

 if [ ! -f "$KEY_FILE" ] || [ ! -f "$CERT_FILE" ]; then

    ############################################
    # AutoGen Certificate if    :            ###
    # ssl-key.key or ssl-cert.crt not exists ###
    ############################################

    key_pass=`date +%s | sha256sum | base64 | head -c 32 ; echo`
    domain_name="sensor-map.infosol.inrae.fr"
    org_name="Inrae"
    city="Orleans"
    country_code="FR"
    validity_days=3650
    openssl req -x509 -nodes -days "$validity_days" -newkey rsa:2048 -keyout "$KEY_FILE" \
            -out "$CERT_FILE" -subj "/C=$country_code/ST=State/L=$city/O=$org_name/CN=$domain_name"
    chmod -R 777 $KEY_FILE
    chmod -R 777 $CERT_FILE
    echo ; echo
 fi 
