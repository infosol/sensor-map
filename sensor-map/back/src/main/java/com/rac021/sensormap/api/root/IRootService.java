
package com.rac021.sensormap.api.root ;

import jakarta.ws.rs.PathParam ;
import jakarta.ws.rs.HeaderParam ;
import jakarta.ws.rs.core.Context ;
import jakarta.ws.rs.core.Response ;
import jakarta.servlet.http.HttpServletRequest ;
import com.rac021.sensormap.api.exceptions.BusinessException ;

/**
 *
 * @author ryahiaoui
 */

public interface IRootService {

    Object subResourceLocators ( @HeaderParam("API-key-Token") String             token   ,
                                 @HeaderParam("Accept")        String             accept  ,
                                 @HeaderParam("Cipher")        String             cipher  ,
                                 @HeaderParam("Keep")          String             keep    ,
                                 @Context                      HttpServletRequest request ,
                                 @PathParam("codeService")     String             codeService ) throws BusinessException ;

    Response authenticationCheck ( @PathParam("login") String login         ,
                                   @PathParam("signature") String signature ,
                                   @PathParam("timeStamp") String timeStamp) throws Exception ;

}
