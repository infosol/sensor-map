
package com.rac021.sensormap.impl.service.sensorthings ;

import java.util.Map ;
import java.util.HashMap ;
import java.sql.ResultSet ;
import java.sql.Connection ;
import java.sql.CallableStatement ;
import java.sql.ResultSetMetaData ;

/**
 *
 * @author ryahiaoui
 */

public class Querier    {
    
    public static  Map<Integer, String > extractColumnsName( Connection conn     ,
                                                             String     sqlQuery ) throws Exception {
        
        if( conn == null     )                       return null ;
        
        if( sqlQuery == null || sqlQuery.isBlank() ) return null ;
        
        Map<Integer, String > colNames = new HashMap<>()         ;

        try ( CallableStatement call = conn.prepareCall( sqlQuery                        , 
                                                         ResultSet.TYPE_SCROLL_SENSITIVE , 
                                                         ResultSet.CONCUR_READ_ONLY   )  )  {       
            call.execute()                                              ;
            
            ResultSetMetaData res  =  call.getResultSet().getMetaData() ; 
 
            int columnCount        =  res.getColumnCount()              ;
 
            for ( int i = 0 ; i < columnCount ; i++ )        {
 
                colNames.put( i, res.getColumnLabel( i + 1 ) )          ;
            }

        } catch ( Exception ex ) {
            throw new RuntimeException(ex) ; 
        }
            
        return colNames ;
    }
}

