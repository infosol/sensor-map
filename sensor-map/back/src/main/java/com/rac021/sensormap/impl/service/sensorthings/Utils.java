
package com.rac021.sensormap.impl.service.sensorthings ;

import java.io.File ;
import java.util.List ;
import jakarta.json.Json ;
import org.json.JSONArray ;
import java.io.IOException ;
import org.json.JSONObject ;
import java.nio.file.Files ;
import okhttp3.OkHttpClient ;
import jakarta.json.JsonObject ;
import java.util.regex.Matcher ;
import java.util.regex.Pattern ;
import java.util.stream.Stream ;
import javax.net.ssl.SSLContext ;
import javax.net.ssl.SSLSession ;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager ;
import javax.net.ssl.HostnameVerifier ;
import javax.net.ssl.SSLSocketFactory ;
import jakarta.ws.rs.core.MultivaluedMap ;
import java.security.cert.CertificateException ;
import org.apache.commons.lang3.math.NumberUtils ;

/**
 *
 * @author ryahiaoui
 */
public class Utils {
    
    
       
    public static String getFileName(MultivaluedMap<String, String> header ) {

	String[] contentDisposition =
                 header.getFirst("Content-Disposition").split(";")           ;

	for ( String filename : contentDisposition)         {
	      if ((filename.trim().startsWith("filename"))) {
		  String[] name        = filename.split("=" )                ;
		  String finalFileName = name[1].trim().replaceAll("\"", "") ;
		  return finalFileName                                       ;
	      }
	}
	return "unknown" ;
    }
    
    public static String applyValue( final List<String> columns    , 
                                      final List<String> values    , 
                                      final String       template  )  {
        
        if( columns == null || values == null || 
            template == null ) return template ;
        
        StringBuilder templateInstance = new StringBuilder(template.replace( "\\\"", "\"") ) ;
        
        for ( int i = 0; i < columns.size() ; i++ ) {
            replaceAllPattern ( templateInstance                       , 
                                Pattern.compile("(?i)\\{\\{" + columns.get(i) +"\\}\\}"  ) ,
                                values.get(i).replaceAll( "\"", "'") ) ;
        }
        
        return templateInstance.toString()                             ;
    }
    
    public static String removeDoubleQuotesIfNumber(String argTemplate )       {
        
        String template = argTemplate                                          ;
            
        Pattern pattern = Pattern.compile( "\\{\\{.*?\\}\\}", Pattern.DOTALL ) ;
        Matcher matcher = pattern.matcher( template )                          ;
        
        while (matcher.find())  {
            template = template.replace( "\"" + matcher.group( 0 ) + "^Num\""  ,
                                         matcher.group( 0 ) )                  ;
        }
          
        return template ;
    }
    
    public static Object handleJsonValue( Object value) {
        
        if ( value instanceof JSONObject )        {
             handleJSONObject((JSONObject) value) ;
        } else if (value instanceof JSONArray )   {
            handleJSONArray( (JSONArray ) value ) ;
        }
        
        return value ;
    }
    
    private static JSONObject handleJSONObject( JSONObject jsonObject ) {

        jsonObject.keys().forEachRemaining( key ->          {

            Object     value = jsonObject.get( key )        ;
            
            if( key.equals( "@iot.id")) {
                jsonObject.put( key, String.valueOf( value  )) ;
                
            } else {
                
              handleJsonValue( value  ) ;

            }
        } ) ;

        return jsonObject ;
    }

    private static JSONArray handleJSONArray( JSONArray jsonArray ) {

        jsonArray.iterator().forEachRemaining(element -> {
            handleJsonValue( element ) ;
        } ) ;
        
        return jsonArray ;
    }
    
    public static String extractNumFromName( String sNum ) {
        
       String check = sNum.split( Pattern.quote( "."))[0].split( "_", 2 )[0] ;
        
       int toInt = NumberUtils.toInt( check , -99999 )                       ;
        
       if( toInt != -99999 ) return String.valueOf( toInt )                  ;
        
       return sNum                                                           ;
    }
    
    public static boolean deleteDirectory(File directoryToBeDeleted)        {
        
        File[] allContents = directoryToBeDeleted.listFiles() ;
        
        if (allContents != null)             {

            for (File file : allContents )   {
                deleteDirectory( file )      ;
            }
        }

        return directoryToBeDeleted.delete() ;
   }
    
   public static boolean isEmpty( java.nio.file.Path path) throws IOException {

      if (Files.isDirectory(path)) {
          try (Stream< java.nio.file.Path> entries = Files.list(path)) {
              return !entries.findFirst().isPresent() ;
          }
      }

      return false ;
   }
  
     public static void replaceAllPattern( StringBuilder sb           , 
                                            Pattern       pattern      , 
                                            String        replacement  ) {
        Matcher m = pattern.matcher(sb) ;
        int start = 0                   ;
        while (m.find(start))  {
            sb.replace(m.start() , m.end(), replacement) ;
            start = m.start()    + replacement.length()  ;
        }
    }
 
    public static JsonObject createJsonObjMssg( String key, String mssg ) {
       
      return Json.createObjectBuilder().add( key, mssg ).build()   ;
    }
    
    public static OkHttpClient.Builder configureToIgnoreCertificate(OkHttpClient.Builder builder) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }
                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
                                throws CertificateException {
                        }
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            } ;
            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true ;
                }
            } ) ;
        } catch (Exception e) {
           throw new RuntimeException(e) ;
        }
        return builder ;
    }
    
    public static String  overrideSensorThingsUrl( String sensorThingsUrl    ,
                                                   String sensorThingsVer    ,
                                                   Boolean overrideUrl       ,
                                                   String newSensorThingsUrl ) {
        
        if( overrideUrl && newSensorThingsUrl != null && ! newSensorThingsUrl.isBlank()) {
            
            System.out.println("")  ;
            
            String overridedSTAUrl = newSensorThingsUrl          + 
                                     sensorThingsUrl.split(sensorThingsVer)[1]   ;
            System.out.println( " => Override sensorThingsUrl [ " + 
                                  sensorThingsUrl + " ] By "      +
                                  overridedSTAUrl )               ;
            System.out.println("")  ;
            
            return overridedSTAUrl  ;
        }
        return sensorThingsUrl ;
    }
}
