
package com.rac021.sensormap.impl.service.sensorthings ;

import java.util.Map ;
import java.util.HashMap ;
import java.sql.ResultSet ;
import java.sql.Connection ;
import java.sql.SQLException ;
import java.util.logging.Level ;
import java.util.logging.Logger ;
import java.sql.PreparedStatement ;
import java.util.concurrent.TimeUnit ;
import com.lmax.disruptor.RingBuffer ;
import com.lmax.disruptor.dsl.Disruptor ;
import com.rac021.sensormap.impl.io.Writer ;

public class Producer {

    static final String NULL_KEY_WORD = "{}" ;
      
    private final Connection  connnnection   ;
    
    private PreparedStatement stm            ;
    private ResultSet         rs             ; 
    
    private final String      jobID          ;
    private final String      pathLog        ;
    
    private final ConsumerHandler consumerHandler ;
   
    private final Disruptor<ValueEvent> disruptor ;

    Producer( Connection            connection ,
              Disruptor<ValueEvent> disruptor  ,
              ConsumerHandler consumerHandler  ,
              String jobID                     ,
              String  pathLog                  ) throws Exception {
        
        this.jobID        = jobID              ;
        this.pathLog      = pathLog            ;
        this.connnnection = connection         ;
        connnnection.setAutoCommit( false )    ;

        this.disruptor       = disruptor       ; 
        this.consumerHandler = consumerHandler ;
    }

    void produce( String sqlQuery  ,
                  int    fetchSize ) throws Exception  {
       
        if( sqlQuery == null || sqlQuery.isBlank() ) {
            System.out.println("SqlQuery or Colums is Null or Empty !") ;
            return ;
        }
         
        try { 
           Map<Integer, String> columns = Querier.extractColumnsName( connnnection, sqlQuery     ) ;
           
           this.stm = this.connnnection.prepareStatement( sqlQuery , ResultSet.TYPE_FORWARD_ONLY ) ;
           
           stm.setFetchSize( fetchSize ) ;

           this.rs =  stm.executeQuery() ;

           RingBuffer<ValueEvent> ringBuffer = disruptor.start() ; 

           while ( rs.next() ) {

               Map<String, String> res = new HashMap<>()      ; 
               for ( int columnIdx : columns.keySet() )       {
                 res.put( columns.get( columnIdx )            , 
                          rs.getString( columns.get(columnIdx ) ) ) ;
               }
               
               long seq = ringBuffer.next()                   ;
               ValueEvent valueEvent = ringBuffer.get(seq)    ;
               valueEvent.setValue(res) ;
               ringBuffer.publish(seq)  ;
           }
               
        } catch ( Exception e )               {
         
           disruptor.shutdown()                ;
           consumerHandler.setThrowException() ;
           throw new RuntimeException( e )     ;
         
        } finally {

           closeResources( connnnection, stm, rs  , jobID ) ;   
                   
           try {
                TimeUnit.MILLISECONDS.sleep( 200  ) ;
                disruptor.shutdown()                ;
           } catch ( InterruptedException ex )    {
               Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex ) ;
               throw new RuntimeException(ex) ;
           }
           
           if( ! consumerHandler.isThrowException() )      {
               String mssg = "Sensor-Map Finished.  ID : " +
                             jobID + " => SUCCESS  "       ;

               Writer.writeTextFile(mssg, pathLog   )      ;
           }
        }
    }

    private static void closeResources( Connection       conn   ,
                                        PreparedStatement ps    , 
                                        ResultSet         rs    ,
                                        String            jobID ) {
    
        try ( conn; ps; rs )       {
             System.out.println( "# Close Connection  : " + jobID ) ;
        } catch ( SQLException e ) {
            System.out.println("Failed to Properly Close Database Sources" + e ) ;
            throw new RuntimeException( e ) ;
        }
    }

}
