
package com.rac021.sensormap.impl.service.sensorthings ;

import java.util.Map ;
import org.json.JSONObject ;
import okhttp3.OkHttpClient ;
import okhttp3.ConnectionPool ;
import java.util.logging.Level ;
import java.util.regex.Pattern ;
import java.util.logging.Logger ;
import java.util.concurrent.TimeUnit ;
import com.lmax.disruptor.EventHandler ;
import com.rac021.sensormap.impl.io.Writer;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.handleJsonValue ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.replaceAllPattern ;

/**
 *
 * @author ryahiaoui
 */
public final class ConsumerHandler implements EventHandler<ValueEvent> {

    private final String       template                ;
    private final String       sensorthingsEndpointUrl ;
    private final String       jobID                   ;
    private final String       pathLog                 ;
    private final OkHttpClient client                  ;

    private boolean            throwException          ;

    public ConsumerHandler( String template                ,
                            String sensorthingsEndpointUrl ,
                            String jobID                   ,
                            String pathLog               ) {
         
         throwException = false                                 ;
         this.template  = template                              ;
         this.jobID     = jobID                                 ;
         this.pathLog   = pathLog                               ;
         this.sensorthingsEndpointUrl = sensorthingsEndpointUrl ;
         
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder() ;
         if( sensorthingsEndpointUrl != null && 
             sensorthingsEndpointUrl.trim().toLowerCase().startsWith("https") ) {
              clientBuilder = Utils.configureToIgnoreCertificate(clientBuilder) ;
         }
         this.client =  clientBuilder.connectTimeout( 500 , TimeUnit.SECONDS )
                                     .writeTimeout  ( 500 , TimeUnit.SECONDS )
                                     .readTimeout   ( 500 , TimeUnit.SECONDS )
                                     .connectionPool( new ConnectionPool( 1 , 1L ,
                                                      TimeUnit.MINUTES ) )
                                     .build()  ;
    }

    @Override
    public void onStart() { }
    
    @Override
    public void onShutdown() { }

    @Override
    public void onEvent( final ValueEvent event      ,
                         final long       sequence   , 
                         final boolean    endOfBatch ) throws Exception    {
        processEvent( event )  ;
    }

    private void processEvent( final ValueEvent event ) throws Exception   {

       
       try {
           
            String data = applyValue( event.getValue() )   ;

            JSONObject jsonObject = new JSONObject(data)   ;

            String instanceTemplate = handleJsonValue( jsonObject ).toString() ;
            
            DataLoader.postDatas( client                   ,
                                  sensorthingsEndpointUrl  ,
                                  instanceTemplate         ,
                                  "application/json"       ,
                                  true                  )  ;

        } catch (Exception ex   ) {

            Logger.getLogger( ServiceSensorThings.class.getName())
                  .log(Level.SEVERE, null, ex  ) ;
               
            String mssg = "\nERROR : ID ( "    +
                          jobID                +
                          " ) [[ \n  "         +
                          ex.getMessage()      +
                          "\n]]\n"             ;

            Writer.writeTextFile(mssg, pathLog ) ;
            
            throwException = true                ;
            
            throw new RuntimeException( ex )     ;
        }
        
        event.clear() ; 
    }
    
    public void setThrowException()   {
       this.throwException = true     ;
    }
    
    public boolean isThrowException() {
       return this.throwException     ;
    }
       
    private String applyValue( final Map<String, String> map  )  {
        
        if( map == null || template == null ) return template ;
        
        StringBuilder templateInstance = new StringBuilder( template )  ;
        
        map.forEach(( k, v) -> {
            replaceAllPattern ( templateInstance                         , 
                                Pattern.compile("(?i)\\{\\{"+k+"\\}\\}") ,
                                v == null ? "" : v.replace("\"", "'"   ) ) ;
        } ) ;
        
        return templateInstance.toString() ;
    }
}
