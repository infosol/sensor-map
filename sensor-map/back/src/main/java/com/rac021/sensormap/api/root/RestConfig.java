
package com.rac021.jaxy.api.root ;

import jakarta.ws.rs.ApplicationPath ;
import jakarta.ws.rs.core.Application ;

/**
 *
 * @author ryahiaoui
 */

@ApplicationPath("rest")
public class RestConfig extends Application {
}
