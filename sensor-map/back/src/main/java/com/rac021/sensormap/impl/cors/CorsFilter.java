
package com.rac021.sensormap.impl.cors ;

/**
 *
 * @author ryahiaoui
 */

import java.io.IOException ;

import jakarta.ws.rs.ext.Provider ;
import jakarta.ws.rs.container.ContainerRequestContext ;
import jakarta.ws.rs.container.ContainerResponseFilter ;
import jakarta.ws.rs.container.ContainerResponseContext ;

@Provider
public class CorsFilter implements ContainerResponseFilter {

    @Override
    public void filter( final ContainerRequestContext requestContext , 
                        final ContainerResponseContext cres )  throws IOException {

        cres.getHeaders().add("sensorMap", "0.9") ;
        cres.getHeaders().add("Access-Control-Allow-Origin", "*") ;
        cres.getHeaders().add("Access-Control-Max-Age", "1209600") ;
        cres.getHeaders().add("Access-Control-Allow-Credentials", "true") ;
        cres.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, OPTIONS, HEAD " ) ;
        /** cres.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD") ; . */
        cres.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization, X-Requested-With ") ;
    }

}
