
package com.rac021.sensormap.impl.service.sensorthings ;

import java.sql.Connection ;
import java.sql.SQLException ;
import org.apache.commons.dbcp.BasicDataSource ;

/**
 *
 * @author ryahiaoui
 */

public class DataBaseCPDataSource {
    
    private final BasicDataSource ds = new BasicDataSource() ;

    public void create( String url, String user,  String password, int maxActiveConnections ) {
        
        ds.setUrl( url )                        ;
        ds.setUsername( user     )              ;
        ds.setPassword( password )              ;
        ds.setMinIdle( 5  )                     ;
        ds.setMaxIdle( 10 )                     ;
        ds.setMaxActive( maxActiveConnections ) ;
        ds.setMaxOpenPreparedStatements( maxActiveConnections * 10 )  ;
    }
    
    public Connection getConnection() throws SQLException   {
        
        return ds.getConnection()  ;
    }
}

