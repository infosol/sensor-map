
package com.rac021.sensormap.impl.service.sensorthings ;

import java.net.URL ;
import okhttp3.Request ;
import okhttp3.Response ;
import okhttp3.MediaType ;
import okhttp3.RequestBody ;
import java.io.IOException ;
import okhttp3.OkHttpClient ;
import java.net.HttpURLConnection ;

/**
 *
 * @author ryahiaoui
 */

public class DataLoader {
         
    public static void postDatas ( OkHttpClient  client    ,
                                   String        url       ,
                                   String        data      ,
                                   String        mdiaType  ,
                                   boolean       verbose   ) throws Exception  {
        
        MediaType mediaType= MediaType.parse( mdiaType ) ;
       
        if( data != null && ! data.isEmpty() ) { 
            
            loadDatas ( url        ,
                        mediaType  ,
                        data       ,
                        client     ,
                        verbose    ) ;
        }
    }

    private static void loadDatas( String       url        , 
                                   MediaType    mediaType  , 
                                   String       data       ,
                                   OkHttpClient client     ,
                                   boolean      verbose    ) throws Exception  {

        RequestBody body = RequestBody.create( mediaType , data ) ;

        Request request  = new Request.Builder()
                                      .url(url)
                                      .post(body)
                                      .addHeader("content-type", "application/json")
                                      .build() ;

        try ( Response res = client.newCall(request ).execute() )     {

		if ( !res.isSuccessful()) {
                    throw new IOException ( "" + ( res.body() != null ? 
                                            res.body().string().replace("\\\"", "\"") : 
                                            res.body() ) )                            ;
                }
                
        } catch ( Exception ex )   {
            throw ex ;
        }
    }
      
    public static boolean isReachable(String url) {
 
	try {
		URL urlObj            = new URL(url)                                ;
		HttpURLConnection con = (HttpURLConnection) urlObj.openConnection() ;
		con.setRequestMethod( "GET" )    ;
                // Set connection timeout
		con.setConnectTimeout( 2500 )    ;
		con.connect()                    ;
 
		int code = con.getResponseCode() ;
		if (code == 200 || code == 404 ) {
			return true ;
		}
	} catch (IOException e )    {
		return false        ;
	}
        return false                ;
    }
}
