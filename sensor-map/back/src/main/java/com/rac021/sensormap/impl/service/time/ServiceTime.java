
package com.rac021.sensormap.impl.service.time ;

/**
 *
 * @author ryahiaoui
 */

import jakarta.ws.rs.GET ;
import java.time.Instant ;
import jakarta.ws.rs.Produces ;
import jakarta.inject.Singleton ;
import io.quarkus.runtime.Startup ;
import jakarta.ws.rs.core.Response ;
import jakarta.annotation.PostConstruct ;
import com.rac021.sensormap.api.qualifiers.ServiceRegistry ;

/**
 *
 * @author R.Yahiaoui
 */


@ServiceRegistry("time")
@Singleton
@Startup
public class ServiceTime {

    @PostConstruct
    public void init()   {
    }

    public ServiceTime() {
    }

    @GET
    @Produces({ "xml/plain", "json/plain" })
    public Response getTime() throws InterruptedException {
        
       System.out.println(" Time Service : " + this ) ;

       return Response.status(Response.Status.OK)
                      .entity(String.valueOf(Instant.now().getEpochSecond() ))
                      .build() ;
    }
}
