
package com.rac021.sensormap.impl.service.sensorthings ;

import org.apache.commons.lang3.StringUtils ;

/**
 *
 * @author ryahiaoui
 */

public class FileParserUtils {
    
    public static String getLogin( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##LOGIN##                     :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getSensorMapEndpoint( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##SensorMapEndPoint##         :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getDbHost( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##DB_HOST##                   :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getDbPort( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##DB_PORT##                   :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getDbName( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##DB_NAME##                   :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getDbSchema( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##DB_SCHEMA##                 :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getDbUser( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##DB_USER##                   :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getSchema( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##LOGIN##                     :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getSensorType( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##SensorType##                :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getSensorThingEndpointUrl( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##SensorThingsEndpointUrl##   :" ,
                                            "\n" ).trim()                     ;
    }
    
    public static String getSensorTemplate( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##SensorTemplate##            :" ,
                                            "##SqlQuery##"  ).trim()          ;
    }
    
    public static String getSqlQuery( String content ) {
        
       return StringUtils.substringBetween( content                           , 
                                            "##SqlQuery##                  :" ,
                                            "##SqlQueryEnd##" ).trim()        ;
    }
}
