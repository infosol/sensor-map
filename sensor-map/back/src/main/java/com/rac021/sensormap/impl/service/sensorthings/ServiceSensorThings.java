
package com.rac021.sensormap.impl.service.sensorthings ;

/**
 *
 * @author ryahiaoui
 */

import java.io.File ;
import java.util.Map ;
import java.util.List ;
import java.util.Arrays ;
import jakarta.ws.rs.GET ;
import jakarta.json.Json ;
import java.time.Instant ;
import jakarta.ws.rs.Path ;
import jakarta.ws.rs.POST ;
import org.json.JSONObject ;
import java.net.URLDecoder ;
import java.sql.Connection ;
import java.io.InputStream ;
import java.io.IOException ;
import java.nio.file.Files ;
import java.nio.file.Paths ;
import java.util.ArrayList ;
import okhttp3.OkHttpClient ;
import java.util.Properties ;
import java.io.OutputStream ;
import jakarta.inject.Inject ;
import jakarta.ws.rs.OPTIONS ;
import jakarta.ws.rs.Consumes ;
import jakarta.ws.rs.Produces ;
import java.io.BufferedReader ;
import okhttp3.ConnectionPool ;
import java.sql.DriverManager ;
import jakarta.json.JsonObject ;
import java.util.logging.Level ;
import java.time.LocalDateTime ;
import jakarta.inject.Singleton ;
import java.util.logging.Logger ;
import java.io.FileOutputStream ;
import jakarta.ws.rs.HeaderParam ;
import net.lingala.zip4j.ZipFile ;
import java.io.InputStreamReader ;
import jakarta.ws.rs.core.Response ;
import jakarta.ws.rs.core.MediaType ;
import java.io.BufferedOutputStream ;
import org.apache.commons.io.IOUtils ;
import java.util.concurrent.TimeUnit ;
import java.util.concurrent.Callable ;
import java.util.concurrent.Executors ;
import io.quarkus.runtime.StartupEvent ;
import com.lmax.disruptor.dsl.Disruptor ;
import jakarta.annotation.PostConstruct ;
import jakarta.enterprise.event.Observes ;
import jakarta.ws.rs.core.MultivaluedMap ;
import jakarta.ws.rs.container.Suspended ;
import java.nio.charset.StandardCharsets ;
import java.time.format.DateTimeFormatter ;
import com.lmax.disruptor.dsl.ProducerType ;
import com.rac021.sensormap.impl.io.Writer ;
import java.util.concurrent.ExecutorService ;
import jakarta.ws.rs.container.AsyncResponse ;
import com.lmax.disruptor.SleepingWaitStrategy ;
import com.lmax.disruptor.FatalExceptionHandler ;
import com.rac021.sensormap.api.security.Policy ;
import static java.util.logging.Logger.getLogger ;
import com.rac021.sensormap.api.security.Secured ;
import com.lmax.disruptor.util.DaemonThreadFactory ;
import com.rac021.sensormap.api.qualifiers.ServiceRegistry ;
import com.rac021.sensormap.impl.utils.checker.TokenManager ;
import org.eclipse.microprofile.config.inject.ConfigProperty ;
import org.jboss.resteasy.plugins.providers.multipart.InputPart ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.isEmpty ;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.applyValue ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.getFileName ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.deleteDirectory ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.handleJsonValue ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.createJsonObjMssg ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.extractNumFromName ;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.overrideSensorThingsUrl;
import static com.rac021.sensormap.impl.service.sensorthings.Utils.removeDoubleQuotesIfNumber ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry( "sensorThings" )
@Secured(policy = Policy.CustomSignOn )
@Singleton

public class ServiceSensorThings              {

    void onStart(@Observes StartupEvent ev) { }

    private final String TMP_DIRECTORY    = "tmp_data" ;
    
    private static final int  timeOut     = 10         ; // In Seconds
    private static final int  fetshSize   = 10_000     ; 
    private static final int  bufferSize  = 8 * 1024   ;
                    
    private static ExecutorService executor = Executors.newFixedThreadPool( 5 ) ;
    
    @Inject
    @ConfigProperty(name = "INTERNAL_SENSORTHINGS_PROXY_URL", defaultValue = "" )
    private String internalSensorThingsProxyURL ;
    
    @Inject
    @ConfigProperty(name = "SENSORTHINGS_SERVER_VERSION", defaultValue = "v1.1" )
    private String sensorThingsVersion ;
    
    @Inject
    @ConfigProperty(name = "REWRITE_SENSORTHINGS_PROXY_URL_FOR_DOCKER", defaultValue = "false" )
    private Boolean rewriteSensorThingsProxyURLForDocker ;
 
    @PostConstruct
    public void init() throws IOException  {
      System.out.println( " + ServiceSensorThings Instanciation : " + this )   ;
      getLogger("io.vertx.core.impl.BlockedThreadChecker").setLevel(Level.OFF) ;
      Files.createDirectories( Paths.get( TMP_DIRECTORY ) )                    ;
    }

    @GET
    @OPTIONS
    @Produces({"xml/plain","json/plain", "xml/encrypted","json/encrypted"} )
    public Response fromDbToSensoThings( @HeaderParam("sensorType")    String sensorType   ,
                                         @HeaderParam("sqlQuery")      String sqlQuery     ,
                                         @HeaderParam("templateQuery") String template     ,
                                         @HeaderParam("db_host")       String db_host      ,
                                         @HeaderParam("db_port")       String db_port      ,
                                         @HeaderParam("db_name")       String db_name      ,
                                         @HeaderParam("db_schema")     String db_schema    ,
                                         @HeaderParam("db_user")       String db_user      ,
                                         @HeaderParam("db_password")   String db_password  ,
                                         @HeaderParam("API-key-Token") String token        ,
                                         @HeaderParam("sensorthings_endpoint_url") String sensorthings_endpoint_url ) throws Exception {
        
        System.out.println(" Call : ServiceSensorThings... ") ;
      
        if( sensorType == null ) {
           JsonObject respObj = createJsonObjMssg("Error","SensorType Can't be Null")    ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build() ;
        }
        
        if( sqlQuery == null ) {
           JsonObject respObj = createJsonObjMssg("Error","Sql Query Can't be Null")     ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build() ;
        }
        
        if( template == null || template.equalsIgnoreCase("undefined")) {
           JsonObject respObj = createJsonObjMssg("Error","Template Can't be Null. Select Template") ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build()             ;
        }
        
        if( db_host == null ) {
           JsonObject respObj = createJsonObjMssg("Error","DB Host Can't be Null")       ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build() ;
        }
        
        if( db_port == null ) {
           JsonObject respObj = createJsonObjMssg("Error","DB Port Can't be Null")        ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build()  ;
        }
        
        if( db_name == null ) {
           JsonObject respObj = createJsonObjMssg("Error","DB NAME Can't be Null") ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build()  ;
        }
        
        if( db_user == null ) {
           JsonObject respObj = createJsonObjMssg("Error","DB USER Can't be Null")        ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build()  ;
        }
        
        if( db_password == null ) {
           JsonObject respObj = createJsonObjMssg("Error","DB PASSWORD Can't be Null")    ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build()  ;
        }
        
        if( sensorthings_endpoint_url == null ) {
           JsonObject respObj = createJsonObjMssg("Error","SensorThings Endpoint Can't be Null") ;
           return Response.status(Response.Status.OK).entity(respObj.toString()).build()         ;
        }
        
        sqlQuery                             = URLDecoder.decode( sqlQuery, StandardCharsets.UTF_8.toString())   ;
       
        sensorType                           = URLDecoder.decode( sensorType, StandardCharsets.UTF_8.toString()) ;
        
        String sensorthings_endpoint_url_dec = URLDecoder.decode( sensorthings_endpoint_url        ,
                                                                  StandardCharsets.UTF_8.toString())   ;
        
        sensorthings_endpoint_url  = (( sensorthings_endpoint_url_dec != null &&
                                        sensorthings_endpoint_url_dec.endsWith("/") ) ?
                                        sensorthings_endpoint_url_dec + sensorType                     :
                                        sensorthings_endpoint_url_dec + "/" + sensorType ).trim()      ;
        
        template                   = removeDoubleQuotesIfNumber ( URLDecoder.decode( template          , 
                                                                  StandardCharsets.UTF_8.toString()) ) ;
        String dbSchema  =  db_schema == null ? "public" : db_schema    ;
        
        db_host          =  URLDecoder.decode( db_host     , "UTF-8"  ) ;
        db_name          =  URLDecoder.decode( db_name     , "UTF-8"  ) ;
        db_user          =  URLDecoder.decode( db_user     , "UTF-8"  ) ;
        db_port          =  URLDecoder.decode( db_port     , "UTF-8"  ) ;
        db_password      =  URLDecoder.decode( db_password , "UTF-8"  ) ;
        
        System.out.println("                                               " ) ;
        System.out.println(" ##########################################    " ) ;
        System.out.println("                                               " ) ;
        System.out.println(" db_name                       : " + db_name   ) ;
        System.out.println(" db_host                       : " + db_host   ) ;
        System.out.println(" db_user                       : " + db_user   ) ;
        System.out.println(" db_port                       : " + db_port   ) ;

        System.out.println(" sensorthings_endpoint_url     : " + 
                             sensorthings_endpoint_url                     ) ;

        System.out.println(" sensorType                    : " + sensorType) ;
        System.out.println(" template                      : " + template.replace( "\n", " " ) ) ;
        System.out.println(" sqlQuery                      : " + sqlQuery.replace( "\n", " " ) ) ;
        System.out.println("                                             " ) ;
        System.out.println(" ##########################################  " ) ;
        System.out.println(" ##########################################  " ) ;
        System.out.println("                                             " ) ;

         String _sqlQuery                  =  sqlQuery                 ;
         String _sensorthings_endpoint_url = sensorthings_endpoint_url ;
         String _template                  = template                  ;
         String _dbSchema                  = dbSchema                  ;
         String _db_port                   = db_port                   ;
         String _db_host                   = db_host                   ;
         String _db_name                   = db_name                   ;
         String _db_user                   = db_user                   ;
         String _db_password               = db_password               ;
         String _token                     = token                     ;
         
         String jobID = LocalDateTime.now().format(DateTimeFormatter
                                     .ofPattern("dd-MM-yyyy_HH:mm:ss.SSS" )) ;
         executor.execute( () -> {

            try {
                
                  processEtl( _sqlQuery    , _sensorthings_endpoint_url ,
                              _template    , _dbSchema                  ,
                              _db_port     , _db_host                   ,
                              _db_name     , _db_user                   ,
                              _db_password , _token                     ,
                              null         , jobID )                    ;

            } catch ( Exception ex )         {
              throw new RuntimeException(ex) ;
            }
        } ) ;
        
        return Response.status(Response.Status.ACCEPTED )
                                       .entity( "Status : Launched ( ID = "       + 
                                                jobID + " ) Take a look at Logs " )
                                       .build() ;
    }
  
    private boolean processEtl( String  sqlQuery                    , 
                                String  sensorthings_endpoint_url   , 
                                String  template                    ,
                                String  dbSchema                    , 
                                String  db_port                     , 
                                String  db_host                     , 
                                String  db_name                     ,
                                String  db_user                     , 
                                String  db_password                 ,
                                String  token                       ,
                                String  fileName                    ,
                                String  jobID  )  throws Exception  {
        
        String     path_logs    = ""    ;
        boolean    connectionOK = false ;
        Connection connection   = null  ;
         
        try {
              String  login     = TokenManager.getLogin(token)                 ;
                      path_logs = TokenManager.builPathLog( "logger" , login ) ;
             
              Writer.createFile( path_logs )                                   ;

              if ( fileName != null ) {
                  
                  Writer.writeTextFile( Arrays.asList  ( " \n++ Process File           : " +
                                                         fileName ) ,  path_logs         ) ;
              }
              
              Writer.writeTextFile( "Sensor-map Started...  ID : " + 
                                    jobID, path_logs )             ;

              Disruptor<ValueEvent> disruptor = new Disruptor<>( ValueEvent::new               , 
                                                                 bufferSize                    ,
                                                                 DaemonThreadFactory.INSTANCE  , 
                                                                 ProducerType.SINGLE           , 
                                                                 new SleepingWaitStrategy()  ) ; // new BlockingWaitStrategy()
              
              disruptor.setDefaultExceptionHandler(new FatalExceptionHandler() ) ;

              ConsumerHandler consumerHandler  = new ConsumerHandler( template                  ,
                                                                      overrideSensorThingsUrl( sensorthings_endpoint_url            ,
                                                                                               sensorThingsVersion                  ,
                                                                                               rewriteSensorThingsProxyURLForDocker , 
                                                                                               internalSensorThingsProxyURL )       ,
                                                                      jobID                     , 
                                                                       path_logs              ) ;
              disruptor.handleEventsWith( consumerHandler ) ;
              
              String jdbcUrl = "jdbc:postgresql://" + db_host + ":" + db_port + "/" + 
                               db_name + "?currentSchema=" + dbSchema               ;
              
              DataBaseCPDataSource dataSource = new DataBaseCPDataSource() ;
              
              dataSource.create( jdbcUrl     , 
                                 db_user     , 
                                 db_password ,
                                 1         ) ;
               
              connection   = dataSource.getConnection() ;
         
              connectionOK = true ; // If true then Connection will be Closed in the Producer
              
              new Producer( connection      ,
                            disruptor       ,
                            consumerHandler , 
                            jobID           , 
                            path_logs  ).produce ( sqlQuery, fetshSize ) ;
              
              return true ;
            
        } catch( Exception ex ) {
            
            String mssg = "\nERROR : ID ( " +
                          jobID             +
                          " ) [[ \n  "      +
                          ex.getMessage()   +
                          "\n]]\n"          ;
            
            Writer.writeTextFile( mssg, path_logs ) ;
            throw new RuntimeException(ex)          ;
        
        } finally {
            if( ! connectionOK ) {
                System.out.println ( "# Connection : " + jobID  +
                                   " - Not Created ! " )        ;
            }
            else if( connection != null && ! connection.isClosed() ) {
                connection.close()  ;
            } 
        }
    }
    
    @GET
    @OPTIONS
    @Path("/headers")
    @Produces( { "xml/plain", "json/plain", "xml/encrypted","json/encrypted" } )
    public Response getHeaders( @HeaderParam("sqlQuery")    String _sqlQuery   ,
                                @HeaderParam("db_host")     String db_host     ,
                                @HeaderParam("db_port")     String db_port     ,
                                @HeaderParam("db_name")     String db_name     ,
                                @HeaderParam("db_schema")   String db_schema   ,
                                @HeaderParam("db_user")     String db_user     ,
                                @HeaderParam("db_password") String db_password ) throws Exception {
        
        String sqlQuery = URLDecoder.decode( _sqlQuery, StandardCharsets.UTF_8.toString() )       ;

        if (  sqlQuery == null || sqlQuery.trim().replaceAll("(?m)^[ \t]*\r?\n", "").isEmpty() )  {
              JsonObject respObj = Json.createObjectBuilder().add("Error", "SQL Query Can't be Null").build() ;
              return Response.status(Response.Status.OK).entity(respObj.toString()).build()                   ;
        }
        
        String dbSchema  = db_schema == null ? "public" : db_schema ; 

        String response        = ""                                 ;
         
        List<String> columns                                        ;
               
        Properties props = new Properties() ;
        props.setProperty("user"            , URLDecoder.decode ( db_user     , "UTF-8") ) ;
        props.setProperty("password"        , URLDecoder.decode ( db_password , "UTF-8") ) ;
        props.setProperty("connectTimeout"  , String.valueOf( timeOut ) )                  ;
        props.setProperty("loginTimeout"    , String.valueOf( timeOut ) )                  ;

        try ( Connection connection = DriverManager.getConnection ( "jdbc:postgresql://"                    +  
                                                                    URLDecoder.decode ( db_host , "UTF-8" ) + ":"  +
                                                                    db_port                                 +  "/" +
                                                                    URLDecoder.decode ( db_name , "UTF-8" ) ,
                                                                    props ) ) {
            connection.setReadOnly( true )   ;
            connection.setSchema( dbSchema ) ;

            try {
                
                columns = new ArrayList( Querier.extractColumnsName( connection, 
                                                                     URLDecoder.decode ( _sqlQuery ,
                                                                                         StandardCharsets.UTF_8.toString() ) ).values() ); 
            } catch( Exception ex )        {
                 ex.printStackTrace()                        ;
                 response = "Exception : " + ex.getMessage() ;
                 return Response.status(Response.Status.BAD_REQUEST).entity( response ).build() ;
            }
        } catch( Exception ex )      {
             ex.printStackTrace()    ;
             response = "Exception : " + ex.getMessage() ;
             return Response.status(Response.Status.BAD_REQUEST).entity( response ).build()       ;
        }
      
        response = String.join( " , " , columns )                                                 ;
        
        return Response.status(Response.Status.OK).entity( response ).build()                     ;
    }
   
    @POST
    @Path("/ziped")
    @Consumes( MediaType.MULTIPART_FORM_DATA )
    @Produces( { "xml/plain", "json/plain", "xml/encrypted","json/encrypted" } )
    public void massProducer( MultipartFormDataInput                    input              ,
                              @HeaderParam("db_user")                   String db_user     ,
                              @HeaderParam("db_password")               String db_password ,
                              @HeaderParam("sensorthings_endpoint_url") String sensorUrl   ,
                              @HeaderParam("API-key-Token")             String token       ,
                              final @Suspended AsyncResponse response ) throws Exception   {
       
        String  login  = TokenManager.getLogin( token )                  ;
        
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap() ;

	List<InputPart> inputParts = uploadForm.get( "file" ) ;

	for ( InputPart inputPart : inputParts  )  {
                    
	     try {
 		  MultivaluedMap<String, String> header = inputPart.getHeaders() ;
 		  String uploadedFileName = getFileName(header)                  ;
 
 		  // convert the uploaded file to inputstream
 		  InputStream inputStream = inputPart.getBody( InputStream.class, null )  ;
 
  		  String localFileName = TMP_DIRECTORY + File.separator + login  + "-"    + 
                                          String.valueOf( Instant.now().getEpochSecond() ) +
                                          "-" + uploadedFileName ;
                   
                  try ( OutputStream output = new BufferedOutputStream ( new FileOutputStream( localFileName ) ,
                                                                         bufferSize ) ) {
                         int    read  = 0                ;
                         byte[] bytes = new byte[ 2048 ] ;
 
                         while ( ( read = inputStream.read ( bytes) ) != -1 ) {
                                   output.write( bytes, 0, read ) ;
                         }
 
                   } catch( Exception ex ) {
            
                       throw new RuntimeException( ex ) ;
 
                  } finally {
            
                       if  ( inputStream != null ) {
                           try {
                               inputStream.close() ;
                           } catch (IOException e) {
                              throw new RuntimeException( e ) ;
                           }
                       }
                  }
                   
                  String unzipedNameDirecotry = localFileName.replace( ".zip", "") + "_forms" ;
        
                  executor.execute( () -> {
 
                       try {
                            new ZipFile( new File( localFileName ) ).extractAll( unzipedNameDirecotry ) ;
                            Files.deleteIfExists( Paths.get( localFileName ) ) ;
                           
                       } catch ( IOException ex ) {
                           throw new RuntimeException( ex ) ;
                       }
 
                       // Process Files in unzipedDirecotry
 
                       File[] listFiles = new File( unzipedNameDirecotry ).listFiles() ;
 
                       Arrays.sort( listFiles, (a, b) -> extractNumFromName(a.getName()).compareTo( extractNumFromName(b.getName())) ) ;
    
                       for ( File file : listFiles ) {
 
                               try {
 
                                    String filePath = file.getAbsolutePath() ;
 
                                    String content  = new String(Files.readAllBytes(Paths.get( filePath ) ) ) ;
 
                                    String sqlQuery    = FileParserUtils.getSqlQuery  ( content ) ;
                                    String sensorType  = FileParserUtils.getSensorType( content ) ;
                                    String db_schema   = FileParserUtils.getDbSchema( content )   ;
                                    String db_port     = FileParserUtils.getDbPort( content )     ;
                                    String db_host     = FileParserUtils.getDbHost( content )     ;
                                    String db_name     = FileParserUtils.getDbName( content )     ;
                                    String _db_user    = FileParserUtils.getDbUser( content )     ;
                                    String template    = FileParserUtils.getSensorTemplate( content ) ;
                                    
                                    if( sensorType == null || sensorType.trim().isEmpty() )           {
                                       throw new RuntimeException("Error : SensorType Can't be Null") ;
                                    }
                                    
                                    String _sensorUrl  = FileParserUtils.getSensorThingEndpointUrl(content ) ;
 
                                    if ( sensorUrl != null && ! sensorUrl.trim().isEmpty() )  {
                                        // Override 
                                        _sensorUrl = (( sensorUrl.endsWith("/") ) ?
                                                        sensorUrl + sensorType                :
                                                        sensorUrl + "/" + sensorType ).trim() ;
                                    } else {
                                        // Get The SensorThing Url from the sensor-Sheet
                                        _sensorUrl = (( _sensorUrl != null &&
                                                        _sensorUrl.endsWith("/") ) ?
                                                        _sensorUrl + sensorType                :
                                                        _sensorUrl + "/" + sensorType ).trim() ;
                                    }
 
                                    if( sqlQuery == null || sqlQuery.trim().isEmpty() ) {
                                       throw new RuntimeException("Error : Sql Query Can't be Null" ) ;
                                    }
 
                                    if( template == null || template.trim().isEmpty()  || 
                                        template.equalsIgnoreCase("undefined")         ){
                                       throw new RuntimeException("Error : Template Can't be Null. Select Template") ;
                                    }
 
                                    if( db_host == null || db_host.trim().isEmpty() )   {
                                       throw new RuntimeException("Error : DB Host Can't be Null") ;
                                    }
 
                                    if( db_port == null || db_port.trim().isEmpty() )   {
                                       throw new RuntimeException("Error : DB Port Can't be Null") ;
                                    }
 
                                    if( db_name == null || db_name.trim().isEmpty() )   {
                                       throw new RuntimeException("Error : DB NAME Can't be Null") ;
                                    }
                                    
                                    if( _db_user == null || _db_user.trim().isEmpty() ) {
                                       throw new RuntimeException("Error : DB USER Can't be Null") ;
                                    }
 
                                    if( db_password == null || db_password.trim().isEmpty() ) {
                                       throw new RuntimeException( "Error : DB PASSWORD Can't be Null") ;
                                    }
 
                                    if( _sensorUrl == null || _sensorUrl.trim().isEmpty() )   {
                                       throw new RuntimeException("Error : SensorThings Endpoint Can't be Null") ;
                                    }
 
                                    System.out.println("                                             "  ) ;
                                    System.out.println(" ########################################    "  ) ;
                                    System.out.println("                                             "  ) ;
                                    System.out.println(" db_name                       : " + db_name    ) ;
                                    System.out.println(" db_host                       : " + db_host    ) ;
 
                                    System.out.println(" db_user                       : " +  (( db_user == null || 
                                                                                                 db_user.trim().isEmpty() ) ?
                                                                                                 _db_user : db_user     ) ) ;
 
                                    System.out.println(" db_port                       : " + db_port    ) ;
                                    
                                    System.out.println(" sensorthings_endpoint_url     : " + _sensorUrl ) ;
                                    
                                    
                                    System.out.println(" sensorType                    : " + sensorType ) ;
                                    System.out.println(" template                      : " + template.replace( "\n", " " ) ) ;
                                    System.out.println(" sqlQuery                      : " + sqlQuery.replace( "\n", " " ) ) ;
                                    System.out.println("                                               "  ) ;
                                    System.out.println(" ##########################################    "  ) ;
                                    System.out.println("                                               "  ) ;
 
                                    String jobID = LocalDateTime.now().format(DateTimeFormatter
                                                                .ofPattern("dd-MM-yyyy_HH:mm:ss.SSS" ))
                                                   + "_#_" + file.getName()                           ;
 
                                    if (
                                         ! processEtl( sqlQuery       , 
                                                       _sensorUrl     , 
                                                       template       , 
                                                       db_schema      ,
                                                       db_port        , 
                                                       db_host        , 
                                                       db_name        , 
                                                       ( db_user == null || 
                                                         db_user.trim().isEmpty() ) ?
                                                         _db_user : db_user         ,
                                                       db_password    , 
                                                       token          ,
                                                       file.getName() ,
                                                       jobID          ) // sync mode
                                    ) {
 
                                      throw new RuntimeException()    ;
                                    }
                                    
                                    Files.deleteIfExists( Paths.get( file.getAbsolutePath() ) ) ;
 
                                    if ( isEmpty( Paths.get( unzipedNameDirecotry ) ))    {
                                         deleteDirectory( new File(unzipedNameDirecotry)  ) ;
                                    }
 
                               } catch ( Exception ex ) {
                                   deleteDirectory( new File(unzipedNameDirecotry) ) ;
                                   throw new RuntimeException( ex )                  ;
 
                               } finally {
 
                               }
                       }  ;
 
                  }  ) ;
 
                  Response respBuild = Response.accepted  ( "Zip Processing Launched : " + 
                                                            uploadedFileName  ).build()  ;
                  response.resume( respBuild )            ;
                    
	     } catch ( IOException | RuntimeException e ) {
		throw new RuntimeException( e )           ;
	     }
	}
    }
     
    @POST
    @Path("/csv")
    @Consumes( MediaType.MULTIPART_FORM_DATA )
    @Produces( { "xml/plain", "json/plain", "xml/encrypted","json/encrypted" } )
    public void csvProducer( InputStream                               input               ,
                             @HeaderParam("sensorthings_endpoint_url") String sensorUrl    ,
                             @HeaderParam("csv_separator")             String csvSeparator ,
                             @HeaderParam("sensorType")                String sensorType   ,
                             @HeaderParam("templateQuery")             String template     ,
                             @HeaderParam("API-key-Token")             String token        ,
                             final @Suspended AsyncResponse response ) throws Exception    {
        
        if( sensorUrl == null || sensorUrl.trim().isEmpty() )                   {
            throw new RuntimeException("Error : SensorThing URL Can't be Null"  )  ;
        }
        
        if( csvSeparator == null || csvSeparator.trim().isEmpty() )             {
            throw new RuntimeException("Error : Csv Separator Can't be Null"    ) ;
        }
        
        if( input == null ) {
            throw new RuntimeException( "Error : Csv File Can't be Null" )      ;
        }
        
        String jobID     = LocalDateTime.now().format(DateTimeFormatter
                                        .ofPattern("dd-MM-yyyy_HH:mm:ss.SSS" )) ;

        String login     = TokenManager.getLogin(token)                         ;
        String path_logs = TokenManager.builPathLog( "logger" , login )         ;
        
        Writer.createFile( path_logs )                                          ;

        InputStream copyInputStream  = IOUtils.toBufferedInputStream(input)     ;
        
        Callable<Void> call = getCallable( copyInputStream                                       , 
                                           URLDecoder.decode( csvSeparator                       ,
                                                              StandardCharsets.UTF_8.toString()) ,
                                           sensorType  , 
                                           URLDecoder.decode( template                           ,
                                                              StandardCharsets.UTF_8.toString()) , 
                                           URLDecoder.decode( overrideSensorThingsUrl( sensorUrl            ,
                                                                                       sensorThingsVersion  ,
                                                                                       rewriteSensorThingsProxyURLForDocker , 
                                                                                       internalSensorThingsProxyURL )       ,
                                                              StandardCharsets.UTF_8.toString()) , 
                                           jobID                                                 , 
                                           path_logs )                                           ;
        executor.submit( call ) ;

        TimeUnit.MILLISECONDS.sleep( 100 )                    ;
        
        Writer.writeTextFile( "Sensor-map Started...   ID : " + 
                              jobID , path_logs )             ;

        Response respBuild = Response.accepted( "Status : Launched ( ID = "       + 
                                                jobID + " ) Take a look at Logs " )
                                     .build()                                     ;

        response.resume( respBuild ) ;
        
    }
    
    private void processCsv( InputStream  input        ,
                             String       csvSeparator ,
                             String       sensorType   ,
                             String       template     ,
                             String       sensorUrl    ,
                             String       jobID        ,
                             String       path_logs    ) throws Exception     {

        int    numLine      = 0  ;
        int    readFromLine = 4  ;
      
        String       line                                                     ;
        List<String> columns   = null                                         ;
        
        String sensorthings_endpoint_url = (( sensorUrl != null &&
                                              sensorUrl.endsWith("/") ) ?
                                              sensorUrl + sensorType.trim()          :
                                              sensorUrl + "/" + sensorType ).trim()  ;
        
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
             
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()          ;
           
            if( sensorthings_endpoint_url.trim().toLowerCase().startsWith("https") ) {
                 clientBuilder = Utils.configureToIgnoreCertificate(clientBuilder) ;
            }
            
            OkHttpClient client = clientBuilder.connectTimeout( 500 , TimeUnit.SECONDS )
                                               .connectTimeout( 500 , TimeUnit.SECONDS )
                                               .writeTimeout  ( 500 , TimeUnit.SECONDS )
                                               .readTimeout   ( 500 , TimeUnit.SECONDS )
                                               .connectionPool( new ConnectionPool( 1 , 1L ,
                                                                TimeUnit.MINUTES ) )
                                               .build()   ;

            while( ( line = reader.readLine()) != null  ) {

                if ( numLine == readFromLine )            {  // Read Columns ( Headers )

                    columns = Arrays.asList( line.split( csvSeparator) ) ;

                } else if ( numLine > readFromLine )      {

                    // Read numeLine : numLine - readFromLine

                    try {

                        if ( line.trim().isEmpty() )      {
                            numLine ++ ;
                            continue   ;
                        }

                        if( line == null || line.trim().isEmpty() ||
                            line.contains("---WebKitFormBoundary" ) && line.endsWith( "--") ) {
                            continue   ;
                        }

                        List<String> values  = Arrays.asList( line.split( csvSeparator) ) ;
                        
                        if(  line.contains( "---WebKitFormBoundary" ) || ( values.size() != columns.size() ) ) {

                            String mssg = "\nWARNING : ID : "  +
                                          jobID                +
                                          " -- Line [ "        +
                                          ( numLine - readFromLine ) +
                                          " ] Ignored \n"            ;

                            Writer.writeTextFile(mssg, path_logs )   ;

                            continue ;
                        }
                        
                        String sensorTemplateInstance = applyValue( columns   ,
                                                                    values    ,
                                                                    template  ) ;

                        JSONObject jsonObject = new JSONObject(sensorTemplateInstance)    ;

                        sensorTemplateInstance = handleJsonValue( jsonObject ).toString() ;

                        String _sensorthings_endpoint_url = applyValue( columns           ,
                                                                        values            ,
                                                                        sensorthings_endpoint_url ) ;

                        DataLoader.postDatas( client                                                ,
                                              overrideSensorThingsUrl ( _sensorthings_endpoint_url           ,
                                                                        sensorThingsVersion                  ,
                                                                        rewriteSensorThingsProxyURLForDocker , 
                                                                        internalSensorThingsProxyURL )       ,
                                              sensorTemplateInstance     ,
                                              "application/json"         ,
                                              true                     ) ;

                    } catch (Exception ex   ) {

                        Logger.getLogger( ServiceSensorThings.class.getName())
                              .log(Level.SEVERE, null, ex )       ;

                        String mssg = "\nERROR : ID ( "           +
                                      jobID                       +
                                      " ) [[ \n    "              +
                                      ex.getMessage()             +
                                      "\n ]] - Num_Line [    "    +
                                      ( numLine - readFromLine )  +
                                     " ] \n"                      ;

                        Writer.writeTextFile(mssg, path_logs )    ;

                        throw new RuntimeException( ex )          ;
                    }
                }

                numLine ++  ;
            }
        }
                    
        String mssg = "Sensor-Map Finished.   ID : "      +
                       jobID + " => SUCCESS  "            ;
         
        Writer.writeTextFile(mssg, path_logs )            ;
    }

    private Callable<Void> getCallable( InputStream input        , 
                                        String      csvSeparator ,
                                        String      sensorType   , 
                                        String      template     , 
                                        String      sensorUrl    , 
                                        String      jobID        , 
                                        String      path_logs  ) {
        
        Callable<Void> call = () ->      {
            
            try {

                processCsv( input        ,
                            csvSeparator ,
                            sensorType   ,
                            template     ,
                            sensorUrl    ,
                            jobID        ,
                            path_logs  ) ;
                
            } catch ( Exception ex )         {
              throw new RuntimeException(ex) ;
            }
            
            return null ;
        } ;
        
        return call ;
    }
}
