
package com.rac021.sensormap.impl.service.sensorthings ;

import java.util.Map ;

public final class ValueEvent {

    private Map<String, String> value     ;
    
    public Map<String, String> getValue() {
       return value ;
    }

    public void setValue( Map<String, String> value) {
       this.value = value ;
    }
    
    public void clear()   {
       this.value = null  ;
    }
}