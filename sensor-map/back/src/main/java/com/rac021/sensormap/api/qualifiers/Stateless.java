
package com.rac021.sensormap.api.qualifiers ;

import java.lang.annotation.Target ;
import java.lang.annotation.Retention ;
import java.lang.annotation.ElementType ;
import jakarta.transaction.Transactional ;
import java.lang.annotation.RetentionPolicy ;
import jakarta.enterprise.inject.Stereotype ;
import jakarta.enterprise.context.RequestScoped ;

/**
 *
 * @author ryahiaoui
 */
@Stereotype
@Transactional
@RequestScoped
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Stateless {}    
