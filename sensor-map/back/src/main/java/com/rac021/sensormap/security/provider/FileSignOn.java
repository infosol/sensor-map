
package com.rac021.sensormap.security.provider ;

import java.util.Map ;
import java.util.HashMap ;
import java.nio.file.Path ;
import java.nio.file.Paths ;
import java.io.IOException ;
import java.nio.file.Files ;
import java.util.Properties ;
import java.util.logging.Level ;
import java.io.FileInputStream ;
import java.util.logging.Logger ;
import java.util.stream.Collectors ;
import jakarta.annotation.PostConstruct ;
import jakarta.enterprise.event.Observes ;
import jakarta.transaction.Transactional ;
import com.rac021.jaxy.api.security.File ;
import com.rac021.jaxy.api.crypto.Digestor ;
import jakarta.enterprise.context.Initialized ;
import java.security.NoSuchAlgorithmException ;
import com.rac021.sensormap.api.security.Policy ;
import com.rac021.sensormap.api.security.ISignOn ;
import jakarta.enterprise.context.ApplicationScoped ;
import org.eclipse.microprofile.config.inject.ConfigProperty ;
import com.rac021.sensormap.api.exceptions.BusinessException ;
import com.rac021.sensormap.security.provider.FileWatcher.Callback ;
import static com.rac021.sensormap.api.logger.LoggerFactory.getLogger ;

/**
 *
 * @author yahiaoui
 */

@File
@ApplicationScoped

public class FileSignOn implements ISignOn           {
         
    private static final Logger LOGGER = getLogger() ;

    @ConfigProperty(name = "usersFilePath", defaultValue = "users.properties" ) 
    String usersFilePath ;
    
    @ConfigProperty(name = "Policy" ) 
    String overridedPolicy          ;
    
    private static final Map<String, String> authorizedVUsers = new HashMap<>() ;
    
    private final String SHA1   = "SHA1"  ;
    private final String SHA2   = "SHA2"  ;
    private final String MD5    = "MD5"   ;
    private final String PLAIN  = "PLAIN" ;
    
    @PostConstruct
    public void init() throws Exception   {
    
       initWatcher() ;
    }
    
    private void initWatcher() throws Exception   {
        
       Policy policy =  Policy.valueOf( overridedPolicy )         ;
       
       if( policy == Policy.FileSignOn )          {
        
          Path path = Paths.get( usersFilePath )  ;
          
          if( ! Files.exists( path ) )            {
           
           throw new RuntimeException( " No Property File : [ "   +
                                       usersFilePath              +
                                       " ] Provided ! "         ) ;
          } else {
       
           readPropertiesFile()       ;
       
           Callback callback = () ->  {
              System.out.println (  "Reload Property File : " + 
                                    usersFilePath  )          ;
              readPropertiesFile()                            ;
           } ;

           FileWatcher fw  = new FileWatcher() ;
           fw.start( path, callback )          ;
           
          }
       }

    }
    
    @Override
    public boolean checkIntegrity( String token, Long expiration, Long now ) throws BusinessException {
        
        String _token[] = token.replaceAll( " + ", " " ).split(" ") ;
        if(_token.length < 3 ) return false                         ;
        
        if( expiration != null && expiration > 0  )                 {
            
            long clientTime   = Long.parseLong( _token[1] )         ;
            // long now       = Instant.now().getEpochSecond()      ;
            
            if( clientTime > now ) {
                throw  new BusinessException( " Error : Not Expected TimeStamp "
                                            + "( Provided TimeStamp > serverTimeStamp ) " ) ;
            }
            
            long expiredTime  = now - expiration ;
            
            if( clientTime < expiredTime )  {
                throw  new BusinessException(  " Error : TimeStamp expired // -->  "
                                             + "Expiration delay = "               + expiration 
                                             + " second(s) // --> Delta Time = " + ( now - clientTime )
                                             + " second(s)" ) ;
            }
        }
        
        return checkIntegrity(_token[0].trim(), _token[1].trim(), _token[2].trim()) ;
    }
    
    @Override
    @Transactional
    public boolean checkIntegrity( String _login       ,  
                                   String _timeStamp   , 
                                   String _clientSign  ) throws BusinessException {

       if( ! authorizedVUsers.containsKey( _login)) return false ;
       
       String password = authorizedVUsers.get( _login )          ;
  
       if ( password == null && password.trim() .isEmpty() ) return false         ;
       
       String loginSignature      = "PLAIN" ; // configurator.getLoginSignature()    ;
       String passwordSignature   = "MD5"   ; //configurator.getPasswordSignature()  ;
       String storedPassword      = "PLAIN" ; // configurator.getPasswordStorage()   ;
       String timeStampSiignature = "PLAIN" ; //configurator.getTimeStampSignature() ;
       String algo                = "SHA2"  ; //configurator.getAlgoSign()           ;
       
       String login               = _login     ;
       String timeStamp           = _timeStamp ;
        
        /** Treat Strategy . */
        
        if( loginSignature.equalsIgnoreCase(SHA1))                  {
           try {
               login = Digestor.toString(Digestor.toSHA1(_login ))  ;
           } catch (NoSuchAlgorithmException ex)                    {
              LOGGER.log(Level.SEVERE, ex.getMessage(), ex )        ;
           }
        }       
        
        else if ( loginSignature.equalsIgnoreCase(MD5))             {
           try {
               login =  Digestor.toString( Digestor.toMD5(_login )) ;
           } catch (NoSuchAlgorithmException ex)                    {
               LOGGER.log(Level.SEVERE, ex.getMessage(), ex)        ;
           }
        }   
        
        else if ( loginSignature.equalsIgnoreCase(SHA2))               {
           try {
               login =  Digestor.toString( Digestor.toSHA256(_login )) ;
           } catch (NoSuchAlgorithmException ex)                       {
               LOGGER.log(Level.SEVERE, ex.getMessage(), ex)           ;
           }
        }       
        
        if ( passwordSignature.equalsIgnoreCase(PLAIN) && 
             storedPassword.equalsIgnoreCase(SHA1)     ||
             passwordSignature.equalsIgnoreCase(SHA1)  && 
             storedPassword.equalsIgnoreCase(PLAIN)  )  {
           try {
               password =  Digestor.toString( Digestor.toSHA1( password )) ;
           } catch (NoSuchAlgorithmException ex) {
               LOGGER.log(Level.SEVERE, ex.getMessage(), ex) ;
           }
        }
        
        else if ( passwordSignature.equalsIgnoreCase(PLAIN) && 
                  storedPassword.equalsIgnoreCase(MD5)      ||
                  passwordSignature.equalsIgnoreCase(MD5)   && 
                  storedPassword.equalsIgnoreCase(PLAIN)  )  {
           try {
               password =  Digestor.toString ( Digestor.toMD5(password ) ) ;
           } catch (NoSuchAlgorithmException ex) {
              LOGGER.log(Level.SEVERE, ex.getMessage(), ex) ;
           }
        }
        else if ( passwordSignature.equalsIgnoreCase(PLAIN) && 
                  storedPassword.equalsIgnoreCase(SHA2)     ||
                  passwordSignature.equalsIgnoreCase(SHA2)  && 
                  storedPassword.equalsIgnoreCase(PLAIN)  )  {
           try {
               password =  Digestor.toString ( Digestor.toSHA256(password ) ) ;
           } catch (NoSuchAlgorithmException ex) {
               LOGGER.log(Level.SEVERE, ex.getMessage(), ex) ;
           }
        }
        
        if( timeStampSiignature.equalsIgnoreCase(SHA1)) {
           try {
               timeStamp =  Digestor.toString ( Digestor.toSHA1(_timeStamp )) ;
           } catch (NoSuchAlgorithmException ex)             {
               LOGGER.log(Level.SEVERE, ex.getMessage(), ex) ;
           }
        }
        else if( timeStampSiignature.equalsIgnoreCase(MD5))  {
           try {
               timeStamp =  Digestor.toString ( Digestor.toMD5(_timeStamp ) ) ;
           } catch (NoSuchAlgorithmException ex)             {
               LOGGER.log(Level.SEVERE, ex.getMessage(), ex) ;
           }
        }
        else if( timeStampSiignature.equalsIgnoreCase(SHA2)) {
           try {
               timeStamp =  Digestor.toString ( Digestor.toSHA256(_timeStamp ) ) ;
           } catch (NoSuchAlgorithmException ex)             {
               LOGGER.log(Level.SEVERE, ex.getMessage(), ex) ;
           }
        }
              
        try {
            
           if( algo.equalsIgnoreCase(SHA1) ) {
            String calculatedSign =  Digestor.toString( Digestor.toSHA1( login        + 
                                                                         password     + 
                                                                         timeStamp )) ;
                if( calculatedSign.equals(_clientSign) )    {
                    // ISignOn.ENCRYPTION_KEY.set(password) ;
                    return true ; 
                }
           }
           
           else if( algo.equalsIgnoreCase(MD5) )     {
            String calculatedSign =  Digestor.toString ( Digestor.toMD5 ( login       + 
                                                                          password    + 
                                                                          timeStamp ) ) ;
                if( calculatedSign.equals(_clientSign) )    {
                    // ISignOn.ENCRYPTION_KEY.set(password) ;
                    return true ; 
                }
           }
           
           else if(algo.equalsIgnoreCase(SHA2)) {
            String calculatedSign =  Digestor.toString ( Digestor.toSHA256( login       + 
                                                                            password    + 
                                                                            timeStamp ) ) ;
                if(calculatedSign.equals(_clientSign))    {
                   
                    return true ; 
                }
           }
           
        } catch (NoSuchAlgorithmException ex)             {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex) ;
        }

        return false ;
    }
    
    private void readPropertiesFile() throws IOException {
        
      FileInputStream fis        = null ;
      Properties      properties = null ;

      try {
         fis = new FileInputStream( usersFilePath ) ;
         properties = new Properties()  ;
         properties.load(fis)           ;

      } catch( IOException ioe )  {
         ioe.printStackTrace()    ;
         throw new 
            RuntimeException(ioe) ;
      } finally {
         if( fis != null )
         fis.close()              ;
      }
      
      authorizedVUsers.clear()    ;

      authorizedVUsers.putAll(properties.entrySet()
                      .stream()
                      .collect(Collectors.toMap( e -> e.getKey().toString().trim()   , 
                                                 e -> e.getValue().toString().trim( ))) ) ;
   }
  
   public void init( @Observes 
                     @Initialized(ApplicationScoped.class ) Object init ) throws Exception { }

}