
package com.rac021.sensormap.impl.service.logs ;

/**
 *
 * @author ryahiaoui
 */

import jakarta.ws.rs.GET ;
import java.nio.file.Files ;
import java.nio.file.Paths ;
import jakarta.ws.rs.OPTIONS ;
import jakarta.inject.Inject ;
import jakarta.ws.rs.Produces ;
import jakarta.inject.Singleton ;
import jakarta.ws.rs.HeaderParam ;
import io.quarkus.runtime.Startup ;
import jakarta.ws.rs.core.UriInfo ;
import jakarta.ws.rs.core.Context ;
import jakarta.ws.rs.core.Response ;
import com.rac021.sensormap.impl.io.Writer ;
import com.rac021.sensormap.api.security.Policy ;
import com.rac021.sensormap.api.security.Secured ;
import com.rac021.sensormap.api.qualifiers.ServiceRegistry ;
import com.rac021.sensormap.impl.utils.checker.TokenManager ;
/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("logs")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Startup

public class SensorLogService  {
   
    @Inject 
    LoggerTask loggerTask   ;
    
    public SensorLogService() {
    }
   
    @GET
    @OPTIONS
    @Produces( { "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted" } )
    public Response getResource ( @HeaderParam("API-key-Token") String  token        ,
                                  @HeaderParam("keep")          String  filterdIndex , 
                                  @Context                      UriInfo uriInfo      ) throws Exception {

        System.out.println(" Logger Service.." )        ;
        
        String login     = TokenManager.getLogin(token) ;
        
        String path_logs = TokenManager.builPathLog( "logger" /*configuration.getLoggerFile()*/, login ) ;
        
        if ( ! Writer.existFile( path_logs ) ||
               Files.lines( Paths.get(path_logs)).count() <=  0 ) {
            
            return Response.status(Response.Status.OK)
                           .entity( " \n Empty LOGS for the user : " + login + "\n" ) 
                           .build() ;        
        }
        
        /*
        return Response.status(Response.Status.OK)
                       .entity( new StreamerLog ( path_logs ,  
                                                  configuration.getFrequencyUpdateTimeMs() ) )
                       .build() ;   
        */
        
        /*
         return Response.status(Response.Status.OK)
                       .entity( new StreamerLogV2 ( path_logs ) )
                       .build() ;  
        */
       
        loggerTask.setLoggerFile( path_logs ) ;
            
        loggerTask.setTailDelayMillis( 5 )    ;
            
        return Response.status ( Response.Status.OK )
                       .entity ( loggerTask ) 
                       .build() ;
    }
}

