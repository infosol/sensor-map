#!/bin/bash

 CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
 
 cd $CURRENT_PATH 
 
 while [[ "$#" > "0" ]] ; do
 
     case $1 in

         (*=*) KEY=${1%%=*}
               VALUE=${1#*=}
               
               case "$KEY" in
               
                    ("port")     PORT=$VALUE 
                    ;;
                    ("with_ssl") WITH_SSL=$VALUE 
                    ;;
               esac
         ;;
         help)   echo
                 echo "   Arguments :                                 "
                 echo
                 echo "      port=      :  Http(s)  Port              "
                 echo "      with_ssl=  :  if = 'ss' then, enable SSL "
                 echo
                 exit
         ;;
         debug)  DEBUG="-Xdebug -Xrunjdwp:transport=dt_socket,address=11555,server=y,suspend=y " 

         esac

     shift

  done    

 JAR_NAME="sensor-map.jar"

 ## POLICY AUTH - :
 ##  - CustomSignOn ( DB_AUTH                    )
 ##  - FileSignOn   ( uses users.properties file )
 ##  - Public       ( no auth                    )
 ##  - SSO          ( keycloak auth              )
 
 POLICY=${POLICY:-"FileSignOn"} # Only FileSignOn is implemented here

######################################
##### SERVER CONFIG ##################
######################################

 ## SERVER PORT ######################

 PORT=${PORT:-"8585"}
 WITH_SSL=${WITH_SSL:-""}
  
######################################

 ## SSL FILES
 
 CERT_FILE="/opt/sensor-map/ca_certificate/ssl-cert.crt"
 KEY_FILE="/opt/sensor-map/ca_certificate/ssl-key.key"

######################################

 echo 
 echo " Port                  : $PORT     " 
 echo " Authentication Policy : $POLICY   " 
 echo " WITH SSL              : $WITH_SSL " 
 
 echo 

 if [ -n "$WITH_SSL" ] && [ "$WITH_SSL" = "ssl" ]; then 
    
    if [ ! -f "$CERT_FILE" ]; then
        echo "Not Provied file : $CERT_FILE"
        exit -1
    fi
    if [ ! -f "$KEY_FILE" ]; then
        echo "Not Provied file : $KEY_FILE"
        exit -1
    fi    
    
    java $DEBUG                                            \
        -Dquarkus.http.http2=true                          \
        -Dquarkus.http.ssl-port=$PORT                      \
        -Dquarkus.http.ssl.certificate.files=$CERT_FILE    \
        -Dquarkus.http.insecure-requests=disabled          \
        -Dquarkus.http.ssl.certificate.key-files=$KEY_FILE \
        -DPolicy="$POLICY"                                 \
        -jar $JAR_NAME

else

 java $DEBUG                                             \
      -Dquarkus.http.http2=true                          \
      -Dquarkus.http.port=$PORT                          \
      -Dquarkus.http.ssl.certificate.key-files=$KEY_FILE \
      -DPolicy="$POLICY"                                 \
      -jar $JAR_NAME
fi

