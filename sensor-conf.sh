
 USE_SSL="true"
 
 DOMAIN_NAME="localhost"

 SENSOR_MAP_PORT="8585" 

 ## Sensor Board Port
 SENSOR_BOARD_PORT="8282"

 ## SensorThings Frost Server Port
 SENSOR_FROST_PROXY_PORT="8181"

 ## SensorThings Frost Server Context
 SENSORTHINGS_ROOT_CONTEXT="FROST-Server"
 
 ## SensorThings Frost Server Version
 SENSORTHINGS_SERVER_VERSION="v1.1"

 ## SensorThings Frost Database Password
 SENSORTHINGS_DB_USERNAME="sensorthings"
 ## SensorThings Frost Database Password
 SENSORTHINGS_DB_PASSWORD="ChangeMe"
 
 ## ONLY for Docker ( rewrite 'localhost' or '127.0.0.1' to 'proxy' )
 ## 'proxy' is the hostname of the proxy container in the docker-compose-yml
 # Ex, rewrite : https://127.0.0.1:8181/FROST-Server/v1.1/
 # to          : https://proxy:80/FROST-Server/v1.1/
 REWRITE_SENSORTHINGS_PROXY_URL_FOR_DOCKER="true"

 ## SECURITY - Prevent Delete All Entities
 ENABLE_DELETE_ALL_ENTITIES="true"
 
 ## AUTHENTICATION :
 #### Enable basic auth by providing the value : 
 #### de.fraunhofer.iosb.ilt.frostserver.auth.basic.BasicAuthProvider
 # AUTH_TYPE="de.fraunhofer.iosb.ilt.frostserver.auth.basic.BasicAuthProvider"

 ## CERTIFICATE DIR
 CA_CERTIFICATE_DIR="ca_certificate"

 ## NGINX PORT ( INSIDE CONTAINERS ) 
 NGINX_PORT="80"
