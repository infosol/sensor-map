#!/bin/sh

echo 
echo "Script : clearAll.sh    "
echo "Deleting All Entities..."
echo 

curl -XDELETE "http://sensorthings:8080/FROST-Server/v1.1/Things"

curl -XDELETE "http://sensorthings:8080/FROST-Server/v1.1/Locations"

curl -XDELETE "http://sensorthings:8080/FROST-Server/v1.1/HistoricalLocations"

curl -XDELETE "http://sensorthings:8080/FROST-Server/v1.1/Datastreams"

curl -XDELETE "http://sensorthings:8080/FROST-Server/v1.1/Observations"

curl -XDELETE "http://sensorthings:8080/FROST-Server/v1.1/FeaturesOfInterest"

curl -XDELETE "http://sensorthings:8080/FROST-Server/v1.1/Sensors"

curl -XDELETE "http://sensorthings:8080/FROST-Server/v1.1/ObservedProperties"

echo 
