#!/bin/bash

set -e

# Function to execute an SQL command and retrieve the number of affected rows
execute_sql_command() {
    SQL_COMMAND="$1"
    RESULT=$(psql -U sensorthings -d sensorthings -t -c "$SQL_COMMAND")
    echo "$RESULT"
}

# Function to display a customized message based on the number of affected rows
display_message() {
    MESSAGE="$1"
    ROWS_AFFECTED="$2"
    echo "$MESSAGE : $ROWS_AFFECTED"
}

# Check if the configuration file is provided as an argument
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 CONFIG_FILE"
    exit 1
fi

CONFIG_FILE=$1

# Check if the configuration file exists
if [ ! -f "$CONFIG_FILE" ]; then
    echo "The configuration file $CONFIG_FILE does not exist."
    exit 1
fi

VALID_ROLES="admin,read,write,delete"


if [ "$CLEAR" ]; then
   echo "DELETE ALL FROM public.\"USER_ROLES\""
   psql -U sensorthings -d sensorthings -t -c "DELETE FROM public.\"USER_ROLES\""
   echo "DELETE ALL FROM public.\"USERS\""
   psql -U sensorthings -d sensorthings -t -c "DELETE FROM public.\"USERS\""
fi


# Read the configuration file line by line and add or update each user with their roles
while IFS=';' read -r USERNAME PASSWORD ROLES; do
    # Avoid empty lines
    if [ -n "$USERNAME" ]; then
    
        USERNAME=$(echo $USERNAME | tr -d ' ')

        # Check if the roles provided are valid
        for role in $(echo "$ROLES" | tr "," "\n"); do
            if ! echo "$VALID_ROLES" | grep -q "\<$(echo -e "${role}" | tr -d '[:space:]')\>"; then
               echo "Error: Invalid role '$role' for user '$USERNAME'. Valid roles are: $VALID_ROLES"
               exit 1
            fi
        done
        
        # SELECT 1 FROM  public."USERS" WHERE "USER_NAME" = 'user021'

        # Check if the user already exists in the USERS table
            if psql -U sensorthings -d sensorthings -t -c "SELECT 1 FROM public.\"USERS\" WHERE \"USER_NAME\" = '${USERNAME}'" | grep -q 1; then
                # Delete the user's roles from the USER_ROLES table
                DELETE_ROLES_RESULT=$(execute_sql_command "DELETE FROM public.\"USER_ROLES\" WHERE \"USER_NAME\" = '${USERNAME}'")
                display_message "Roles deleted for user $USERNAME" "$DELETE_ROLES_RESULT"

                # Delete the existing user from the USERS table
                DELETE_USER_RESULT=$(execute_sql_command "DELETE FROM public.\"USERS\" WHERE \"USER_NAME\" = '${USERNAME}'")
                display_message "Existing user $USERNAME deleted" "$DELETE_USER_RESULT"
            fi
            
            # Insert the user into the USERS table
            INSERT_USER_RESULT=$(execute_sql_command "INSERT INTO public.\"USERS\" (\"USER_NAME\", \"USER_PASS\") VALUES ('$USERNAME', TRIM('$PASSWORD'))")
            display_message "User $USERNAME inserted" "$INSERT_USER_RESULT"

            # Check if the user was successfully inserted
            if [[ "$INSERT_USER_RESULT" != *"ERROR"* ]]; then
                
                # Insert the user's new roles into the USER_ROLES table
                psql -U sensorthings -d sensorthings <<EOF
                -- Split roles by commas and insert into the USER_ROLES table
                DO \$\$
                DECLARE
                    role_name text;
                BEGIN
                    FOREACH role_name IN ARRAY string_to_array('$(echo -e "${ROLES}" | tr -d '[:space:]')', ',')
                    LOOP
                        INSERT INTO "USER_ROLES" ("USER_NAME", "ROLE_NAME") VALUES ('$USERNAME', TRIM(role_name));
                    END LOOP;
                END \$\$;
EOF

                # Check if the operation was successful
                if [ $? -eq 0 ]; then
                    echo "User [ $USERNAME ] updated successfully with new roles : $ROLES"
                else
                    echo "Error updating user [ $USERNAME ] "
                fi
        fi
    fi
done < "$CONFIG_FILE"
