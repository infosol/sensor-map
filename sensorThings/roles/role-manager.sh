#!/bin/bash

set -e

while [[ "$#" > "0" ]] ; do
   
     case $1 in
     
         (*=*) KEY=${1%%=*}
         
               VALUE=${1#*=}
               
               case "$KEY" in
               
                    ("input_file_roles")      INPUT_FILE_ROLES="$VALUE"
                    ;; 
                    ("output_file_sql_roles") OUTPUT_FILE_SQL_ROLES="$VALUE"
                    ;;
                    ("owner")                 OWNER="$VALUE"
                    
               esac
         ;;
         
         help)  echo
                echo " Total Arguments : Three                                     "
                echo 
                echo "   input_file_roles=      :  Input file roles                "
                echo "   output_file_sql_roles= :  Output file Sql roles           "
                echo "   clear_all_roles=       :  clear all intial existing roles "
                echo "   owner=                 :  Table owner                     "
               
                exit ;
        ;; 
        ("clear_all_roles")  CLEAR_ALL_ROLES="clear_all_roles"
        
        ;;
                    
     esac
     
     shift
     
  done   
  
INPUT_FILE_ROLES=${INPUT_FILE_ROLES:-"roles.txt"}
OUTPUT_FILE_SQL_ROLES=${OUTPUT_FILE_SQL_ROLES:-"roles.sql"}
CLEAR_ALL_ROLES=${CLEAR_ALL_ROLES:-""}

OWNER=${OWNER:-"sensorthings"}

VALID_ROLES="read,update,delete,create,admin"

# Function to generate SQL commands and append them to OUTPUT_FILE_SQL_ROLES
generate_sql_commands() {
    SQL_COMMAND="$1"
    echo "$SQL_COMMAND" >> $OUTPUT_FILE_SQL_ROLES
}

function create_users_table_if_not_exists {
  generate_sql_commands "
    CREATE TABLE IF NOT EXISTS public.\"USERS\" (
        \"USER_NAME\" character varying(25) COLLATE pg_catalog.\"default\" NOT NULL,
        \"USER_PASS\" character varying(255) COLLATE pg_catalog.\"default\",
        CONSTRAINT \"USERS_PKEY\" PRIMARY KEY (\"USER_NAME\")
    )
    TABLESPACE pg_default;
    ALTER TABLE IF EXISTS public.\"USERS\"
    OWNER TO $OWNER ; "
}

function create_user_roles_table_if_not_exists {
  generate_sql_commands "
    CREATE TABLE IF NOT EXISTS public.\"USER_ROLES\" (
    \"USER_NAME\" character varying(25) COLLATE pg_catalog.\"default\" NOT NULL,
    \"ROLE_NAME\" character varying(15) COLLATE pg_catalog.\"default\" NOT NULL,
    CONSTRAINT \"USER_ROLES_pkey\" PRIMARY KEY (\"USER_NAME\", \"ROLE_NAME\"),
    CONSTRAINT \"USER_ROLES_USERS_FKEY\" FOREIGN KEY (\"USER_NAME\")
        REFERENCES public.\"USERS\" (\"USER_NAME\") MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE )
    TABLESPACE pg_default ;
    ALTER TABLE IF EXISTS public.\"USER_ROLES\"
    OWNER to $OWNER ; "
}

# Check if the configuration file exists
if [ ! -f "$INPUT_FILE_ROLES" ]; then
    echo "The configuration file $INPUT_FILE_ROLES does not exist."
    exit 1
fi

## Create table users if not exists
create_users_table_if_not_exists
## Create table user_roles if not exists
create_user_roles_table_if_not_exists

if [ "$CLEAR_ALL_ROLES" ]; then
   generate_sql_commands "DELETE FROM public.\"USER_ROLES\";"
   generate_sql_commands "DELETE FROM public.\"USERS\";"
fi

# Read the configuration file line by line and generate SQL commands
while IFS=';' read -r USERNAME PASSWORD ROLES; do

    # Ignore lines starting with #
    if [[ $USERNAME == \#* ]]; then
        continue
    fi
    
    # Avoid empty lines
    if [ -n "$USERNAME" ]; then
    
        USERNAME=$(echo "$USERNAME" | tr -d ' ')

        # Check if the roles provided are valid
        for role in $(echo "$ROLES" | tr "," "\n"); do
            if ! echo "$VALID_ROLES" | grep -q "\<$(echo -e "${role}" | tr -d '[:space:]')\>"; then
               echo "Error: Invalid role '$role' for user '$USERNAME'. Valid roles are: $VALID_ROLES"
               exit 1
            fi
        done
        
        # Generate SQL commands to delete existing user and roles
        generate_sql_commands "DELETE FROM public.\"USER_ROLES\" WHERE \"USER_NAME\" = '${USERNAME}';"
        generate_sql_commands "DELETE FROM public.\"USERS\" WHERE \"USER_NAME\" = '${USERNAME}';"
            
        # Generate SQL command to insert the user into the USERS table
        generate_sql_commands "INSERT INTO public.\"USERS\" (\"USER_NAME\", \"USER_PASS\") VALUES ('$USERNAME', TRIM('$PASSWORD'));"

        # Generate SQL commands to insert the user's new roles into the USER_ROLES table
        for role_name in $(echo "$ROLES" | tr ',' '\n'); do
            generate_sql_commands "INSERT INTO public.\"USER_ROLES\" (\"USER_NAME\", \"ROLE_NAME\") VALUES ('$USERNAME', TRIM('$role_name'));"
            # INSERT INTO "USERS" VALUES ('my_user', crypt('my_password', gen_salt('bf', 12)))
        done
    fi
done < "$INPUT_FILE_ROLES"
