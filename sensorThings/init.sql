
-- Required for String values for entity ids
-- https://fraunhoferiosb.github.io/FROST-Server/settings/plugins.html 

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Manual proc :
-- docker exec -it sensorthings-frost-database bash
-- psql -p 5432 -U sensorthings
-- CREATE EXTENSION "uuid-ossp";
