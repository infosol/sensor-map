! function(t) {
    "use strict";
    var e = window.angulartics || (window.angulartics = {});
    e.waitForVendorCount = 0, e.waitForVendorApi = function(t, n, i, o, r) {
        r || e.waitForVendorCount++, o || (o = i, i = void 0), !Object.prototype.hasOwnProperty.call(window, t) || void 0 !== i && void 0 === window[t][i] ? setTimeout(function() {
            e.waitForVendorApi(t, n, i, o, !0)
        }, n) : (e.waitForVendorCount--, o(window[t]))
    }, t.module("angulartics", []).provider("$analytics", function() {
        var n = {
                pageTracking: {
                    autoTrackFirstPage: !0,
                    autoTrackVirtualPages: !0,
                    trackRelativePath: !1,
                    autoBasePath: !1,
                    basePath: "",
                    excludedRoutes: []
                },
                eventTracking: {},
                bufferFlushDelay: 1e3,
                developerMode: !1
            },
            i = ["pageTrack", "eventTrack", "setAlias", "setUsername", "setUserProperties", "setUserPropertiesOnce", "setSuperProperties", "setSuperPropertiesOnce"],
            o = {},
            r = {},
            a = function(t) {
                return function() {
                    e.waitForVendorCount && (o[t] || (o[t] = []), o[t].push(arguments))
                }
            },
            s = function(e, n) {
                return r[e] || (r[e] = []), r[e].push(n),
                    function() {
                        var n = arguments;
                        t.forEach(r[e], function(t) {
                            t.apply(this, n)
                        }, this)
                    }
            },
            l = {
                settings: n
            },
            u = function(t, e) {
                e ? setTimeout(t, e) : t()
            },
            c = {
                $get: function() {
                    return l
                },
                api: l,
                settings: n,
                virtualPageviews: function(t) {
                    this.settings.pageTracking.autoTrackVirtualPages = t
                },
                excludeRoutes: function(t) {
                    this.settings.pageTracking.excludedRoutes = t
                },
                firstPageview: function(t) {
                    this.settings.pageTracking.autoTrackFirstPage = t
                },
                withBase: function(e) {
                    this.settings.pageTracking.basePath = e ? t.element(document).find("base").attr("href") : ""
                },
                withAutoBase: function(t) {
                    this.settings.pageTracking.autoBasePath = t
                },
                developerMode: function(t) {
                    this.settings.developerMode = t
                }
            },
            h = function(e, i) {
                l[e] = s(e, i);
                var r = n[e],
                    a = r ? r.bufferFlushDelay : null,
                    c = null !== a ? a : n.bufferFlushDelay;
                t.forEach(o[e], function(t, e) {
                    u(function() {
                        i.apply(this, t)
                    }, e * c)
                })
            },
            d = function(t) {
                return t.replace(/^./, function(t) {
                    return t.toUpperCase()
                })
            },
            p = function(t) {
                var e = "register" + d(t);
                c[e] = function(e) {
                    h(t, e)
                }, l[t] = s(t, a(t))
            };
        return t.forEach(i, p), c
    }).run(["$rootScope", "$window", "$analytics", "$injector", function(e, n, i, o) {
        function r(t) {
            for (var e = 0; e < i.settings.pageTracking.excludedRoutes.length; e++) {
                var n = i.settings.pageTracking.excludedRoutes[e];
                if (n instanceof RegExp && n.test(t) || t.indexOf(n) > -1) return !0
            }
            return !1
        }

        function a(t, e) {
            r(t) || i.pageTrack(t, e)
        }
        i.settings.pageTracking.autoTrackFirstPage && o.invoke(["$location", function(t) {
            var e = !0;
            if (o.has("$route")) {
                var r = o.get("$route");
                for (var s in r.routes) {
                    e = !1;
                    break
                }
            } else if (o.has("$state")) {
                var l = o.get("$state");
                for (var u in l.get()) {
                    e = !1;
                    break
                }
            }
            if (e)
                if (i.settings.pageTracking.autoBasePath && (i.settings.pageTracking.basePath = n.location.pathname), i.settings.pageTracking.trackRelativePath) {
                    var c = i.settings.pageTracking.basePath + t.url();
                    a(c, t)
                } else a(t.absUrl(), t)
        }]), i.settings.pageTracking.autoTrackVirtualPages && o.invoke(["$location", function(t) {
            i.settings.pageTracking.autoBasePath && (i.settings.pageTracking.basePath = n.location.pathname + "#");
            var r = !0;
            if (o.has("$route")) {
                var s = o.get("$route");
                for (var l in s.routes) {
                    r = !1;
                    break
                }
                e.$on("$routeChangeSuccess", function(e, n) {
                    if (!n || !(n.$$route || n).redirectTo) {
                        var o = i.settings.pageTracking.basePath + t.url();
                        a(o, t)
                    }
                })
            }
            o.has("$state") && (r = !1, e.$on("$stateChangeSuccess", function() {
                var e = i.settings.pageTracking.basePath + t.url();
                a(e, t)
            })), r && e.$on("$locationChangeSuccess", function(e, n) {
                if (!n || !(n.$$route || n).redirectTo)
                    if (i.settings.pageTracking.trackRelativePath) {
                        var o = i.settings.pageTracking.basePath + t.url();
                        a(o, t)
                    } else a(t.absUrl(), t)
            })
        }]), i.settings.developerMode && t.forEach(i, function(t, e) {
            "function" == typeof t && (i[e] = function() {})
        })
    }]).directive("analyticsOn", ["$analytics", function(e) {
        function n(t) {
            return ["a:", "button:", "button:button", "button:submit", "input:button", "input:submit"].indexOf(t.tagName.toLowerCase() + ":" + (t.type || "")) >= 0
        }

        function i(t) {
            return n(t), "click"
        }

        function o(t) {
            return n(t) ? t.innerText || t.value : t.id || t.name || t.tagName
        }

        function r(t) {
            return "analytics" === t.substr(0, 9) && -1 === ["On", "Event", "If", "Properties", "EventType"].indexOf(t.substr(9))
        }

        function a(t) {
            var e = t.slice(9);
            return "undefined" != typeof e && null !== e && e.length > 0 ? e.substring(0, 1).toLowerCase() + e.substring(1) : e
        }
        return {
            restrict: "A",
            link: function(n, s, l) {
                var u = l.analyticsOn || i(s[0]),
                    c = {};
                t.forEach(l.$attr, function(t, e) {
                    r(e) && (c[a(e)] = l[e], l.$observe(e, function(t) {
                        c[a(e)] = t
                    }))
                }), t.element(s[0]).bind(u, function(i) {
                    var r = l.analyticsEvent || o(s[0]);
                    c.eventType = i.type, (!l.analyticsIf || n.$eval(l.analyticsIf)) && (l.analyticsProperties && t.extend(c, n.$eval(l.analyticsProperties)), e.eventTrack(r, c))
                })
            }
        }
    }])
}(angular);
