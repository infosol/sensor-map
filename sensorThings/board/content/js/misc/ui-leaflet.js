! function(angular) {
    "use strict";
    angular.module("ui-leaflet", ["nemLogging"]).directive("leaflet", ["$q", "leafletData", "leafletMapDefaults", "leafletHelpers", "leafletMapEvents", function(t, e, n, i, r) {
        return {
            restrict: "EA",
            replace: !0,
            scope: {
                center: "=",
                lfCenter: "=",
                defaults: "=",
                maxbounds: "=",
                bounds: "=",
                markers: "=",
                legend: "=",
                geojson: "=",
                paths: "=",
                tiles: "=",
                layers: "=",
                controls: "=",
                decorations: "=",
                eventBroadcast: "=",
                markersWatchOptions: "=",
                geojsonWatchOptions: "="
            },
            transclude: !0,
            template: '<div class="angular-leaflet-map"><div ng-transclude></div></div>',
            controller: ["$scope", function(e) {
                this._leafletMap = t.defer(), this.getMap = function() {
                    return this._leafletMap.promise
                }, this.getLeafletScope = function() {
                    return e
                }
            }],
            link: function(t, o, a, s) {
                function l() {
                    isNaN(a.width) ? o.css("width", a.width) : o.css("width", a.width + "px")
                }

                function u() {
                    isNaN(a.height) ? o.css("height", a.height) : o.css("height", a.height + "px")
                }
                var c = i.isDefined,
                    h = n.setDefaults(t.defaults, a.id),
                    d = r.getAvailableMapEvents(),
                    f = r.addEvents;
                t.mapId = a.id, e.setDirectiveControls({}, a.id), c(a.width) && (l(), t.$watch(function() {
                    return o[0].getAttribute("width")
                }, function() {
                    l(), p.invalidateSize()
                })), c(a.height) && (u(), t.$watch(function() {
                    return o[0].getAttribute("height")
                }, function() {
                    u(), p.invalidateSize()
                }));
                var p = new L.Map(o[0], n.getMapCreationDefaults(a.id));
                if (s._leafletMap.resolve(p), c(a.center) || c(a.lfCenter) || p.setView([h.center.lat, h.center.lng], h.center.zoom), !c(a.tiles) && !c(a.layers)) {
                    var m = L.tileLayer(h.tileLayer, h.tileLayerOptions);
                    m.addTo(p), e.setTiles(m, a.id)
                }
                if (c(p.zoomControl) && c(h.zoomControlPosition) && p.zoomControl.setPosition(h.zoomControlPosition), c(p.zoomControl) && h.zoomControl === !1 && p.zoomControl.removeFrom(p), c(p.zoomsliderControl) && c(h.zoomsliderControl) && h.zoomsliderControl === !1 && p.zoomsliderControl.removeFrom(p), !c(a.eventBroadcast)) {
                    var g = "broadcast";
                    f(p, d, "eventName", t, g)
                }
                p.whenReady(function() {
                    e.setMap(p, a.id)
                }), t.$on("$destroy", function() {
                    n.reset(), p.remove(), e.unresolveMap(a.id)
                }), t.$on("invalidateSize", function() {
                    p.invalidateSize()
                })
            }
        }
    }]), angular.module("ui-leaflet").factory("leafletBoundsHelpers", ["leafletLogger", "leafletHelpers", function(t, e) {
        function n(t) {
            return angular.isDefined(t) && angular.isDefined(t.southWest) && angular.isDefined(t.northEast) && angular.isNumber(t.southWest.lat) && angular.isNumber(t.southWest.lng) && angular.isNumber(t.northEast.lat) && angular.isNumber(t.northEast.lng)
        }
        var i = e.isArray,
            r = e.isNumber,
            o = e.isFunction,
            a = e.isDefined,
            s = t;
        return {
            createLeafletBounds: function(t) {
                return n(t) ? L.latLngBounds([t.southWest.lat, t.southWest.lng], [t.northEast.lat, t.northEast.lng]) : void 0
            },
            isValidBounds: n,
            createBoundsFromArray: function(t) {
                return i(t) && 2 === t.length && i(t[0]) && i(t[1]) && 2 === t[0].length && 2 === t[1].length && r(t[0][0]) && r(t[0][1]) && r(t[1][0]) && r(t[1][1]) ? {
                    northEast: {
                        lat: t[0][0],
                        lng: t[0][1]
                    },
                    southWest: {
                        lat: t[1][0],
                        lng: t[1][1]
                    }
                } : void s.error("[AngularJS - Leaflet] The bounds array is not valid.")
            },
            createBoundsFromLeaflet: function(t) {
                if (!(a(t) && o(t.getNorthEast) && o(t.getSouthWest))) return void s.error("[AngularJS - Leaflet] The leaflet bounds is not valid object.");
                var e = t.getNorthEast(),
                    n = t.getSouthWest();
                return {
                    northEast: {
                        lat: e.lat,
                        lng: e.lng
                    },
                    southWest: {
                        lat: n.lat,
                        lng: n.lng
                    }
                }
            }
        }
    }]), angular.module("ui-leaflet").factory("leafletControlHelpers", ["$rootScope", "leafletLogger", "leafletHelpers", "leafletLayerHelpers", "leafletMapDefaults", function(t, e, n, i, r) {
        var o = n.isDefined,
            a = n.isObject,
            s = i.createLayer,
            l = {},
            u = n.errorHeader + " [Controls] ",
            c = e,
            h = function(t, e, n) {
                var i = r.getDefaults(n);
                if (!i.controls.layers.visible) return !1;
                var s = !1;
                return a(t) && Object.keys(t).forEach(function(e) {
                    var n = t[e];
                    o(n.layerOptions) && n.layerOptions.showOnSelector === !1 || (s = !0)
                }), a(e) && Object.keys(e).forEach(function(t) {
                    var n = e[t];
                    o(n.layerParams) && n.layerParams.showOnSelector === !1 || (s = !0)
                }), s
            },
            d = function(t) {
                var e = r.getDefaults(t),
                    n = {
                        collapsed: e.controls.layers.collapsed,
                        position: e.controls.layers.position,
                        autoZIndex: !1
                    };
                angular.extend(n, e.controls.layers.options);
                var i;
                return i = e.controls.layers && o(e.controls.layers.control) ? e.controls.layers.control.apply(this, [
                    [],
                    [], n
                ]) : new L.control.layers([], [], n)
            },
            f = {
                draw: {
                    isPluginLoaded: function() {
                        return angular.isDefined(L.Control.Draw) ? !0 : (c.error(u + " Draw plugin is not loaded."), !1)
                    },
                    checkValidParams: function() {
                        return !0
                    },
                    createControl: function(t) {
                        return new L.Control.Draw(t)
                    }
                },
                scale: {
                    isPluginLoaded: function() {
                        return !0
                    },
                    checkValidParams: function() {
                        return !0
                    },
                    createControl: function(t) {
                        return new L.control.scale(t)
                    }
                },
                fullscreen: {
                    isPluginLoaded: function() {
                        return angular.isDefined(L.Control.Fullscreen) ? !0 : (c.error(u + " Fullscreen plugin is not loaded."), !1)
                    },
                    checkValidParams: function() {
                        return !0
                    },
                    createControl: function(t) {
                        return new L.Control.Fullscreen(t)
                    }
                },
                search: {
                    isPluginLoaded: function() {
                        return angular.isDefined(L.Control.Search) ? !0 : (c.error(u + " Search plugin is not loaded."), !1)
                    },
                    checkValidParams: function() {
                        return !0
                    },
                    createControl: function(t) {
                        return new L.Control.Search(t)
                    }
                },
                custom: {},
                minimap: {
                    isPluginLoaded: function() {
                        return angular.isDefined(L.Control.MiniMap) ? !0 : (c.error(u + " Minimap plugin is not loaded."), !1)
                    },
                    checkValidParams: function(t) {
                        return o(t.layer) ? !0 : (c.warn(u + ' minimap "layer" option should be defined.'), !1)
                    },
                    createControl: function(t) {
                        var e = s(t.layer);
                        return o(e) ? new L.Control.MiniMap(e, t) : void c.warn(u + ' minimap control "layer" could not be created.')
                    }
                }
            };
        return {
            layersControlMustBeVisible: h,
            isValidControlType: function(t) {
                return -1 !== Object.keys(f).indexOf(t)
            },
            createControl: function(t, e) {
                return f[t].checkValidParams(e) ? f[t].createControl(e) : void 0
            },
            updateLayersControl: function(t, e, n, i, r, a) {
                var s, u = l[e],
                    c = h(i, r, e);
                if (o(u) && n) {
                    for (s in a.baselayers) u.removeLayer(a.baselayers[s]);
                    for (s in a.overlays) u.removeLayer(a.overlays[s]);
                    t.removeControl(u), delete l[e]
                }
                if (c) {
                    u = d(e), l[e] = u;
                    for (s in i) {
                        var f = o(i[s].layerOptions) && i[s].layerOptions.showOnSelector === !1;
                        !f && o(a.baselayers[s]) && u.addBaseLayer(a.baselayers[s], i[s].name)
                    }
                    for (s in r) {
                        var p = o(r[s].layerParams) && r[s].layerParams.showOnSelector === !1;
                        !p && o(a.overlays[s]) && u.addOverlay(a.overlays[s], r[s].name)
                    }
                    t.addControl(u)
                }
                return c
            }
        }
    }]), angular.module("ui-leaflet").service("leafletData", ["leafletLogger", "$q", "leafletHelpers", function(t, e, n) {
        var i = n.getDefer,
            r = n.getUnresolvedDefer,
            o = n.setResolvedDefer,
            a = {},
            s = this,
            l = function(t) {
                return t.charAt(0).toUpperCase() + t.slice(1)
            },
            u = ["map", "tiles", "layers", "paths", "markers", "geoJSON", "UTFGrid", "decorations", "directiveControls"];
        u.forEach(function(t) {
            a[t] = {}
        }), this.unresolveMap = function(t) {
            var e = n.obtainEffectiveMapId(a.map, t);
            u.forEach(function(t) {
                a[t][e] = void 0
            })
        }, u.forEach(function(t) {
            var e = l(t);
            s["set" + e] = function(e, n) {
                var i = r(a[t], n);
                i.resolve(e), o(a[t], n)
            }, s["get" + e] = function(e) {
                var n = i(a[t], e);
                return n.promise
            }
        })
    }]), angular.module("ui-leaflet").service("leafletDirectiveControlsHelpers", ["leafletLogger", "leafletData", "leafletHelpers", function(t, e, n) {
        var i = n.isDefined,
            r = n.isString,
            o = n.isObject,
            a = n.errorHeader,
            s = t,
            l = a + "[leafletDirectiveControlsHelpers",
            u = function(t, n, a, u) {
                var c = l + ".extend] ",
                    h = {};
                if (!i(n)) return void s.error(c + "thingToAddName cannot be undefined");
                if (r(n) && i(a) && i(u)) h[n] = {
                    create: a,
                    clean: u
                };
                else {
                    if (!o(n) || i(a) || i(u)) return void s.error(c + "incorrect arguments");
                    h = n
                }
                e.getDirectiveControls().then(function(n) {
                    angular.extend(n, h), e.setDirectiveControls(n, t)
                })
            };
        return {
            extend: u
        }
    }]), angular.module("ui-leaflet").service("leafletGeoJsonHelpers", ["leafletHelpers", "leafletIterators", function(t, e) {
        var n = t,
            i = e,
            r = function(t, e) {
                return this.lat = t, this.lng = e, this
            },
            o = function(t) {
                return Array.isArray(t) && 2 === t.length ? t[1] : n.isDefined(t.type) && "Point" === t.type ? +t.coordinates[1] : +t.lat
            },
            a = function(t) {
                return Array.isArray(t) && 2 === t.length ? t[0] : n.isDefined(t.type) && "Point" === t.type ? +t.coordinates[0] : +t.lng
            },
            s = function(t) {
                if (n.isUndefined(t)) return !1;
                if (n.isArray(t)) {
                    if (2 === t.length && n.isNumber(t[0]) && n.isNumber(t[1])) return !0
                } else if (n.isDefined(t.type) && "Point" === t.type && n.isArray(t.coordinates) && 2 === t.coordinates.length && n.isNumber(t.coordinates[0]) && n.isNumber(t.coordinates[1])) return !0;
                var e = i.all(["lat", "lng"], function(e) {
                    return n.isDefined(t[e]) && n.isNumber(t[e])
                });
                return e
            },
            l = function(t) {
                if (t && s(t)) {
                    var e = null;
                    if (Array.isArray(t) && 2 === t.length) e = new r(t[1], t[0]);
                    else {
                        if (!n.isDefined(t.type) || "Point" !== t.type) return t;
                        e = new r(t.coordinates[1], t.coordinates[0])
                    }
                    return angular.extend(t, e)
                }
            };
        return {
            getLat: o,
            getLng: a,
            validateCoords: s,
            getCoords: l
        }
    }]), angular.module("ui-leaflet").service("leafletHelpers", ["$q", "$log", function(t, e) {
        function n(t, n) {
            var i, o;
            if (angular.isDefined(n)) i = n;
            else if (0 === Object.keys(t).length) i = "main";
            else if (Object.keys(t).length >= 1)
                for (o in t) t.hasOwnProperty(o) && (i = o);
            else e.error(r + "- You have more than 1 map on the DOM, you must provide the map ID to the leafletData.getXXX call");
            return i
        }

        function i(e, i) {
            var r, o = n(e, i);
            return angular.isDefined(e[o]) && e[o].resolvedDefer !== !0 ? r = e[o].defer : (r = t.defer(), e[o] = {
                defer: r,
                resolvedDefer: !1
            }), r
        }
        var r = "[AngularJS - Leaflet] ",
            o = angular.copy,
            a = o,
            s = function(t, e) {
                var n;
                if (t && angular.isObject(t)) return null !== e && angular.isString(e) ? (n = t, e.split(".").forEach(function(t) {
                    n && (n = n[t])
                }), n) : e
            },
            l = function(t) {
                return t.split(".").reduce(function(t, e) {
                    return t + '["' + e + '"]'
                })
            },
            u = function(t) {
                return t.reduce(function(t, e) {
                    return t + "." + e
                })
            },
            c = function(t) {
                return angular.isDefined(t) && null !== t
            },
            h = function(t) {
                return !c(t)
            },
            d = /([\:\-\_]+(.))/g,
            f = /^moz([A-Z])/,
            p = /^((?:x|data)[\:\-_])/i,
            m = function(t) {
                return t.replace(d, function(t, e, n, i) {
                    return i ? n.toUpperCase() : n
                }).replace(f, "Moz$1")
            },
            g = function(t) {
                return m(t.replace(p, ""))
            };
        return {
            camelCase: m,
            directiveNormalize: g,
            copy: o,
            clone: a,
            errorHeader: r,
            getObjectValue: s,
            getObjectArrayPath: l,
            getObjectDotPath: u,
            defaultTo: function(t, e) {
                return c(t) ? t : e
            },
            isTruthy: function(t) {
                return "true" === t || t === !0
            },
            isEmpty: function(t) {
                return 0 === Object.keys(t).length
            },
            isUndefinedOrEmpty: function(t) {
                return angular.isUndefined(t) || null === t || 0 === Object.keys(t).length
            },
            isDefined: c,
            isUndefined: h,
            isNumber: angular.isNumber,
            isString: angular.isString,
            isArray: angular.isArray,
            isObject: angular.isObject,
            isFunction: angular.isFunction,
            equals: angular.equals,
            isValidCenter: function(t) {
                return angular.isDefined(t) && angular.isNumber(t.lat) && angular.isNumber(t.lng) && angular.isNumber(t.zoom)
            },
            isValidPoint: function(t) {
                return angular.isDefined(t) ? angular.isArray(t) ? 2 === t.length && angular.isNumber(t[0]) && angular.isNumber(t[1]) : angular.isNumber(t.lat) && angular.isNumber(t.lng) : !1
            },
            isSameCenterOnMap: function(t, e) {
                var n = e.getCenter(),
                    i = e.getZoom();
                return t.lat && t.lng && n.lat.toFixed(4) === t.lat.toFixed(4) && n.lng.toFixed(4) === t.lng.toFixed(4) && i === t.zoom ? !0 : !1
            },
            safeApply: function(t, e) {
                var n = t.$root.$$phase;
                "$apply" === n || "$digest" === n ? t.$eval(e) : t.$evalAsync(e)
            },
            obtainEffectiveMapId: n,
            getDefer: function(t, e) {
                var r, o = n(t, e);
                return r = angular.isDefined(t[o]) && t[o].resolvedDefer !== !1 ? t[o].defer : i(t, e)
            },
            getUnresolvedDefer: i,
            setResolvedDefer: function(t, e) {
                var i = n(t, e);
                t[i].resolvedDefer = !0
            },
            rangeIsSupported: function() {
                var t = document.createElement("input");
                return t.setAttribute("type", "range"), "range" === t.type
            },
            FullScreenControlPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.Control.Fullscreen)
                }
            },
            MiniMapControlPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.Control.MiniMap)
                }
            },
            AwesomeMarkersPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.AwesomeMarkers) && angular.isDefined(L.AwesomeMarkers.Icon)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.AwesomeMarkers.Icon : !1
                },
                equal: function(t, e) {
                    return this.isLoaded() && this.is(t) ? angular.equals(t, e) : !1
                }
            },
            VectorMarkersPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.VectorMarkers) && angular.isDefined(L.VectorMarkers.Icon)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.VectorMarkers.Icon : !1
                },
                equal: function(t, e) {
                    return this.isLoaded() && this.is(t) ? angular.equals(t, e) : !1
                }
            },
            DomMarkersPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.DomMarkers) && angular.isDefined(L.DomMarkers.Icon) ? !0 : !1
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.DomMarkers.Icon : !1
                },
                equal: function(t, e) {
                    return this.isLoaded() && this.is(t) ? angular.equals(t, e) : !1
                }
            },
            PolylineDecoratorPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.PolylineDecorator) ? !0 : !1
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.PolylineDecorator : !1
                },
                equal: function(t, e) {
                    return this.isLoaded() && this.is(t) ? angular.equals(t, e) : !1
                }
            },
            MakiMarkersPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.MakiMarkers) && angular.isDefined(L.MakiMarkers.Icon) ? !0 : !1
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.MakiMarkers.Icon : !1
                },
                equal: function(t, e) {
                    return this.isLoaded() && this.is(t) ? angular.equals(t, e) : !1
                }
            },
            ExtraMarkersPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.ExtraMarkers) && angular.isDefined(L.ExtraMarkers.Icon) ? !0 : !1
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.ExtraMarkers.Icon : !1
                },
                equal: function(t, e) {
                    return this.isLoaded() && this.is(t) ? angular.equals(t, e) : !1
                }
            },
            LabelPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.Label)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.MarkerClusterGroup : !1
                }
            },
            MarkerClusterPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.MarkerClusterGroup)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.MarkerClusterGroup : !1
                }
            },
            GoogleLayerPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.Google)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.Google : !1
                }
            },
            LeafletProviderPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.TileLayer.Provider)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.TileLayer.Provider : !1
                }
            },
            ChinaLayerPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.tileLayer.chinaProvider)
                }
            },
            HeatLayerPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.heatLayer)
                }
            },
            WebGLHeatMapLayerPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.TileLayer.WebGLHeatMap)
                }
            },
            BingLayerPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.BingLayer)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.BingLayer : !1
                }
            },
            WFSLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== L.GeoJSON.WFS
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.GeoJSON.WFS : !1
                }
            },
            AGSBaseLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== L.esri && void 0 !== L.esri.basemapLayer
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.esri.basemapLayer : !1
                }
            },
            AGSLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== lvector && void 0 !== lvector.AGS
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof lvector.AGS : !1
                }
            },
            AGSFeatureLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== L.esri && void 0 !== L.esri.featureLayer
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.esri.featureLayer : !1
                }
            },
            AGSTiledMapLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== L.esri && void 0 !== L.esri.tiledMapLayer
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.esri.tiledMapLayer : !1
                }
            },
            AGSDynamicMapLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== L.esri && void 0 !== L.esri.dynamicMapLayer
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.esri.dynamicMapLayer : !1
                }
            },
            AGSImageMapLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== L.esri && void 0 !== L.esri.imageMapLayer
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.esri.imageMapLayer : !1
                }
            },
            AGSClusteredLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== L.esri && void 0 !== L.esri.clusteredFeatureLayer
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.esri.clusteredFeatureLayer : !1
                }
            },
            AGSHeatmapLayerPlugin: {
                isLoaded: function() {
                    return void 0 !== L.esri && void 0 !== L.esri.heatmapFeatureLayer
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.esri.heatmapFeatureLayer : !1
                }
            },
            YandexLayerPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.Yandex)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.Yandex : !1
                }
            },
            GeoJSONPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.TileLayer.GeoJSON)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.TileLayer.GeoJSON : !1
                }
            },
            UTFGridPlugin: {
                isLoaded: function() {
                    return angular.isDefined(L.UtfGrid)
                },
                is: function(t) {
                    return this.isLoaded() ? t instanceof L.UtfGrid : (e.error("[AngularJS - Leaflet] No UtfGrid plugin found."), !1)
                }
            },
            CartoDB: {
                isLoaded: function() {
                    return cartodb
                },
                is: function() {
                    return !0
                }
            },
            Leaflet: {
                DivIcon: {
                    is: function(t) {
                        return t instanceof L.DivIcon
                    },
                    equal: function(t, e) {
                        return this.is(t) ? angular.equals(t, e) : !1
                    }
                },
                Icon: {
                    is: function(t) {
                        return t instanceof L.Icon
                    },
                    equal: function(t, e) {
                        return this.is(t) ? angular.equals(t, e) : !1
                    }
                }
            },
            watchOptions: {
                doWatch: !0,
                isDeep: !0,
                individual: {
                    doWatch: !0,
                    isDeep: !0
                }
            }
        }
    }]), angular.module("ui-leaflet").service("leafletIterators", ["leafletLogger", "leafletHelpers", function(t, e) {
        var n, i = e,
            r = e.errorHeader + "leafletIterators: ",
            o = Object.keys,
            a = i.isFunction,
            s = i.isObject,
            l = t,
            u = Math.pow(2, 53) - 1,
            c = function(t) {
                var e = null !== t && t.length;
                return i.isNumber(e) && e >= 0 && u >= e
            },
            h = function(t) {
                return t
            },
            d = function(t) {
                return function(e) {
                    return null === e ? void 0 : e[t]
                }
            },
            f = function(t, e, n) {
                if (void 0 === e) return t;
                switch (null === n ? 3 : n) {
                    case 1:
                        return function(n) {
                            return t.call(e, n)
                        };
                    case 2:
                        return function(n, i) {
                            return t.call(e, n, i)
                        };
                    case 3:
                        return function(n, i, r) {
                            return t.call(e, n, i, r)
                        };
                    case 4:
                        return function(n, i, r, o) {
                            return t.call(e, n, i, r, o)
                        }
                }
                return function() {
                    return t.apply(e, arguments)
                }
            },
            p = function(t, e) {
                return function(n) {
                    var i = arguments.length;
                    if (2 > i || null === n) return n;
                    for (var r = 1; i > r; r++)
                        for (var o = arguments[r], a = t(o), s = a.length, l = 0; s > l; l++) {
                            var u = a[l];
                            e && void 0 !== n[u] || (n[u] = o[u])
                        }
                    return n
                }
            },
            m = null;
        n = m = p(o);
        var g, v = function(t, e) {
                var n = o(e),
                    i = n.length;
                if (null === t) return !i;
                for (var r = Object(t), a = 0; i > a; a++) {
                    var s = n[a];
                    if (e[s] !== r[s] || !(s in r)) return !1
                }
                return !0
            },
            y = null;
        g = y = function(t) {
            return t = n({}, t),
                function(e) {
                    return v(e, t)
                }
        };
        var _, b = function(t, e, n) {
                return null === t ? h : a(t) ? f(t, e, n) : s(t) ? g(t) : d(t)
            },
            $ = null;
        _ = $ = function(t, e, n) {
            e = b(e, n);
            for (var i = !c(t) && o(t), r = (i || t).length, a = 0; r > a; a++) {
                var s = i ? i[a] : a;
                if (!e(t[s], s, t)) return !1
            }
            return !0
        };
        var w = function(t, e, n, o) {
                return n || i.isDefined(t) && i.isDefined(e) ? i.isFunction(e) ? !1 : (o = i.defaultTo(e, "cb"), l.error(r + o + " is not a function"), !0) : !0
            },
            L = function(t, e, n) {
                if (!w(void 0, n, !0, "internalCb") && !w(t, e))
                    for (var i in t) t.hasOwnProperty(i) && n(t[i], i)
            },
            x = function(t, e) {
                L(t, e, function(t, n) {
                    e(t, n)
                })
            };
        return {
            each: x,
            forEach: x,
            every: _,
            all: $
        }
    }]), angular.module("ui-leaflet").factory("leafletLayerHelpers", ["$rootScope", "$q", "leafletLogger", "leafletHelpers", "leafletIterators", function($rootScope, $q, leafletLogger, leafletHelpers, leafletIterators) {
        function isValidLayerType(t) {
            return isString(t.type) ? -1 === Object.keys(layerTypes).indexOf(t.type) ? ($log.error("[AngularJS - Leaflet] A layer must have a valid type: " + Object.keys(layerTypes)), !1) : layerTypes[t.type].mustHaveUrl && !isString(t.url) ? ($log.error("[AngularJS - Leaflet] A base layer must have an url"), !1) : layerTypes[t.type].mustHaveData && !isDefined(t.data) ? ($log.error('[AngularJS - Leaflet] The base layer must have a "data" array attribute'), !1) : layerTypes[t.type].mustHaveLayer && !isDefined(t.layer) ? ($log.error("[AngularJS - Leaflet] The type of layer " + t.type + " must have an layer defined"), !1) : layerTypes[t.type].mustHaveBounds && !isDefined(t.bounds) ? ($log.error("[AngularJS - Leaflet] The type of layer " + t.type + " must have bounds defined"), !1) : layerTypes[t.type].mustHaveKey && !isDefined(t.key) ? ($log.error("[AngularJS - Leaflet] The type of layer " + t.type + " must have key defined"), !1) : !0 : ($log.error("[AngularJS - Leaflet] A layer must have a valid type defined."), !1)
        }

        function createLayer(t) {
            if (isValidLayerType(t)) {
                if (!isString(t.name)) return void $log.error("[AngularJS - Leaflet] A base layer must have a name");
                isObject(t.layerParams) || (t.layerParams = {}), isObject(t.layerOptions) || (t.layerOptions = {});
                for (var e in t.layerParams) t.layerOptions[e] = t.layerParams[e];
                var n = {
                    url: t.url,
                    data: t.data,
                    options: t.layerOptions,
                    layer: t.layer,
                    icon: t.icon,
                    type: t.layerType,
                    bounds: t.bounds,
                    key: t.key,
                    apiKey: t.apiKey,
                    pluginOptions: t.pluginOptions,
                    user: t.user
                };
                return layerTypes[t.type].createLayer(n)
            }
        }

        function safeAddLayer(t, e) {
            e && "function" == typeof e.addTo ? e.addTo(t) : t.addLayer(e)
        }

        function safeRemoveLayer(t, e, n) {
            if (isDefined(n) && isDefined(n.loadedDefer))
                if (angular.isFunction(n.loadedDefer)) {
                    var i = n.loadedDefer();
                    $log.debug("Loaded Deferred", i);
                    var r = i.length;
                    if (r > 0)
                        for (var o = function() {
                                r--, 0 === r && t.removeLayer(e)
                            }, a = 0; a < i.length; a++) i[a].promise.then(o);
                    else t.removeLayer(e)
                } else n.loadedDefer.promise.then(function() {
                    t.removeLayer(e)
                });
            else t.removeLayer(e)
        }
        var Helpers = leafletHelpers,
            isString = leafletHelpers.isString,
            isObject = leafletHelpers.isObject,
            isArray = leafletHelpers.isArray,
            isDefined = leafletHelpers.isDefined,
            errorHeader = leafletHelpers.errorHeader,
            $it = leafletIterators,
            $log = leafletLogger,
            utfGridCreateLayer = function(t) {
                if (!Helpers.UTFGridPlugin.isLoaded()) return void $log.error("[AngularJS - Leaflet] The UTFGrid plugin is not loaded.");
                var e = new L.UtfGrid(t.url, t.pluginOptions);
                return e.on("mouseover", function(t) {
                    $rootScope.$broadcast("leafletDirectiveMap.utfgridMouseover", t)
                }), e.on("mouseout", function(t) {
                    $rootScope.$broadcast("leafletDirectiveMap.utfgridMouseout", t)
                }), e.on("click", function(t) {
                    $rootScope.$broadcast("leafletDirectiveMap.utfgridClick", t)
                }), e.on("mousemove", function(t) {
                    $rootScope.$broadcast("leafletDirectiveMap.utfgridMousemove", t)
                }), e
            },
            layerTypes = {
                xyz: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return L.tileLayer(t.url, t.options)
                    }
                },
                mapbox: {
                    mustHaveKey: !0,
                    createLayer: function(t) {
                        var e = 3;
                        isDefined(t.options.version) && 4 === t.options.version && (e = t.options.version);
                        var n = 3 === e ? "//{s}.tiles.mapbox.com/v3/" + t.key + "/{z}/{x}/{y}.png" : "//api.tiles.mapbox.com/v4/" + t.key + "/{z}/{x}/{y}.png?access_token=" + t.apiKey;
                        return L.tileLayer(n, t.options)
                    }
                },
                geoJSON: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return Helpers.GeoJSONPlugin.isLoaded() ? new L.TileLayer.GeoJSON(t.url, t.pluginOptions, t.options) : void 0
                    }
                },
                geoJSONShape: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        return new L.GeoJSON(t.data, t.options)
                    }
                },
                geoJSONAwesomeMarker: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        return new L.geoJson(t.data, {
                            pointToLayer: function(e, n) {
                                return L.marker(n, {
                                    icon: L.AwesomeMarkers.icon(t.icon)
                                })
                            }
                        })
                    }
                },
                geoJSONVectorMarker: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        return new L.geoJson(t.data, {
                            pointToLayer: function(e, n) {
                                return L.marker(n, {
                                    icon: L.VectorMarkers.icon(t.icon)
                                })
                            }
                        })
                    }
                },
                utfGrid: {
                    mustHaveUrl: !0,
                    createLayer: utfGridCreateLayer
                },
                cartodbTiles: {
                    mustHaveKey: !0,
                    createLayer: function(t) {
                        var e = "//" + t.user + ".cartodb.com/api/v1/map/" + t.key + "/{z}/{x}/{y}.png";
                        return L.tileLayer(e, t.options)
                    }
                },
                cartodbUTFGrid: {
                    mustHaveKey: !0,
                    mustHaveLayer: !0,
                    createLayer: function(t) {
                        return t.url = "//" + t.user + ".cartodb.com/api/v1/map/" + t.key + "/" + t.layer + "/{z}/{x}/{y}.grid.json", utfGridCreateLayer(t)
                    }
                },
                cartodbInteractive: {
                    mustHaveKey: !0,
                    mustHaveLayer: !0,
                    createLayer: function(t) {
                        var e = "//" + t.user + ".cartodb.com/api/v1/map/" + t.key + "/{z}/{x}/{y}.png",
                            n = L.tileLayer(e, t.options);
                        t.url = "//" + t.user + ".cartodb.com/api/v1/map/" + t.key + "/" + t.layer + "/{z}/{x}/{y}.grid.json";
                        var i = utfGridCreateLayer(t);
                        return L.layerGroup([n, i])
                    }
                },
                wms: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return L.tileLayer.wms(t.url, t.options)
                    }
                },
                wmts: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return L.tileLayer.wmts(t.url, t.options)
                    }
                },
                wfs: {
                    mustHaveUrl: !0,
                    mustHaveLayer: !0,
                    createLayer: function(params) {
                        if (Helpers.WFSLayerPlugin.isLoaded()) {
                            var options = angular.copy(params.options);
                            return options.crs && "string" == typeof options.crs && (options.crs = eval(options.crs)), new L.GeoJSON.WFS(params.url, params.layer, options)
                        }
                    }
                },
                group: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        var e = [];
                        return $it.each(t.options.layers, function(t) {
                            e.push(createLayer(t))
                        }), t.options.loadedDefer = function() {
                            var e = [];
                            if (isDefined(t.options.layers))
                                for (var n = 0; n < t.options.layers.length; n++) {
                                    var i = t.options.layers[n].layerOptions.loadedDefer;
                                    isDefined(i) && e.push(i)
                                }
                            return e
                        }, L.layerGroup(e)
                    }
                },
                featureGroup: {
                    mustHaveUrl: !1,
                    createLayer: function() {
                        return L.featureGroup()
                    }
                },
                google: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        var e = t.type || "SATELLITE";
                        if (Helpers.GoogleLayerPlugin.isLoaded()) return new L.Google(e, t.options)
                    }
                },
                here: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        var e = t.provider || "HERE.terrainDay";
                        if (Helpers.LeafletProviderPlugin.isLoaded()) return new L.TileLayer.Provider(e, t.options)
                    }
                },
                china: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        var e = t.type || "";
                        if (Helpers.ChinaLayerPlugin.isLoaded()) return L.tileLayer.chinaProvider(e, t.options)
                    }
                },
                agsBase: {
                    mustHaveLayer: !0,
                    createLayer: function(t) {
                        return Helpers.AGSBaseLayerPlugin.isLoaded() ? L.esri.basemapLayer(t.layer, t.options) : void 0
                    }
                },
                ags: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        if (Helpers.AGSLayerPlugin.isLoaded()) {
                            var e = angular.copy(t.options);
                            angular.extend(e, {
                                url: t.url
                            });
                            var n = new lvector.AGS(e);
                            return n.onAdd = function(t) {
                                this.setMap(t)
                            }, n.onRemove = function() {
                                this.setMap(null)
                            }, n
                        }
                    }
                },
                agsFeature: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        if (!Helpers.AGSFeatureLayerPlugin.isLoaded()) return void $log.warn(errorHeader + " The esri plugin is not loaded.");
                        t.options.url = t.url;
                        var e = L.esri.featureLayer(t.options),
                            n = function() {
                                isDefined(t.options.loadedDefer) && t.options.loadedDefer.resolve()
                            };
                        return e.on("loading", function() {
                            t.options.loadedDefer = $q.defer(), e.off("load", n), e.on("load", n)
                        }), e
                    }
                },
                agsTiled: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return Helpers.AGSTiledMapLayerPlugin.isLoaded() ? (t.options.url = t.url, L.esri.tiledMapLayer(t.options)) : void $log.warn(errorHeader + " The esri plugin is not loaded.")
                    }
                },
                agsDynamic: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return Helpers.AGSDynamicMapLayerPlugin.isLoaded() ? (t.options.url = t.url, L.esri.dynamicMapLayer(t.options)) : void $log.warn(errorHeader + " The esri plugin is not loaded.")
                    }
                },
                agsImage: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return Helpers.AGSImageMapLayerPlugin.isLoaded() ? (t.options.url = t.url, L.esri.imageMapLayer(t.options)) : void $log.warn(errorHeader + " The esri plugin is not loaded.")
                    }
                },
                agsClustered: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return Helpers.AGSClusteredLayerPlugin.isLoaded() ? Helpers.MarkerClusterPlugin.isLoaded() ? L.esri.clusteredFeatureLayer(t.url, t.options) : void $log.warn(errorHeader + " The markercluster plugin is not loaded.") : void $log.warn(errorHeader + " The esri clustered layer plugin is not loaded.")
                    }
                },
                agsHeatmap: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return Helpers.AGSHeatmapLayerPlugin.isLoaded() ? Helpers.HeatLayerPlugin.isLoaded() ? L.esri.heatmapFeatureLayer(t.url, t.options) : void $log.warn(errorHeader + " The heatlayer plugin is not loaded.") : void $log.warn(errorHeader + " The esri heatmap layer plugin is not loaded.")
                    }
                },
                markercluster: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        return Helpers.MarkerClusterPlugin.isLoaded() ? new L.MarkerClusterGroup(t.options) : void $log.warn(errorHeader + " The markercluster plugin is not loaded.")
                    }
                },
                bing: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        return Helpers.BingLayerPlugin.isLoaded() ? new L.BingLayer(t.key, t.options) : void 0
                    }
                },
                webGLHeatmap: {
                    mustHaveUrl: !1,
                    mustHaveData: !0,
                    createLayer: function(t) {
                        if (Helpers.WebGLHeatMapLayerPlugin.isLoaded()) {
                            var e = new L.TileLayer.WebGLHeatMap(t.options);
                            return isDefined(t.data) && e.setData(t.data), e
                        }
                    }
                },
                heat: {
                    mustHaveUrl: !1,
                    mustHaveData: !0,
                    createLayer: function(t) {
                        if (Helpers.HeatLayerPlugin.isLoaded()) {
                            var e = new L.heatLayer;
                            return isArray(t.data) && e.setLatLngs(t.data), isObject(t.options) && e.setOptions(t.options), e
                        }
                    }
                },
                yandex: {
                    mustHaveUrl: !1,
                    createLayer: function(t) {
                        var e = t.type || "map";
                        if (Helpers.YandexLayerPlugin.isLoaded()) return new L.Yandex(e, t.options)
                    }
                },
                imageOverlay: {
                    mustHaveUrl: !0,
                    mustHaveBounds: !0,
                    createLayer: function(t) {
                        return L.imageOverlay(t.url, t.bounds, t.options)
                    }
                },
                iip: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return L.tileLayer.iip(t.url, t.options)
                    }
                },
                custom: {
                    createLayer: function(t) {
                        return t.layer instanceof L.Class ? angular.copy(t.layer) : void $log.error("[AngularJS - Leaflet] A custom layer must be a leaflet Class")
                    }
                },
                cartodb: {
                    mustHaveUrl: !0,
                    createLayer: function(t) {
                        return cartodb.createLayer(t.map, t.url)
                    }
                }
            };
        return {
            createLayer: createLayer,
            safeAddLayer: safeAddLayer,
            safeRemoveLayer: safeRemoveLayer
        }
    }]), angular.module("ui-leaflet").factory("leafletLegendHelpers", function() {
        var t = function(t, e, n, i) {
                if (t.innerHTML = "", e.error) t.innerHTML += '<div class="info-title alert alert-danger">' + e.error.message + "</div>";
                else if ("arcgis" === n)
                    for (var r = 0; r < e.layers.length; r++) {
                        var o = e.layers[r];
                        t.innerHTML += '<div class="info-title" data-layerid="' + o.layerId + '">' + o.layerName + "</div>";
                        for (var a = 0; a < o.legend.length; a++) {
                            var s = o.legend[a];
                            t.innerHTML += '<div class="inline" data-layerid="' + o.layerId + '"><img src="data:' + s.contentType + ";base64," + s.imageData + '" /></div><div class="info-label" data-layerid="' + o.layerId + '">' + s.label + "</div>"
                        }
                    } else "image" === n && (t.innerHTML = '<img src="' + i + '"/>')
            },
            e = function(e, n, i, r) {
                return function() {
                    var o = L.DomUtil.create("div", n);
                    return L.Browser.touch ? L.DomEvent.on(o, "click", L.DomEvent.stopPropagation) : (L.DomEvent.disableClickPropagation(o), L.DomEvent.on(o, "mousewheel", L.DomEvent.stopPropagation)), t(o, e, i, r), o
                }
            },
            n = function(t, e) {
                return function() {
                    for (var n = L.DomUtil.create("div", e), i = 0; i < t.colors.length; i++) n.innerHTML += '<div class="outline"><i style="background:' + t.colors[i] + '"></i></div><div class="info-label">' + t.labels[i] + "</div>";
                    return L.Browser.touch ? L.DomEvent.on(n, "click", L.DomEvent.stopPropagation) : (L.DomEvent.disableClickPropagation(n), L.DomEvent.on(n, "mousewheel", L.DomEvent.stopPropagation)), n
                }
            };
        return {
            getOnAddLegend: e,
            getOnAddArrayLegend: n,
            updateLegend: t
        }
    }), angular.module("ui-leaflet").factory("leafletMapDefaults", ["$q", "leafletHelpers", function(t, e) {
        function n() {
            return {
                keyboard: !0,
                dragging: !0,
                worldCopyJump: !1,
                doubleClickZoom: !0,
                /* scrollWheelZoom: !0, Removed in order to get scrollWheelZoom working -- Rac021 */ 
                tap: !0,
                touchZoom: !0,
                zoomControl: !0,
                zoomsliderControl: !1,
                zoomControlPosition: "topleft",
                attributionControl: !0,
                controls: {
                    layers: {
                        visible: !0,
                        position: "topright",
                        collapsed: !0
                    }
                },
                nominatim: {
                    server: " http://nominatim.openstreetmap.org/search"
                },
                crs: L.CRS.EPSG3857,
                tileLayer: "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                tileLayerOptions: {
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                },
                path: {
                    weight: 10,
                    opacity: 1,
                    color: "#0000ff"
                },
                center: {
                    lat: 0,
                    lng: 0,
                    zoom: 1
                }
            }
        }
        var i = e.isDefined,
            r = e.isObject,
            o = e.obtainEffectiveMapId,
            a = {};
        return {
            reset: function() {
                a = {}
            },
            getDefaults: function(t) {
                var e = o(a, t);
                return a[e]
            },
            getMapCreationDefaults: function(t) {
                var e = o(a, t),
                    n = a[e],
                    r = {
                        maxZoom: n.maxZoom,
                        keyboard: n.keyboard,
                        dragging: n.dragging,
                        zoomControl: n.zoomControl,
                        doubleClickZoom: n.doubleClickZoom,
                        tap: n.tap,
                        touchZoom: n.touchZoom,
                        attributionControl: n.attributionControl,
                        worldCopyJump: n.worldCopyJump,
                        crs: n.crs
                    };
                if (i(n.minZoom) && (r.minZoom = n.minZoom), i(n.zoomAnimation) && (r.zoomAnimation = n.zoomAnimation), i(n.fadeAnimation) && (r.fadeAnimation = n.fadeAnimation), i(n.markerZoomAnimation) && (r.markerZoomAnimation = n.markerZoomAnimation), n.map)
                    for (var s in n.map) r[s] = n.map[s];
                return r
            },
            setDefaults: function(t, e) {
                var s = n();
                i(t) && (s.doubleClickZoom = i(t.doubleClickZoom) ? t.doubleClickZoom : s.doubleClickZoom, s.scrollWheelZoom = i(t.scrollWheelZoom) ? t.scrollWheelZoom : s.doubleClickZoom, s.tap = i(t.tap) ? t.tap : s.tap, s.touchZoom = i(t.touchZoom) ? t.touchZoom : s.doubleClickZoom, s.zoomControl = i(t.zoomControl) ? t.zoomControl : s.zoomControl, s.zoomsliderControl = i(t.zoomsliderControl) ? t.zoomsliderControl : s.zoomsliderControl, s.attributionControl = i(t.attributionControl) ? t.attributionControl : s.attributionControl, s.tileLayer = i(t.tileLayer) ? t.tileLayer : s.tileLayer, s.zoomControlPosition = i(t.zoomControlPosition) ? t.zoomControlPosition : s.zoomControlPosition, s.keyboard = i(t.keyboard) ? t.keyboard : s.keyboard, s.dragging = i(t.dragging) ? t.dragging : s.dragging, i(t.controls) && angular.extend(s.controls, t.controls), r(t.crs) ? s.crs = t.crs : i(L.CRS[t.crs]) && (s.crs = L.CRS[t.crs]), i(t.center) && angular.copy(t.center, s.center), i(t.tileLayerOptions) && angular.copy(t.tileLayerOptions, s.tileLayerOptions), i(t.maxZoom) && (s.maxZoom = t.maxZoom), i(t.minZoom) && (s.minZoom = t.minZoom), i(t.zoomAnimation) && (s.zoomAnimation = t.zoomAnimation), i(t.fadeAnimation) && (s.fadeAnimation = t.fadeAnimation), i(t.markerZoomAnimation) && (s.markerZoomAnimation = t.markerZoomAnimation), i(t.worldCopyJump) && (s.worldCopyJump = t.worldCopyJump), i(t.map) && (s.map = t.map), i(t.path) && (s.path = t.path));
                var l = o(a, e);
                return a[l] = s, s
            }
        }
    }]), angular.module("ui-leaflet").service("leafletMarkersHelpers", ["$rootScope", "$timeout", "leafletHelpers", "leafletLogger", "$compile", "leafletGeoJsonHelpers", function(t, e, n, i, r, o) {
        var a = n.isDefined,
            s = n.defaultTo,
            l = n.MarkerClusterPlugin,
            u = n.AwesomeMarkersPlugin,
            c = n.VectorMarkersPlugin,
            h = n.MakiMarkersPlugin,
            d = n.ExtraMarkersPlugin,
            f = n.DomMarkersPlugin,
            p = n.safeApply,
            m = n,
            g = n.isString,
            v = n.isNumber,
            y = n.isObject,
            _ = {},
            b = o,
            $ = n.errorHeader,
            w = i,
            x = function(t) {
                var e = "";
                return ["_icon", "_latlng", "_leaflet_id", "_map", "_shadow"].forEach(function(n) {
                    e += n + ": " + s(t[n], "undefined") + " \n"
                }), "[leafletMarker] : \n" + e
            },
            C = function(t, e) {
                var n = e ? console : w;
                n.debug(x(t))
            },
            S = function(e) {
                if (a(e) && a(e.type) && "awesomeMarker" === e.type) return u.isLoaded() || w.error($ + " The AwesomeMarkers Plugin is not loaded."), new L.AwesomeMarkers.icon(e);
                if (a(e) && a(e.type) && "vectorMarker" === e.type) return c.isLoaded()  || w.error($ + " The VectorMarkers Plugin is not loaded."), new L.VectorMarkers.icon(e);
                if (a(e) && a(e.type) && "makiMarker" === e.type) return h.isLoaded()    || w.error($ + " The MakiMarkers Plugin is not loaded."), new L.MakiMarkers.icon(e);
                if (a(e) && a(e.type) && "extraMarker" === e.type) return d.isLoaded()   || w.error($ + " The ExtraMarkers Plugin is not loaded."), new L.ExtraMarkers.icon(e);
                if (a(e) && a(e.type) && "div" === e.type) return new L.divIcon(e);
                if (a(e) && a(e.type) && "dom" === e.type) {
                    f.isLoaded() || w.error($ + "The DomMarkers Plugin is not loaded.");
                    var n = angular.isFunction(e.getMarkerScope) ? e.getMarkerScope() : t,
                        i = r(e.template)(n),
                        o = angular.copy(e);
                    return o.element = i[0], new L.DomMarkers.icon(o)
                }
                if (a(e) && a(e.type) && "icon" === e.type) return e.icon;
                var s = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAGmklEQVRYw7VXeUyTZxjvNnfELFuyIzOabermMZEeQC/OclkO49CpOHXOLJl/CAURuYbQi3KLgEhbrhZ1aDwmaoGqKII6odATmH/scDFbdC7LvFqOCc+e95s2VG50X/LLm/f4/Z7neY/ne18aANCmAr5E/xZf1uDOkTcGcWR6hl9247tT5U7Y6SNvWsKT63P58qbfeLJG8M5qcgTknrvvrdDbsT7Ml+tv82X6vVxJE33aRmgSyYtcWVMqX97Yv2JvW39UhRE2HuyBL+t+gK1116ly06EeWFNlAmHxlQE0OMiV6mQCScusKRlhS3QLeVJdl1+23h5dY4FNB3thrbYboqptEFlphTC1hSpJnbRvxP4NWgsE5Jyz86QNNi/5qSUTGuFk1gu54tN9wuK2wc3o+Wc13RCmsoBwEqzGcZsxsvCSy/9wJKf7UWf1mEY8JWfewc67UUoDbDjQC+FqK4QqLVMGGR9d2wurKzqBk3nqIT/9zLxRRjgZ9bqQgub+DdoeCC03Q8j+0QhFhBHR/eP3U/zCln7Uu+hihJ1+bBNffLIvmkyP0gpBZWYXhKussK6mBz5HT6M1Nqpcp+mBCPXosYQfrekGvrjewd59/GvKCE7TbK/04/ZV5QZYVWmDwH1mF3xa2Q3ra3DBC5vBT1oP7PTj4C0+CcL8c7C2CtejqhuCnuIQHaKHzvcRfZpnylFfXsYJx3pNLwhKzRAwAhEqG0SpusBHfAKkxw3w4627MPhoCH798z7s0ZnBJ/MEJbZSbXPhER2ih7p2ok/zSj2cEJDd4CAe+5WYnBCgR2uruyEw6zRoW6/DWJ/OeAP8pd/BGtzOZKpG8oke0SX6GMmRk6GFlyAc59K32OTEinILRJRchah8HQwND8N435Z9Z0FY1EqtxUg+0SO6RJ/mmXz4VuS+DpxXC3gXmZwIL7dBSH4zKE50wESf8qwVgrP1EIlTO5JP9Igu0aexdh28F1lmAEGJGfh7jE6ElyM5Rw/FDcYJjWhbeiBYoYNIpc2FT/SILivp0F1ipDWk4BIEo2VuodEJUifhbiltnNBIXPUFCMpthtAyqws/BPlEF/VbaIxErdxPphsU7rcCp8DohC+GvBIPJS/tW2jtvTmmAeuNO8BNOYQeG8G/2OzCJ3q+soYB5i6NhMaKr17FSal7GIHheuV3uSCY8qYVuEm1cOzqdWr7ku/R0BDoTT+DT+ohCM6/CCvKLKO4RI+dXPeAuaMqksaKrZ7L3FE5FIFbkIceeOZ2OcHO6wIhTkNo0ffgjRGxEqogXHYUPHfWAC/lADpwGcLRY3aeK4/oRGCKYcZXPVoeX/kelVYY8dUGf8V5EBRbgJXT5QIPhP9ePJi428JKOiEYhYXFBqou2Guh+p/mEB1/RfMw6rY7cxcjTrneI1FrDyuzUSRm9miwEJx8E/gUmqlyvHGkneiwErR21F3tNOK5Tf0yXaT+O7DgCvALTUBXdM4YhC/IawPU+2PduqMvuaR6eoxSwUk75ggqsYJ7VicsnwGIkZBSXKOUww73WGXyqP+J2/b9c+gi1YAg/xpwck3gJuucNrh5JvDPvQr0WFXf0piyt8f8/WI0hV4pRxxkQZdJDfDJNOAmM0Ag8jyT6hz0WGXWuP94Yh2jcfjmXAGvHCMslRimDHYuHuDsy2QtHuIavznhbYURq5R57KpzBBRZKPJi8eQg48h4j8SDdowifdIrEVdU+gbO6QNvRRt4ZBthUaZhUnjlYObNagV3keoeru3rU7rcuceqU1mJBxy+BWZYlNEBH+0eH4vRiB+OYybU2hnblYlTvkHinM4m54YnxSyaZYSF6R3jwgP7udKLGIX6r/lbNa9N6y5MFynjWDtrHd75ZvTYAPO/6RgF0k76mQla3FGq7dO+cH8sKn0Vo7nDllwAhqwLPkxrHwWmHJOo+AKJ4rab5OgrM7rVu8eWb2Pu0Dh4eDgXoOfvp7Y7QeqknRmvcTBEyq9m/HQQSCSz6LHq3z0yzsNySRfMS253wl2KyRDbcZPcfJKjZmSEOjcxyi+Y8dUOtsIEH6R2wNykdqrkYJ0RV92H0W58pkfQk7cKevsLK10Py8SdMGfXNXATY+pPbyJR/ET6n9nIfztNtZYRV9XniQu9IA2vOVgy4ir7GCLVmmd+zjkH0eAF9Po6K61pmCXHxU5rHMYd1ftc3owjwRSVRzLjKvqZEty6cRUD7jGqiOdu5HG6MdHjNcNYGqfDm5YRzLBBCCDl/2bk8a8gdbqcfwECu62Fg/HrggAAAABJRU5ErkJggg==",
                    l = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApCAYAAACoYAD2AAAC5ElEQVRYw+2YW4/TMBCF45S0S1luXZCABy5CgLQgwf//S4BYBLTdJLax0fFqmB07nnQfEGqkIydpVH85M+NLjPe++dcPc4Q8Qh4hj5D/AaQJx6H/4TMwB0PeBNwU7EGQAmAtsNfAzoZkgIa0ZgLMa4Aj6CxIAsjhjOCoL5z7Glg1JAOkaicgvQBXuncwJAWjksLtBTWZe04CnYRktUGdilALppZBOgHGZcBzL6OClABvMSVIzyBjazOgrvACf1ydC5mguqAVg6RhdkSWQFj2uxfaq/BrIZOLEWgZdALIDvcMcZLD8ZbLC9de4yR1sYMi4G20S4Q/PWeJYxTOZn5zJXANZHIxAd4JWhPIloTJZhzMQduM89WQ3MUVAE/RnhAXpTycqys3NZALOBbB7kFrgLesQl2h45Fcj8L1tTSohUwuxhy8H/Qg6K7gIs+3kkaigQCOcyEXCHN07wyQazhrmIulvKMQAwMcmLNqyCVyMAI+BuxSMeTk3OPikLY2J1uE+VHQk6ANrhds+tNARqBeaGc72cK550FP4WhXmFmcMGhTwAR1ifOe3EvPqIegFmF+C8gVy0OfAaWQPMR7gF1OQKqGoBjq90HPMP01BUjPOqGFksC4emE48tWQAH0YmvOgF3DST6xieJgHAWxPAHMuNhrImIdvoNOKNWIOcE+UXE0pYAnkX6uhWsgVXDxHdTfCmrEEmMB2zMFimLVOtiiajxiGWrbU52EeCdyOwPEQD8LqyPH9Ti2kgYMf4OhSKB7qYILbBv3CuVTJ11Y80oaseiMWOONc/Y7kJYe0xL2f0BaiFTxknHO5HaMGMublKwxFGzYdWsBF174H/QDknhTHmHHN39iWFnkZx8lPyM8WHfYELmlLKtgWNmFNzQcC1b47gJ4hL19i7o65dhH0Negbca8vONZoP7doIeOC9zXm8RjuL0Gf4d4OYaU5ljo3GYiqzrWQHfJxA6ALhDpVKv9qYeZA8eM3EhfPSCmpuD0AAAAASUVORK5CYII=";
                return a(e) && a(e.iconUrl) ? new L.Icon(e) : new L.Icon.Default({
                    iconUrl: s,
                    shadowUrl: l,
                    iconSize: [25, 41],
                    iconAnchor: [12, 41],
                    popupAnchor: [1, -34],
                    shadowSize: [41, 41]
                })
            },
            E = function(t) {
                a(_[t]) && _.splice(t, 1)
            },
            k = function() {
                _ = {}
            },
            P = function(t, e, n) {
                if (t.closePopup(), a(n) && a(n.overlays))
                    for (var i in n.overlays)
                        if ((n.overlays[i] instanceof L.LayerGroup || n.overlays[i] instanceof L.FeatureGroup) && n.overlays[i].hasLayer(t)) return void n.overlays[i].removeLayer(t);
                if (a(_))
                    for (var r in _) _[r].hasLayer(t) && _[r].removeLayer(t);
                e.hasLayer(t) && e.removeLayer(t)
            },
            T = function(t, e) {
                var n = t._popup._container.offsetHeight,
                    i = new L.Point(t._popup._containerLeft, -n - t._popup._containerBottom),
                    r = e.layerPointToContainerPoint(i);
                null !== r && t._popup._adjustPan()
            },
            M = function(t, e) {
                r(t._popup._contentNode)(e)
            },
            D = function(t, n, i) {
                var r = t._popup._contentNode.innerText || t._popup._contentNode.textContent;
                r.length < 1 && e(function() {
                    D(t, n, i)
                });
                var o = t._popup._contentNode.offsetWidth;
                return t._popup._updateLayout(), t._popup._updatePosition(), t._popup.options.autoPan && T(t, i), o
            },
            O = function(e, n, i) {
                var r = angular.isFunction(n.getMessageScope) ? n.getMessageScope() : t,
                    o = a(n.compileMessage) ? n.compileMessage : !0;
                if (o) {
                    if (!a(e._popup) || !a(e._popup._contentNode)) return w.error($ + "Popup is invalid or does not have any content."), !1;
                    M(e, r), D(e, n, i)
                }
            },
            A = function(e, n) {
                var i = angular.isFunction(n.getMessageScope) ? n.getMessageScope() : t,
                    o = angular.isFunction(n.getLabelScope) ? n.getLabelScope() : i,
                    s = a(n.compileMessage) ? n.compileMessage : !0;
                m.LabelPlugin.isLoaded() && a(n.label) && (a(n.label.options) && n.label.options.noHide === !0 && e.showLabel(), s && a(e.label) && r(e.label._container)(o))
            },
            I = function(t, e, n, i, r, o, s) {
                if (a(e)) {
                    if (!b.validateCoords(t)) return w.warn("There are problems with lat-lng data, please verify your marker model"), void P(n, s, o);
                    var l = t === e;
                    if (a(t.iconAngle) && e.iconAngle !== t.iconAngle && n.setIconAngle(t.iconAngle), g(t.layer) || g(e.layer) && (a(o.overlays[e.layer]) && o.overlays[e.layer].hasLayer(n) && (o.overlays[e.layer].removeLayer(n), n.closePopup()), s.hasLayer(n) || s.addLayer(n)), (v(t.opacity) || v(parseFloat(t.opacity))) && t.opacity !== e.opacity && n.setOpacity(t.opacity), g(t.layer) && e.layer !== t.layer) {
                        if (g(e.layer) && a(o.overlays[e.layer]) && o.overlays[e.layer].hasLayer(n) && o.overlays[e.layer].removeLayer(n), n.closePopup(), s.hasLayer(n) && s.removeLayer(n), !a(o.overlays[t.layer])) return void w.error($ + "You must use a name of an existing layer");
                        var u = o.overlays[t.layer];
                        if (!(u instanceof L.LayerGroup || u instanceof L.FeatureGroup)) return void w.error($ + 'A marker can only be added to a layer of type "group" or "featureGroup"');
                        u.addLayer(n), s.hasLayer(n) && t.focus === !0 && n.openPopup()
                    }
                    if (t.draggable !== !0 && e.draggable === !0 && a(n.dragging) && n.dragging.disable(), t.draggable === !0 && e.draggable !== !0 && (n.dragging ? n.dragging.enable() : L.Handler.MarkerDrag && (n.dragging = new L.Handler.MarkerDrag(n), n.options.draggable = !0, n.dragging.enable())), y(t.icon) || y(e.icon) && (n.setIcon(S()), n.closePopup(), n.unbindPopup(), g(t.message) && n.bindPopup(t.message, t.popupOptions)), y(t.icon) && y(e.icon) && !angular.equals(t.icon, e.icon)) {
                        var c = !1;
                        n.dragging && (c = n.dragging.enabled()), n.setIcon(S(t.icon)), c && n.dragging.enable(), n.closePopup(), n.unbindPopup(), g(t.message) && (n.bindPopup(t.message, t.popupOptions), s.hasLayer(n) && t.focus === !0 && n.openPopup())
                    }!g(t.message) && g(e.message) && (n.closePopup(), n.unbindPopup()), m.LabelPlugin.isLoaded() && (a(t.label) && a(t.label.message) ? "label" in e && "message" in e.label && !angular.equals(t.label.message, e.label.message) ? n.updateLabelContent(t.label.message) : !angular.isFunction(n.getLabel) || angular.isFunction(n.getLabel) && !a(n.getLabel()) ? (n.bindLabel(t.label.message, t.label.options), A(n, t)) : A(n, t) : (!("label" in t) || "message" in t.label) && angular.isFunction(n.unbindLabel) && n.unbindLabel()), g(t.message) && !g(e.message) && n.bindPopup(t.message, t.popupOptions), g(t.message) && g(e.message) && t.message !== e.message && n.setPopupContent(t.message);
                    var h = !1;
                    t.focus !== !0 && e.focus === !0 && (n.closePopup(), h = !0), (t.focus === !0 && (!a(e.focus) || e.focus === !1) || l && t.focus === !0) && (n.openPopup(), h = !0), e.zIndexOffset !== t.zIndexOffset && n.setZIndexOffset(t.zIndexOffset);
                    var d = n.getLatLng(),
                        f = g(t.layer) && m.MarkerClusterPlugin.is(o.overlays[t.layer]);
                    f ? h ? (t.lat !== e.lat || t.lng !== e.lng) && (o.overlays[t.layer].removeLayer(n), n.setLatLng([t.lat, t.lng]), o.overlays[t.layer].addLayer(n)) : d.lat !== t.lat || d.lng !== t.lng ? (o.overlays[t.layer].removeLayer(n), n.setLatLng([t.lat, t.lng]), o.overlays[t.layer].addLayer(n)) : t.lat !== e.lat || t.lng !== e.lng ? (o.overlays[t.layer].removeLayer(n), n.setLatLng([t.lat, t.lng]), o.overlays[t.layer].addLayer(n)) : y(t.icon) && y(e.icon) && !angular.equals(t.icon, e.icon) && (o.overlays[t.layer].removeLayer(n), o.overlays[t.layer].addLayer(n)) : (d.lat !== t.lat || d.lng !== t.lng) && n.setLatLng([t.lat, t.lng])
                }
            };
        return {
            resetMarkerGroup: E,
            resetMarkerGroups: k,
            deleteMarker: P,
            manageOpenPopup: O,
            manageOpenLabel: A,
            createMarker: function(t) {
                if (!a(t) || !b.validateCoords(t)) return void w.error($ + "The marker definition is not valid.");
                var e = b.getCoords(t);
                if (!a(e)) return void w.error($ + "Unable to get coordinates from markerData.");
                var n = {
                    icon: S(t.icon),
                    title: a(t.title) ? t.title : "",
                    draggable: a(t.draggable) ? t.draggable : !1,
                    clickable: a(t.clickable) ? t.clickable : !0,
                    riseOnHover: a(t.riseOnHover) ? t.riseOnHover : !1,
                    zIndexOffset: a(t.zIndexOffset) ? t.zIndexOffset : 0,
                    iconAngle: a(t.iconAngle) ? t.iconAngle : 0
                };
                for (var i in t) t.hasOwnProperty(i) && !n.hasOwnProperty(i) && (n[i] = t[i]);
                var r = new L.marker(e, n);
                return g(t.message) || r.unbindPopup(), r
            },
            addMarkerToGroup: function(t, e, n, i) {
                return g(e) ? l.isLoaded() ? (a(_[e]) || (_[e] = new L.MarkerClusterGroup(n), i.addLayer(_[e])), void _[e].addLayer(t)) : void w.error($ + "The MarkerCluster plugin is not loaded.") : void w.error($ + "The marker group you have specified is invalid.")
            },
            listenMarkerEvents: function(t, e, n, i, r) {
                t.on("popupopen", function() {
                    p(n, function() {
                        (a(t._popup) || a(t._popup._contentNode)) && (e.focus = !0, O(t, e, r))
                    })
                }), t.on("popupclose", function() {
                    p(n, function() {
                        e.focus = !1
                    })
                }), t.on("add", function() {
                    p(n, function() {
                        "label" in e && A(t, e)
                    })
                })
            },
            updateMarker: I,
            addMarkerWatcher: function(t, e, n, i, r, o) {
                var l = m.getObjectArrayPath("markers." + e);
                o = s(o, !0);
                var u = n.$watch(l, function(o, s) {
                    return a(o) ? void I(o, s, t, e, n, i, r) : (P(t, r, i), void u())
                }, o)
            },
            string: x,
            log: C
        }
    }]), angular.module("ui-leaflet").factory("leafletPathsHelpers", ["$rootScope", "leafletLogger", "leafletHelpers", function(t, e, n) {
        function i(t) {
            return t.filter(function(t) {
                return c(t)
            }).map(function(t) {
                return r(t)
            })
        }

        function r(t) {
            return l(t) ? new L.LatLng(t[0], t[1]) : new L.LatLng(t.lat, t.lng)
        }

        function o(t) {
            return t.map(function(t) {
                return i(t)
            })
        }

        function a(t, e) {
            for (var n = {}, i = 0; i < d.length; i++) {
                var r = d[i];
                s(t[r]) ? n[r] = t[r] : s(e.path[r]) && (n[r] = e.path[r])
            }
            return n
        }
        var s = n.isDefined,
            l = n.isArray,
            u = n.isNumber,
            c = n.isValidPoint,
            h = e,
            d = ["stroke", "weight", "color", "opacity", "fill", "fillColor", "fillOpacity", "dashArray", "lineCap", "lineJoin", "clickable", "pointerEvents", "className", "smoothFactor", "noClip"],
            f = function(t, e) {
                for (var n = {}, i = 0; i < d.length; i++) {
                    var r = d[i];
                    s(e[r]) && (n[r] = e[r])
                }
                t.setStyle(e)
            },
            p = function(t) {
                if (!l(t)) return !1;
                for (var e = 0; e < t.length; e++) {
                    var n = t[e];
                    if (!c(n)) return !1
                }
                return !0
            },
            m = {
                polyline: {
                    isValid: function(t) {
                        var e = t.latlngs;
                        return p(e)
                    },
                    createPath: function(t) {
                        return new L.Polyline([], t)
                    },
                    setPath: function(t, e) {
                        t.setLatLngs(i(e.latlngs)), f(t, e)
                    }
                },
                multiPolyline: {
                    isValid: function(t) {
                        var e = t.latlngs;
                        if (!l(e)) return !1;
                        for (var n in e) {
                            var i = e[n];
                            if (!p(i)) return !1
                        }
                        return !0
                    },
                    createPath: function(t) {
                        return new L.multiPolyline([
                            [
                                [0, 0],
                                [1, 1]
                            ]
                        ], t)
                    },
                    setPath: function(t, e) {
                        t.setLatLngs(o(e.latlngs)), f(t, e)
                    }
                },
                polygon: {
                    isValid: function(t) {
                        var e = t.latlngs;
                        return p(e)
                    },
                    createPath: function(t) {
                        return new L.Polygon([], t)
                    },
                    setPath: function(t, e) {
                        t.setLatLngs(i(e.latlngs)), f(t, e)
                    }
                },
                multiPolygon: {
                    isValid: function(t) {
                        var e = t.latlngs;
                        if (!l(e)) return !1;
                        for (var n in e) {
                            var i = e[n];
                            if (!p(i)) return !1
                        }
                        return !0
                    },
                    createPath: function(t) {
                        return new L.MultiPolygon([
                            [
                                [0, 0],
                                [1, 1],
                                [0, 1]
                            ]
                        ], t)
                    },
                    setPath: function(t, e) {
                        t.setLatLngs(o(e.latlngs)), f(t, e)
                    }
                },
                rectangle: {
                    isValid: function(t) {
                        var e = t.latlngs;
                        if (!l(e) || 2 !== e.length) return !1;
                        for (var n in e) {
                            var i = e[n];
                            if (!c(i)) return !1
                        }
                        return !0
                    },
                    createPath: function(t) {
                        return new L.Rectangle([
                            [0, 0],
                            [1, 1]
                        ], t)
                    },
                    setPath: function(t, e) {
                        t.setBounds(new L.LatLngBounds(i(e.latlngs))), f(t, e)
                    }
                },
                circle: {
                    isValid: function(t) {
                        var e = t.latlngs;
                        return c(e) && u(t.radius)
                    },
                    createPath: function(t) {
                        return new L.Circle([0, 0], 1, t)
                    },
                    setPath: function(t, e) {
                        t.setLatLng(r(e.latlngs)), s(e.radius) && t.setRadius(e.radius), f(t, e)
                    }
                },
                circleMarker: {
                    isValid: function(t) {
                        var e = t.latlngs;
                        return c(e) && u(t.radius)
                    },
                    createPath: function(t) {
                        return new L.CircleMarker([0, 0], t)
                    },
                    setPath: function(t, e) {
                        t.setLatLng(r(e.latlngs)), s(e.radius) && t.setRadius(e.radius), f(t, e)
                    }
                }
            },
            g = function(t) {
                var e = {};
                return t.latlngs && (e.latlngs = t.latlngs), t.radius && (e.radius = t.radius), e
            };
        return {
            setPathOptions: function(t, e, n) {
                s(e) || (e = "polyline"), m[e].setPath(t, n)
            },
            createPath: function(t, e, n) {
                s(e.type) || (e.type = "polyline");
                var i = a(e, n),
                    r = g(e);
                return m[e.type].isValid(r) ? m[e.type].createPath(i) : void h.error("[AngularJS - Leaflet] Invalid data passed to the " + e.type + " path")
            }
        }
    }]), angular.module("ui-leaflet").service("leafletWatchHelpers", function() {
        var t = function(t, e, n, i, r) {
                var o = t[e](n, function(t, e) {
                    r(t, e), i.doWatch || o()
                }, i.isDeep);
                return o
            },
            e = function(e, n, i, r) {
                return t(e, "$watch", n, i, r)
            },
            n = function(e, n, i, r) {
                return t(e, "$watchCollection", n, i, r)
            };
        return {
            maybeWatch: e,
            maybeWatchCollection: n
        }
    }), angular.module("ui-leaflet").service("leafletLogger", ["nemSimpleLogger", function(t) {
        return t.spawn()
    }]), angular.module("ui-leaflet").factory("nominatimService", ["$q", "$http", "leafletHelpers", "leafletMapDefaults", function(t, e, n, i) {
        var r = n.isDefined;
        return {
            query: function(n, o) {
                var a = i.getDefaults(o),
                    s = a.nominatim.server,
                    l = t.defer();
                return e.get(s, {
                    params: {
                        format: "json",
                        limit: 1,
                        q: n
                    }
                }).success(function(t) {
                    t.length > 0 && r(t[0].boundingbox) ? l.resolve(t[0]) : l.reject("[Nominatim] Invalid address")
                }), l.promise
            }
        }
    }]), angular.module("ui-leaflet").directive("bounds", ["leafletLogger", "$timeout", "$http", "leafletHelpers", "nominatimService", "leafletBoundsHelpers", function(t, e, n, i, r, o) {
        var a = t;
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: ["leaflet"],
            link: function(t, n, s, l) {
                var u = i.isDefined,
                    c = o.createLeafletBounds,
                    h = l[0].getLeafletScope(),
                    d = l[0],
                    f = i.errorHeader + " [Bounds] ",
                    p = function(t) {
                        return 0 === t._southWest.lat && 0 === t._southWest.lng && 0 === t._northEast.lat && 0 === t._northEast.lng
                    };
                d.getMap().then(function(n) {
                    h.$on("boundsChanged", function(t) {
                        var i = t.currentScope,
                            r = n.getBounds();
                        if (!p(r) && !i.settingBoundsFromScope) {
                            i.settingBoundsFromLeaflet = !0;
                            var o = {
                                northEast: {
                                    lat: r._northEast.lat,
                                    lng: r._northEast.lng
                                },
                                southWest: {
                                    lat: r._southWest.lat,
                                    lng: r._southWest.lng
                                },
                                options: r.options
                            };
                            angular.equals(i.bounds, o) || (i.bounds = o), e(function() {
                                i.settingBoundsFromLeaflet = !1
                            })
                        }
                    });
                    var i;
                    h.$watch("bounds", function(o) {
                        if (!t.settingBoundsFromLeaflet) {
                            if (u(o.address) && o.address !== i) return t.settingBoundsFromScope = !0, r.query(o.address, s.id).then(function(t) {
                                var e = t.boundingbox,
                                    i = [
                                        [e[0], e[2]],
                                        [e[1], e[3]]
                                    ];
                                n.fitBounds(i)
                            }, function(t) {
                                a.error(f + " " + t + ".")
                            }), i = o.address, void e(function() {
                                t.settingBoundsFromScope = !1
                            });
                            var l = c(o);
                            l && !n.getBounds().equals(l) && (t.settingBoundsFromScope = !0, n.fitBounds(l, o.options), e(function() {
                                t.settingBoundsFromScope = !1
                            }))
                        }
                    }, !0)
                })
            }
        }
    }]);
    var centerDirectiveTypes = ["center", "lfCenter"],
        centerDirectives = {};
    centerDirectiveTypes.forEach(function(t) {
        centerDirectives[t] = ["leafletLogger", "$q", "$location", "$timeout", "leafletMapDefaults", "leafletHelpers", "leafletBoundsHelpers", "leafletMapEvents", function(e, n, i, r, o, a, s, l) {
            var u, c = a.isDefined,
                h = a.isNumber,
                d = a.isSameCenterOnMap,
                f = a.safeApply,
                p = a.isValidCenter,
                m = s.isValidBounds,
                g = a.isUndefinedOrEmpty,
                v = a.errorHeader,
                y = e,
                _ = function(t, e) {
                    return c(t) && m(t) && g(e)
                };
            return {
                restrict: "A",
                scope: !1,
                replace: !1,
                require: "leaflet",
                controller: function() {
                    u = n.defer(), this.getCenter = function() {
                        return u.promise
                    }
                },
                link: function(e, n, a, m) {
                    var g = m.getLeafletScope(),
                        b = g[t];
                    m.getMap().then(function(e) {
                        var n = o.getDefaults(a.id);
                        if (-1 !== a[t].search("-")) return y.error(v + ' The "center" variable can\'t use a "-" on its key name: "' + a[t] + '".'), void e.setView([n.center.lat, n.center.lng], n.center.zoom);
                        if (_(g.bounds, b)) e.fitBounds(s.createLeafletBounds(g.bounds), g.bounds.options), b = e.getCenter(), f(g, function(n) {
                            angular.extend(n[t], {
                                lat: e.getCenter().lat,
                                lng: e.getCenter().lng,
                                zoom: e.getZoom(),
                                autoDiscover: !1
                            })
                        }), f(g, function(t) {
                            var n = e.getBounds();
                            t.bounds = {
                                northEast: {
                                    lat: n._northEast.lat,
                                    lng: n._northEast.lng
                                },
                                southWest: {
                                    lat: n._southWest.lat,
                                    lng: n._southWest.lng
                                }
                            }
                        });
                        else {
                            if (!c(b)) return y.error(v + ' The "center" property is not defined in the main scope'), void e.setView([n.center.lat, n.center.lng], n.center.zoom);
                            c(b.lat) && c(b.lng) || c(b.autoDiscover) || angular.copy(n.center, b)
                        }
                        var m, $;
                        if ("yes" === a.urlHashCenter) {
                            var w = function() {
                                var t, e = i.search();
                                if (c(e.c)) {
                                    var n = e.c.split(":");
                                    3 === n.length && (t = {
                                        lat: parseFloat(n[0]),
                                        lng: parseFloat(n[1]),
                                        zoom: parseInt(n[2], 10)
                                    })
                                }
                                return t
                            };
                            m = w(), g.$on("$locationChangeSuccess", function(n) {
                                var i = n.currentScope,
                                    r = w();
                                c(r) && !d(r, e) && angular.extend(i[t], {
                                    lat: r.lat,
                                    lng: r.lng,
                                    zoom: r.zoom
                                })
                            })
                        }
                        g.$watch(t, function(t) {
                            return g.settingCenterFromLeaflet ? void 0 : (c(m) && (angular.copy(m, t), m = void 0), p(t) || t.autoDiscover === !0 ? t.autoDiscover === !0 ? (h(t.zoom) || e.setView([n.center.lat, n.center.lng], n.center.zoom), void e.locate(h(t.zoom) && t.zoom > n.center.zoom ? {
                                setView: !0,
                                maxZoom: t.zoom
                            } : c(n.maxZoom) ? {
                                setView: !0,
                                maxZoom: n.maxZoom
                            } : {
                                setView: !0
                            })) : void($ && d(t, e) || (g.settingCenterFromScope = !0, e.setView([t.lat, t.lng], t.zoom), l.notifyCenterChangedToBounds(g, e), r(function() {
                                g.settingCenterFromScope = !1
                            }))) : void y.warn(v + " invalid 'center'"))
                        }, !0), e.whenReady(function() {
                            $ = !0
                        }), e.on("moveend", function() {
                            u.resolve(), l.notifyCenterUrlHashChanged(g, e, a, i.search()), d(b, e) || g.settingCenterFromScope || (g.settingCenterFromLeaflet = !0, f(g, function(n) {
                                g.settingCenterFromScope || angular.extend(n[t], {
                                    lat: e.getCenter().lat,
                                    lng: e.getCenter().lng,
                                    zoom: e.getZoom(),
                                    autoDiscover: !1
                                }), l.notifyCenterChangedToBounds(g, e), r(function() {
                                    g.settingCenterFromLeaflet = !1
                                })
                            }))
                        }), b.autoDiscover === !0 && e.on("locationerror", function() {
                            y.warn(v + " The Geolocation API is unauthorized on this page."), p(b) ? (e.setView([b.lat, b.lng], b.zoom), l.notifyCenterChangedToBounds(g, e)) : (e.setView([n.center.lat, n.center.lng], n.center.zoom), l.notifyCenterChangedToBounds(g, e))
                        })
                    })
                }
            }
        }]
    }), centerDirectiveTypes.forEach(function(t) {
        angular.module("ui-leaflet").directive(t, centerDirectives[t])
    }), angular.module("ui-leaflet").directive("controls", ["leafletLogger", "leafletHelpers", "leafletControlHelpers", function(t, e, n) {
        var i = t;
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: "?^leaflet",
            link: function(t, r, o, a) {
                if (a) {
                    var s = n.createControl,
                        l = n.isValidControlType,
                        u = a.getLeafletScope(),
                        c = e.isDefined,
                        h = e.isArray,
                        d = {},
                        f = e.errorHeader + " [Controls] ";
                    a.getMap().then(function(t) {
                        u.$watchCollection("controls", function(e) {
                            for (var n in d) c(e[n]) || (t.hasControl(d[n]) && t.removeControl(d[n]), delete d[n]);
                            for (var r in e) {
                                var o, a = c(e[r].type) ? e[r].type : r;
                                if (!l(a)) return void i.error(f + " Invalid control type: " + a + ".");
                                if ("custom" !== a) o = s(a, e[r]), t.addControl(o), d[r] = o;
                                else {
                                    var u = e[r];
                                    if (h(u))
                                        for (var p in u) {
                                            var m = u[p];
                                            t.addControl(m), d[r] = c(d[r]) ? d[r].concat([m]) : [m]
                                        } else t.addControl(u), d[r] = u
                                }
                            }
                        })
                    })
                }
            }
        }
    }]), angular.module("ui-leaflet").directive("decorations", ["leafletLogger", "leafletHelpers", function(t, e) {
        var n = t;
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: "leaflet",
            link: function(t, i, r, o) {
                function a(t) {
                    return c(t) && c(t.coordinates) && (u.isLoaded() || n.error("[AngularJS - Leaflet] The PolylineDecorator Plugin is not loaded.")), L.polylineDecorator(t.coordinates)
                }

                function s(t, e) {
                    return c(t) && c(e) && c(e.coordinates) && c(e.patterns) ? (t.setPaths(e.coordinates), t.setPatterns(e.patterns), t) : void 0
                }
                var l = o.getLeafletScope(),
                    u = e.PolylineDecoratorPlugin,
                    c = e.isDefined,
                    h = {};
                o.getMap().then(function(t) {
                    l.$watch("decorations", function(e) {
                        for (var n in h) c(e[n]) && angular.equals(e[n], h) || (t.removeLayer(h[n]), delete h[n]);
                        for (var i in e) {
                            var r = e[i],
                                o = a(r);
                            c(o) && (h[i] = o, t.addLayer(o), s(o, r))
                        }
                    }, !0)
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("eventBroadcast", ["leafletLogger", "$rootScope", "leafletHelpers", "leafletMapEvents", "leafletIterators", function(t, e, n, i, r) {
        var o = t;
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: "leaflet",
            link: function(t, e, a, s) {
                var l = n.isObject,
                    u = n.isDefined,
                    c = s.getLeafletScope(),
                    h = c.eventBroadcast,
                    d = i.getAvailableMapEvents(),
                    f = i.addEvents;
                s.getMap().then(function(t) {
                    var e = [],
                        n = "broadcast";
                    u(h.map) ? l(h.map) ? ("emit" !== h.map.logic && "broadcast" !== h.map.logic ? o.warn("[AngularJS - Leaflet] Available event propagation logic are: 'emit' or 'broadcast'.") : n = h.map.logic, l(h.map.enable) && h.map.enable.length >= 0 ? r.each(h.map.enable, function(t) {
                        -1 === e.indexOf(t) && -1 !== d.indexOf(t) && e.push(t)
                    }) : o.warn("[AngularJS - Leaflet] event-broadcast.map.enable must be an object check your model.")) : o.warn("[AngularJS - Leaflet] event-broadcast.map must be an object check your model.") : e = d, f(t, e, "eventName", c, n)
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("geojson", ["leafletLogger", "$rootScope", "leafletData", "leafletHelpers", "leafletWatchHelpers", "leafletDirectiveControlsHelpers", "leafletIterators", "leafletGeoJsonEvents", function(t, e, n, i, r, o, a, s) {
        var l = r.maybeWatch,
            u = i.watchOptions,
            c = o.extend,
            h = i,
            d = a;
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: "leaflet",
            link: function(t, e, r, o) {
                var a = i.isDefined,
                    f = o.getLeafletScope(),
                    p = {},
                    m = !1;
                o.getMap().then(function(t) {
                    var e = f.geojsonWatchOptions || u,
                        o = function(t, e) {
                            var n;
                            return n = angular.isFunction(t.onEachFeature) ? t.onEachFeature : function(n, o) {
                                i.LabelPlugin.isLoaded() && a(n.properties.description) && o.bindLabel(n.properties.description), s.bindEvents(r.id, o, null, n, f, e, {
                                    resetStyleOnMouseout: t.resetStyleOnMouseout,
                                    mapId: r.id
                                })
                            }
                        },
                        g = h.isDefined(r.geojsonNested) && h.isTruthy(r.geojsonNested),
                        v = function() {
                            if (p) {
                                var e = function(e) {
                                    a(e) && t.hasLayer(e) && t.removeLayer(e)
                                };
                                return g ? void d.each(p, function(t) {
                                    e(t)
                                }) : void e(p)
                            }
                        },
                        y = function(e, i) {
                            var s = angular.copy(e);
                            if (a(s) && a(s.data)) {
                                var l = o(s, i);
                                a(s.options) || (s.options = {
                                    style: s.style,
                                    filter: s.filter,
                                    onEachFeature: l,
                                    pointToLayer: s.pointToLayer
                                });
                                var u = L.geoJson(s.data, s.options);
                                i && h.isString(i) ? p[i] = u : p = u, u.addTo(t), m || (m = !0, n.setGeoJSON(p, r.id))
                            }
                        },
                        _ = function(t) {
                            if (v(), g) {
                                if (!t || !Object.keys(t).length) return;
                                return void d.each(t, function(t, e) {
                                    y(t, e)
                                })
                            }
                            y(t)
                        };
                    c(r.id, "geojson", _, v), l(f, "geojson", e, function(t) {
                        _(t)
                    })
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("layercontrol", ["$filter", "leafletLogger", "leafletData", "leafletHelpers", function(t, e, n, i) {
        var r = e;
        return {
            restrict: "E",
            scope: {
                icons: "=?",
                autoHideOpacity: "=?",
                showGroups: "=?",
                title: "@",
                baseTitle: "@",
                overlaysTitle: "@"
            },
            replace: !0,
            transclude: !1,
            require: "^leaflet",
            controller: ["$scope", "$element", "$sce", function(t, e, o) {
                r.debug("[Angular Directive - Layers] layers", t, e);
                var a = i.safeApply,
                    s = i.isDefined;
                angular.extend(t, {
                    baselayer: "",
                    oldGroup: "",
                    layerProperties: {},
                    groupProperties: {},
                    rangeIsSupported: i.rangeIsSupported(),
                    changeBaseLayer: function(e, r) {
                        i.safeApply(t, function(i) {
                            i.baselayer = e, n.getMap().then(function(r) {
                                n.getLayers().then(function(n) {
                                    if (!r.hasLayer(n.baselayers[e])) {
                                        for (var o in i.layers.baselayers) i.layers.baselayers[o].icon = i.icons.unradio, r.hasLayer(n.baselayers[o]) && r.removeLayer(n.baselayers[o]);
                                        r.addLayer(n.baselayers[e]), i.layers.baselayers[e].icon = t.icons.radio
                                    }
                                })
                            })
                        }), r.preventDefault()
                    },
                    moveLayer: function(e, n, i) {
                        var r = Object.keys(t.layers.baselayers).length;
                        if (n >= 1 + r && n <= t.overlaysArray.length + r) {
                            var o;
                            for (var s in t.layers.overlays)
                                if (t.layers.overlays[s].index === n) {
                                    o = t.layers.overlays[s];
                                    break
                                } o && a(t, function() {
                                o.index = e.index, e.index = n
                            })
                        }
                        i.stopPropagation(), i.preventDefault()
                    },
                    initIndex: function(e, n) {
                        var i = Object.keys(t.layers.baselayers).length;
                        e.index = s(e.index) ? e.index : n + i + 1
                    },
                    initGroup: function(e) {
                        t.groupProperties[e] = t.groupProperties[e] ? t.groupProperties[e] : {}
                    },
                    toggleOpacity: function(e, n) {
                        if (n.visible) {
                            if (t.autoHideOpacity && !t.layerProperties[n.name].opacityControl)
                                for (var i in t.layerProperties) t.layerProperties[i].opacityControl = !1;
                            t.layerProperties[n.name].opacityControl = !t.layerProperties[n.name].opacityControl
                        }
                        e.stopPropagation(), e.preventDefault()
                    },
                    toggleLegend: function(e) {
                        t.layerProperties[e.name].showLegend = !t.layerProperties[e.name].showLegend
                    },
                    showLegend: function(e) {
                        return e.legend && t.layerProperties[e.name].showLegend
                    },
                    unsafeHTML: function(t) {
                        return o.trustAsHtml(t)
                    },
                    getOpacityIcon: function(e) {
                        return e.visible && t.layerProperties[e.name].opacityControl ? t.icons.close : t.icons.open
                    },
                    getGroupIcon: function(e) {
                        return e.visible ? t.icons.check : t.icons.uncheck
                    },
                    changeOpacity: function(e) {
                        var i = t.layerProperties[e.name].opacity;
                        n.getMap().then(function(r) {
                            n.getLayers().then(function(n) {
                                var o;
                                for (var a in t.layers.overlays)
                                    if (t.layers.overlays[a] === e) {
                                        o = n.overlays[a];
                                        break
                                    } r.hasLayer(o) && (o.setOpacity && o.setOpacity(i / 100), o.getLayers && o.eachLayer && o.eachLayer(function(t) {
                                    t.setOpacity && t.setOpacity(i / 100)
                                }))
                            })
                        })
                    },
                    changeGroupVisibility: function(e) {
                        if (s(t.groupProperties[e])) {
                            var n = t.groupProperties[e].visible;
                            for (var i in t.layers.overlays) {
                                var r = t.layers.overlays[i];
                                r.group === e && (r.visible = n)
                            }
                        }
                    }
                });
                var l = e.get(0);
                L.Browser.touch ? L.DomEvent.on(l, "click", L.DomEvent.stopPropagation) : (L.DomEvent.disableClickPropagation(l), L.DomEvent.on(l, "mousewheel", L.DomEvent.stopPropagation))
            }],
            template: '<div class="angular-leaflet-control-layers" ng-show="overlaysArray.length"><h4 ng-if="title">{{ title }}</h4><div class="lf-baselayers"><h5 class="lf-title" ng-if="baseTitle">{{ baseTitle }}</h5><div class="lf-row" ng-repeat="(key, layer) in baselayersArray"><label class="lf-icon-bl" ng-click="changeBaseLayer(key, $event)"><input class="leaflet-control-layers-selector" type="radio" name="lf-radio" ng-show="false" ng-checked="baselayer === key" ng-value="key" /> <i class="lf-icon lf-icon-radio" ng-class="layer.icon"></i><div class="lf-text">{{layer.name}}</div></label></div></div><div class="lf-overlays"><h5 class="lf-title" ng-if="overlaysTitle">{{ overlaysTitle }}</h5><div class="lf-container"><div class="lf-row" ng-repeat="layer in (o = (overlaysArray | orderBy:\'index\':order))" ng-init="initIndex(layer, $index)"><label class="lf-icon-ol-group" ng-if="showGroups &amp;&amp; layer.group &amp;&amp; layer.group != o[$index-1].group"><input class="lf-control-layers-selector" type="checkbox" ng-show="false" ng-change="changeGroupVisibility(layer.group)" ng-model="groupProperties[layer.group].visible"/> <i class="lf-icon lf-icon-check" ng-class="getGroupIcon(groupProperties[layer.group])"></i><div class="lf-text">{{ layer.group }}</div></label><label class="lf-icon-ol"><input class="lf-control-layers-selector" type="checkbox" ng-show="false" ng-model="layer.visible"/> <i class="lf-icon lf-icon-check" ng-class="layer.icon"></i><div class="lf-text">{{layer.name}}</div></label><div class="lf-icons"><i class="lf-icon lf-up" ng-class="icons.up" ng-click="moveLayer(layer, layer.index - orderNumber, $event)"></i> <i class="lf-icon lf-down" ng-class="icons.down" ng-click="moveLayer(layer, layer.index + orderNumber, $event)"></i> <i class="lf-icon lf-toggle-legend" ng-class="icons.toggleLegend" ng-if="layer.legend" ng-click="toggleLegend(layer)"></i> <i class="lf-icon lf-open" ng-class="getOpacityIcon(layer)" ng-click="toggleOpacity($event, layer)"></i></div><div class="lf-legend" ng-if="showLegend(layer)" ng-bind-html="unsafeHTML(layer.legend)"></div><div class="lf-opacity clearfix" ng-if="layer.visible &amp;&amp; layerProperties[layer.name].opacityControl"><label ng-if="rangeIsSupported" class="pull-left" style="width: 50%">0</label><label ng-if="rangeIsSupported" class="pull-left text-right" style="width: 50%">100</label><input ng-if="rangeIsSupported" class="clearfix" type="range" min="0" max="100" class="lf-opacity-control" ng-model="layerProperties[layer.name].opacity" ng-change="changeOpacity(layer)"/><h6 ng-if="!rangeIsSupported">Range is not supported in this browser</h6></div></div></div></div></div>',
            link: function(t, e, r, o) {
                var a = i.isDefined,
                    s = o.getLeafletScope(),
                    l = s.layers;
                t.$watch("icons", function() {
                    var e = {
                        uncheck: "fa fa-square-o",
                        check: "fa fa-check-square-o",
                        radio: "fa fa-dot-circle-o",
                        unradio: "fa fa-circle-o",
                        up: "fa fa-angle-up",
                        down: "fa fa-angle-down",
                        open: "fa fa-angle-double-down",
                        close: "fa fa-angle-double-up",
                        toggleLegend: "fa fa-pencil-square-o"
                    };
                    a(t.icons) ? (angular.extend(e, t.icons), angular.extend(t.icons, e)) : t.icons = e
                }), r.order = !a(r.order) || "normal" !== r.order && "reverse" !== r.order ? "normal" : r.order, t.order = "normal" === r.order, t.orderNumber = "normal" === r.order ? -1 : 1, t.layers = l, o.getMap().then(function(e) {
                    s.$watch("layers.baselayers", function(i) {
                        var r = {};
                        n.getLayers().then(function(n) {
                            var o;
                            for (o in i) {
                                var a = i[o];
                                a.icon = t.icons[e.hasLayer(n.baselayers[o]) ? "radio" : "unradio"], r[o] = a
                            }
                            t.baselayersArray = r
                        })
                    }), s.$watch("layers.overlays", function(e) {
                        var i = [],
                            r = {};
                        n.getLayers().then(function(n) {
                            var o;
                            for (o in e) {
                                var s = e[o];
                                s.icon = t.icons[s.visible ? "check" : "uncheck"], i.push(s), a(t.layerProperties[s.name]) || (t.layerProperties[s.name] = {
                                    opacity: a(s.layerOptions.opacity) ? 100 * s.layerOptions.opacity : 100,
                                    opacityControl: !1,
                                    showLegend: !0
                                }), a(s.group) && (a(t.groupProperties[s.group]) || (t.groupProperties[s.group] = {
                                    visible: !1
                                }), r[s.group] = a(r[s.group]) ? r[s.group] : {
                                    count: 0,
                                    visibles: 0
                                }, r[s.group].count++, s.visible && r[s.group].visibles++), a(s.index) && n.overlays[o].setZIndex && n.overlays[o].setZIndex(e[o].index)
                            }
                            for (o in r) t.groupProperties[o].visible = r[o].visibles === r[o].count;
                            t.overlaysArray = i
                        })
                    }, !0)
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("layers", ["leafletLogger", "$q", "leafletData", "leafletHelpers", "leafletLayerHelpers", "leafletControlHelpers", function(t, e, n, i, r, o) {
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: "leaflet",
            controller: ["$scope", function(t) {
                t._leafletLayers = e.defer(), this.getLayers = function() {
                    return t._leafletLayers.promise
                }
            }],
            link: function(t, e, a, s) {
                var l = i.isDefined,
                    u = {},
                    c = s.getLeafletScope(),
                    h = c.layers,
                    d = r.createLayer,
                    f = r.safeAddLayer,
                    p = r.safeRemoveLayer,
                    m = o.updateLayersControl,
                    g = !1;
                s.getMap().then(function(e) {
                    t._leafletLayers.resolve(u), n.setLayers(u, a.id), u.baselayers = {}, u.overlays = {};
                    var i = a.id,
                        r = !1;
                    for (var o in h.baselayers) {
                        var s = d(h.baselayers[o]);
                        l(s) ? (u.baselayers[o] = s, h.baselayers[o].top === !0 && (f(e, u.baselayers[o]), r = !0)) : delete h.baselayers[o]
                    }!r && Object.keys(u.baselayers).length > 0 && f(e, u.baselayers[Object.keys(h.baselayers)[0]]);
                    for (o in h.overlays) {
                        "cartodb" === h.overlays[o].type;
                        var v = d(h.overlays[o]);
                        l(v) ? (u.overlays[o] = v, h.overlays[o].visible === !0 && f(e, u.overlays[o])) : delete h.overlays[o]
                    }
                    c.$watch("layers.baselayers", function(t, n) {
                        if (angular.equals(t, n)) return g = m(e, i, g, t, h.overlays, u), !0;
                        for (var r in u.baselayers)(!l(t[r]) || t[r].doRefresh) && (e.hasLayer(u.baselayers[r]) && e.removeLayer(u.baselayers[r]), delete u.baselayers[r], t[r] && t[r].doRefresh && (t[r].doRefresh = !1));
                        for (var o in t)
                            if (l(u.baselayers[o])) t[o].top !== !0 || e.hasLayer(u.baselayers[o]) ? t[o].top === !1 && e.hasLayer(u.baselayers[o]) && e.removeLayer(u.baselayers[o]) : f(e, u.baselayers[o]);
                            else {
                                var a = d(t[o]);
                                l(a) && (u.baselayers[o] = a, t[o].top === !0 && f(e, u.baselayers[o]))
                            } var s = !1;
                        for (var c in u.baselayers)
                            if (e.hasLayer(u.baselayers[c])) {
                                s = !0;
                                break
                            }! s && Object.keys(u.baselayers).length > 0 && f(e, u.baselayers[Object.keys(u.baselayers)[0]]), g = m(e, i, g, t, h.overlays, u)
                    }, !0), c.$watch("layers.overlays", function(t, n) {
                        if (angular.equals(t, n)) return g = m(e, i, g, h.baselayers, t, u), !0;
                        for (var r in u.overlays)
                            if (!l(t[r]) || t[r].doRefresh) {
                                if (e.hasLayer(u.overlays[r])) {
                                    var o = l(t[r]) ? t[r].layerOptions : null;
                                    p(e, u.overlays[r], o)
                                }
                                delete u.overlays[r], t[r] && t[r].doRefresh && (t[r].doRefresh = !1)
                            } for (var a in t) {
                            if (l(u.overlays[a])) t[a].visible && !e.hasLayer(u.overlays[a]) ? f(e, u.overlays[a]) : t[a].visible === !1 && e.hasLayer(u.overlays[a]) && p(e, u.overlays[a], t[a].layerOptions);
                            else {
                                var s = d(t[a]);
                                if (!l(s)) continue;
                                u.overlays[a] = s, t[a].visible === !0 && f(e, u.overlays[a])
                            }
                            t[a].visible && e._loaded && t[a].data && "heatmap" === t[a].type && (u.overlays[a].setData(t[a].data), u.overlays[a].update())
                        }
                        g = m(e, i, g, h.baselayers, t, u)
                    }, !0)
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("legend", ["leafletLogger", "$http", "leafletHelpers", "leafletLegendHelpers", function(t, e, n, i) {
        var r = t;
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: "leaflet",
            link: function(t, o, a, s) {
                var l, u, c, h, d = n.isArray,
                    f = n.isDefined,
                    p = n.isFunction,
                    m = s.getLeafletScope(),
                    g = m.legend;
                m.$watch("legend", function(t) {
                    f(t) && (l = t.legendClass ? t.legendClass : "legend", u = t.position || "bottomright", h = t.type || "arcgis")
                }, !0), s.getMap().then(function(t) {
                    m.$watch("legend", function(e) {
                        return f(e) ? f(e.url) || "arcgis" !== h || d(e.colors) && d(e.labels) && e.colors.length === e.labels.length ? f(e.url) ? void r.info("[AngularJS - Leaflet] loading legend service.") : (f(c) && (c.removeFrom(t), c = null), c = L.control({
                            position: u
                        }), "arcgis" === h && (c.onAdd = i.getOnAddArrayLegend(e, l)), void c.addTo(t)) : void r.warn("[AngularJS - Leaflet] legend.colors and legend.labels must be set.") : void(f(c) && (c.removeFrom(t), c = null))
                    }), m.$watch("legend.url", function(n) {
                        f(n) && e.get(n).success(function(e) {
                            f(c) ? i.updateLegend(c.getContainer(), e, h, n) : (c = L.control({
                                position: u
                            }), c.onAdd = i.getOnAddLegend(e, l, h, n), c.addTo(t)), f(g.loadedData) && p(g.loadedData) && g.loadedData()
                        }).error(function() {
                            r.warn("[AngularJS - Leaflet] legend.url not loaded.")
                        })
                    })
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("markers", ["leafletLogger", "$rootScope", "$q", "leafletData", "leafletHelpers", "leafletMapDefaults", "leafletMarkersHelpers", "leafletMarkerEvents", "leafletIterators", "leafletWatchHelpers", "leafletDirectiveControlsHelpers", function(t, e, n, i, r, o, a, s, l, u, c) {
        var h = r.isDefined,
            d = r.errorHeader,
            f = r,
            p = r.isString,
            m = a.addMarkerWatcher,
            g = a.updateMarker,
            v = a.listenMarkerEvents,
            y = a.addMarkerToGroup,
            _ = a.createMarker,
            b = a.deleteMarker,
            $ = l,
            w = r.watchOptions,
            x = u.maybeWatch,
            C = c.extend,
            S = t,
            E = function(t, e, n) {
                if (Object.keys(t).length) {
                    if (n && p(n)) {
                        if (!t[n] || !Object.keys(t[n]).length) return;
                        return t[n][e]
                    }
                    return t[e]
                }
            },
            k = function(t, e, n, i) {
                return i && p(i) ? (h(e[i]) || (e[i] = {}), e[i][n] = t) : e[n] = t, t
            },
            P = function(t, e, n, i, r, o) {
                if (!p(t)) return S.error(d + " A layername must be a string"), !1;
                if (!h(e)) return S.error(d + " You must add layers to the directive if the markers are going to use this functionality."), !1;
                if (!h(e.overlays) || !h(e.overlays[t])) return S.error(d + ' A marker can only be added to a layer of type "group"'), !1;
                var a = e.overlays[t];
                return a instanceof L.LayerGroup || a instanceof L.FeatureGroup ? (a.addLayer(i), !r && o.hasLayer(i) && n.focus === !0 && i.openPopup(), !0) : (S.error(d + ' Adding a marker to an overlay needs a overlay of the type "group" or "featureGroup"'), !1)
            },
            T = function(t, e, n, i, r, o, a, l, u, c) {
                for (var p in e)
                    if (!c[p]) 
                    {
                        // if (-1 === p.search("-"))  -- Rac021
                        // {
                            var b = f.copy(e[p]),
                                $ = f.getObjectDotPath(u ? [u, p] : [p]),
                                w = E(o, p, u);
                            if (h(w)) {
                                var L = h(L) ? n[p] : void 0;
                                g(b, L, w, $, a, r, i)
                            } else {
                                var x = _(b),
                                    C = (b ? b.layer : void 0) || u;
                                if (!h(x)) {
                                    S.error(d + " Received invalid data on the marker " + p + ".");
                                    continue
                                }
                                if (k(x, o, p, u), h(b.message) && x.bindPopup(b.message, b.popupOptions), h(b.group)) {
                                    var T = h(b.groupOption) ? b.groupOption : null;
                                    y(x, b.group, T, i)
                                }
                                if (f.LabelPlugin.isLoaded() && h(b.label) && h(b.label.message) && x.bindLabel(b.label.message, b.label.options), h(b) && (h(b.layer) || h(u))) {
                                    var M = P(C, r, b, x, l.individual.doWatch, i);
                                    if (!M) continue
                                } else h(b.group) || (i.addLayer(x), l.individual.doWatch || b.focus !== !0 || x.openPopup());
                                l.individual.doWatch && m(x, $, a, r, i, l.individual.isDeep), v(x, b, a, l.individual.doWatch, i), s.bindEvents(t, x, $, b, a, C)
                            }
                        // } else S.error('The marker can\'t use a "-" on his key name: "' + p + '".')
                    }
            },
            M = function(t, e, n, i, r) {
                var o, a, s = !1,
                    l = !1,
                    u = h(e);
                for (var c in n) s || (S.debug(d + "[markers] destroy: "), s = !0), u && (a = t[c], o = e[c], l = angular.equals(a, o) && i), h(t) && Object.keys(t).length && h(t[c]) && Object.keys(t[c]).length && !l || r && f.isFunction(r) && r(a, o, c)
            },
            D = function(t, e, n, i, r) {
                M(t, e, n, !1, function(t, e, o) {
                    S.debug(d + "[marker] is deleting marker: " + o), b(n[o], i, r), delete n[o]
                })
            },
            O = function(t, e, n) {
                var i = {};
                return M(t, e, n, !0, function(t, e, n) {
                    S.debug(d + "[marker] is already rendered, marker: " + n), i[n] = t
                }), i
            };
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: ["leaflet", "?layers"],
            link: function(t, e, r, o) {
                var a = o[0],
                    s = a.getLeafletScope();
                a.getMap().then(function(t) {
                    var e, a = {};
                    e = h(o[1]) ? o[1].getLayers : function() {
                        var t = n.defer();
                        return t.resolve(), t.promise
                    };
                    var l = s.markersWatchOptions || w;
                    h(r.watchMarkers) && (l.doWatch = l.individual.doWatch = !h(r.watchMarkers) || f.isTruthy(r.watchMarkers));
                    var u = h(r.markersNested) && f.isTruthy(r.markersNested);
                    e().then(function(e) {
                        var n = function(n, i) {
                                return u ? void $.each(n, function(n, r) {
                                    var o = h(o) ? i[r] : void 0;
                                    D(n, o, a[r], t, e)
                                }) : void D(n, i, a, t, e)
                            },
                            o = function(i, o) {
                                n(i, o);
                                var c = null;
                                return u ? void $.each(i, function(n, u) {
                                    var d = h(d) ? o[u] : void 0;
                                    c = O(i[u], d, a[u]), T(r.id, n, o, t, e, a, s, l, u, c)
                                }) : (c = O(i, o, a), void T(r.id, i, o, t, e, a, s, l, void 0, c))
                            };
                        C(r.id, "markers", o, n), i.setMarkers(a, r.id), x(s, "markers", l, function(t, e) {
                            o(t, e)
                        })
                    })
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("maxbounds", ["leafletLogger", "leafletMapDefaults", "leafletBoundsHelpers", "leafletHelpers", function(t, e, n, i) {
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: "leaflet",
            link: function(t, e, r, o) {
                var a = o.getLeafletScope(),
                    s = n.isValidBounds,
                    l = i.isNumber;
                o.getMap().then(function(t) {
                    a.$watch("maxbounds", function(e) {
                        if (!s(e)) return void t.setMaxBounds();
                        var i = n.createLeafletBounds(e);
                        l(e.pad) && (i = i.pad(e.pad)), t.setMaxBounds(i), r.center || r.lfCenter || t.fitBounds(i)
                    })
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("paths", ["leafletLogger", "$q", "leafletData", "leafletMapDefaults", "leafletHelpers", "leafletPathsHelpers", "leafletPathEvents", function(t, e, n, i, r, o, a) {
        var s = t;
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: ["leaflet", "?layers"],
            link: function(t, l, u, c) {
                var h = c[0],
                    d = r.isDefined,
                    f = r.isString,
                    p = h.getLeafletScope(),
                    m = p.paths,
                    g = o.createPath,
                    v = a.bindPathEvents,
                    y = o.setPathOptions;
                h.getMap().then(function(t) {
                    var o, a = i.getDefaults(u.id);
                    o = d(c[1]) ? c[1].getLayers : function() {
                        var t = e.defer();
                        return t.resolve(), t.promise
                    }, d(m) && o().then(function(e) {
                        var i = {};
                        n.setPaths(i, u.id);
                        var o = !d(u.watchPaths) || "true" === u.watchPaths,
                            l = function(n, i) {
                                var r = p.$watch('paths["' + i + '"]', function(i, o) {
                                    if (!d(i)) {
                                        if (d(o.layer))
                                            for (var a in e.overlays) {
                                                var s = e.overlays[a];
                                                s.removeLayer(n)
                                            }
                                        return t.removeLayer(n), void r()
                                    }
                                    y(n, i.type, i)
                                }, !0)
                            };
                        p.$watchCollection("paths", function(n) {
                            for (var c in i) d(n[c]) || (t.removeLayer(i[c]), delete i[c]);
                            for (var h in n)
                                if (0 !== h.search("\\$"))
                                    if (-1 === h.search("-")) {
                                        if (!d(i[h])) {
                                            var m = n[h],
                                                _ = g(h, n[h], a);
                                            if (d(_) && d(m.message) && _.bindPopup(m.message, m.popupOptions), r.LabelPlugin.isLoaded() && d(m.label) && d(m.label.message) && _.bindLabel(m.label.message, m.label.options), d(m) && d(m.layer)) {
                                                if (!f(m.layer)) {
                                                    s.error("[AngularJS - Leaflet] A layername must be a string");
                                                    continue
                                                }
                                                if (!d(e)) {
                                                    s.error("[AngularJS - Leaflet] You must add layers to the directive if the markers are going to use this functionality.");
                                                    continue
                                                }
                                                if (!d(e.overlays) || !d(e.overlays[m.layer])) {
                                                    s.error('[AngularJS - Leaflet] A path can only be added to a layer of type "group"');
                                                    continue
                                                }
                                                var b = e.overlays[m.layer];
                                                if (!(b instanceof L.LayerGroup || b instanceof L.FeatureGroup)) {
                                                    s.error('[AngularJS - Leaflet] Adding a path to an overlay needs a overlay of the type "group" or "featureGroup"');
                                                    continue
                                                }
                                                i[h] = _, b.addLayer(_), o ? l(_, h) : y(_, m.type, m)
                                            } else d(_) && (i[h] = _, t.addLayer(_), o ? l(_, h) : y(_, m.type, m));
                                            v(u.id, _, h, m, p)
                                        }
                                    } else s.error('[AngularJS - Leaflet] The path name "' + h + '" is not valid. It must not include "-" and a number.')
                        })
                    })
                })
            }
        }
    }]), angular.module("ui-leaflet").directive("tiles", ["leafletLogger", "leafletData", "leafletMapDefaults", "leafletHelpers", function(t, e, n, i) {
        var r = t;
        return {
            restrict: "A",
            scope: !1,
            replace: !1,
            require: "leaflet",
            link: function(t, o, a, s) {
                var l = i.isDefined,
                    u = s.getLeafletScope(),
                    c = u.tiles;
                return l(c) && l(c.url) ? void s.getMap().then(function(t) {
                    var i, r = n.getDefaults(a.id);
                    u.$watch("tiles", function(n) {
                        var o = r.tileLayerOptions,
                            s = r.tileLayer;
                        return !l(n.url) && l(i) ? void t.removeLayer(i) : l(i) ? l(n.url) && l(n.options) && !angular.equals(n.options, o) ? (t.removeLayer(i), o = r.tileLayerOptions, angular.copy(n.options, o), s = n.url, i = L.tileLayer(s, o), i.addTo(t), void e.setTiles(i, a.id)) : void(l(n.url) && i.setUrl(n.url)) : (l(n.options) && angular.copy(n.options, o), l(n.url) && (s = n.url), i = L.tileLayer(s, o), i.addTo(t), void e.setTiles(i, a.id))
                    }, !0)
                }) : void r.warn("[AngularJS - Leaflet] The 'tiles' definition doesn't have the 'url' property.")
            }
        }
    }]), ["markers", "geojson"].forEach(function(t) {
        angular.module("ui-leaflet").directive(t + "WatchOptions", ["$log", "$rootScope", "$q", "leafletData", "leafletHelpers", function(e, n, i, r, o) {
            var a = o.isDefined,
                s = o.errorHeader,
                l = o.isObject,
                u = o.watchOptions,
                c = e;
            return {
                restrict: "A",
                scope: !1,
                replace: !1,
                require: ["leaflet"],
                link: function(e, n, i, r) {
                    var o = r[0],
                        h = o.getLeafletScope();
                    o.getMap().then(function() {
                        a(e[t + "WatchOptions"]) && (l(e[t + "WatchOptions"]) ? angular.extend(u, e[t + "WatchOptions"]) : c.error(s + "[" + t + "WatchOptions] is not an object"), h[t + "WatchOptions"] = u)
                    })
                }
            }
        }])
    }), angular.module("ui-leaflet").factory("leafletEventsHelpersFactory", ["$rootScope", "$q", "leafletLogger", "leafletHelpers", function(t, e, n, i) {
        var r = i.safeApply,
            o = i.isDefined,
            a = i.isObject,
            s = i.isArray,
            l = i.errorHeader,
            u = n,
            c = function(t, e) {
                this.rootBroadcastName = t, u.debug("leafletEventsHelpersFactory: lObjectType: " + e + "rootBroadcastName: " + t), this.lObjectType = e
            };
        return c.prototype.getAvailableEvents = function() {
            return []
        }, c.prototype.genDispatchEvent = function(t, e, n, i, r, o, a, s, l) {
            var c = this;
            return t = t || "", t && (t = "." + t),
                function(h) {
                    var d = c.rootBroadcastName + t + "." + e;
                    u.debug(d), c.fire(i, d, n, h, h.target || r, a, o, s, l)
                }
        }, c.prototype.fire = function(e, n, i, a, s, l, u, c) {
            r(e, function() {
                var r = {
                    leafletEvent: a,
                    leafletObject: s,
                    modelName: u,
                    model: l
                };
                o(c) && angular.extend(r, {
                    layerName: c
                }), "emit" === i ? e.$emit(n, r) : t.$broadcast(n, r)
            })
        }, c.prototype.bindEvents = function(t, e, n, i, r, c, h) {
            var d = [],
                f = "emit",
                p = this;
            if (o(r.eventBroadcast))
                if (a(r.eventBroadcast))
                    if (o(r.eventBroadcast[p.lObjectType]))
                        if (a(r.eventBroadcast[p.lObjectType])) {
                            o(r.eventBroadcast[this.lObjectType].logic) && "emit" !== r.eventBroadcast[p.lObjectType].logic && "broadcast" !== r.eventBroadcast[p.lObjectType].logic && u.warn(l + "Available event propagation logic are: 'emit' or 'broadcast'.");
                            var m = !1,
                                g = !1;
                            o(r.eventBroadcast[p.lObjectType].enable) && s(r.eventBroadcast[p.lObjectType].enable) && (m = !0), o(r.eventBroadcast[p.lObjectType].disable) && s(r.eventBroadcast[p.lObjectType].disable) && (g = !0), m && g ? u.warn(l + "can not enable and disable events at the same time") : m || g ? m ? r.eventBroadcast[this.lObjectType].enable.forEach(function(t) {
                                -1 !== d.indexOf(t) ? u.warn(l + "This event " + t + " is already enabled") : -1 === p.getAvailableEvents().indexOf(t) ? u.warn(l + "This event " + t + " does not exist") : d.push(t)
                            }) : (d = this.getAvailableEvents(), r.eventBroadcast[p.lObjectType].disable.forEach(function(t) {
                                var e = d.indexOf(t); - 1 === e ? u.warn(l + "This event " + t + " does not exist or has been already disabled") : d.splice(e, 1)
                            })) : u.warn(l + "must enable or disable events")
                        } else u.warn(l + "event-broadcast." + [p.lObjectType] + " must be an object check your model.");
            else d = this.getAvailableEvents();
            else u.error(l + "event-broadcast must be an object check your model.");
            else d = this.getAvailableEvents();
            return d.forEach(function(o) {
                e.on(o, p.genDispatchEvent(t, o, f, r, e, n, i, c, h))
            }), f
        }, c
    }]).service("leafletEventsHelpers", ["leafletEventsHelpersFactory", function(t) {
        return new t
    }]), angular.module("ui-leaflet").factory("leafletGeoJsonEvents", ["$rootScope", "$q", "leafletLogger", "leafletHelpers", "leafletEventsHelpersFactory", "leafletData", function(t, e, n, i, r, o) {
        var a = i.safeApply,
            s = r,
            l = function() {
                s.call(this, "leafletDirectiveGeoJson", "geojson")
            };
        return l.prototype = new s, l.prototype.genDispatchEvent = function(e, n, i, r, l, u, c, h, d) {
            var f = s.prototype.genDispatchEvent.call(this, e, n, i, r, l, u, c, h),
                p = this;
            return function(e) {
                "mouseout" === n && (d.resetStyleOnMouseout && o.getGeoJSON(d.mapId).then(function(t) {
                    var n = h ? t[h] : t;
                    n.resetStyle(e.target)
                }), a(r, function() {
                    t.$broadcast(p.rootBroadcastName + ".mouseout", e)
                })), f(e)
            }
        }, l.prototype.getAvailableEvents = function() {
            return ["click", "dblclick", "mouseover", "mouseout"]
        }, new l
    }]), angular.module("ui-leaflet").factory("leafletLabelEvents", ["$rootScope", "$q", "leafletLogger", "leafletHelpers", "leafletEventsHelpersFactory", function(t, e, n, i, r) {
        var o = i,
            a = r,
            s = function() {
                a.call(this, "leafletDirectiveLabel", "markers")
            };
        return s.prototype = new a, s.prototype.genDispatchEvent = function(t, e, n, i, r, o, s, l) {
            var u = o.replace("markers.", "");
            return a.prototype.genDispatchEvent.call(this, t, e, n, i, r, u, s, l)
        }, s.prototype.getAvailableEvents = function() {
            return ["click", "dblclick", "mousedown", "mouseover", "mouseout", "contextmenu"]
        }, s.prototype.genEvents = function(t, e, n, i, r, a, s, l) {
            var u = this,
                c = this.getAvailableEvents(),
                h = o.getObjectArrayPath("markers." + a);
            c.forEach(function(e) {
                r.label.on(e, u.genDispatchEvent(t, e, n, i, r.label, h, s, l))
            })
        }, s.prototype.bindEvents = function() {}, new s
    }]), angular.module("ui-leaflet").factory("leafletMapEvents", ["$rootScope", "$q", "leafletLogger", "leafletHelpers", "leafletEventsHelpers", "leafletIterators", function(t, e, n, i, r, o) {
        var a = i.isDefined,
            s = r.fire,
            l = function() {
                return ["click", "dblclick", "mousedown", "mouseup", "mouseover", "mouseout", "mousemove", "contextmenu", "focus", "blur", "preclick", "load", "unload", "viewreset", "movestart", "move", "moveend", "dragstart", "drag", "dragend", "zoomstart", "zoomanim", "zoomend", "zoomlevelschange", "resize", "autopanstart", "layeradd", "layerremove", "baselayerchange", "overlayadd", "overlayremove", "locationfound", "locationerror", "popupopen", "popupclose", "draw:created", "draw:edited", "draw:deleted", "draw:drawstart", "draw:drawstop", "draw:editstart", "draw:editstop", "draw:deletestart", "draw:deletestop"]
            },
            u = function(t, e, i, r) {
                return r && (r += "."),
                    function(o) {
                        var a = "leafletDirectiveMap." + r + e;
                        n.debug(a), s(t, a, i, o, o.target, t)
                    }
            },
            c = function(t) {
                t.$broadcast("boundsChanged")
            },
            h = function(t, e, n, i) {
                if (a(n.urlHashCenter)) {
                    var r = e.getCenter(),
                        o = r.lat.toFixed(4) + ":" + r.lng.toFixed(4) + ":" + e.getZoom();
                    a(i.c) && i.c === o || t.$emit("centerUrlHash", o)
                }
            },
            d = function(t, e, n, i, r) {
                o.each(e, function(e) {
                    var o = {};
                    o[n] = e, t.on(e, u(i, e, r, t._container.id || ""), o)
                })
            };
        return {
            getAvailableMapEvents: l,
            genDispatchMapEvent: u,
            notifyCenterChangedToBounds: c,
            notifyCenterUrlHashChanged: h,
            addEvents: d
        }
    }]), angular.module("ui-leaflet").factory("leafletMarkerEvents", ["$rootScope", "$q", "leafletLogger", "leafletHelpers", "leafletEventsHelpersFactory", "leafletLabelEvents", function(t, e, n, i, r, o) {
        var a = i.safeApply,
            s = i.isDefined,
            l = i,
            u = o,
            c = r,
            h = function() {
                c.call(this, "leafletDirectiveMarker", "markers")
            };
        return h.prototype = new c, h.prototype.genDispatchEvent = function(e, n, i, r, o, s, l, u) {
            var h = c.prototype.genDispatchEvent.call(this, e, n, i, r, o, s, l, u);
            return function(e) {
                "click" === n ? a(r, function() {
                    t.$broadcast("leafletDirectiveMarkersClick", s)
                }) : "dragend" === n && (a(r, function() {
                    l.lat = o.getLatLng().lat, l.lng = o.getLatLng().lng
                }), l.message && l.focus === !0 && o.openPopup()), h(e)
            }
        }, h.prototype.getAvailableEvents = function() {
            return ["click", "dblclick", "mousedown", "mouseover", "mouseout", "contextmenu", "dragstart", "drag", "dragend", "move", "remove", "popupopen", "popupclose", "touchend", "touchstart", "touchmove", "touchcancel", "touchleave"]
        }, h.prototype.bindEvents = function(t, e, n, i, r, o) {
            var a = c.prototype.bindEvents.call(this, t, e, n, i, r, o);
            l.LabelPlugin.isLoaded() && s(e.label) && u.genEvents(t, n, a, r, e, i, o)
        }, new h
    }]), angular.module("ui-leaflet").factory("leafletPathEvents", ["$rootScope", "$q", "leafletLogger", "leafletHelpers", "leafletLabelEvents", "leafletEventsHelpers", function(t, e, n, i, r, o) {
        var a = i.isDefined,
            s = i.isObject,
            l = i,
            u = i.errorHeader,
            c = r,
            h = o.fire,
            d = n,
            f = function(t, e, n, i, r, o, a, s) {
                return t = t || "", t && (t = "." + t),
                    function(l) {
                        var u = "leafletDirectivePath" + t + "." + e;
                        d.debug(u), h(i, u, n, l, l.target || r, a, o, s)
                    }
            },
            p = function(t, e, n, i, r) {
                var o, h, p = [],
                    g = "broadcast";
                if (a(r.eventBroadcast))
                    if (s(r.eventBroadcast))
                        if (a(r.eventBroadcast.path))
                            if (s(r.eventBroadcast.paths)) d.warn(u + "event-broadcast.path must be an object check your model.");
                            else {
                                void 0 !== r.eventBroadcast.path.logic && null !== r.eventBroadcast.path.logic && ("emit" !== r.eventBroadcast.path.logic && "broadcast" !== r.eventBroadcast.path.logic ? d.warn(u + "Available event propagation logic are: 'emit' or 'broadcast'.") : "emit" === r.eventBroadcast.path.logic && (g = "emit"));
                                var v = !1,
                                    y = !1;
                                if (void 0 !== r.eventBroadcast.path.enable && null !== r.eventBroadcast.path.enable && "object" == typeof r.eventBroadcast.path.enable && (v = !0), void 0 !== r.eventBroadcast.path.disable && null !== r.eventBroadcast.path.disable && "object" == typeof r.eventBroadcast.path.disable && (y = !0), v && y) d.warn(u + "can not enable and disable events at the same time");
                                else if (v || y)
                                    if (v)
                                        for (o = 0; o < r.eventBroadcast.path.enable.length; o++) h = r.eventBroadcast.path.enable[o], -1 !== p.indexOf(h) ? d.warn(u + "This event " + h + " is already enabled") : -1 === m().indexOf(h) ? d.warn(u + "This event " + h + " does not exist") : p.push(h);
                                    else
                                        for (p = m(), o = 0; o < r.eventBroadcast.path.disable.length; o++) {
                                            h = r.eventBroadcast.path.disable[o];
                                            var _ = p.indexOf(h); - 1 === _ ? d.warn(u + "This event " + h + " does not exist or has been already disabled") : p.splice(_, 1)
                                        } else d.warn(u + "must enable or disable events")
                            }
                else p = m();
                else d.error(u + "event-broadcast must be an object check your model.");
                else p = m();
                for (o = 0; o < p.length; o++) h = p[o], e.on(h, f(t, h, g, r, p, n));
                l.LabelPlugin.isLoaded() && a(e.label) && c.genEvents(t, n, g, r, e, i)
            },
            m = function() {
                return ["click", "dblclick", "mousedown", "mouseover", "mouseout", "contextmenu", "add", "remove", "popupopen", "popupclose"]
            };
        return {
            getAvailablePathEvents: m,
            bindPathEvents: p
        }
    }])
}(angular);
