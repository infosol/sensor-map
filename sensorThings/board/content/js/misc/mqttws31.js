"undefined" == typeof Paho && (Paho = {}), Paho.MQTT = function(t) {
    function e(t, e) {
        var n = e,
            i = t[e],
            o = i >> 4,
            s = i &= 15;
        e += 1;
        var a, u = 0,
            h = 1;
        do {
            if (e == t.length) return [null, n];
            a = t[e++], u += (127 & a) * h, h *= 128
        } while (0 != (128 & a));
        var d = e + u;
        if (d > t.length) return [null, n];
        var f = new y(o);
        switch (o) {
            case c.CONNACK:
                var p = t[e++];
                1 & p && (f.sessionPresent = !0), f.returnCode = t[e++];
                break;
            case c.PUBLISH:
                var m = s >> 1 & 3,
                    g = r(t, e);
                e += 2;
                var v = l(t, e, g);
                e += g, m > 0 && (f.messageIdentifier = r(t, e), e += 2);
                var _ = new Paho.MQTT.Message(t.subarray(e, d));
                1 == (1 & s) && (_.retained = !0), 8 == (8 & s) && (_.duplicate = !0), _.qos = m, _.destinationName = v, f.payloadMessage = _;
                break;
            case c.PUBACK:
            case c.PUBREC:
            case c.PUBREL:
            case c.PUBCOMP:
            case c.UNSUBACK:
                f.messageIdentifier = r(t, e);
                break;
            case c.SUBACK:
                f.messageIdentifier = r(t, e), e += 2, f.returnCode = t.subarray(e, d)
        }
        return [f, d]
    }

    function n(t, e, n) {
        return e[n++] = t >> 8, e[n++] = t % 256, n
    }

    function i(t, e, i, r) {
        return r = n(e, i, r), a(t, i, r), r + e
    }

    function r(t, e) {
        return 256 * t[e] + t[e + 1]
    }

    function o(t) {
        var e = new Array(1),
            n = 0;
        do {
            var i = t % 128;
            t >>= 7, t > 0 && (i |= 128), e[n++] = i
        } while (t > 0 && 4 > n);
        return e
    }

    function s(t) {
        for (var e = 0, n = 0; n < t.length; n++) {
            var i = t.charCodeAt(n);
            i > 2047 ? (i >= 55296 && 56319 >= i && (n++, e++), e += 3) : i > 127 ? e += 2 : e++
        }
        return e
    }

    function a(t, e, n) {
        for (var i = n, r = 0; r < t.length; r++) {
            var o = t.charCodeAt(r);
            if (o >= 55296 && 56319 >= o) {
                var s = t.charCodeAt(++r);
                if (isNaN(s)) throw new Error(m(f.MALFORMED_UNICODE, [o, s]));
                o = (o - 55296 << 10) + (s - 56320) + 65536
            }
            127 >= o ? e[i++] = o : 2047 >= o ? (e[i++] = o >> 6 & 31 | 192, e[i++] = 63 & o | 128) : 65535 >= o ? (e[i++] = o >> 12 & 15 | 224, e[i++] = o >> 6 & 63 | 128, e[i++] = 63 & o | 128) : (e[i++] = o >> 18 & 7 | 240, e[i++] = o >> 12 & 63 | 128, e[i++] = o >> 6 & 63 | 128, e[i++] = 63 & o | 128)
        }
        return e
    }

    function l(t, e, n) {
        for (var i, r = "", o = e; e + n > o;) {
            var s = t[o++];
            if (128 > s) i = s;
            else {
                var a = t[o++] - 128;
                if (0 > a) throw new Error(m(f.MALFORMED_UTF, [s.toString(16), a.toString(16), ""]));
                if (224 > s) i = 64 * (s - 192) + a;
                else {
                    var l = t[o++] - 128;
                    if (0 > l) throw new Error(m(f.MALFORMED_UTF, [s.toString(16), a.toString(16), l.toString(16)]));
                    if (240 > s) i = 4096 * (s - 224) + 64 * a + l;
                    else {
                        var u = t[o++] - 128;
                        if (0 > u) throw new Error(m(f.MALFORMED_UTF, [s.toString(16), a.toString(16), l.toString(16), u.toString(16)]));
                        if (!(248 > s)) throw new Error(m(f.MALFORMED_UTF, [s.toString(16), a.toString(16), l.toString(16), u.toString(16)]));
                        i = 262144 * (s - 240) + 4096 * a + 64 * l + u
                    }
                }
            }
            i > 65535 && (i -= 65536, r += String.fromCharCode(55296 + (i >> 10)), i = 56320 + (1023 & i)), r += String.fromCharCode(i)
        }
        return r
    }
    var u = "@VERSION@",
        c = {
            CONNECT: 1,
            CONNACK: 2,
            PUBLISH: 3,
            PUBACK: 4,
            PUBREC: 5,
            PUBREL: 6,
            PUBCOMP: 7,
            SUBSCRIBE: 8,
            SUBACK: 9,
            UNSUBSCRIBE: 10,
            UNSUBACK: 11,
            PINGREQ: 12,
            PINGRESP: 13,
            DISCONNECT: 14
        },
        h = function(t, e) {
            for (var n in t)
                if (t.hasOwnProperty(n)) {
                    if (!e.hasOwnProperty(n)) {
                        var i = "Unknown property, " + n + ". Valid properties are:";
                        for (var n in e) e.hasOwnProperty(n) && (i = i + " " + n);
                        throw new Error(i)
                    }
                    if (typeof t[n] !== e[n]) throw new Error(m(f.INVALID_TYPE, [typeof t[n], n]))
                }
        },
        d = function(t, e) {
            return function() {
                return t.apply(e, arguments)
            }
        },
        f = {
            OK: {
                code: 0,
                text: "AMQJSC0000I OK."
            },
            CONNECT_TIMEOUT: {
                code: 1,
                text: "AMQJSC0001E Connect timed out."
            },
            SUBSCRIBE_TIMEOUT: {
                code: 2,
                text: "AMQJS0002E Subscribe timed out."
            },
            UNSUBSCRIBE_TIMEOUT: {
                code: 3,
                text: "AMQJS0003E Unsubscribe timed out."
            },
            PING_TIMEOUT: {
                code: 4,
                text: "AMQJS0004E Ping timed out."
            },
            INTERNAL_ERROR: {
                code: 5,
                text: "AMQJS0005E Internal error. Error Message: {0}, Stack trace: {1}"
            },
            CONNACK_RETURNCODE: {
                code: 6,
                text: "AMQJS0006E Bad Connack return code:{0} {1}."
            },
            SOCKET_ERROR: {
                code: 7,
                text: "AMQJS0007E Socket error:{0}."
            },
            SOCKET_CLOSE: {
                code: 8,
                text: "AMQJS0008I Socket closed."
            },
            MALFORMED_UTF: {
                code: 9,
                text: "AMQJS0009E Malformed UTF data:{0} {1} {2}."
            },
            UNSUPPORTED: {
                code: 10,
                text: "AMQJS0010E {0} is not supported by this browser."
            },
            INVALID_STATE: {
                code: 11,
                text: "AMQJS0011E Invalid state {0}."
            },
            INVALID_TYPE: {
                code: 12,
                text: "AMQJS0012E Invalid type {0} for {1}."
            },
            INVALID_ARGUMENT: {
                code: 13,
                text: "AMQJS0013E Invalid argument {0} for {1}."
            },
            UNSUPPORTED_OPERATION: {
                code: 14,
                text: "AMQJS0014E Unsupported operation."
            },
            INVALID_STORED_DATA: {
                code: 15,
                text: "AMQJS0015E Invalid data in local storage key={0} value={1}."
            },
            INVALID_MQTT_MESSAGE_TYPE: {
                code: 16,
                text: "AMQJS0016E Invalid MQTT message type {0}."
            },
            MALFORMED_UNICODE: {
                code: 17,
                text: "AMQJS0017E Malformed Unicode string:{0} {1}."
            }
        },
        p = {
            0: "Connection Accepted",
            1: "Connection Refused: unacceptable protocol version",
            2: "Connection Refused: identifier rejected",
            3: "Connection Refused: server unavailable",
            4: "Connection Refused: bad user name or password",
            5: "Connection Refused: not authorized"
        },
        m = function(t, e) {
            var n = t.text;
            if (e)
                for (var i, r, o = 0; o < e.length; o++)
                    if (i = "{" + o + "}", r = n.indexOf(i), r > 0) {
                        var s = n.substring(0, r),
                            a = n.substring(r + i.length);
                        n = s + e[o] + a
                    } return n
        },
        g = [0, 6, 77, 81, 73, 115, 100, 112, 3],
        v = [0, 4, 77, 81, 84, 84, 4],
        y = function(t, e) {
            this.type = t;
            for (var n in e) e.hasOwnProperty(n) && (this[n] = e[n])
        };
    y.prototype.encode = function() {
        var t = (15 & this.type) << 4,
            e = 0,
            r = new Array,
            a = 0;
        switch (void 0 != this.messageIdentifier && (e += 2), this.type) {
            case c.CONNECT:
                switch (this.mqttVersion) {
                    case 3:
                        e += g.length + 3;
                        break;
                    case 4:
                        e += v.length + 3
                }
                if (e += s(this.clientId) + 2, void 0 != this.willMessage) {
                    e += s(this.willMessage.destinationName) + 2;
                    var l = this.willMessage.payloadBytes;
                    l instanceof Uint8Array || (l = new Uint8Array(h)), e += l.byteLength + 2
                }
                void 0 != this.userName && (e += s(this.userName) + 2), void 0 != this.password && (e += s(this.password) + 2);
                break;
            case c.SUBSCRIBE:
                t |= 2;
                for (var u = 0; u < this.topics.length; u++) r[u] = s(this.topics[u]), e += r[u] + 2;
                e += this.requestedQos.length;
                break;
            case c.UNSUBSCRIBE:
                t |= 2;
                for (var u = 0; u < this.topics.length; u++) r[u] = s(this.topics[u]), e += r[u] + 2;
                break;
            case c.PUBREL:
                t |= 2;
                break;
            case c.PUBLISH:
                this.payloadMessage.duplicate && (t |= 8), t = t |= this.payloadMessage.qos << 1, this.payloadMessage.retained && (t |= 1), a = s(this.payloadMessage.destinationName), e += a + 2;
                var h = this.payloadMessage.payloadBytes;
                e += h.byteLength, h instanceof ArrayBuffer ? h = new Uint8Array(h) : h instanceof Uint8Array || (h = new Uint8Array(h.buffer));
                break;
            case c.DISCONNECT:
        }
        var d = o(e),
            f = d.length + 1,
            p = new ArrayBuffer(e + f),
            m = new Uint8Array(p);
        if (m[0] = t, m.set(d, 1), this.type == c.PUBLISH) f = i(this.payloadMessage.destinationName, a, m, f);
        else if (this.type == c.CONNECT) {
            switch (this.mqttVersion) {
                case 3:
                    m.set(g, f), f += g.length;
                    break;
                case 4:
                    m.set(v, f), f += v.length
            }
            var y = 0;
            this.cleanSession && (y = 2), void 0 != this.willMessage && (y |= 4, y |= this.willMessage.qos << 3, this.willMessage.retained && (y |= 32)), void 0 != this.userName && (y |= 128), void 0 != this.password && (y |= 64), m[f++] = y, f = n(this.keepAliveInterval, m, f)
        }
        switch (void 0 != this.messageIdentifier && (f = n(this.messageIdentifier, m, f)), this.type) {
            case c.CONNECT:
                f = i(this.clientId, s(this.clientId), m, f), void 0 != this.willMessage && (f = i(this.willMessage.destinationName, s(this.willMessage.destinationName), m, f), f = n(l.byteLength, m, f), m.set(l, f), f += l.byteLength), void 0 != this.userName && (f = i(this.userName, s(this.userName), m, f)), void 0 != this.password && (f = i(this.password, s(this.password), m, f));
                break;
            case c.PUBLISH:
                m.set(h, f);
                break;
            case c.SUBSCRIBE:
                for (var u = 0; u < this.topics.length; u++) f = i(this.topics[u], r[u], m, f), m[f++] = this.requestedQos[u];
                break;
            case c.UNSUBSCRIBE:
                for (var u = 0; u < this.topics.length; u++) f = i(this.topics[u], r[u], m, f)
        }
        return p
    };
    var _ = function(t, e, n) {
            this._client = t, this._window = e, this._keepAliveInterval = 1e3 * n, this.isReset = !1;
            var i = new y(c.PINGREQ).encode(),
                r = function(t) {
                    return function() {
                        return o.apply(t)
                    }
                },
                o = function() {
                    this.isReset ? (this.isReset = !1, this._client._trace("Pinger.doPing", "send PINGREQ"), this._client.socket.send(i), this.timeout = this._window.setTimeout(r(this), this._keepAliveInterval)) : (this._client._trace("Pinger.doPing", "Timed out"), this._client._disconnected(f.PING_TIMEOUT.code, m(f.PING_TIMEOUT)))
                };
            this.reset = function() {
                this.isReset = !0, this._window.clearTimeout(this.timeout), this._keepAliveInterval > 0 && (this.timeout = setTimeout(r(this), this._keepAliveInterval))
            }, this.cancel = function() {
                this._window.clearTimeout(this.timeout)
            }
        },
        b = function(t, e, n, i, r) {
            this._window = e, n || (n = 30);
            var o = function(t, e, n) {
                return function() {
                    return t.apply(e, n)
                }
            };
            this.timeout = setTimeout(o(i, t, r), 1e3 * n), this.cancel = function() {
                this._window.clearTimeout(this.timeout)
            }
        },
        $ = function(e, n, i, r, o) {
            if (!("WebSocket" in t && null !== t.WebSocket)) throw new Error(m(f.UNSUPPORTED, ["WebSocket"]));
            if (!("localStorage" in t && null !== t.localStorage)) throw new Error(m(f.UNSUPPORTED, ["localStorage"]));
            if (!("ArrayBuffer" in t && null !== t.ArrayBuffer)) throw new Error(m(f.UNSUPPORTED, ["ArrayBuffer"]));
            this._trace("Paho.MQTT.Client", e, n, i, r, o), this.host = n, this.port = i, this.path = r, this.uri = e, this.clientId = o, this._localKey = n + ":" + i + ("/mqtt" != r ? ":" + r : "") + ":" + o + ":", this._msg_queue = [], this._sentMessages = {}, this._receivedMessages = {}, this._notify_msg_sent = {}, this._message_identifier = 1, this._sequence = 0;
            for (var s in localStorage)(0 == s.indexOf("Sent:" + this._localKey) || 0 == s.indexOf("Received:" + this._localKey)) && this.restore(s)
        };
    $.prototype.host, $.prototype.port, $.prototype.path, $.prototype.uri, $.prototype.clientId, $.prototype.socket, $.prototype.connected = !1, $.prototype.maxMessageIdentifier = 65536, $.prototype.connectOptions, $.prototype.hostIndex, $.prototype.onConnectionLost, $.prototype.onMessageDelivered, $.prototype.onMessageArrived, $.prototype.traceFunction, $.prototype._msg_queue = null, $.prototype._connectTimeout, $.prototype.sendPinger = null, $.prototype.receivePinger = null, $.prototype.receiveBuffer = null, $.prototype._traceBuffer = null, $.prototype._MAX_TRACE_ENTRIES = 100, $.prototype.connect = function(t) {
        var e = this._traceMask(t, "password");
        if (this._trace("Client.connect", e, this.socket, this.connected), this.connected) throw new Error(m(f.INVALID_STATE, ["already connected"]));
        if (this.socket) throw new Error(m(f.INVALID_STATE, ["already connected"]));
        this.connectOptions = t, t.uris ? (this.hostIndex = 0, this._doConnect(t.uris[0])) : this._doConnect(this.uri)
    }, $.prototype.subscribe = function(t, e) {
        if (this._trace("Client.subscribe", t, e), !this.connected) throw new Error(m(f.INVALID_STATE, ["not connected"]));
        var n = new y(c.SUBSCRIBE);
        n.topics = [t], n.requestedQos = void 0 != e.qos ? [e.qos] : [0], e.onSuccess && (n.onSuccess = function(t) {
            e.onSuccess({
                invocationContext: e.invocationContext,
                grantedQos: t
            })
        }), e.onFailure && (n.onFailure = function(t) {
            e.onFailure({
                invocationContext: e.invocationContext,
                errorCode: t
            })
        }), e.timeout && (n.timeOut = new b(this, window, e.timeout, e.onFailure, [{
            invocationContext: e.invocationContext,
            errorCode: f.SUBSCRIBE_TIMEOUT.code,
            errorMessage: m(f.SUBSCRIBE_TIMEOUT)
        }])), this._requires_ack(n), this._schedule_message(n)
    }, $.prototype.unsubscribe = function(t, e) {
        if (this._trace("Client.unsubscribe", t, e), !this.connected) throw new Error(m(f.INVALID_STATE, ["not connected"]));
        var n = new y(c.UNSUBSCRIBE);
        n.topics = [t], e.onSuccess && (n.callback = function() {
            e.onSuccess({
                invocationContext: e.invocationContext
            })
        }), e.timeout && (n.timeOut = new b(this, window, e.timeout, e.onFailure, [{
            invocationContext: e.invocationContext,
            errorCode: f.UNSUBSCRIBE_TIMEOUT.code,
            errorMessage: m(f.UNSUBSCRIBE_TIMEOUT)
        }])), this._requires_ack(n), this._schedule_message(n)
    }, $.prototype.send = function(t) {
        if (this._trace("Client.send", t), !this.connected) throw new Error(m(f.INVALID_STATE, ["not connected"]));
        wireMessage = new y(c.PUBLISH), wireMessage.payloadMessage = t, t.qos > 0 ? this._requires_ack(wireMessage) : this.onMessageDelivered && (this._notify_msg_sent[wireMessage] = this.onMessageDelivered(wireMessage.payloadMessage)), this._schedule_message(wireMessage)
    }, $.prototype.disconnect = function() {
        if (this._trace("Client.disconnect"), !this.socket) throw new Error(m(f.INVALID_STATE, ["not connecting or connected"]));
        wireMessage = new y(c.DISCONNECT), this._notify_msg_sent[wireMessage] = d(this._disconnected, this), this._schedule_message(wireMessage)
    }, $.prototype.getTraceLog = function() {
        if (null !== this._traceBuffer) {
            this._trace("Client.getTraceLog", new Date), this._trace("Client.getTraceLog in flight messages", this._sentMessages.length);
            for (var t in this._sentMessages) this._trace("_sentMessages ", t, this._sentMessages[t]);
            for (var t in this._receivedMessages) this._trace("_receivedMessages ", t, this._receivedMessages[t]);
            return this._traceBuffer
        }
    }, $.prototype.startTrace = function() {
        null === this._traceBuffer && (this._traceBuffer = []), this._trace("Client.startTrace", new Date, u)
    }, $.prototype.stopTrace = function() {
        delete this._traceBuffer
    }, $.prototype._doConnect = function(t) {
        if (this.connectOptions.useSSL) {
            var e = t.split(":");
            e[0] = "wss", t = e.join(":")
        }
        this.connected = !1, this.socket = this.connectOptions.mqttVersion < 4 ? new WebSocket(t, ["mqttv3.1"]) : new WebSocket(t, ["mqtt"]), this.socket.binaryType = "arraybuffer", this.socket.onopen = d(this._on_socket_open, this), this.socket.onmessage = d(this._on_socket_message, this), this.socket.onerror = d(this._on_socket_error, this), this.socket.onclose = d(this._on_socket_close, this), this.sendPinger = new _(this, window, this.connectOptions.keepAliveInterval), this.receivePinger = new _(this, window, this.connectOptions.keepAliveInterval), this._connectTimeout = new b(this, window, this.connectOptions.timeout, this._disconnected, [f.CONNECT_TIMEOUT.code, m(f.CONNECT_TIMEOUT)])
    }, $.prototype._schedule_message = function(t) {
        this._msg_queue.push(t), this.connected && this._process_queue()
    }, $.prototype.store = function(t, e) {
        var n = {
            type: e.type,
            messageIdentifier: e.messageIdentifier,
            version: 1
        };
        switch (e.type) {
            case c.PUBLISH:
                e.pubRecReceived && (n.pubRecReceived = !0), n.payloadMessage = {};
                for (var i = "", r = e.payloadMessage.payloadBytes, o = 0; o < r.length; o++) r[o] <= 15 ? i = i + "0" + r[o].toString(16) : i += r[o].toString(16);
                n.payloadMessage.payloadHex = i, n.payloadMessage.qos = e.payloadMessage.qos, n.payloadMessage.destinationName = e.payloadMessage.destinationName, e.payloadMessage.duplicate && (n.payloadMessage.duplicate = !0), e.payloadMessage.retained && (n.payloadMessage.retained = !0), 0 == t.indexOf("Sent:") && (void 0 === e.sequence && (e.sequence = ++this._sequence), n.sequence = e.sequence);
                break;
            default:
                throw Error(m(f.INVALID_STORED_DATA, [key, n]))
        }
        localStorage.setItem(t + this._localKey + e.messageIdentifier, JSON.stringify(n))
    }, $.prototype.restore = function(t) {
        var e = localStorage.getItem(t),
            n = JSON.parse(e),
            i = new y(n.type, n);
        switch (n.type) {
            case c.PUBLISH:
                for (var r = n.payloadMessage.payloadHex, o = new ArrayBuffer(r.length / 2), s = new Uint8Array(o), a = 0; r.length >= 2;) {
                    var l = parseInt(r.substring(0, 2), 16);
                    r = r.substring(2, r.length), s[a++] = l
                }
                var u = new Paho.MQTT.Message(s);
                u.qos = n.payloadMessage.qos, u.destinationName = n.payloadMessage.destinationName, n.payloadMessage.duplicate && (u.duplicate = !0), n.payloadMessage.retained && (u.retained = !0), i.payloadMessage = u;
                break;
            default:
                throw Error(m(f.INVALID_STORED_DATA, [t, e]))
        }
        0 == t.indexOf("Sent:" + this._localKey) ? (i.payloadMessage.duplicate = !0, this._sentMessages[i.messageIdentifier] = i) : 0 == t.indexOf("Received:" + this._localKey) && (this._receivedMessages[i.messageIdentifier] = i)
    }, $.prototype._process_queue = function() {
        for (var t = null, e = this._msg_queue.reverse(); t = e.pop();) this._socket_send(t), this._notify_msg_sent[t] && (this._notify_msg_sent[t](), delete this._notify_msg_sent[t])
    }, $.prototype._requires_ack = function(t) {
        var e = Object.keys(this._sentMessages).length;
        if (e > this.maxMessageIdentifier) throw Error("Too many messages:" + e);
        for (; void 0 !== this._sentMessages[this._message_identifier];) this._message_identifier++;
        t.messageIdentifier = this._message_identifier, this._sentMessages[t.messageIdentifier] = t, t.type === c.PUBLISH && this.store("Sent:", t), this._message_identifier === this.maxMessageIdentifier && (this._message_identifier = 1)
    }, $.prototype._on_socket_open = function() {
        var t = new y(c.CONNECT, this.connectOptions);
        t.clientId = this.clientId, this._socket_send(t)
    }, $.prototype._on_socket_message = function(t) {
        this._trace("Client._on_socket_message", t.data), this.receivePinger.reset();
        for (var e = this._deframeMessages(t.data), n = 0; n < e.length; n += 1) this._handleMessage(e[n])
    }, $.prototype._deframeMessages = function(t) {
        var n = new Uint8Array(t);
        if (this.receiveBuffer) {
            var i = new Uint8Array(this.receiveBuffer.length + n.length);
            i.set(this.receiveBuffer), i.set(n, this.receiveBuffer.length), n = i, delete this.receiveBuffer
        }
        try {
            for (var r = 0, o = []; r < n.length;) {
                var s = e(n, r),
                    a = s[0];
                if (r = s[1], null === a) break;
                o.push(a)
            }
            r < n.length && (this.receiveBuffer = n.subarray(r))
        } catch (l) {
            return void this._disconnected(f.INTERNAL_ERROR.code, m(f.INTERNAL_ERROR, [l.message, l.stack.toString()]))
        }
        return o
    }, $.prototype._handleMessage = function(t) {
        this._trace("Client._handleMessage", t);
        try {
            switch (t.type) {
                case c.CONNACK:
                    if (this._connectTimeout.cancel(), this.connectOptions.cleanSession) {
                        for (var e in this._sentMessages) {
                            var n = this._sentMessages[e];
                            localStorage.removeItem("Sent:" + this._localKey + n.messageIdentifier)
                        }
                        this._sentMessages = {};
                        for (var e in this._receivedMessages) {
                            var i = this._receivedMessages[e];
                            localStorage.removeItem("Received:" + this._localKey + i.messageIdentifier)
                        }
                        this._receivedMessages = {}
                    }
                    if (0 !== t.returnCode) {
                        this._disconnected(f.CONNACK_RETURNCODE.code, m(f.CONNACK_RETURNCODE, [t.returnCode, p[t.returnCode]]));
                        break
                    }
                    this.connected = !0, this.connectOptions.uris && (this.hostIndex = this.connectOptions.uris.length);
                    var r = new Array;
                    for (var o in this._sentMessages) this._sentMessages.hasOwnProperty(o) && r.push(this._sentMessages[o]);
                    for (var r = r.sort(function(t, e) {
                            return t.sequence - e.sequence
                        }), s = 0, a = r.length; a > s; s++) {
                        var n = r[s];
                        if (n.type == c.PUBLISH && n.pubRecReceived) {
                            var l = new y(c.PUBREL, {
                                messageIdentifier: n.messageIdentifier
                            });
                            this._schedule_message(l)
                        } else this._schedule_message(n)
                    }
                    this.connectOptions.onSuccess && this.connectOptions.onSuccess({
                        invocationContext: this.connectOptions.invocationContext
                    }), this._process_queue();
                    break;
                case c.PUBLISH:
                    this._receivePublish(t);
                    break;
                case c.PUBACK:
                    var n = this._sentMessages[t.messageIdentifier];
                    n && (delete this._sentMessages[t.messageIdentifier], localStorage.removeItem("Sent:" + this._localKey + t.messageIdentifier), this.onMessageDelivered && this.onMessageDelivered(n.payloadMessage));
                    break;
                case c.PUBREC:
                    var n = this._sentMessages[t.messageIdentifier];
                    if (n) {
                        n.pubRecReceived = !0;
                        var l = new y(c.PUBREL, {
                            messageIdentifier: t.messageIdentifier
                        });
                        this.store("Sent:", n), this._schedule_message(l)
                    }
                    break;
                case c.PUBREL:
                    var i = this._receivedMessages[t.messageIdentifier];
                    localStorage.removeItem("Received:" + this._localKey + t.messageIdentifier), i && (this._receiveMessage(i), delete this._receivedMessages[t.messageIdentifier]);
                    var u = new y(c.PUBCOMP, {
                        messageIdentifier: t.messageIdentifier
                    });
                    this._schedule_message(u);
                    break;
                case c.PUBCOMP:
                    var n = this._sentMessages[t.messageIdentifier];
                    delete this._sentMessages[t.messageIdentifier], localStorage.removeItem("Sent:" + this._localKey + t.messageIdentifier), this.onMessageDelivered && this.onMessageDelivered(n.payloadMessage);
                    break;
                case c.SUBACK:
                    var n = this._sentMessages[t.messageIdentifier];
                    n && (n.timeOut && n.timeOut.cancel(), 128 === t.returnCode[0] ? n.onFailure && n.onFailure(t.returnCode) : n.onSuccess && n.onSuccess(t.returnCode), delete this._sentMessages[t.messageIdentifier]);
                    break;
                case c.UNSUBACK:
                    var n = this._sentMessages[t.messageIdentifier];
                    n && (n.timeOut && n.timeOut.cancel(), n.callback && n.callback(), delete this._sentMessages[t.messageIdentifier]);
                    break;
                case c.PINGRESP:
                    this.sendPinger.reset();
                    break;
                case c.DISCONNECT:
                    this._disconnected(f.INVALID_MQTT_MESSAGE_TYPE.code, m(f.INVALID_MQTT_MESSAGE_TYPE, [t.type]));
                    break;
                default:
                    this._disconnected(f.INVALID_MQTT_MESSAGE_TYPE.code, m(f.INVALID_MQTT_MESSAGE_TYPE, [t.type]))
            }
        } catch (h) {
            return void this._disconnected(f.INTERNAL_ERROR.code, m(f.INTERNAL_ERROR, [h.message, h.stack.toString()]))
        }
    }, $.prototype._on_socket_error = function(t) {
        this._disconnected(f.SOCKET_ERROR.code, m(f.SOCKET_ERROR, [t.data]))
    }, $.prototype._on_socket_close = function() {
        this._disconnected(f.SOCKET_CLOSE.code, m(f.SOCKET_CLOSE))
    }, $.prototype._socket_send = function(t) {
        if (1 == t.type) {
            var e = this._traceMask(t, "password");
            this._trace("Client._socket_send", e)
        } else this._trace("Client._socket_send", t);
        this.socket.send(t.encode()), this.sendPinger.reset()
    }, $.prototype._receivePublish = function(t) {
        switch (t.payloadMessage.qos) {
            case "undefined":
            case 0:
                this._receiveMessage(t);
                break;
            case 1:
                var e = new y(c.PUBACK, {
                    messageIdentifier: t.messageIdentifier
                });
                this._schedule_message(e), this._receiveMessage(t);
                break;
            case 2:
                this._receivedMessages[t.messageIdentifier] = t, this.store("Received:", t);
                var n = new y(c.PUBREC, {
                    messageIdentifier: t.messageIdentifier
                });
                this._schedule_message(n);
                break;
            default:
                throw Error("Invaild qos=" + wireMmessage.payloadMessage.qos)
        }
    }, $.prototype._receiveMessage = function(t) {
        this.onMessageArrived && this.onMessageArrived(t.payloadMessage)
    }, $.prototype._disconnected = function(t, e) {
        this._trace("Client._disconnected", t, e), this.sendPinger.cancel(), this.receivePinger.cancel(), this._connectTimeout && this._connectTimeout.cancel(), this._msg_queue = [], this._notify_msg_sent = {}, this.socket && (this.socket.onopen = null, this.socket.onmessage = null, this.socket.onerror = null, this.socket.onclose = null, 1 === this.socket.readyState && this.socket.close(), delete this.socket), this.connectOptions.uris && this.hostIndex < this.connectOptions.uris.length - 1 ? (this.hostIndex++, this._doConnect(this.connectOptions.uris[this.hostIndex])) : (void 0 === t && (t = f.OK.code, e = m(f.OK)), this.connected ? (this.connected = !1, this.onConnectionLost && this.onConnectionLost({
            errorCode: t,
            errorMessage: e
        })) : 4 === this.connectOptions.mqttVersion && this.connectOptions.mqttVersionExplicit === !1 ? (this._trace("Failed to connect V4, dropping back to V3"), this.connectOptions.mqttVersion = 3, this.connectOptions.uris ? (this.hostIndex = 0, this._doConnect(this.connectOptions.uris[0])) : this._doConnect(this.uri)) : this.connectOptions.onFailure && this.connectOptions.onFailure({
            invocationContext: this.connectOptions.invocationContext,
            errorCode: t,
            errorMessage: e
        }))
    }, $.prototype._trace = function() {
        if (this.traceFunction) {
            for (var t in arguments) "undefined" != typeof arguments[t] && (arguments[t] = JSON.stringify(arguments[t]));
            var e = Array.prototype.slice.call(arguments).join("");
            this.traceFunction({
                severity: "Debug",
                message: e
            })
        }
        if (null !== this._traceBuffer)
            for (var t = 0, n = arguments.length; n > t; t++) this._traceBuffer.length == this._MAX_TRACE_ENTRIES && this._traceBuffer.shift(), this._traceBuffer.push(0 === t ? arguments[t] : "undefined" == typeof arguments[t] ? arguments[t] : "  " + JSON.stringify(arguments[t]))
    }, $.prototype._traceMask = function(t, e) {
        var n = {};
        for (var i in t) t.hasOwnProperty(i) && (n[i] = i == e ? "******" : t[i]);
        return n
    };
    var w = function(t, e, n, i) {
        var r;
        if ("string" != typeof t) throw new Error(m(f.INVALID_TYPE, [typeof t, "host"]));
        if (2 == arguments.length) {
            i = e, r = t;
            var o = r.match(/^(wss?):\/\/((\[(.+)\])|([^\/]+?))(:(\d+))?(\/.*)$/);
            if (!o) throw new Error(m(f.INVALID_ARGUMENT, [t, "host"]));
            t = o[4] || o[2], e = parseInt(o[7]), n = o[8]
        } else {
            if (3 == arguments.length && (i = n, n = "/mqtt"), "number" != typeof e || 0 > e) throw new Error(m(f.INVALID_TYPE, [typeof e, "port"]));
            if ("string" != typeof n) throw new Error(m(f.INVALID_TYPE, [typeof n, "path"]));
            var s = -1 != t.indexOf(":") && "[" != t.slice(0, 1) && "]" != t.slice(-1);
            r = "ws://" + (s ? "[" + t + "]" : t) + ":" + e + n
        }
        for (var a = 0, l = 0; l < i.length; l++) {
            var u = i.charCodeAt(l);
            u >= 55296 && 56319 >= u && l++, a++
        }
        if ("string" != typeof i || a > 65535) throw new Error(m(f.INVALID_ARGUMENT, [i, "clientId"]));
        var c = new $(r, t, e, n, i);
        this._getHost = function() {
            return t
        }, this._setHost = function() {
            throw new Error(m(f.UNSUPPORTED_OPERATION))
        }, this._getPort = function() {
            return e
        }, this._setPort = function() {
            throw new Error(m(f.UNSUPPORTED_OPERATION))
        }, this._getPath = function() {
            return n
        }, this._setPath = function() {
            throw new Error(m(f.UNSUPPORTED_OPERATION))
        }, this._getURI = function() {
            return r
        }, this._setURI = function() {
            throw new Error(m(f.UNSUPPORTED_OPERATION))
        }, this._getClientId = function() {
            return c.clientId
        }, this._setClientId = function() {
            throw new Error(m(f.UNSUPPORTED_OPERATION))
        }, this._getOnConnectionLost = function() {
            return c.onConnectionLost
        }, this._setOnConnectionLost = function(t) {
            if ("function" != typeof t) throw new Error(m(f.INVALID_TYPE, [typeof t, "onConnectionLost"]));
            c.onConnectionLost = t
        }, this._getOnMessageDelivered = function() {
            return c.onMessageDelivered
        }, this._setOnMessageDelivered = function(t) {
            if ("function" != typeof t) throw new Error(m(f.INVALID_TYPE, [typeof t, "onMessageDelivered"]));
            c.onMessageDelivered = t
        }, this._getOnMessageArrived = function() {
            return c.onMessageArrived
        }, this._setOnMessageArrived = function(t) {
            if ("function" != typeof t) throw new Error(m(f.INVALID_TYPE, [typeof t, "onMessageArrived"]));
            c.onMessageArrived = t
        }, this._getTrace = function() {
            return c.traceFunction
        }, this._setTrace = function(t) {
            if ("function" != typeof t) throw new Error(m(f.INVALID_TYPE, [typeof t, "onTrace"]));
            c.traceFunction = t
        }, this.connect = function(t) {
            if (t = t || {}, h(t, {
                    timeout: "number",
                    userName: "string",
                    password: "string",
                    willMessage: "object",
                    keepAliveInterval: "number",
                    cleanSession: "boolean",
                    useSSL: "boolean",
                    invocationContext: "object",
                    onSuccess: "function",
                    onFailure: "function",
                    hosts: "object",
                    ports: "object",
                    mqttVersion: "number"
                }), void 0 === t.keepAliveInterval && (t.keepAliveInterval = 60), t.mqttVersion > 4 || t.mqttVersion < 3) throw new Error(m(f.INVALID_ARGUMENT, [t.mqttVersion, "connectOptions.mqttVersion"]));
            if (void 0 === t.mqttVersion ? (t.mqttVersionExplicit = !1, t.mqttVersion = 4) : t.mqttVersionExplicit = !0, void 0 === t.password && void 0 !== t.userName) throw new Error(m(f.INVALID_ARGUMENT, [t.password, "connectOptions.password"]));
            if (t.willMessage) {
                if (!(t.willMessage instanceof x)) throw new Error(m(f.INVALID_TYPE, [t.willMessage, "connectOptions.willMessage"]));
                if (t.willMessage.stringPayload, "undefined" == typeof t.willMessage.destinationName) throw new Error(m(f.INVALID_TYPE, [typeof t.willMessage.destinationName, "connectOptions.willMessage.destinationName"]))
            }
            if ("undefined" == typeof t.cleanSession && (t.cleanSession = !0), t.hosts) {
                if (!(t.hosts instanceof Array)) throw new Error(m(f.INVALID_ARGUMENT, [t.hosts, "connectOptions.hosts"]));
                if (t.hosts.length < 1) throw new Error(m(f.INVALID_ARGUMENT, [t.hosts, "connectOptions.hosts"]));
                for (var e = !1, i = 0; i < t.hosts.length; i++) {
                    if ("string" != typeof t.hosts[i]) throw new Error(m(f.INVALID_TYPE, [typeof t.hosts[i], "connectOptions.hosts[" + i + "]"]));
                    if (/^(wss?):\/\/((\[(.+)\])|([^\/]+?))(:(\d+))?(\/.*)$/.test(t.hosts[i])) {
                        if (0 == i) e = !0;
                        else if (!e) throw new Error(m(f.INVALID_ARGUMENT, [t.hosts[i], "connectOptions.hosts[" + i + "]"]))
                    } else if (e) throw new Error(m(f.INVALID_ARGUMENT, [t.hosts[i], "connectOptions.hosts[" + i + "]"]))
                }
                if (e) t.uris = t.hosts;
                else {
                    if (!t.ports) throw new Error(m(f.INVALID_ARGUMENT, [t.ports, "connectOptions.ports"]));
                    if (!(t.ports instanceof Array)) throw new Error(m(f.INVALID_ARGUMENT, [t.ports, "connectOptions.ports"]));
                    if (t.hosts.length != t.ports.length) throw new Error(m(f.INVALID_ARGUMENT, [t.ports, "connectOptions.ports"]));
                    t.uris = [];
                    for (var i = 0; i < t.hosts.length; i++) {
                        if ("number" != typeof t.ports[i] || t.ports[i] < 0) throw new Error(m(f.INVALID_TYPE, [typeof t.ports[i], "connectOptions.ports[" + i + "]"]));
                        var o = t.hosts[i],
                            s = t.ports[i],
                            a = -1 != o.indexOf(":");
                        r = "ws://" + (a ? "[" + o + "]" : o) + ":" + s + n, t.uris.push(r)
                    }
                }
            }
            c.connect(t)
        }, this.subscribe = function(t, e) {
            if ("string" != typeof t) throw new Error("Invalid argument:" + t);
            if (e = e || {}, h(e, {
                    qos: "number",
                    invocationContext: "object",
                    onSuccess: "function",
                    onFailure: "function",
                    timeout: "number"
                }), e.timeout && !e.onFailure) throw new Error("subscribeOptions.timeout specified with no onFailure callback.");
            if ("undefined" != typeof e.qos && 0 !== e.qos && 1 !== e.qos && 2 !== e.qos) throw new Error(m(f.INVALID_ARGUMENT, [e.qos, "subscribeOptions.qos"]));
            c.subscribe(t, e)
        }, this.unsubscribe = function(t, e) {
            if ("string" != typeof t) throw new Error("Invalid argument:" + t);
            if (e = e || {}, h(e, {
                    invocationContext: "object",
                    onSuccess: "function",
                    onFailure: "function",
                    timeout: "number"
                }), e.timeout && !e.onFailure) throw new Error("unsubscribeOptions.timeout specified with no onFailure callback.");
            c.unsubscribe(t, e)
        }, this.send = function(t, e, n, i) {
            var r;
            if (0 == arguments.length) throw new Error("Invalid argument.length");
            if (1 == arguments.length) {
                if (!(t instanceof x) && "string" != typeof t) throw new Error("Invalid argument:" + typeof t);
                if (r = t, "undefined" == typeof r.destinationName) throw new Error(m(f.INVALID_ARGUMENT, [r.destinationName, "Message.destinationName"]));
                c.send(r)
            } else r = new x(e), r.destinationName = t, arguments.length >= 3 && (r.qos = n), arguments.length >= 4 && (r.retained = i), c.send(r)
        }, this.disconnect = function() {
            c.disconnect()
        }, this.getTraceLog = function() {
            return c.getTraceLog()
        }, this.startTrace = function() {
            c.startTrace()
        }, this.stopTrace = function() {
            c.stopTrace()
        }, this.isConnected = function() {
            return c.connected
        }
    };
    w.prototype = {
        get host() {
            return this._getHost()
        },
        set host(t) {
            this._setHost(t)
        },
        get port() {
            return this._getPort()
        },
        set port(t) {
            this._setPort(t)
        },
        get path() {
            return this._getPath()
        },
        set path(t) {
            this._setPath(t)
        },
        get clientId() {
            return this._getClientId()
        },
        set clientId(t) {
            this._setClientId(t)
        },
        get onConnectionLost() {
            return this._getOnConnectionLost()
        },
        set onConnectionLost(t) {
            this._setOnConnectionLost(t)
        },
        get onMessageDelivered() {
            return this._getOnMessageDelivered()
        },
        set onMessageDelivered(t) {
            this._setOnMessageDelivered(t)
        },
        get onMessageArrived() {
            return this._getOnMessageArrived()
        },
        set onMessageArrived(t) {
            this._setOnMessageArrived(t)
        },
        get trace() {
            return this._getTrace()
        },
        set trace(t) {
            this._setTrace(t)
        }
    };
    var x = function(t) {
        var e;
        if (!("string" == typeof t || t instanceof ArrayBuffer || t instanceof Int8Array || t instanceof Uint8Array || t instanceof Int16Array || t instanceof Uint16Array || t instanceof Int32Array || t instanceof Uint32Array || t instanceof Float32Array || t instanceof Float64Array)) throw m(f.INVALID_ARGUMENT, [t, "newPayload"]);
        e = t, this._getPayloadString = function() {
            return "string" == typeof e ? e : l(e, 0, e.length)
        }, this._getPayloadBytes = function() {
            if ("string" == typeof e) {
                var t = new ArrayBuffer(s(e)),
                    n = new Uint8Array(t);
                return a(e, n, 0), n
            }
            return e
        };
        var n = void 0;
        this._getDestinationName = function() {
            return n
        }, this._setDestinationName = function(t) {
            if ("string" != typeof t) throw new Error(m(f.INVALID_ARGUMENT, [t, "newDestinationName"]));
            n = t
        };
        var i = 0;
        this._getQos = function() {
            return i
        }, this._setQos = function(t) {
            if (0 !== t && 1 !== t && 2 !== t) throw new Error("Invalid argument:" + t);
            i = t
        };
        var r = !1;
        this._getRetained = function() {
            return r
        }, this._setRetained = function(t) {
            if ("boolean" != typeof t) throw new Error(m(f.INVALID_ARGUMENT, [t, "newRetained"]));
            r = t
        };
        var o = !1;
        this._getDuplicate = function() {
            return o
        }, this._setDuplicate = function(t) {
            o = t
        }
    };
    return x.prototype = {
        get payloadString() {
            return this._getPayloadString()
        },
        get payloadBytes() {
            return this._getPayloadBytes()
        },
        get destinationName() {
            return this._getDestinationName()
        },
        set destinationName(t) {
            this._setDestinationName(t)
        },
        get qos() {
            return this._getQos()
        },
        set qos(t) {
            this._setQos(t)
        },
        get retained() {
            return this._getRetained()
        },
        set retained(t) {
            this._setRetained(t)
        },
        get duplicate() {
            return this._getDuplicate()
        },
        set duplicate(t) {
            this._setDuplicate(t)
        }
    }, {
        Client: w,
        Message: x
    }
}(window);
