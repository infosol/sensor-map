! function() {
    "use strict";

    function t(t, e, n, i, o, r, s) {
        function a(t) {
            if (1 !== arguments.length || t)
                if (t) d(t.toastId);
                else
                    for (var e = 0; e < _.length; e++) d(_[e].toastId)
        }

        function l(t, e, n) {
            var i = f().iconClasses.error;
            return p(i, t, e, n)
        }

        function u(t, e, n) {
            var i = f().iconClasses.info;
            return p(i, t, e, n)
        }

        function c(t, e, n) {
            var i = f().iconClasses.success;
            return p(i, t, e, n)
        }

        function h(t, e, n) {
            var i = f().iconClasses.warning;
            return p(i, t, e, n)
        }

        function d(e, n) {
            function i(t) {
                for (var e = 0; e < _.length; e++)
                    if (_[e].toastId === t) return _[e]
            }

            function o() {
                return !_.length
            }
            var a = i(e);
            a && !a.deleting && (a.deleting = !0, a.isOpened = !1, t.leave(a.el).then(function() {
                a.scope.options.onHidden && a.scope.options.onHidden(!!n, a), a.scope.$destroy();
                var t = _.indexOf(a);
                delete b[a.scope.message], _.splice(t, 1);
                var e = r.maxOpened;
                e && _.length >= e && _[e - 1].open.resolve(), o() && (v.remove(), v = null, w = s.defer())
            }))
        }

        function p(t, e, n, i) {
            return angular.isObject(n) && (i = n, n = null), g({
                iconClass: t,
                message: e,
                optionsOverride: i,
                title: n
            })
        }

        function f() {
            return angular.extend({}, r)
        }

        function m(e) {
            if (v) return w.promise;
            v = angular.element("<div></div>"), v.attr("id", e.containerId), v.addClass(e.positionClass), v.css({
                "pointer-events": "auto"
            });
            var n = angular.element(document.querySelector(e.target));
            if (!n || !n.length) throw "Target for toasts doesn't exist";
            return t.enter(v, n).then(function() {
                w.resolve()
            }), w.promise
        }

        function g(n) {
            function r() {
                return p.autoDismiss && p.maxOpened && _.length > p.maxOpened
            }

            function a(t, e, n) {
                function i(e) {
                    return n[e] ? function() {
                        n[e](t)
                    } : void 0
                }
                n.allowHtml ? (t.scope.allowHtml = !0, t.scope.title = o.trustAsHtml(e.title), t.scope.message = o.trustAsHtml(e.message)) : (t.scope.title = e.title, t.scope.message = e.message), t.scope.toastType = t.iconClass, t.scope.toastId = t.toastId, t.scope.extraData = n.extraData, t.scope.options = {
                    extendedTimeOut: n.extendedTimeOut,
                    messageClass: n.messageClass,
                    onHidden: n.onHidden,
                    onShown: i("onShown"),
                    onTap: i("onTap"),
                    progressBar: n.progressBar,
                    tapToDismiss: n.tapToDismiss,
                    timeOut: n.timeOut,
                    titleClass: n.titleClass,
                    toastClass: n.toastClass
                }, n.closeButton && (t.scope.options.closeHtml = n.closeHtml)
            }

            function l() {
                function t(t) {
                    for (var e = ["containerId", "iconClasses", "maxOpened", "newestOnTop", "positionClass", "preventDuplicates", "preventOpenDuplicates", "templates"], n = 0, i = e.length; i > n; n++) delete t[e[n]];
                    return t
                }
                var e = {
                    toastId: y++,
                    isOpened: !1,
                    scope: i.$new(),
                    open: s.defer()
                };
                return e.iconClass = n.iconClass, n.optionsOverride && (angular.extend(p, t(n.optionsOverride)), e.iconClass = n.optionsOverride.iconClass || e.iconClass), a(e, n, p), e.el = u(e.scope), e
            }

            function u(t) {
                var n = angular.element("<div toast></div>"),
                    i = e.get("$compile");
                return i(n)(t)
            }

            function c() {
                return p.maxOpened && _.length <= p.maxOpened || !p.maxOpened
            }

            function h() {
                var t = p.preventDuplicates && n.message === $,
                    e = p.preventOpenDuplicates && b[n.message];
                return t || e ? !0 : ($ = n.message, b[n.message] = !0, !1)
            }
            var p = f();
            if (!h()) {
                var g = l();
                if (_.push(g), r())
                    for (var w = _.slice(0, _.length - p.maxOpened), x = 0, L = w.length; L > x; x++) d(w[x].toastId);
                return c() && g.open.resolve(), g.open.promise.then(function() {
                    m(p).then(function() {
                        if (g.isOpened = !0, p.newestOnTop) t.enter(g.el, v).then(function() {
                            g.scope.init()
                        });
                        else {
                            var e = v[0].lastChild ? angular.element(v[0].lastChild) : null;
                            t.enter(g.el, v, e).then(function() {
                                g.scope.init()
                            })
                        }
                    })
                }), g
            }
        }
        var v, y = 0,
            _ = [],
            $ = "",
            b = {},
            w = s.defer(),
            x = {
                clear: a,
                error: l,
                info: u,
                remove: d,
                success: c,
                warning: h
            };
        return x
    }
    angular.module("toastr", []).factory("toastr", t), t.$inject = ["$animate", "$injector", "$document", "$rootScope", "$sce", "toastrConfig", "$q"]
}(),
function() {
    "use strict";
    angular.module("toastr").constant("toastrConfig", {
        allowHtml: !1,
        autoDismiss: !1,
        closeButton: !1,
        closeHtml: "<button>&times;</button>",
        containerId: "toast-container",
        extendedTimeOut: 1e3,
        iconClasses: {
            error: "toast-error",
            info: "toast-info",
            success: "toast-success",
            warning: "toast-warning"
        },
        maxOpened: 0,
        messageClass: "toast-message",
        newestOnTop: !0,
        onHidden: null,
        onShown: null,
        onTap: null,
        positionClass: "toast-top-right",
        preventDuplicates: !1,
        preventOpenDuplicates: !1,
        progressBar: !1,
        tapToDismiss: !0,
        target: "body",
        templates: {
            toast: "directives/toast/toast.html",
            progressbar: "directives/progressbar/progressbar.html"
        },
        timeOut: 5e3,
        titleClass: "toast-title",
        toastClass: "toast"
    })
}(),
function() {
    "use strict";

    function t(t) {
        function e(t, e, n, i) {
            function o() {
                var t = (a - (new Date).getTime()) / s * 100;
                e.css("width", t + "%")
            }
            var r, s, a;
            i.progressBar = t, t.start = function(t) {
                r && clearInterval(r), s = parseFloat(t), a = (new Date).getTime() + s, r = setInterval(o, 10)
            }, t.stop = function() {
                r && clearInterval(r)
            }, t.$on("$destroy", function() {
                clearInterval(r)
            })
        }
        return {
            replace: !0,
            require: "^toast",
            templateUrl: function() {
                return t.templates.progressbar
            },
            link: e
        }
    }
    angular.module("toastr").directive("progressBar", t), t.$inject = ["toastrConfig"]
}(),
function() {
    "use strict";

    function t() {
        this.progressBar = null, this.startProgressBar = function(t) {
            this.progressBar && this.progressBar.start(t)
        }, this.stopProgressBar = function() {
            this.progressBar && this.progressBar.stop()
        }
    }
    angular.module("toastr").controller("ToastController", t)
}(),
function() {
    "use strict";

    function t(t, e, n, i) {
        function o(n, o, r, s) {
            function a(t) {
                return s.startProgressBar(t), e(function() {
                    s.stopProgressBar(), i.remove(n.toastId)
                }, t, 1)
            }

            function l() {
                n.progressBar = !1, s.stopProgressBar()
            }

            function u() {
                return n.options.closeHtml
            }
            var c;
            if (n.toastClass = n.options.toastClass, n.titleClass = n.options.titleClass, n.messageClass = n.options.messageClass, n.progressBar = n.options.progressBar, u()) {
                var h = angular.element(n.options.closeHtml),
                    d = t.get("$compile");
                h.addClass("toast-close-button"), h.attr("ng-click", "close(true, $event)"), d(h)(n), o.prepend(h)
            }
            n.init = function() {
                n.options.timeOut && (c = a(n.options.timeOut)), n.options.onShown && n.options.onShown()
            }, o.on("mouseenter", function() {
                l(), c && e.cancel(c)
            }), n.tapToast = function() {
                angular.isFunction(n.options.onTap) && n.options.onTap(), n.options.tapToDismiss && n.close(!0)
            }, n.close = function(t, e) {
                e && angular.isFunction(e.stopPropagation) && e.stopPropagation(), i.remove(n.toastId, t)
            }, o.on("mouseleave", function() {
                (0 !== n.options.timeOut || 0 !== n.options.extendedTimeOut) && (n.$apply(function() {
                    n.progressBar = n.options.progressBar
                }), c = a(n.options.extendedTimeOut))
            })
        }
        return {
            replace: !0,
            templateUrl: function() {
                return n.templates.toast
            },
            controller: "ToastController",
            link: o
        }
    }
    angular.module("toastr").directive("toast", t), t.$inject = ["$injector", "$interval", "toastrConfig", "toastr"]
}(), angular.module("toastr").run(["$templateCache", function(t) {
    t.put("directives/progressbar/progressbar.html", '<div class="toast-progress"></div>\n'), t.put("directives/toast/toast.html", '<div class="{{toastClass}} {{toastType}}" ng-click="tapToast()">\n  <div ng-switch on="allowHtml">\n    <div ng-switch-default ng-if="title" class="{{titleClass}}" aria-label="{{title}}">{{title}}</div>\n    <div ng-switch-default class="{{messageClass}}" aria-label="{{message}}">{{message}}</div>\n    <div ng-switch-when="true" ng-if="title" class="{{titleClass}}" ng-bind-html="title"></div>\n    <div ng-switch-when="true" class="{{messageClass}}" ng-bind-html="message"></div>\n  </div>\n  <progress-bar ng-if="progressBar"></progress-bar>\n</div>\n')
}]);
