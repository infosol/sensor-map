! function(t, e) {
    "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? module.exports = e() : t.returnExports = e()
}(this, function() {
    "use strict";
    var t, e = Function.call.bind(Function.apply),
        n = Function.call.bind(Function.call),
        i = Array.isArray,
        r = function(t) {
            return function() {
                return !e(t, this, arguments)
            }
        },
        o = function(t) {
            try {
                return t(), !1
            } catch (e) {
                return !0
            }
        },
        a = function(t) {
            try {
                return t()
            } catch (e) {
                return !1
            }
        },
        s = r(o),
        l = function() {
            return !o(function() {
                Object.defineProperty({}, "x", {
                    get: function() {}
                })
            })
        },
        u = !!Object.defineProperty && l(),
        c = "foo" === function() {}.name,
        h = Function.call.bind(Array.prototype.forEach),
        d = Function.call.bind(Array.prototype.reduce),
        p = Function.call.bind(Array.prototype.filter),
        f = Function.call.bind(Array.prototype.some),
        m = function(t, e, n, i) {
            !i && e in t || (u ? Object.defineProperty(t, e, {
                configurable: !0,
                enumerable: !1,
                writable: !0,
                value: n
            }) : t[e] = n)
        },
        g = function(t, e) {
            h(Object.keys(e), function(n) {
                var i = e[n];
                m(t, n, i, !1)
            })
        },
        v = Object.create || function(t, e) {
            var n = function() {};
            n.prototype = t;
            var i = new n;
            return "undefined" != typeof e && Object.keys(e).forEach(function(t) {
                Z.defineByDescriptor(i, t, e[t])
            }), i
        },
        y = function(t, e) {
            return Object.setPrototypeOf ? a(function() {
                var n = function i(e) {
                    var n = new t(e);
                    return Object.setPrototypeOf(n, i.prototype), n
                };
                return Object.setPrototypeOf(n, t), n.prototype = v(t.prototype, {
                    constructor: {
                        value: n
                    }
                }), e(n)
            }) : !1
        },
        _ = function() {
            if ("undefined" != typeof self) return self;
            if ("undefined" != typeof window) return window;
            if ("undefined" != typeof global) return global;
            throw new Error("unable to locate global object")
        },
        b = _(),
        $ = b.isFinite,
        w = Function.call.bind(String.prototype.indexOf),
        x = Function.call.bind(Object.prototype.toString),
        L = Function.call.bind(Array.prototype.concat),
        C = Function.call.bind(String.prototype.slice),
        S = Function.call.bind(Array.prototype.push),
        E = Function.apply.bind(Array.prototype.push),
        k = Function.call.bind(Array.prototype.shift),
        P = Math.max,
        T = Math.min,
        M = Math.floor,
        D = Math.abs,
        O = Math.log,
        A = Math.sqrt,
        I = Function.call.bind(Object.prototype.hasOwnProperty),
        N = function() {},
        R = b.Symbol || {},
        U = R.species || "@@species",
        j = Number.isNaN || function(t) {
            return t !== t
        },
        B = Number.isFinite || function(t) {
            return "number" == typeof t && $(t)
        },
        F = function(t) {
            return "[object Arguments]" === x(t)
        },
        z = function(t) {
            return null !== t && "object" == typeof t && "number" == typeof t.length && t.length >= 0 && "[object Array]" !== x(t) && "[object Function]" === x(t.callee)
        },
        H = F(arguments) ? F : z,
        q = {
            primitive: function(t) {
                return null === t || "function" != typeof t && "object" != typeof t
            },
            object: function(t) {
                return null !== t && "object" == typeof t
            },
            string: function(t) {
                return "[object String]" === x(t)
            },
            regex: function(t) {
                return "[object RegExp]" === x(t)
            },
            symbol: function(t) {
                return "function" == typeof b.Symbol && "symbol" == typeof t
            }
        },
        V = q.symbol(R.iterator) ? R.iterator : "_es6-shim iterator_";
    b.Set && "function" == typeof(new b.Set)["@@iterator"] && (V = "@@iterator"), b.Reflect || m(b, "Reflect", {});
    var W = b.Reflect,
        G = {
            Call: function(t, n) {
                var i = arguments.length > 2 ? arguments[2] : [];
                if (!G.IsCallable(t)) throw new TypeError(t + " is not a function");
                return e(t, n, i)
            },
            RequireObjectCoercible: function(t, e) {
                if (null == t) throw new TypeError(e || "Cannot call method on " + t)
            },
            TypeIsObject: function(t) {
                return null != t && Object(t) === t
            },
            ToObject: function(t, e) {
                return G.RequireObjectCoercible(t, e), Object(t)
            },
            IsCallable: function(t) {
                return "function" == typeof t && "[object Function]" === x(t)
            },
            IsConstructor: function(t) {
                return G.IsCallable(t)
            },
            ToInt32: function(t) {
                return G.ToNumber(t) >> 0
            },
            ToUint32: function(t) {
                return G.ToNumber(t) >>> 0
            },
            ToNumber: function(t) {
                if ("[object Symbol]" === x(t)) throw new TypeError("Cannot convert a Symbol value to a number");
                return +t
            },
            ToInteger: function(t) {
                var e = G.ToNumber(t);
                return j(e) ? 0 : 0 !== e && B(e) ? (e > 0 ? 1 : -1) * M(D(e)) : e
            },
            ToLength: function(t) {
                var e = G.ToInteger(t);
                return 0 >= e ? 0 : e > Number.MAX_SAFE_INTEGER ? Number.MAX_SAFE_INTEGER : e
            },
            SameValue: function(t, e) {
                return t === e ? 0 === t ? 1 / t === 1 / e : !0 : j(t) && j(e)
            },
            SameValueZero: function(t, e) {
                return t === e || j(t) && j(e)
            },
            IsIterable: function(t) {
                return G.TypeIsObject(t) && ("undefined" != typeof t[V] || H(t))
            },
            GetIterator: function(e) {
                if (H(e)) return new t(e, "value");
                var i = G.GetMethod(e, V);
                if (!G.IsCallable(i)) throw new TypeError("value is not an iterable");
                var r = n(i, e);
                if (!G.TypeIsObject(r)) throw new TypeError("bad iterator");
                return r
            },
            GetMethod: function(t, e) {
                var n = G.ToObject(t)[e];
                if (void 0 === n || null === n) return void 0;
                if (!G.IsCallable(n)) throw new TypeError("Method not callable: " + e);
                return n
            },
            IteratorComplete: function(t) {
                return !!t.done
            },
            IteratorClose: function(t, e) {
                var i = G.GetMethod(t, "return");
                if (void 0 !== i) {
                    var r, o;
                    try {
                        r = n(i, t)
                    } catch (a) {
                        o = a
                    }
                    if (!e) {
                        if (o) throw o;
                        if (!G.TypeIsObject(r)) throw new TypeError("Iterator's return method returned a non-object.")
                    }
                }
            },
            IteratorNext: function(t) {
                var e = arguments.length > 1 ? t.next(arguments[1]) : t.next();
                if (!G.TypeIsObject(e)) throw new TypeError("bad iterator");
                return e
            },
            IteratorStep: function(t) {
                var e = G.IteratorNext(t),
                    n = G.IteratorComplete(e);
                return n ? !1 : e
            },
            Construct: function(t, e, n, i) {
                var r = "undefined" == typeof n ? t : n;
                if (!i) return W.construct(t, e, r);
                var o = r.prototype;
                G.TypeIsObject(o) || (o = Object.prototype);
                var a = v(o),
                    s = G.Call(t, a, e);
                return G.TypeIsObject(s) ? s : a
            },
            SpeciesConstructor: function(t, e) {
                var n = t.constructor;
                if (void 0 === n) return e;
                if (!G.TypeIsObject(n)) throw new TypeError("Bad constructor");
                var i = n[U];
                if (void 0 === i || null === i) return e;
                if (!G.IsConstructor(i)) throw new TypeError("Bad @@species");
                return i
            },
            CreateHTML: function(t, e, n, i) {
                var r = String(t),
                    o = "<" + e;
                if ("" !== n) {
                    var a = String(i),
                        s = a.replace(/"/g, "&quot;");
                    o += " " + n + '="' + s + '"'
                }
                var l = o + ">",
                    u = l + r;
                return u + "</" + e + ">"
            }
        },
        Z = {
            getter: function(t, e, n) {
                if (!u) throw new TypeError("getters require true ES5 support");
                Object.defineProperty(t, e, {
                    configurable: !0,
                    enumerable: !1,
                    get: n
                })
            },
            proxy: function(t, e, n) {
                if (!u) throw new TypeError("getters require true ES5 support");
                var i = Object.getOwnPropertyDescriptor(t, e);
                Object.defineProperty(n, e, {
                    configurable: i.configurable,
                    enumerable: i.enumerable,
                    get: function() {
                        return t[e]
                    },
                    set: function(n) {
                        t[e] = n
                    }
                })
            },
            redefine: function(t, e, n) {
                if (u) {
                    var i = Object.getOwnPropertyDescriptor(t, e);
                    i.value = n, Object.defineProperty(t, e, i)
                } else t[e] = n
            },
            defineByDescriptor: function(t, e, n) {
                u ? Object.defineProperty(t, e, n) : "value" in n && (t[e] = n.value)
            },
            preserveToString: function(t, e) {
                e && G.IsCallable(e.toString) && m(t, "toString", e.toString.bind(e), !0)
            }
        },
        Y = function(t, e, n) {
            Z.preserveToString(e, t), Object.setPrototypeOf && Object.setPrototypeOf(t, e), u ? h(Object.getOwnPropertyNames(t), function(i) {
                i in N || n[i] || Z.proxy(t, i, e)
            }) : h(Object.keys(t), function(i) {
                i in N || n[i] || (e[i] = t[i])
            }), e.prototype = t.prototype, Z.redefine(t.prototype, "constructor", e)
        },
        J = function() {
            return this
        },
        K = function(t) {
            u && !I(t, U) && Z.getter(t, U, J)
        },
        X = function(t, e, n) {
            var i = t[e];
            m(t, e, n, !0), Z.preserveToString(t[e], i)
        },
        Q = function(t, e) {
            var n = e || function() {
                return this
            };
            m(t, V, n), !t[V] && q.symbol(V) && (t[V] = n)
        },
        te = function(t, e, n) {
            u ? Object.defineProperty(t, e, {
                configurable: !0,
                enumerable: !0,
                writable: !0,
                value: n
            }) : t[e] = n
        },
        ee = function(t, e, n) {
            if (te(t, e, n), !G.SameValue(t[e], n)) throw new TypeError("property is nonconfigurable")
        },
        ne = function(t, e, n, i) {
            if (!G.TypeIsObject(t)) throw new TypeError("Constructor requires `new`: " + e.name);
            var r = e.prototype;
            G.TypeIsObject(r) || (r = n);
            var o = v(r);
            for (var a in i)
                if (I(i, a)) {
                    var s = i[a];
                    m(o, a, s, !0)
                } return o
        };
    if (String.fromCodePoint && 1 !== String.fromCodePoint.length) {
        var ie = String.fromCodePoint;
        X(String, "fromCodePoint", function() {
            return e(ie, this, arguments)
        })
    }
    var re = {
        fromCodePoint: function() {
            for (var t, e = [], n = 0, i = arguments.length; i > n; n++) {
                if (t = Number(arguments[n]), !G.SameValue(t, G.ToInteger(t)) || 0 > t || t > 1114111) throw new RangeError("Invalid code point " + t);
                65536 > t ? S(e, String.fromCharCode(t)) : (t -= 65536, S(e, String.fromCharCode((t >> 10) + 55296)), S(e, String.fromCharCode(t % 1024 + 56320)))
            }
            return e.join("")
        },
        raw: function(t) {
            var e = G.ToObject(t, "bad callSite"),
                n = G.ToObject(e.raw, "bad raw value"),
                i = n.length,
                r = G.ToLength(i);
            if (0 >= r) return "";
            for (var o, a, s, l, u = [], c = 0; r > c && (o = String(c), s = String(n[o]), S(u, s), !(c + 1 >= r));) a = c + 1 < arguments.length ? arguments[c + 1] : "", l = String(a), S(u, l), c += 1;
            return u.join("")
        }
    };
    String.raw && "xy" !== String.raw({
        raw: {
            0: "x",
            1: "y",
            length: 2
        }
    }) && X(String, "raw", re.raw), g(String, re);
    var oe = function Hi(t, e) {
            if (1 > e) return "";
            if (e % 2) return Hi(t, e - 1) + t;
            var n = Hi(t, e / 2);
            return n + n
        },
        ae = 1 / 0,
        se = {
            repeat: function(t) {
                G.RequireObjectCoercible(this);
                var e = String(this),
                    n = G.ToInteger(t);
                if (0 > n || n >= ae) throw new RangeError("repeat count must be less than infinity and not overflow maximum string size");
                return oe(e, n)
            },
            startsWith: function(t) {
                G.RequireObjectCoercible(this);
                var e = String(this);
                if (q.regex(t)) throw new TypeError('Cannot call method "startsWith" with a regex');
                var n = String(t),
                    i = arguments.length > 1 ? arguments[1] : void 0,
                    r = P(G.ToInteger(i), 0);
                return C(e, r, r + n.length) === n
            },
            endsWith: function(t) {
                G.RequireObjectCoercible(this);
                var e = String(this);
                if (q.regex(t)) throw new TypeError('Cannot call method "endsWith" with a regex');
                var n = String(t),
                    i = e.length,
                    r = arguments.length > 1 ? arguments[1] : void 0,
                    o = "undefined" == typeof r ? i : G.ToInteger(r),
                    a = T(P(o, 0), i);
                return C(e, a - n.length, a) === n
            },
            includes: function(t) {
                if (q.regex(t)) throw new TypeError('"includes" does not accept a RegExp');
                var e;
                return arguments.length > 1 && (e = arguments[1]), -1 !== w(this, t, e)
            },
            codePointAt: function(t) {
                G.RequireObjectCoercible(this);
                var e = String(this),
                    n = G.ToInteger(t),
                    i = e.length;
                if (n >= 0 && i > n) {
                    var r = e.charCodeAt(n),
                        o = n + 1 === i;
                    if (55296 > r || r > 56319 || o) return r;
                    var a = e.charCodeAt(n + 1);
                    return 56320 > a || a > 57343 ? r : 1024 * (r - 55296) + (a - 56320) + 65536
                }
            }
        };
    if (String.prototype.includes && "a".includes("a", 1 / 0) !== !1 && X(String.prototype, "includes", se.includes), String.prototype.startsWith && String.prototype.endsWith) {
        var le = o(function() {
                "/a/".startsWith(/a/)
            }),
            ue = "abc".startsWith("a", 1 / 0) === !1;
        le && ue || (X(String.prototype, "startsWith", se.startsWith), X(String.prototype, "endsWith", se.endsWith))
    }
    g(String.prototype, se);
    var ce = ["	\n\f\r   ᠎    ", "         　\u2028", "\u2029﻿"].join(""),
        he = new RegExp("(^[" + ce + "]+)|([" + ce + "]+$)", "g"),
        de = function() {
            if ("undefined" == typeof this || null === this) throw new TypeError("can't convert " + this + " to object");
            return String(this).replace(he, "")
        },
        pe = ["", "​", "￾"].join(""),
        fe = new RegExp("[" + pe + "]", "g"),
        me = /^[\-+]0x[0-9a-f]+$/i,
        ge = pe.trim().length !== pe.length;
    m(String.prototype, "trim", de, ge);
    var ve = function(t) {
        G.RequireObjectCoercible(t), this._s = String(t), this._i = 0
    };
    ve.prototype.next = function() {
        var t = this._s,
            e = this._i;
        if ("undefined" == typeof t || e >= t.length) return this._s = void 0, {
            value: void 0,
            done: !0
        };
        var n, i, r = t.charCodeAt(e);
        return 55296 > r || r > 56319 || e + 1 === t.length ? i = 1 : (n = t.charCodeAt(e + 1), i = 56320 > n || n > 57343 ? 1 : 2), this._i = e + i, {
            value: t.substr(e, i),
            done: !1
        }
    }, Q(ve.prototype), Q(String.prototype, function() {
        return new ve(this)
    });
    var ye = {
        from: function(t) {
            var e, i, r = this,
                o = arguments.length > 1 ? arguments[1] : void 0;
            if (void 0 === o) e = !1;
            else {
                if (!G.IsCallable(o)) throw new TypeError("Array.from: when provided, the second argument must be a function");
                i = arguments.length > 2 ? arguments[2] : void 0, e = !0
            }
            var a, s, l, u = "undefined" != typeof(H(t) || G.GetMethod(t, V));
            if (u) {
                s = G.IsConstructor(r) ? Object(new r) : [];
                var c, h, d = G.GetIterator(t);
                for (l = 0;;) {
                    if (c = G.IteratorStep(d), c === !1) break;
                    h = c.value;
                    try {
                        e && (h = void 0 === i ? o(h, l) : n(o, i, h, l)), s[l] = h
                    } catch (p) {
                        throw G.IteratorClose(d, !0), p
                    }
                    l += 1
                }
                a = l
            } else {
                var f = G.ToObject(t);
                a = G.ToLength(f.length), s = G.IsConstructor(r) ? Object(new r(a)) : new Array(a);
                var m;
                for (l = 0; a > l; ++l) m = f[l], e && (m = void 0 !== i ? n(o, i, m, l) : o(m, l)), s[l] = m
            }
            return s.length = a, s
        },
        of: function() {
            for (var t = arguments.length, e = this, n = i(e) || !G.IsCallable(e) ? new Array(t) : G.Construct(e, [t]), r = 0; t > r; ++r) ee(n, r, arguments[r]);
            return n.length = t, n
        }
    };
    g(Array, ye), K(Array);
    var _e = function(t) {
        return {
            value: t,
            done: 0 === arguments.length
        }
    };
    t = function(t, e) {
        this.i = 0, this.array = t, this.kind = e
    }, g(t.prototype, {
        next: function() {
            var e = this.i,
                n = this.array;
            if (!(this instanceof t)) throw new TypeError("Not an ArrayIterator");
            if ("undefined" != typeof n)
                for (var i = G.ToLength(n.length); i > e; e++) {
                    var r, o = this.kind;
                    return "key" === o ? r = e : "value" === o ? r = n[e] : "entry" === o && (r = [e, n[e]]), this.i = e + 1, {
                        value: r,
                        done: !1
                    }
                }
            return this.array = void 0, {
                value: void 0,
                done: !0
            }
        }
    }), Q(t.prototype);
    var be = function(t) {
            var e = [];
            for (var n in t) S(e, n);
            return e
        },
        $e = function(t, e) {
            g(this, {
                object: t,
                array: be(t),
                kind: e
            })
        };
    g($e.prototype, {
        next: function() {
            var t, e = this.array;
            if (!(this instanceof $e)) throw new TypeError("Not an ObjectIterator");
            for (; e.length > 0;)
                if (t = k(e), t in this.object) return _e("key" === this.kind ? t : "value" === this.kind ? this.object[t] : [t, this.object[t]]);
            return _e()
        }
    }), Q($e.prototype);
    var we = Array.of === ye.of || function() {
        var t = function(t) {
            this.length = t
        };
        t.prototype = [];
        var e = Array.of.apply(t, [1, 2]);
        return e instanceof t && 2 === e.length
    }();
    we || X(Array, "of", ye.of);
    var xe = {
        copyWithin: function(t, e) {
            var n = arguments[2],
                i = G.ToObject(this),
                r = G.ToLength(i.length),
                o = G.ToInteger(t),
                a = G.ToInteger(e),
                s = 0 > o ? P(r + o, 0) : T(o, r),
                l = 0 > a ? P(r + a, 0) : T(a, r);
            n = "undefined" == typeof n ? r : G.ToInteger(n);
            var u = 0 > n ? P(r + n, 0) : T(n, r),
                c = T(u - l, r - s),
                h = 1;
            for (s > l && l + c > s && (h = -1, l += c - 1, s += c - 1); c > 0;) I(i, l) ? i[s] = i[l] : delete i[l], l += h, s += h, c -= 1;
            return i
        },
        fill: function(t) {
            var e = arguments.length > 1 ? arguments[1] : void 0,
                n = arguments.length > 2 ? arguments[2] : void 0,
                i = G.ToObject(this),
                r = G.ToLength(i.length);
            e = G.ToInteger("undefined" == typeof e ? 0 : e), n = G.ToInteger("undefined" == typeof n ? r : n);
            for (var o = 0 > e ? P(r + e, 0) : T(e, r), a = 0 > n ? r + n : n, s = o; r > s && a > s; ++s) i[s] = t;
            return i
        },
        find: function(t) {
            var e = G.ToObject(this),
                i = G.ToLength(e.length);
            if (!G.IsCallable(t)) throw new TypeError("Array#find: predicate must be a function");
            for (var r, o = arguments.length > 1 ? arguments[1] : null, a = 0; i > a; a++)
                if (r = e[a], o) {
                    if (n(t, o, r, a, e)) return r
                } else if (t(r, a, e)) return r
        },
        findIndex: function(t) {
            var e = G.ToObject(this),
                i = G.ToLength(e.length);
            if (!G.IsCallable(t)) throw new TypeError("Array#findIndex: predicate must be a function");
            for (var r = arguments.length > 1 ? arguments[1] : null, o = 0; i > o; o++)
                if (r) {
                    if (n(t, r, e[o], o, e)) return o
                } else if (t(e[o], o, e)) return o;
            return -1
        },
        keys: function() {
            return new t(this, "key")
        },
        values: function() {
            return new t(this, "value")
        },
        entries: function() {
            return new t(this, "entry")
        }
    };
    if (Array.prototype.keys && !G.IsCallable([1].keys().next) && delete Array.prototype.keys, Array.prototype.entries && !G.IsCallable([1].entries().next) && delete Array.prototype.entries, Array.prototype.keys && Array.prototype.entries && !Array.prototype.values && Array.prototype[V] && (g(Array.prototype, {
            values: Array.prototype[V]
        }), q.symbol(R.unscopables) && (Array.prototype[R.unscopables].values = !0)), c && Array.prototype.values && "values" !== Array.prototype.values.name) {
        var Le = Array.prototype.values;
        X(Array.prototype, "values", function() {
            return n(Le, this)
        }), m(Array.prototype, V, Array.prototype.values, !0)
    }
    g(Array.prototype, xe), Q(Array.prototype, function() {
        return this.values()
    }), Object.getPrototypeOf && Q(Object.getPrototypeOf([].values()));
    var Ce = function() {
            return a(function() {
                return 0 === Array.from({
                    length: -1
                }).length
            })
        }(),
        Se = function() {
            var t = Array.from([0].entries());
            return 1 === t.length && i(t[0]) && 0 === t[0][0] && 0 === t[0][1]
        }();
    Ce && Se || X(Array, "from", ye.from);
    var Ee = function() {
        return a(function() {
            return Array.from([0], void 0)
        })
    }();
    if (!Ee) {
        var ke = Array.from;
        X(Array, "from", function(t) {
            return arguments.length > 0 && "undefined" != typeof arguments[1] ? e(ke, this, arguments) : n(ke, this, t)
        })
    }
    var Pe = function(t, e) {
        var i = {
            length: -1
        };
        return i[e ? 4294967294 : 0] = !0, a(function() {
            n(t, i, function() {
                throw new RangeError("should not reach here")
            }, [])
        })
    };
    if (!Pe(Array.prototype.forEach)) {
        var Te = Array.prototype.forEach;
        X(Array.prototype, "forEach", function() {
            return e(Te, this.length >= 0 ? this : [], arguments)
        }, !0)
    }
    if (!Pe(Array.prototype.map)) {
        var Me = Array.prototype.map;
        X(Array.prototype, "map", function() {
            return e(Me, this.length >= 0 ? this : [], arguments)
        }, !0)
    }
    if (!Pe(Array.prototype.filter)) {
        var De = Array.prototype.filter;
        X(Array.prototype, "filter", function() {
            return e(De, this.length >= 0 ? this : [], arguments)
        }, !0)
    }
    if (!Pe(Array.prototype.some)) {
        var Oe = Array.prototype.some;
        X(Array.prototype, "some", function() {
            return e(Oe, this.length >= 0 ? this : [], arguments)
        }, !0)
    }
    if (!Pe(Array.prototype.every)) {
        var Ae = Array.prototype.every;
        X(Array.prototype, "every", function() {
            return e(Ae, this.length >= 0 ? this : [], arguments)
        }, !0)
    }
    if (!Pe(Array.prototype.reduce)) {
        var Ie = Array.prototype.reduce;
        X(Array.prototype, "reduce", function() {
            return e(Ie, this.length >= 0 ? this : [], arguments)
        }, !0)
    }
    if (!Pe(Array.prototype.reduceRight, !0)) {
        var Ne = Array.prototype.reduceRight;
        X(Array.prototype, "reduceRight", function() {
            return e(Ne, this.length >= 0 ? this : [], arguments)
        }, !0)
    }
    var Re = 8 !== Number("0o10"),
        Ue = 2 !== Number("0b10"),
        je = f(pe, function(t) {
            return 0 === Number(t + 0 + t)
        });
    if (Re || Ue || je) {
        var Be = Number,
            Fe = /^0b[01]+$/i,
            ze = /^0o[0-7]+$/i,
            He = Fe.test.bind(Fe),
            qe = ze.test.bind(ze),
            Ve = function(t) {
                var e;
                if ("function" == typeof t.valueOf && (e = t.valueOf(), q.primitive(e))) return e;
                if ("function" == typeof t.toString && (e = t.toString(), q.primitive(e))) return e;
                throw new TypeError("No default value")
            },
            We = fe.test.bind(fe),
            Ge = me.test.bind(me),
            Ze = function() {
                var t = function(e) {
                    var i = q.primitive(e) ? e : Ve(e, "number");
                    "string" == typeof i && (i = He(i) ? parseInt(C(i, 2), 2) : qe(i) ? parseInt(C(i, 2), 8) : We(i) || Ge(i) ? 0 / 0 : n(de, i));
                    var r = this,
                        o = a(function() {
                            return Be.prototype.valueOf.call(r), !0
                        });
                    return r instanceof t && !o ? new Be(i) : Be(i)
                };
                return t
            }();
        Y(Be, Ze, {}), Number = Ze, Z.redefine(b, "Number", Ze)
    }
    var Ye = Math.pow(2, 53) - 1;
    g(Number, {
        MAX_SAFE_INTEGER: Ye,
        MIN_SAFE_INTEGER: -Ye,
        EPSILON: 2.220446049250313e-16,
        parseInt: b.parseInt,
        parseFloat: b.parseFloat,
        isFinite: B,
        isInteger: function(t) {
            return B(t) && G.ToInteger(t) === t
        },
        isSafeInteger: function(t) {
            return Number.isInteger(t) && D(t) <= Number.MAX_SAFE_INTEGER
        },
        isNaN: j
    }), m(Number, "parseInt", b.parseInt, Number.parseInt !== b.parseInt), [, 1].find(function(t, e) {
        return 0 === e
    }) || X(Array.prototype, "find", xe.find), 0 !== [, 1].findIndex(function(t, e) {
        return 0 === e
    }) && X(Array.prototype, "findIndex", xe.findIndex);
    var Je = Function.bind.call(Function.bind, Object.prototype.propertyIsEnumerable),
        Ke = function() {
            for (var t = Number(this), e = arguments.length, n = e - t, i = new Array(0 > n ? 0 : n), r = t; e > r; ++r) i[r - t] = arguments[r];
            return i
        },
        Xe = function(t) {
            return function(e, n) {
                return e[n] = t[n], e
            }
        },
        Qe = function(t, e) {
            var n, i = Object.keys(Object(e));
            return G.IsCallable(Object.getOwnPropertySymbols) && (n = p(Object.getOwnPropertySymbols(Object(e)), Je(e))), d(L(i, n || []), Xe(e), t)
        },
        tn = {
            assign: function(t) {
                var n = G.ToObject(t, "Cannot convert undefined or null to object");
                return d(e(Ke, 1, arguments), Qe, n)
            },
            is: function(t, e) {
                return G.SameValue(t, e)
            }
        },
        en = Object.assign && Object.preventExtensions && function() {
            var t = Object.preventExtensions({
                1: 2
            });
            try {
                Object.assign(t, "xy")
            } catch (e) {
                return "y" === t[1]
            }
        }();
    if (en && X(Object, "assign", tn.assign), g(Object, tn), u) {
        var nn = {
            setPrototypeOf: function(t, e) {
                var i, r = function(t, e) {
                        if (!G.TypeIsObject(t)) throw new TypeError("cannot set prototype on a non-object");
                        if (null !== e && !G.TypeIsObject(e)) throw new TypeError("can only set prototype to an object or null" + e)
                    },
                    o = function(t, e) {
                        return r(t, e), n(i, t, e), t
                    };
                try {
                    i = t.getOwnPropertyDescriptor(t.prototype, e).set, n(i, {}, null)
                } catch (a) {
                    if (t.prototype !== {} [e]) return;
                    i = function(t) {
                        this[e] = t
                    }, o.polyfill = o(o({}, null), t.prototype) instanceof t
                }
                return o
            }(Object, "__proto__")
        };
        g(Object, nn)
    }
    Object.setPrototypeOf && Object.getPrototypeOf && null !== Object.getPrototypeOf(Object.setPrototypeOf({}, null)) && null === Object.getPrototypeOf(Object.create(null)) && ! function() {
        var t = Object.create(null),
            e = Object.getPrototypeOf,
            n = Object.setPrototypeOf;
        Object.getPrototypeOf = function(n) {
            var i = e(n);
            return i === t ? null : i
        }, Object.setPrototypeOf = function(e, i) {
            var r = null === i ? t : i;
            return n(e, r)
        }, Object.setPrototypeOf.polyfill = !1
    }();
    var rn = !o(function() {
        Object.keys("foo")
    });
    if (!rn) {
        var on = Object.keys;
        X(Object, "keys", function(t) {
            return on(G.ToObject(t))
        })
    }
    if (Object.getOwnPropertyNames) {
        var an = !o(function() {
            Object.getOwnPropertyNames("foo")
        });
        if (!an) {
            var sn = "object" == typeof window ? Object.getOwnPropertyNames(window) : [],
                ln = Object.getOwnPropertyNames;
            X(Object, "getOwnPropertyNames", function(t) {
                var e = G.ToObject(t);
                if ("[object Window]" === x(e)) try {
                    return ln(e)
                } catch (n) {
                    return L([], sn)
                }
                return ln(e)
            })
        }
    }
    if (Object.getOwnPropertyDescriptor) {
        var un = !o(function() {
            Object.getOwnPropertyDescriptor("foo", "bar")
        });
        if (!un) {
            var cn = Object.getOwnPropertyDescriptor;
            X(Object, "getOwnPropertyDescriptor", function(t, e) {
                return cn(G.ToObject(t), e)
            })
        }
    }
    if (Object.seal) {
        var hn = !o(function() {
            Object.seal("foo")
        });
        if (!hn) {
            var dn = Object.seal;
            X(Object, "seal", function(t) {
                return q.object(t) ? dn(t) : t
            })
        }
    }
    if (Object.isSealed) {
        var pn = !o(function() {
            Object.isSealed("foo")
        });
        if (!pn) {
            var fn = Object.isSealed;
            X(Object, "isSealed", function(t) {
                return q.object(t) ? fn(t) : !0
            })
        }
    }
    if (Object.freeze) {
        var mn = !o(function() {
            Object.freeze("foo")
        });
        if (!mn) {
            var gn = Object.freeze;
            X(Object, "freeze", function(t) {
                return q.object(t) ? gn(t) : t
            })
        }
    }
    if (Object.isFrozen) {
        var vn = !o(function() {
            Object.isFrozen("foo")
        });
        if (!vn) {
            var yn = Object.isFrozen;
            X(Object, "isFrozen", function(t) {
                return q.object(t) ? yn(t) : !0
            })
        }
    }
    if (Object.preventExtensions) {
        var _n = !o(function() {
            Object.preventExtensions("foo")
        });
        if (!_n) {
            var bn = Object.preventExtensions;
            X(Object, "preventExtensions", function(t) {
                return q.object(t) ? bn(t) : t
            })
        }
    }
    if (Object.isExtensible) {
        var $n = !o(function() {
            Object.isExtensible("foo")
        });
        if (!$n) {
            var wn = Object.isExtensible;
            X(Object, "isExtensible", function(t) {
                return q.object(t) ? wn(t) : !1
            })
        }
    }
    if (Object.getPrototypeOf) {
        var xn = !o(function() {
            Object.getPrototypeOf("foo")
        });
        if (!xn) {
            var Ln = Object.getPrototypeOf;
            X(Object, "getPrototypeOf", function(t) {
                return Ln(G.ToObject(t))
            })
        }
    }
    var Cn = u && function() {
        var t = Object.getOwnPropertyDescriptor(RegExp.prototype, "flags");
        return t && G.IsCallable(t.get)
    }();
    if (u && !Cn) {
        var Sn = function() {
            if (!G.TypeIsObject(this)) throw new TypeError("Method called on incompatible type: must be an object.");
            var t = "";
            return this.global && (t += "g"), this.ignoreCase && (t += "i"), this.multiline && (t += "m"), this.unicode && (t += "u"), this.sticky && (t += "y"), t
        };
        Z.getter(RegExp.prototype, "flags", Sn)
    }
    var En = a(function() {
        return "/a/i" === String(new RegExp(/a/g, "i"))
    });
    if (!En && u) {
        var kn = RegExp,
            Pn = function() {
                return function t(e, n) {
                    var i = this instanceof t;
                    return !i && (q.regex(e) || e && e.constructor === t) ? e : q.regex(e) && q.string(n) ? new t(e.source, n) : new kn(e, n)
                }
            }();
        Y(kn, Pn, {
            $input: !0
        }), RegExp = Pn, Z.redefine(b, "RegExp", Pn)
    }
    if (u) {
        var Tn = {
            input: "$_",
            lastMatch: "$&",
            lastParen: "$+",
            leftContext: "$`",
            rightContext: "$'"
        };
        h(Object.keys(Tn), function(t) {
            t in RegExp && !(Tn[t] in RegExp) && Z.getter(RegExp, Tn[t], function() {
                return RegExp[t]
            })
        })
    }
    K(RegExp);
    var Mn = 1 / Number.EPSILON,
        Dn = function(t) {
            return t + Mn - Mn
        },
        On = Math.pow(2, -23),
        An = Math.pow(2, 127) * (2 - On),
        In = Math.pow(2, -126),
        Nn = Number.prototype.clz;
    delete Number.prototype.clz;
    var Rn = {
        acosh: function(t) {
            var e = Number(t);
            return Number.isNaN(e) || 1 > t ? 0 / 0 : 1 === e ? 0 : 1 / 0 === e ? e : O(e / Math.E + A(e + 1) * A(e - 1) / Math.E) + 1
        },
        asinh: function(t) {
            var e = Number(t);
            return 0 !== e && $(e) ? 0 > e ? -Math.asinh(-e) : O(e + A(e * e + 1)) : e
        },
        atanh: function(t) {
            var e = Number(t);
            return Number.isNaN(e) || -1 > e || e > 1 ? 0 / 0 : -1 === e ? -1 / 0 : 1 === e ? 1 / 0 : 0 === e ? e : .5 * O((1 + e) / (1 - e))
        },
        cbrt: function(t) {
            var e = Number(t);
            if (0 === e) return e;
            var n, i = 0 > e;
            return i && (e = -e), 1 / 0 === e ? n = 1 / 0 : (n = Math.exp(O(e) / 3), n = (e / (n * n) + 2 * n) / 3), i ? -n : n
        },
        clz32: function(t) {
            var e = Number(t),
                i = G.ToUint32(e);
            return 0 === i ? 32 : Nn ? n(Nn, i) : 31 - M(O(i + .5) * Math.LOG2E)
        },
        cosh: function(t) {
            var e = Number(t);
            return 0 === e ? 1 : Number.isNaN(e) ? 0 / 0 : $(e) ? (0 > e && (e = -e), e > 21 ? Math.exp(e) / 2 : (Math.exp(e) + Math.exp(-e)) / 2) : 1 / 0
        },
        expm1: function(t) {
            var e = Number(t);
            if (e === -1 / 0) return -1;
            if (!$(e) || 0 === e) return e;
            if (D(e) > .5) return Math.exp(e) - 1;
            for (var n = e, i = 0, r = 1; i + n !== i;) i += n, r += 1, n *= e / r;
            return i
        },
        hypot: function() {
            for (var t = 0, e = 0, n = 0; n < arguments.length; ++n) {
                var i = D(Number(arguments[n]));
                i > e ? (t *= e / i * (e / i), t += 1, e = i) : t += i > 0 ? i / e * (i / e) : i
            }
            return 1 / 0 === e ? 1 / 0 : e * A(t)
        },
        log2: function(t) {
            return O(t) * Math.LOG2E
        },
        log10: function(t) {
            return O(t) * Math.LOG10E
        },
        log1p: function(t) {
            var e = Number(t);
            return -1 > e || Number.isNaN(e) ? 0 / 0 : 0 === e || 1 / 0 === e ? e : -1 === e ? -1 / 0 : 1 + e - 1 === 0 ? e : e * (O(1 + e) / (1 + e - 1))
        },
        sign: function(t) {
            var e = Number(t);
            return 0 === e ? e : Number.isNaN(e) ? e : 0 > e ? -1 : 1
        },
        sinh: function(t) {
            var e = Number(t);
            return $(e) && 0 !== e ? D(e) < 1 ? (Math.expm1(e) - Math.expm1(-e)) / 2 : (Math.exp(e - 1) - Math.exp(-e - 1)) * Math.E / 2 : e
        },
        tanh: function(t) {
            var e = Number(t);
            if (Number.isNaN(e) || 0 === e) return e;
            if (1 / 0 === e) return 1;
            if (e === -1 / 0) return -1;
            var n = Math.expm1(e),
                i = Math.expm1(-e);
            return 1 / 0 === n ? 1 : 1 / 0 === i ? -1 : (n - i) / (Math.exp(e) + Math.exp(-e))
        },
        trunc: function(t) {
            var e = Number(t);
            return 0 > e ? -M(-e) : M(e)
        },
        imul: function(t, e) {
            var n = G.ToUint32(t),
                i = G.ToUint32(e),
                r = n >>> 16 & 65535,
                o = 65535 & n,
                a = i >>> 16 & 65535,
                s = 65535 & i;
            return o * s + (r * s + o * a << 16 >>> 0) | 0
        },
        fround: function(t) {
            var e = Number(t);
            if (0 === e || 1 / 0 === e || e === -1 / 0 || j(e)) return e;
            var n = Math.sign(e),
                i = D(e);
            if (In > i) return n * Dn(i / In / On) * In * On;
            var r = (1 + On / Number.EPSILON) * i,
                o = r - (r - i);
            return o > An || j(o) ? 1 / 0 * n : n * o
        }
    };
    g(Math, Rn), m(Math, "log1p", Rn.log1p, -1e-17 !== Math.log1p(-1e-17)), m(Math, "asinh", Rn.asinh, Math.asinh(-1e7) !== -Math.asinh(1e7)), m(Math, "tanh", Rn.tanh, -2e-17 !== Math.tanh(-2e-17)), m(Math, "acosh", Rn.acosh, 1 / 0 === Math.acosh(Number.MAX_VALUE)), m(Math, "cbrt", Rn.cbrt, Math.abs(1 - Math.cbrt(1e-300) / 1e-100) / Number.EPSILON > 8), m(Math, "sinh", Rn.sinh, -2e-17 !== Math.sinh(-2e-17));
    var Un = Math.expm1(10);
    m(Math, "expm1", Rn.expm1, Un > 22025.465794806718 || 22025.465794806718 > Un);
    var jn = Math.round,
        Bn = 0 === Math.round(.5 - Number.EPSILON / 4) && 1 === Math.round(-.5 + Number.EPSILON / 3.99),
        Fn = Mn + 1,
        zn = 2 * Mn - 1,
        Hn = [Fn, zn].every(function(t) {
            return Math.round(t) === t
        });
    m(Math, "round", function(t) {
        var e = M(t),
            n = -1 === e ? -0 : e + 1;
        return .5 > t - e ? e : n
    }, !Bn || !Hn), Z.preserveToString(Math.round, jn);
    var qn = Math.imul; - 5 !== Math.imul(4294967295, 5) && (Math.imul = Rn.imul, Z.preserveToString(Math.imul, qn)), 2 !== Math.imul.length && X(Math, "imul", function() {
        return e(qn, Math, arguments)
    });
    var Vn = function() {
        var t = b.setTimeout;
        if ("function" == typeof t || "object" == typeof t) {
            G.IsPromise = function(t) {
                return G.TypeIsObject(t) ? "undefined" == typeof t._promise ? !1 : !0 : !1
            };
            var e, i = function(t) {
                if (!G.IsConstructor(t)) throw new TypeError("Bad promise constructor");
                var e = this,
                    n = function(t, n) {
                        if (void 0 !== e.resolve || void 0 !== e.reject) throw new TypeError("Bad Promise implementation!");
                        e.resolve = t, e.reject = n
                    };
                if (e.promise = new t(n), !G.IsCallable(e.resolve) || !G.IsCallable(e.reject)) throw new TypeError("Bad promise constructor")
            };
            "undefined" != typeof window && G.IsCallable(window.postMessage) && (e = function() {
                var t = [],
                    e = "zero-timeout-message",
                    n = function(n) {
                        S(t, n), window.postMessage(e, "*")
                    },
                    i = function(n) {
                        if (n.source === window && n.data === e) {
                            if (n.stopPropagation(), 0 === t.length) return;
                            var i = k(t);
                            i()
                        }
                    };
                return window.addEventListener("message", i, !0), n
            });
            var r, o = function() {
                    var t = b.Promise;
                    return t && t.resolve && function(e) {
                        return t.resolve().then(e)
                    }
                },
                a = G.IsCallable(b.setImmediate) ? b.setImmediate.bind(b) : "object" == typeof process && process.nextTick ? process.nextTick : o() || (G.IsCallable(e) ? e() : function(e) {
                    t(e, 0)
                }),
                s = 1,
                l = 2,
                u = 3,
                c = 4,
                d = 5,
                p = function(t, e) {
                    var n, i, r = t.capabilities,
                        o = t.handler,
                        a = !1;
                    if (o === s) n = e;
                    else if (o === l) n = e, a = !0;
                    else try {
                        n = o(e)
                    } catch (u) {
                        n = u, a = !0
                    }(i = a ? r.reject : r.resolve)(n)
                },
                f = function(t, e) {
                    h(t, function(t) {
                        a(function() {
                            p(t, e)
                        })
                    })
                },
                m = function(t, e) {
                    var n = t._promise,
                        i = n.fulfillReactions;
                    n.result = e, n.fulfillReactions = void 0, n.rejectReactions = void 0, n.state = c, f(i, e)
                },
                v = function(t, e) {
                    var n = t._promise,
                        i = n.rejectReactions;
                    n.result = e, n.fulfillReactions = void 0, n.rejectReactions = void 0, n.state = d, f(i, e)
                },
                y = function(t) {
                    var e = !1,
                        n = function(n) {
                            var i;
                            if (!e) {
                                if (e = !0, n === t) return v(t, new TypeError("Self resolution"));
                                if (!G.TypeIsObject(n)) return m(t, n);
                                try {
                                    i = n.then
                                } catch (r) {
                                    return v(t, r)
                                }
                                return G.IsCallable(i) ? void a(function() {
                                    _(t, n, i)
                                }) : m(t, n)
                            }
                        },
                        i = function(n) {
                            return e ? void 0 : (e = !0, v(t, n))
                        };
                    return {
                        resolve: n,
                        reject: i
                    }
                },
                _ = function(t, e, i) {
                    var r = y(t),
                        o = r.resolve,
                        a = r.reject;
                    try {
                        n(i, e, o, a)
                    } catch (s) {
                        a(s)
                    }
                },
                $ = function(t) {
                    if (!G.TypeIsObject(t)) throw new TypeError("Promise is not object");
                    var e = t[U];
                    return void 0 !== e && null !== e ? e : t
                },
                w = function() {
                    var t = function(e) {
                        if (!(this instanceof t)) throw new TypeError('Constructor Promise requires "new"');
                        if (this && this._promise) throw new TypeError("Bad construction");
                        if (!G.IsCallable(e)) throw new TypeError("not a valid resolver");
                        var n = ne(this, t, r, {
                                _promise: {
                                    result: void 0,
                                    state: u,
                                    fulfillReactions: [],
                                    rejectReactions: []
                                }
                            }),
                            i = y(n),
                            o = i.reject;
                        try {
                            e(i.resolve, o)
                        } catch (a) {
                            o(a)
                        }
                        return n
                    };
                    return t
                }();
            r = w.prototype;
            var x = function(t, e, n, i) {
                    var r = !1;
                    return function(o) {
                        if (!r && (r = !0, e[t] = o, 0 === --i.count)) {
                            var a = n.resolve;
                            a(e)
                        }
                    }
                },
                L = function(t, e, n) {
                    for (var i, r, o = t.iterator, a = [], s = {
                            count: 1
                        }, l = 0;;) {
                        try {
                            if (i = G.IteratorStep(o), i === !1) {
                                t.done = !0;
                                break
                            }
                            r = i.value
                        } catch (u) {
                            throw t.done = !0, u
                        }
                        a[l] = void 0;
                        var c = e.resolve(r),
                            h = x(l, a, n, s);
                        s.count += 1, c.then(h, n.reject), l += 1
                    }
                    if (0 === --s.count) {
                        var d = n.resolve;
                        d(a)
                    }
                    return n.promise
                },
                C = function(t, e, n) {
                    for (var i, r, o, a = t.iterator;;) {
                        try {
                            if (i = G.IteratorStep(a), i === !1) {
                                t.done = !0;
                                break
                            }
                            r = i.value
                        } catch (s) {
                            throw t.done = !0, s
                        }
                        o = e.resolve(r), o.then(n.resolve, n.reject)
                    }
                    return n.promise
                };
            return g(w, {
                all: function(t) {
                    var e, n, r = $(this),
                        o = new i(r);
                    try {
                        return e = G.GetIterator(t), n = {
                            iterator: e,
                            done: !1
                        }, L(n, r, o)
                    } catch (a) {
                        var s = a;
                        if (n && !n.done) try {
                            G.IteratorClose(e, !0)
                        } catch (l) {
                            s = l
                        }
                        var u = o.reject;
                        return u(s), o.promise
                    }
                },
                race: function(t) {
                    var e, n, r = $(this),
                        o = new i(r);
                    try {
                        return e = G.GetIterator(t), n = {
                            iterator: e,
                            done: !1
                        }, C(n, r, o)
                    } catch (a) {
                        var s = a;
                        if (n && !n.done) try {
                            G.IteratorClose(e, !0)
                        } catch (l) {
                            s = l
                        }
                        var u = o.reject;
                        return u(s), o.promise
                    }
                },
                reject: function(t) {
                    var e = this,
                        n = new i(e),
                        r = n.reject;
                    return r(t), n.promise
                },
                resolve: function(t) {
                    var e = this;
                    if (G.IsPromise(t)) {
                        var n = t.constructor;
                        if (n === e) return t
                    }
                    var r = new i(e),
                        o = r.resolve;
                    return o(t), r.promise
                }
            }), g(r, {
                "catch": function(t) {
                    return this.then(void 0, t)
                },
                then: function(t, e) {
                    var n = this;
                    if (!G.IsPromise(n)) throw new TypeError("not a promise");
                    var r, o = G.SpeciesConstructor(n, w),
                        h = new i(o),
                        f = {
                            capabilities: h,
                            handler: G.IsCallable(t) ? t : s
                        },
                        m = {
                            capabilities: h,
                            handler: G.IsCallable(e) ? e : l
                        },
                        g = n._promise;
                    if (g.state === u) S(g.fulfillReactions, f), S(g.rejectReactions, m);
                    else if (g.state === c) r = g.result, a(function() {
                        p(f, r)
                    });
                    else {
                        if (g.state !== d) throw new TypeError("unexpected Promise state");
                        r = g.result, a(function() {
                            p(m, r)
                        })
                    }
                    return h.promise
                }
            }), w
        }
    }();
    if (b.Promise && (delete b.Promise.accept, delete b.Promise.defer, delete b.Promise.prototype.chain), "function" == typeof Vn) {
        g(b, {
            Promise: Vn
        });
        var Wn = y(b.Promise, function(t) {
                return t.resolve(42).then(function() {}) instanceof t
            }),
            Gn = !o(function() {
                b.Promise.reject(42).then(null, 5).then(null, N)
            }),
            Zn = o(function() {
                b.Promise.call(3, N)
            }),
            Yn = function(t) {
                var e = t.resolve(5);
                e.constructor = {};
                var n = t.resolve(e);
                return e === n
            }(b.Promise);
        Wn && Gn && Zn && !Yn || (Promise = Vn, X(b, "Promise", Vn)), K(Promise)
    }
    var Jn = function(t) {
            var e = Object.keys(d(t, function(t, e) {
                return t[e] = !0, t
            }, {}));
            return t.join(":") === e.join(":")
        },
        Kn = Jn(["z", "a", "bb"]),
        Xn = Jn(["z", 1, "a", "3", 2]);
    if (u) {
        var Qn = function(t) {
                if (!Kn) return null;
                var e = typeof t;
                return "undefined" === e || null === t ? "^" + String(t) : "string" === e ? "$" + t : "number" === e ? Xn ? t : "n" + t : "boolean" === e ? "b" + t : null
            },
            ti = function() {
                return Object.create ? Object.create(null) : {}
            },
            ei = function(t, e, r) {
                if (i(r) || q.string(r)) h(r, function(t) {
                    e.set(t[0], t[1])
                });
                else if (r instanceof t) n(t.prototype.forEach, r, function(t, n) {
                    e.set(n, t)
                });
                else {
                    var o, a;
                    if (null !== r && "undefined" != typeof r) {
                        if (a = e.set, !G.IsCallable(a)) throw new TypeError("bad map");
                        o = G.GetIterator(r)
                    }
                    if ("undefined" != typeof o)
                        for (;;) {
                            var s = G.IteratorStep(o);
                            if (s === !1) break;
                            var l = s.value;
                            try {
                                if (!G.TypeIsObject(l)) throw new TypeError("expected iterable of pairs");
                                n(a, e, l[0], l[1])
                            } catch (u) {
                                throw G.IteratorClose(o, !0), u
                            }
                        }
                }
            },
            ni = function(t, e, r) {
                if (i(r) || q.string(r)) h(r, function(t) {
                    e.add(t)
                });
                else if (r instanceof t) n(t.prototype.forEach, r, function(t) {
                    e.add(t)
                });
                else {
                    var o, a;
                    if (null !== r && "undefined" != typeof r) {
                        if (a = e.add, !G.IsCallable(a)) throw new TypeError("bad set");
                        o = G.GetIterator(r)
                    }
                    if ("undefined" != typeof o)
                        for (;;) {
                            var s = G.IteratorStep(o);
                            if (s === !1) break;
                            var l = s.value;
                            try {
                                n(a, e, l)
                            } catch (u) {
                                throw G.IteratorClose(o, !0), u
                            }
                        }
                }
            },
            ii = {
                Map: function() {
                    var t = {},
                        e = function(t, e) {
                            this.key = t, this.value = e, this.next = null, this.prev = null
                        };
                    e.prototype.isRemoved = function() {
                        return this.key === t
                    };
                    var i = function(t) {
                            return !!t._es6map
                        },
                        r = function(t, e) {
                            if (!G.TypeIsObject(t) || !i(t)) throw new TypeError("Method Map.prototype." + e + " called on incompatible receiver " + String(t))
                        },
                        o = function(t, e) {
                            r(t, "[[MapIterator]]"), this.head = t._head, this.i = this.head, this.kind = e
                        };
                    o.prototype = {
                        next: function() {
                            var t, e = this.i,
                                n = this.kind,
                                i = this.head;
                            if ("undefined" == typeof this.i) return {
                                value: void 0,
                                done: !0
                            };
                            for (; e.isRemoved() && e !== i;) e = e.prev;
                            for (; e.next !== i;)
                                if (e = e.next, !e.isRemoved()) return t = "key" === n ? e.key : "value" === n ? e.value : [e.key, e.value], this.i = e, {
                                    value: t,
                                    done: !1
                                };
                            return this.i = void 0, {
                                value: void 0,
                                done: !0
                            }
                        }
                    }, Q(o.prototype);
                    var a, s = function l() {
                        if (!(this instanceof l)) throw new TypeError('Constructor Map requires "new"');
                        if (this && this._es6map) throw new TypeError("Bad construction");
                        var t = ne(this, l, a, {
                                _es6map: !0,
                                _head: null,
                                _storage: ti(),
                                _size: 0
                            }),
                            n = new e(null, null);
                        return n.next = n.prev = n, t._head = n, arguments.length > 0 && ei(l, t, arguments[0]), t
                    };
                    return a = s.prototype, Z.getter(a, "size", function() {
                        if ("undefined" == typeof this._size) throw new TypeError("size method called on incompatible Map");
                        return this._size
                    }), g(a, {
                        get: function(t) {
                            r(this, "get");
                            var e = Qn(t);
                            if (null !== e) {
                                var n = this._storage[e];
                                return n ? n.value : void 0
                            }
                            for (var i = this._head, o = i;
                                (o = o.next) !== i;)
                                if (G.SameValueZero(o.key, t)) return o.value
                        },
                        has: function(t) {
                            r(this, "has");
                            var e = Qn(t);
                            if (null !== e) return "undefined" != typeof this._storage[e];
                            for (var n = this._head, i = n;
                                (i = i.next) !== n;)
                                if (G.SameValueZero(i.key, t)) return !0;
                            return !1
                        },
                        set: function(t, n) {
                            r(this, "set");
                            var i, o = this._head,
                                a = o,
                                s = Qn(t);
                            if (null !== s) {
                                if ("undefined" != typeof this._storage[s]) return this._storage[s].value = n, this;
                                i = this._storage[s] = new e(t, n), a = o.prev
                            }
                            for (;
                                (a = a.next) !== o;)
                                if (G.SameValueZero(a.key, t)) return a.value = n, this;
                            return i = i || new e(t, n), G.SameValue(-0, t) && (i.key = 0), i.next = this._head, i.prev = this._head.prev, i.prev.next = i, i.next.prev = i, this._size += 1, this
                        },
                        "delete": function(e) {
                            r(this, "delete");
                            var n = this._head,
                                i = n,
                                o = Qn(e);
                            if (null !== o) {
                                if ("undefined" == typeof this._storage[o]) return !1;
                                i = this._storage[o].prev, delete this._storage[o]
                            }
                            for (;
                                (i = i.next) !== n;)
                                if (G.SameValueZero(i.key, e)) return i.key = i.value = t, i.prev.next = i.next, i.next.prev = i.prev, this._size -= 1, !0;
                            return !1
                        },
                        clear: function() {
                            r(this, "clear"), this._size = 0, this._storage = ti();
                            for (var e = this._head, n = e, i = n.next;
                                (n = i) !== e;) n.key = n.value = t, i = n.next, n.next = n.prev = e;
                            e.next = e.prev = e
                        },
                        keys: function() {
                            return r(this, "keys"), new o(this, "key")
                        },
                        values: function() {
                            return r(this, "values"), new o(this, "value")
                        },
                        entries: function() {
                            return r(this, "entries"), new o(this, "key+value")
                        },
                        forEach: function(t) {
                            r(this, "forEach");
                            for (var e = arguments.length > 1 ? arguments[1] : null, i = this.entries(), o = i.next(); !o.done; o = i.next()) e ? n(t, e, o.value[1], o.value[0], this) : t(o.value[1], o.value[0], this)
                        }
                    }), Q(a, a.entries), s
                }(),
                Set: function() {
                    var t, e = function(t) {
                            return t._es6set && "undefined" != typeof t._storage
                        },
                        i = function(t, n) {
                            if (!G.TypeIsObject(t) || !e(t)) throw new TypeError("Set.prototype." + n + " called on incompatible receiver " + String(t))
                        },
                        r = function a() {
                            if (!(this instanceof a)) throw new TypeError('Constructor Set requires "new"');
                            if (this && this._es6set) throw new TypeError("Bad construction");
                            var e = ne(this, a, t, {
                                _es6set: !0,
                                "[[SetData]]": null,
                                _storage: ti()
                            });
                            if (!e._es6set) throw new TypeError("bad set");
                            return arguments.length > 0 && ni(a, e, arguments[0]), e
                        };
                    t = r.prototype;
                    var o = function(t) {
                        if (!t["[[SetData]]"]) {
                            var e = t["[[SetData]]"] = new ii.Map;
                            h(Object.keys(t._storage), function(t) {
                                var n = t;
                                if ("^null" === n) n = null;
                                else if ("^undefined" === n) n = void 0;
                                else {
                                    var i = n.charAt(0);
                                    n = "$" === i ? C(n, 1) : "n" === i ? +C(n, 1) : "b" === i ? "btrue" === n : +n
                                }
                                e.set(n, n)
                            }), t._storage = null
                        }
                    };
                    return Z.getter(r.prototype, "size", function() {
                        return i(this, "size"), o(this), this["[[SetData]]"].size
                    }), g(r.prototype, {
                        has: function(t) {
                            i(this, "has");
                            var e;
                            return this._storage && null !== (e = Qn(t)) ? !!this._storage[e] : (o(this), this["[[SetData]]"].has(t))
                        },
                        add: function(t) {
                            i(this, "add");
                            var e;
                            return this._storage && null !== (e = Qn(t)) ? (this._storage[e] = !0, this) : (o(this), this["[[SetData]]"].set(t, t), this)
                        },
                        "delete": function(t) {
                            i(this, "delete");
                            var e;
                            if (this._storage && null !== (e = Qn(t))) {
                                var n = I(this._storage, e);
                                return delete this._storage[e] && n
                            }
                            return o(this), this["[[SetData]]"]["delete"](t)
                        },
                        clear: function() {
                            i(this, "clear"), this._storage ? this._storage = ti() : this["[[SetData]]"].clear()
                        },
                        values: function() {
                            return i(this, "values"), o(this), this["[[SetData]]"].values()
                        },
                        entries: function() {
                            return i(this, "entries"), o(this), this["[[SetData]]"].entries()
                        },
                        forEach: function(t) {
                            i(this, "forEach");
                            var e = arguments.length > 1 ? arguments[1] : null,
                                r = this;
                            o(r), this["[[SetData]]"].forEach(function(i, o) {
                                e ? n(t, e, o, o, r) : t(o, o, r)
                            })
                        }
                    }), m(r.prototype, "keys", r.prototype.values, !0), Q(r.prototype, r.prototype.values), r
                }()
            };
        if (b.Map || b.Set) {
            var ri = a(function() {
                return 2 === new Map([
                    [1, 2]
                ]).get(1)
            });
            if (!ri) {
                var oi = b.Map;
                b.Map = function qi() {
                    if (!(this instanceof qi)) throw new TypeError('Constructor Map requires "new"');
                    var t = new oi;
                    return arguments.length > 0 && ei(qi, t, arguments[0]), Object.setPrototypeOf(t, b.Map.prototype), m(t, "constructor", qi, !0), t
                }, b.Map.prototype = v(oi.prototype), Z.preserveToString(b.Map, oi)
            }
            var ai = new Map,
                si = function(t) {
                    return t["delete"](0), t["delete"](-0), t.set(0, 3), t.get(-0, 4), 3 === t.get(0) && 4 === t.get(-0)
                }(ai),
                li = ai.set(1, 2) === ai;
            if (!si || !li) {
                var ui = Map.prototype.set;
                X(Map.prototype, "set", function(t, e) {
                    return n(ui, this, 0 === t ? 0 : t, e), this
                })
            }
            if (!si) {
                var ci = Map.prototype.get,
                    hi = Map.prototype.has;
                g(Map.prototype, {
                    get: function(t) {
                        return n(ci, this, 0 === t ? 0 : t)
                    },
                    has: function(t) {
                        return n(hi, this, 0 === t ? 0 : t)
                    }
                }, !0), Z.preserveToString(Map.prototype.get, ci), Z.preserveToString(Map.prototype.has, hi)
            }
            var di = new Set,
                pi = function(t) {
                    return t["delete"](0), t.add(-0), !t.has(0)
                }(di),
                fi = di.add(1) === di;
            if (!pi || !fi) {
                var mi = Set.prototype.add;
                Set.prototype.add = function(t) {
                    return n(mi, this, 0 === t ? 0 : t), this
                }, Z.preserveToString(Set.prototype.add, mi)
            }
            if (!pi) {
                var gi = Set.prototype.has;
                Set.prototype.has = function(t) {
                    return n(gi, this, 0 === t ? 0 : t)
                }, Z.preserveToString(Set.prototype.has, gi);
                var vi = Set.prototype["delete"];
                Set.prototype["delete"] = function(t) {
                    return n(vi, this, 0 === t ? 0 : t)
                }, Z.preserveToString(Set.prototype["delete"], vi)
            }
            var yi = y(b.Map, function(t) {
                    var e = new t([]);
                    return e.set(42, 42), e instanceof t
                }),
                _i = Object.setPrototypeOf && !yi,
                bi = function() {
                    try {
                        return !(b.Map() instanceof b.Map)
                    } catch (t) {
                        return t instanceof TypeError
                    }
                }();
            if (0 !== b.Map.length || _i || !bi) {
                var $i = b.Map;
                b.Map = function Vi() {
                    if (!(this instanceof Vi)) throw new TypeError('Constructor Map requires "new"');
                    var t = new $i;
                    return arguments.length > 0 && ei(Vi, t, arguments[0]), Object.setPrototypeOf(t, Vi.prototype), m(t, "constructor", Vi, !0), t
                }, b.Map.prototype = $i.prototype, Z.preserveToString(b.Map, $i)
            }
            var wi = y(b.Set, function(t) {
                    var e = new t([]);
                    return e.add(42, 42), e instanceof t
                }),
                xi = Object.setPrototypeOf && !wi,
                Li = function() {
                    try {
                        return !(b.Set() instanceof b.Set)
                    } catch (t) {
                        return t instanceof TypeError
                    }
                }();
            if (0 !== b.Set.length || xi || !Li) {
                var Ci = b.Set;
                b.Set = function Wi() {
                    if (!(this instanceof Wi)) throw new TypeError('Constructor Set requires "new"');
                    var t = new Ci;
                    return arguments.length > 0 && ni(Wi, t, arguments[0]), Object.setPrototypeOf(t, Wi.prototype), m(t, "constructor", Wi, !0), t
                }, b.Set.prototype = Ci.prototype, Z.preserveToString(b.Set, Ci)
            }
            var Si = !a(function() {
                return (new Map).keys().next().done
            });
            if (("function" != typeof b.Map.prototype.clear || 0 !== (new b.Set).size || 0 !== (new b.Map).size || "function" != typeof b.Map.prototype.keys || "function" != typeof b.Set.prototype.keys || "function" != typeof b.Map.prototype.forEach || "function" != typeof b.Set.prototype.forEach || s(b.Map) || s(b.Set) || "function" != typeof(new b.Map).keys().next || Si || !yi) && (delete b.Map, delete b.Set, g(b, {
                    Map: ii.Map,
                    Set: ii.Set
                }, !0)), b.Set.prototype.keys !== b.Set.prototype.values && m(b.Set.prototype, "keys", b.Set.prototype.values, !0), Q(Object.getPrototypeOf((new b.Map).keys())), Q(Object.getPrototypeOf((new b.Set).keys())), c && "has" !== b.Set.prototype.has.name) {
                var Ei = b.Set.prototype.has;
                X(b.Set.prototype, "has", function(t) {
                    return n(Ei, this, t)
                })
            }
        }
        g(b, ii), K(b.Map), K(b.Set)
    }
    var ki = function(t) {
            if (!G.TypeIsObject(t)) throw new TypeError("target must be an object")
        },
        Pi = {
            apply: function() {
                return e(G.Call, null, arguments)
            },
            construct: function(t, e) {
                if (!G.IsConstructor(t)) throw new TypeError("First argument must be a constructor.");
                var n = arguments.length < 3 ? t : arguments[2];
                if (!G.IsConstructor(n)) throw new TypeError("new.target must be a constructor.");
                return G.Construct(t, e, n, "internal")
            },
            deleteProperty: function(t, e) {
                if (ki(t), u) {
                    var n = Object.getOwnPropertyDescriptor(t, e);
                    if (n && !n.configurable) return !1
                }
                return delete t[e]
            },
            enumerate: function(t) {
                return ki(t), new $e(t, "key")
            },
            has: function(t, e) {
                return ki(t), e in t
            }
        };
    Object.getOwnPropertyNames && Object.assign(Pi, {
        ownKeys: function(t) {
            ki(t);
            var e = Object.getOwnPropertyNames(t);
            return G.IsCallable(Object.getOwnPropertySymbols) && E(e, Object.getOwnPropertySymbols(t)), e
        }
    });
    var Ti = function(t) {
        return !o(t)
    };
    if (Object.preventExtensions && Object.assign(Pi, {
            isExtensible: function(t) {
                return ki(t), Object.isExtensible(t)
            },
            preventExtensions: function(t) {
                return ki(t), Ti(function() {
                    Object.preventExtensions(t)
                })
            }
        }), u) {
        var Mi = function(t, e, i) {
                var r = Object.getOwnPropertyDescriptor(t, e);
                if (!r) {
                    var o = Object.getPrototypeOf(t);
                    return null === o ? void 0 : Mi(o, e, i)
                }
                return "value" in r ? r.value : r.get ? n(r.get, i) : void 0
            },
            Di = function(t, e, i, r) {
                var o = Object.getOwnPropertyDescriptor(t, e);
                if (!o) {
                    var a = Object.getPrototypeOf(t);
                    if (null !== a) return Di(a, e, i, r);
                    o = {
                        value: void 0,
                        writable: !0,
                        enumerable: !0,
                        configurable: !0
                    }
                }
                if ("value" in o) {
                    if (!o.writable) return !1;
                    if (!G.TypeIsObject(r)) return !1;
                    var s = Object.getOwnPropertyDescriptor(r, e);
                    return s ? W.defineProperty(r, e, {
                        value: i
                    }) : W.defineProperty(r, e, {
                        value: i,
                        writable: !0,
                        enumerable: !0,
                        configurable: !0
                    })
                }
                return o.set ? (n(o.set, r, i), !0) : !1
            };
        Object.assign(Pi, {
            defineProperty: function(t, e, n) {
                return ki(t), Ti(function() {
                    Object.defineProperty(t, e, n)
                })
            },
            getOwnPropertyDescriptor: function(t, e) {
                return ki(t), Object.getOwnPropertyDescriptor(t, e)
            },
            get: function(t, e) {
                ki(t);
                var n = arguments.length > 2 ? arguments[2] : t;
                return Mi(t, e, n)
            },
            set: function(t, e, n) {
                ki(t);
                var i = arguments.length > 3 ? arguments[3] : t;
                return Di(t, e, n, i)
            }
        })
    }
    if (Object.getPrototypeOf) {
        var Oi = Object.getPrototypeOf;
        Pi.getPrototypeOf = function(t) {
            return ki(t), Oi(t)
        }
    }
    if (Object.setPrototypeOf && Pi.getPrototypeOf) {
        var Ai = function(t, e) {
            for (var n = e; n;) {
                if (t === n) return !0;
                n = Pi.getPrototypeOf(n)
            }
            return !1
        };
        Object.assign(Pi, {
            setPrototypeOf: function(t, e) {
                if (ki(t), null !== e && !G.TypeIsObject(e)) throw new TypeError("proto must be an object or null");
                return e === W.getPrototypeOf(t) ? !0 : W.isExtensible && !W.isExtensible(t) ? !1 : Ai(t, e) ? !1 : (Object.setPrototypeOf(t, e), !0)
            }
        })
    }
    var Ii = function(t, e) {
        if (G.IsCallable(b.Reflect[t])) {
            var n = a(function() {
                return b.Reflect[t](1), b.Reflect[t](0 / 0), b.Reflect[t](!0), !0
            });
            n && X(b.Reflect, t, e)
        } else m(b.Reflect, t, e)
    };
    if (Object.keys(Pi).forEach(function(t) {
            Ii(t, Pi[t])
        }), c && "getPrototypeOf" !== b.Reflect.getPrototypeOf.name) {
        var Ni = b.Reflect.getPrototypeOf;
        X(b.Reflect, "getPrototypeOf", function(t) {
            return n(Ni, b.Reflect, t)
        })
    }
    if (b.Reflect.setPrototypeOf && a(function() {
            return b.Reflect.setPrototypeOf(1, {}), !0
        }) && X(b.Reflect, "setPrototypeOf", Pi.setPrototypeOf), b.Reflect.defineProperty && (a(function() {
            var t = !b.Reflect.defineProperty(1, "test", {
                    value: 1
                }),
                e = "function" != typeof Object.preventExtensions || !b.Reflect.defineProperty(Object.preventExtensions({}), "test", {});
            return t && e
        }) || X(b.Reflect, "defineProperty", Pi.defineProperty)), b.Reflect.construct && (a(function() {
            var t = function() {};
            return b.Reflect.construct(function() {}, [], t) instanceof t
        }) || X(b.Reflect, "construct", Pi.construct)), "Invalid Date" !== String(new Date(0 / 0))) {
        var Ri = Date.prototype.toString,
            Ui = function() {
                var t = +this;
                return t !== t ? "Invalid Date" : n(Ri, this)
            };
        X(Date.prototype, "toString", Ui)
    }
    var ji = {
        anchor: function(t) {
            return G.CreateHTML(this, "a", "name", t)
        },
        big: function() {
            return G.CreateHTML(this, "big", "", "")
        },
        blink: function() {
            return G.CreateHTML(this, "blink", "", "")
        },
        bold: function() {
            return G.CreateHTML(this, "b", "", "")
        },
        fixed: function() {
            return G.CreateHTML(this, "tt", "", "")
        },
        fontcolor: function(t) {
            return G.CreateHTML(this, "font", "color", t)
        },
        fontsize: function(t) {
            return G.CreateHTML(this, "font", "size", t)
        },
        italics: function() {
            return G.CreateHTML(this, "i", "", "")
        },
        link: function(t) {
            return G.CreateHTML(this, "a", "href", t)
        },
        small: function() {
            return G.CreateHTML(this, "small", "", "")
        },
        strike: function() {
            return G.CreateHTML(this, "strike", "", "")
        },
        sub: function() {
            return G.CreateHTML(this, "sub", "", "")
        },
        sup: function() {
            return G.CreateHTML(this, "sup", "", "")
        }
    };
    h(Object.keys(ji), function(t) {
        var e = String.prototype[t],
            i = !1;
        if (G.IsCallable(e)) {
            var r = n(e, "", ' " '),
                o = L([], r.match(/"/g)).length;
            i = r !== r.toLowerCase() || o > 2
        } else i = !0;
        i && X(String.prototype, t, ji[t])
    });
    var Bi = function() {
            if (!q.symbol(R.iterator)) return !1;
            var t = "object" == typeof JSON && "function" == typeof JSON.stringify ? JSON.stringify : null;
            if (!t) return !1;
            if ("undefined" != typeof t(R())) return !0;
            if ("[null]" !== t([R()])) return !0;
            var e = {
                a: R()
            };
            return e[R()] = !0, "{}" !== t(e) ? !0 : !1
        }(),
        Fi = a(function() {
            return q.symbol(R.iterator) ? "{}" === JSON.stringify(Object(R())) && "[{}]" === JSON.stringify([Object(R())]) : !0
        });
    if (Bi || !Fi) {
        var zi = JSON.stringify;
        X(JSON, "stringify", function(t) {
            if ("symbol" != typeof t) {
                var e;
                arguments.length > 1 && (e = arguments[1]);
                var r = [t];
                if (i(e)) r.push(e);
                else {
                    var o = (G.IsCallable(e) ? e : null, function(t, i) {
                        var r = e ? n(e, this, t, i) : i;
                        return "symbol" != typeof r ? q.symbol(r) ? Xe({})(r) : r : void 0
                    });
                    r.push(o)
                }
                return arguments.length > 2 && r.push(arguments[2]), zi.apply(this, r)
            }
        })
    }
    return b
});
