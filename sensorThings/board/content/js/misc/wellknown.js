! function(t) {
    if ("object" == typeof exports && "undefined" != typeof module) module.exports = t();
    else if ("function" == typeof define && define.amd) define([], t);
    else {
        var e;
        e = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, e.wellknown = t()
    }
}(function() {
    return function t(e, n, i) {
        function r(s, a) {
            if (!n[s]) {
                if (!e[s]) {
                    var l = "function" == typeof require && require;
                    if (!a && l) return l(s, !0);
                    if (o) return o(s, !0);
                    var u = new Error("Cannot find module '" + s + "'");
                    throw u.code = "MODULE_NOT_FOUND", u
                }
                var c = n[s] = {
                    exports: {}
                };
                e[s][0].call(c.exports, function(t) {
                    var n = e[s][1][t];
                    return r(n ? n : t)
                }, c, c.exports, t, e, n, i)
            }
            return n[s].exports
        }
        for (var o = "function" == typeof require && require, s = 0; s < i.length; s++) r(i[s]);
        return r
    }({
        1: [function(require, module) {
            function t(t) {
                function e(t) {
                    var e = g.substring(y).match(t);
                    return e ? (y += e[0].length, e[0]) : null
                }

                function n(t) {
                    return t && v.match(/\d+/) && (t.crs = {
                        type: "name",
                        properties: {
                            name: "urn:ogc:def:crs:EPSG::" + v
                        }
                    }), t
                }

                function r() {
                    e(/^\s*/)
                }

                function o() {
                    r();
                    for (var t, n = 0, o = [], s = [o], a = o; t = e(/^(\()/) || e(/^(\))/) || e(/^(\,)/) || e(i);) {
                        if ("(" === t) s.push(a), a = [], s[s.length - 1].push(a), n++;
                        else if (")" === t) {
                            if (0 === a.length) return null;
                            if (a = s.pop(), !a) return null;
                            if (n--, 0 === n) break
                        } else if ("," === t) a = [], s[s.length - 1].push(a);
                        else {
                            if (t.split(/\s/g).some(isNaN)) return null;
                            Array.prototype.push.apply(a, t.split(/\s/g).map(parseFloat))
                        }
                        r()
                    }
                    return 0 !== n ? null : o
                }

                function s() {
                    for (var t, n, o = []; n = e(i) || e(/^(\,)/);) "," === n ? (o.push(t), t = []) : n.split(/\s/g).some(isNaN) || (t || (t = []), Array.prototype.push.apply(t, n.split(/\s/g).map(parseFloat))), r();
                    return t ? (o.push(t), o.length ? o : null) : null
                }

                function a() {
                    if (!e(/^(point)/i)) return null;
                    if (r(), !e(/^(\()/)) return null;
                    var t = s();
                    return t ? (r(), e(/^(\))/) ? {
                        type: "Point",
                        coordinates: t[0]
                    } : null) : null
                }

                function l() {
                    if (!e(/^(multipoint)/i)) return null;
                    r();
                    var t = o();
                    return t ? (r(), {
                        type: "MultiPoint",
                        coordinates: t
                    }) : null
                }

                function u() {
                    if (!e(/^(multilinestring)/i)) return null;
                    r();
                    var t = o();
                    return t ? (r(), {
                        type: "MultiLineString",
                        coordinates: t
                    }) : null
                }

                function c() {
                    if (!e(/^(linestring)/i)) return null;
                    if (r(), !e(/^(\()/)) return null;
                    var t = s();
                    return t && e(/^(\))/) ? {
                        type: "LineString",
                        coordinates: t
                    } : null
                }

                function h() {
                    if (!e(/^(polygon)/i)) return null;
                    r();
                    var t = o();
                    return t ? {
                        type: "Polygon",
                        coordinates: t
                    } : null
                }

                function d() {
                    if (!e(/^(multipolygon)/i)) return null;
                    r();
                    var t = o();
                    return t ? {
                        type: "MultiPolygon",
                        coordinates: t
                    } : null
                }

                function f() {
                    var t, n = [];
                    if (!e(/^(geometrycollection)/i)) return null;
                    if (r(), !e(/^(\()/)) return null;
                    for (; t = p();) n.push(t), r(), e(/^(\,)/), r();
                    return e(/^(\))/) ? {
                        type: "GeometryCollection",
                        geometries: n
                    } : null
                }

                function p() {
                    return a() || c() || h() || l() || u() || d() || f()
                }
                var m = t.split(";"),
                    g = m.pop(),
                    v = (m.shift() || "").split("=").pop(),
                    y = 0;
                return n(p())
            }

            function e(t) {
                function n(t) {
                    return 2 === t.length ? t[0] + " " + t[1] : 3 === t.length ? t[0] + " " + t[1] + " " + t[2] : void 0
                }

                function i(t) {
                    return t.map(n).join(", ")
                }

                function r(t) {
                    return t.map(i).map(s).join(", ")
                }

                function o(t) {
                    return t.map(r).map(s).join(", ")
                }

                function s(t) {
                    return "(" + t + ")"
                }
                switch ("Feature" === t.type && (t = t.geometry), t.type) {
                    case "Point":
                        return "POINT (" + n(t.coordinates) + ")";
                    case "LineString":
                        return "LINESTRING (" + i(t.coordinates) + ")";
                    case "Polygon":
                        return "POLYGON (" + r(t.coordinates) + ")";
                    case "MultiPoint":
                        return "MULTIPOINT (" + i(t.coordinates) + ")";
                    case "MultiPolygon":
                        return "MULTIPOLYGON (" + o(t.coordinates) + ")";
                    case "MultiLineString":
                        return "MULTILINESTRING (" + r(t.coordinates) + ")";
                    case "GeometryCollection":
                        return "GEOMETRYCOLLECTION (" + t.geometries.map(e).join(", ") + ")";
                    default:
                        throw new Error("stringify requires a valid GeoJSON Feature or geometry object as input")
                }
            }
            module.exports = t, module.exports.parse = t, module.exports.stringify = e;
            var n = /[-+]?([0-9]*\.[0-9]+|[0-9]+)([eE][-+]?[0-9]+)?/,
                i = new RegExp("^" + n.source + "\\s" + n.source + "(\\s" + n.source + ")?")
        }, {}]
    }, {}, [1])(1)
});
