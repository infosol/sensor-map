! function t(e, n, i) {
    function o(a, s) {
        if (!n[a]) {
            if (!e[a]) {
                var l = "function" == typeof require && require;
                if (!s && l) return l(a, !0);
                if (r) return r(a, !0);
                var u = new Error("Cannot find module '" + a + "'");
                throw u.code = "MODULE_NOT_FOUND", u
            }
            var c = n[a] = {
                exports: {}
            };
            e[a][0].call(c.exports, function(t) {
                var n = e[a][1][t];
                return o(n ? n : t)
            }, c, c.exports, t, e, n, i)
        }
        return n[a].exports
    }
    for (var r = "function" == typeof require && require, a = 0; a < i.length; a++) o(i[a]);
    return o
}({
    1: [function(require) {
        angular.module("nemLogging", []), angular.module("nemLogging").provider("nemDebug", function() {
            var t = null;
            return t = require("debug"), this.$get = function() {
                return t
            }, this.debug = t, this
        });
        var t = function(t, e) {
            return function() {
                return t.apply(e, arguments)
            }
        };
        angular.module("nemLogging").provider("nemSimpleLogger", ["nemDebugProvider", function(e) {
            var n, i, o, r, a, s, l, u, c, h, d;
            for (h = e.debug, o = ["debug", "info", "warn", "error", "log"], n = {}, u = l = 0, c = o.length; c > l; u = ++l) d = o[u], n[d] = u;
            return a = function(t, e, n) {
                return t >= e ? n() : void 0
            }, r = function(t) {
                var e, n, i;
                if (e = !1, !t) return e;
                for (n = 0, i = o.length; i > n && (d = o[n], e = null != t[d] && "function" == typeof t[d], e); n++);
                return e
            }, s = function(t, e) {
                var n, i, r, a;
                for (n = h(t), a = {}, i = 0, r = o.length; r > i; i++) d = o[i], a[d] = "debug" === d ? n : e[d];
                return a
            }, i = function() {
                function e(e) {
                    var i, s, l, u, c;
                    if (this.$log = e, this.spawn = t(this.spawn, this), !this.$log) throw "internalLogger undefined";
                    if (!r(this.$log)) throw "@$log is invalid";
                    for (this.doLog = !0, c = {}, i = function(t) {
                            return function(e) {
                                return c[e] = function(i) {
                                    return t.doLog ? a(n[e], t.currentLevel, function() {
                                        return t.$log[e](i)
                                    }) : void 0
                                }, t[e] = c[e]
                            }
                        }(this), s = 0, l = o.length; l > s; s++) u = o[s], i(u);
                    this.LEVELS = n, this.currentLevel = n.error
                }
                return e.prototype.spawn = function(t) {
                    if ("string" == typeof t) {
                        if (!r(this.$log)) throw "@$log is invalid";
                        if (!h) throw "nemDebug is undefined this is probably the light version of this library sep debug logggers is not supported!";
                        return s(t, this.$log)
                    }
                    return new e(t || this.$log)
                }, e
            }(), this.decorator = ["$log", function(t) {
                var e;
                return e = new i(t), e.currentLevel = n.debug, e
            }], this.$get = ["$log", function(t) {
                return new i(t)
            }], this
        }])
    }, {
        debug: 2
    }],
    2: [function(require, module, exports) {
        function t() {
            return "WebkitAppearance" in document.documentElement.style || window.console && (console.firebug || console.exception && console.table) || navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31
        }

        function e() {
            var t = arguments,
                e = this.useColors;
            if (t[0] = (e ? "%c" : "") + this.namespace + (e ? " %c" : " ") + t[0] + (e ? "%c " : " ") + "+" + exports.humanize(this.diff), !e) return t;
            var n = "color: " + this.color;
            t = [t[0], n, "color: inherit"].concat(Array.prototype.slice.call(t, 1));
            var i = 0,
                o = 0;
            return t[0].replace(/%[a-z%]/g, function(t) {
                "%%" !== t && (i++, "%c" === t && (o = i))
            }), t.splice(o, 0, n), t
        }

        function n() {
            return "object" == typeof console && console.log && Function.prototype.apply.call(console.log, console, arguments)
        }

        function i(t) {
            try {
                null == t ? exports.storage.removeItem("debug") : exports.storage.debug = t
            } catch (e) {}
        }

        function o() {
            var t;
            try {
                t = exports.storage.debug
            } catch (e) {}
            return t
        }

        function r() {
            try {
                return window.localStorage
            } catch (t) {}
        }
        exports = module.exports = require("./debug"), exports.log = n, exports.formatArgs = e, exports.save = i, exports.load = o, exports.useColors = t, exports.storage = "undefined" != typeof chrome && "undefined" != typeof chrome.storage ? chrome.storage.local : r(), exports.colors = ["lightseagreen", "forestgreen", "goldenrod", "dodgerblue", "darkorchid", "crimson"], exports.formatters.j = function(t) {
            return JSON.stringify(t)
        }, exports.enable(o())
    }, {
        "./debug": 3
    }],
    3: [function(require, module, exports) {
        function t() {
            return exports.colors[s++ % exports.colors.length]
        }

        function e(e) {
            function n() {}

            function i() {
                var e = i,
                    n = +new Date,
                    o = n - (a || n);
                e.diff = o, e.prev = a, e.curr = n, a = n, null == e.useColors && (e.useColors = exports.useColors()), null == e.color && e.useColors && (e.color = t());
                var r = Array.prototype.slice.call(arguments);
                r[0] = exports.coerce(r[0]), "string" != typeof r[0] && (r = ["%o"].concat(r));
                var s = 0;
                r[0] = r[0].replace(/%([a-z%])/g, function(t, n) {
                    if ("%%" === t) return t;
                    s++;
                    var i = exports.formatters[n];
                    if ("function" == typeof i) {
                        var o = r[s];
                        t = i.call(e, o), r.splice(s, 1), s--
                    }
                    return t
                }), "function" == typeof exports.formatArgs && (r = exports.formatArgs.apply(e, r));
                var l = i.log || exports.log || console.log.bind(console);
                l.apply(e, r)
            }
            n.enabled = !1, i.enabled = !0;
            var o = exports.enabled(e) ? i : n;
            return o.namespace = e, o
        }

        function n(t) {
            exports.save(t);
            for (var e = (t || "").split(/[\s,]+/), n = e.length, i = 0; n > i; i++) e[i] && (t = e[i].replace(/\*/g, ".*?"), "-" === t[0] ? exports.skips.push(new RegExp("^" + t.substr(1) + "$")) : exports.names.push(new RegExp("^" + t + "$")))
        }

        function i() {
            exports.enable("")
        }

        function o(t) {
            var e, n;
            for (e = 0, n = exports.skips.length; n > e; e++)
                if (exports.skips[e].test(t)) return !1;
            for (e = 0, n = exports.names.length; n > e; e++)
                if (exports.names[e].test(t)) return !0;
            return !1
        }

        function r(t) {
            return t instanceof Error ? t.stack || t.message : t
        }
        exports = module.exports = e, exports.coerce = r, exports.disable = i, exports.enable = n, exports.enabled = o, exports.humanize = require("ms"), exports.names = [], exports.skips = [], exports.formatters = {};
        var a, s = 0
    }, {
        ms: 4
    }],
    4: [function(require, module) {
        function t(t) {
            if (t = "" + t, !(t.length > 1e4)) {
                var e = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(t);
                if (e) {
                    var n = parseFloat(e[1]),
                        i = (e[2] || "ms").toLowerCase();
                    switch (i) {
                        case "years":
                        case "year":
                        case "yrs":
                        case "yr":
                        case "y":
                            return n * l;
                        case "days":
                        case "day":
                        case "d":
                            return n * s;
                        case "hours":
                        case "hour":
                        case "hrs":
                        case "hr":
                        case "h":
                            return n * a;
                        case "minutes":
                        case "minute":
                        case "mins":
                        case "min":
                        case "m":
                            return n * r;
                        case "seconds":
                        case "second":
                        case "secs":
                        case "sec":
                        case "s":
                            return n * o;
                        case "milliseconds":
                        case "millisecond":
                        case "msecs":
                        case "msec":
                        case "ms":
                            return n
                    }
                }
            }
        }

        function e(t) {
            return t >= s ? Math.round(t / s) + "d" : t >= a ? Math.round(t / a) + "h" : t >= r ? Math.round(t / r) + "m" : t >= o ? Math.round(t / o) + "s" : t + "ms"
        }

        function n(t) {
            return i(t, s, "day") || i(t, a, "hour") || i(t, r, "minute") || i(t, o, "second") || t + " ms"
        }

        function i(t, e, n) {
            return e > t ? void 0 : 1.5 * e > t ? Math.floor(t / e) + " " + n : Math.ceil(t / e) + " " + n + "s"
        }
        var o = 1e3,
            r = 60 * o,
            a = 60 * r,
            s = 24 * a,
            l = 365.25 * s;
        module.exports = function(i, o) {
            return o = o || {}, "string" == typeof i ? t(i) : o["long"] ? n(i) : e(i)
        }
    }, {}]
}, {}, [1]);
