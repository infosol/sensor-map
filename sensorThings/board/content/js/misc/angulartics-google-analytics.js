! function(t, e, n) {
    "use strict";
    e.module("angulartics.google.analytics", ["angulartics"]).config(["$analyticsProvider", function(i) {
        function o(e) {
            if (t.ga)
                for (var n = 1; 200 >= n; n++) e["dimension" + n.toString()] && ga("set", "dimension" + n.toString(), e["dimension" + n.toString()]), e["metric" + n.toString()] && ga("set", "metric" + n.toString(), e["metric" + n.toString()])
        }
        i.settings.pageTracking.trackRelativePath = !0, i.settings.ga = {
            additionalAccountNames: n,
            userId: null
        }, i.registerPageTrack(function(n) {
            t._gaq && (_gaq.push(["_trackPageview", n]), e.forEach(i.settings.ga.additionalAccountNames, function(t) {
                _gaq.push([t + "._trackPageview", n])
            })), t.ga && (i.settings.ga.userId && ga("set", "&uid", i.settings.ga.userId), ga("send", "pageview", n), e.forEach(i.settings.ga.additionalAccountNames, function(t) {
                ga(t + ".send", "pageview", n)
            }))
        }), i.registerEventTrack(function(n, r) {
            if (r && r.category || (r = r || {}, r.category = "Event"), r.value) {
                var s = parseInt(r.value, 10);
                r.value = isNaN(s) ? 0 : s
            }
            if (t.ga) {
                var a = {
                    eventCategory: r.category,
                    eventAction: n,
                    eventLabel: r.label,
                    eventValue: r.value,
                    nonInteraction: r.noninteraction,
                    page: r.page || t.location.hash.substring(1) || t.location.pathname,
                    userId: i.settings.ga.userId
                };
                o(r), ga("send", "event", a), e.forEach(i.settings.ga.additionalAccountNames, function(t) {
                    ga(t + ".send", "event", a)
                })
            } else t._gaq && _gaq.push(["_trackEvent", r.category, n, r.label, r.value, r.noninteraction])
        }), i.registerSetUsername(function(t) {
            i.settings.ga.userId = t
        }), i.registerSetUserProperties(function(t) {
            o(t)
        })
    }])
}(window, window.angular);
