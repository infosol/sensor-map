function AngularUUID() {
    function t(t) {
        function e(t, e, n) {
            var i = e && n || 0,
                o = 0;
            for (e = e || [], t.toLowerCase().replace(/[0-9a-f]{2}/g, function(t) {
                    16 > o && (e[i + o++] = p[t])
                }); 16 > o;) e[i + o++] = 0;
            return e
        }

        function n(t, e) {
            var n = e || 0,
                i = d;
            return i[t[n++]] + i[t[n++]] + i[t[n++]] + i[t[n++]] + "-" + i[t[n++]] + i[t[n++]] + "-" + i[t[n++]] + i[t[n++]] + "-" + i[t[n++]] + i[t[n++]] + "-" + i[t[n++]] + i[t[n++]] + i[t[n++]] + i[t[n++]] + i[t[n++]] + i[t[n++]]
        }

        function i(t, e, i) {
            var o = e && i || 0,
                r = e || [];
            t = t || {};
            var s = null != t.clockseq ? t.clockseq : v,
                a = null != t.msecs ? t.msecs : (new Date).getTime(),
                l = null != t.nsecs ? t.nsecs : _ + 1,
                u = a - y + (l - _) / 1e4;
            if (0 > u && null == t.clockseq && (s = s + 1 & 16383), (0 > u || a > y) && null == t.nsecs && (l = 0), l >= 1e4) throw new Error("uuid.v1(): Can't create more than 10M uuids/sec");
            y = a, _ = l, v = s, a += 122192928e5;
            var c = (1e4 * (268435455 & a) + l) % 4294967296;
            r[o++] = c >>> 24 & 255, r[o++] = c >>> 16 & 255, r[o++] = c >>> 8 & 255, r[o++] = 255 & c;
            var h = a / 4294967296 * 1e4 & 268435455;
            r[o++] = h >>> 8 & 255, r[o++] = 255 & h, r[o++] = h >>> 24 & 15 | 16, r[o++] = h >>> 16 & 255, r[o++] = s >>> 8 | 128, r[o++] = 255 & s;
            for (var d = t.node || g, p = 0; 6 > p; p++) r[o + p] = d[p];
            return e ? e : n(r)
        }

        function o(t, e, i) {
            var o = e && i || 0;
            "string" == typeof t && (e = "binary" == t ? new h(16) : null, t = null), t = t || {};
            var s = t.random || (t.rng || r)();
            if (s[6] = 15 & s[6] | 64, s[8] = 63 & s[8] | 128, e)
                for (var a = 0; 16 > a; a++) e[o + a] = s[a];
            return e || n(s)
        }
        var r, s = t;
        if ("function" == typeof s.require) try {
            var a = s.require("crypto").randomBytes;
            r = a && function() {
                return a(16)
            }
        } catch (l) {}
        if (!r && s.crypto && crypto.getRandomValues) {
            var u = new Uint8Array(16);
            r = function() {
                return crypto.getRandomValues(u), u
            }
        }
        if (!r) {
            var c = new Array(16);
            r = function() {
                for (var t, e = 0; 16 > e; e++) 0 === (3 & e) && (t = 4294967296 * Math.random()), c[e] = t >>> ((3 & e) << 3) & 255;
                return c
            }
        }
        for (var h = "function" == typeof s.Buffer ? s.Buffer : Array, d = [], p = {}, f = 0; 256 > f; f++) d[f] = (f + 256).toString(16).substr(1), p[d[f]] = f;
        var m = r(),
            g = [1 | m[0], m[1], m[2], m[3], m[4], m[5]],
            v = 16383 & (m[6] << 8 | m[7]),
            y = 0,
            _ = 0,
            $ = o;
        return $.v1 = i, $.v4 = o, $.parse = e, $.unparse = n, $.BufferClass = h, $
    }
    angular.module("angular-uuid", []).factory("uuid", ["$window", t])
}
"undefined" != typeof module && module.exports ? module.exports = new AngularUUID : "undefined" != typeof define && define.amd ? define(function() {
    return new AngularUUID
}) : AngularUUID();
