angular.module("angular-clipboard", []).directive("clipboard", ["$document", function(t) {
    return {
        restrict: "A",
        scope: {
            onCopied: "&",
            onError: "&",
            text: "="
        },
        link: function(e, n) {
            function i(e) {
                var n = t[0].createElement("textarea");
                return n.style.position = "absolute", n.style.left = "-10000px", n.textContent = e, n
            }

            function o(e) {
                t[0].body.style.webkitUserSelect = "initial";
                var n = t[0].getSelection();
                if (n.removeAllRanges(), e.select(), !t[0].execCommand("copy")) throw "failure copy";
                n.removeAllRanges(), t[0].body.style.webkitUserSelect = ""
            }

            function r(e) {
                var n = i(e);
                t[0].body.appendChild(n), o(n), t[0].body.removeChild(n)
            }
            n.on("click", function() {
                try {
                    r(e.text), e.onCopied && e.onCopied()
                } catch (t) {
                    e.onError && e.onError({
                        err: t
                    })
                }
            })
        }
    }
}]);
