angular.module("sensorBoard").factory("helpers", ["$http", "$q", "$timeout", function(t, e, r) {
    var a = {};
    return a.entityIdChanged = function() {
        var t = null;
        return function(e, a, o, i) {
            r.cancel(t), t = r(function() {
                e(a).then(function(t) {
                    o(t)
                }, function(t) {
                    i(t)
                })
            }, 1e3)
        }
    }(), a.selectExtent = function(t) {
        return t.length >= 400 ? [t[250][0], t[0][0]] : t.length >= 250 ? [t[150][0], t[0][0]] : t.length >= 150 ? [t[100][0], t[0][0]] : t.length >= 50 ? [t[40][0], t[0][0]] : [t[t.length - 1][0], t[0][0]]
    }, a.sampleValues = function(t) {
        return t.length > 500 ? _.sample(t, _.min([t.length, 500])) : t
    }, a
}]);
