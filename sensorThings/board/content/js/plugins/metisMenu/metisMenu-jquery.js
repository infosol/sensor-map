! function(t) {
    function e(e, r) {
        this.element = e, this.settings = t.extend({}, i, r), this._defaults = i, this._name = n, this.init()
    }
    var n = "metisMenu",
        i = {
            toggle: !0
        };
    e.prototype = {
        init: function() {
            var e = t(this.element),
                n = this.settings.toggle;
            e.find("li.active").has("ul").children("ul").addClass("collapse in"), e.find("li").not(".active").has("ul").children("ul").addClass("collapse"), e.find("li").has("ul").children("a").on("click", function(e) {
                e.preventDefault(), t(this).parent("li").toggleClass("active").children("ul").collapse("toggle"), n && t(this).parent("li").siblings().removeClass("active").children("ul.in").collapse("hide")
            })
        }
    }, t.fn[n] = function(i) {
        return this.each(function() {
            t.data(this, "plugin_" + n) || t.data(this, "plugin_" + n, new e(this, i))
        })
    }
}(jQuery, window, document);
