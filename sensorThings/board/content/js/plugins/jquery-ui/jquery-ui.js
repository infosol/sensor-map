! function(t, e) {
    function n(e, n) {
        var r, o, s, a = e.nodeName.toLowerCase();
        return "area" === a ? (r = e.parentNode, o = r.name, e.href && o && "map" === r.nodeName.toLowerCase() ? (s = t("img[usemap=#" + o + "]")[0], !!s && i(s)) : !1) : (/input|select|textarea|button|object/.test(a) ? !e.disabled : "a" === a ? e.href || n : n) && i(e)
    }

    function i(e) {
        return t.expr.filters.visible(e) && !t(e).parents().addBack().filter(function() {
            return "hidden" === t.css(this, "visibility")
        }).length
    }
    var r = 0,
        o = /^ui-id-\d+$/;
    t.ui = t.ui || {}, t.extend(t.ui, {
        version: "1.10.4",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            NUMPAD_ADD: 107,
            NUMPAD_DECIMAL: 110,
            NUMPAD_DIVIDE: 111,
            NUMPAD_ENTER: 108,
            NUMPAD_MULTIPLY: 106,
            NUMPAD_SUBTRACT: 109,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    }), t.fn.extend({
        focus: function(e) {
            return function(n, i) {
                return "number" == typeof n ? this.each(function() {
                    var e = this;
                    setTimeout(function() {
                        t(e).focus(), i && i.call(e)
                    }, n)
                }) : e.apply(this, arguments)
            }
        }(t.fn.focus),
        scrollParent: function() {
            var e;
            return e = t.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function() {
                return /(relative|absolute|fixed)/.test(t.css(this, "position")) && /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
            }).eq(0) : this.parents().filter(function() {
                return /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
            }).eq(0), /fixed/.test(this.css("position")) || !e.length ? t(document) : e
        },
        zIndex: function(n) {
            if (n !== e) return this.css("zIndex", n);
            if (this.length)
                for (var i, r, o = t(this[0]); o.length && o[0] !== document;) {
                    if (i = o.css("position"), ("absolute" === i || "relative" === i || "fixed" === i) && (r = parseInt(o.css("zIndex"), 10), !isNaN(r) && 0 !== r)) return r;
                    o = o.parent()
                }
            return 0
        },
        uniqueId: function() {
            return this.each(function() {
                this.id || (this.id = "ui-id-" + ++r)
            })
        },
        removeUniqueId: function() {
            return this.each(function() {
                o.test(this.id) && t(this).removeAttr("id")
            })
        }
    }), t.extend(t.expr[":"], {
        data: t.expr.createPseudo ? t.expr.createPseudo(function(e) {
            return function(n) {
                return !!t.data(n, e)
            }
        }) : function(e, n, i) {
            return !!t.data(e, i[3])
        },
        focusable: function(e) {
            return n(e, !isNaN(t.attr(e, "tabindex")))
        },
        tabbable: function(e) {
            var i = t.attr(e, "tabindex"),
                r = isNaN(i);
            return (r || i >= 0) && n(e, !r)
        }
    }), t("<a>").outerWidth(1).jquery || t.each(["Width", "Height"], function(n, i) {
        function r(e, n, i, r) {
            return t.each(o, function() {
                n -= parseFloat(t.css(e, "padding" + this)) || 0, i && (n -= parseFloat(t.css(e, "border" + this + "Width")) || 0), r && (n -= parseFloat(t.css(e, "margin" + this)) || 0)
            }), n
        }
        var o = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"],
            s = i.toLowerCase(),
            a = {
                innerWidth: t.fn.innerWidth,
                innerHeight: t.fn.innerHeight,
                outerWidth: t.fn.outerWidth,
                outerHeight: t.fn.outerHeight
            };
        t.fn["inner" + i] = function(n) {
            return n === e ? a["inner" + i].call(this) : this.each(function() {
                t(this).css(s, r(this, n) + "px")
            })
        }, t.fn["outer" + i] = function(e, n) {
            return "number" != typeof e ? a["outer" + i].call(this, e) : this.each(function() {
                t(this).css(s, r(this, e, !0, n) + "px")
            })
        }
    }), t.fn.addBack || (t.fn.addBack = function(t) {
        return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
    }), t("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (t.fn.removeData = function(e) {
        return function(n) {
            return arguments.length ? e.call(this, t.camelCase(n)) : e.call(this)
        }
    }(t.fn.removeData)), t.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), t.support.selectstart = "onselectstart" in document.createElement("div"), t.fn.extend({
        disableSelection: function() {
            return this.bind((t.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function(t) {
                t.preventDefault()
            })
        },
        enableSelection: function() {
            return this.unbind(".ui-disableSelection")
        }
    }), t.extend(t.ui, {
        plugin: {
            add: function(module, e, n) {
                var i, r = t.ui[module].prototype;
                for (i in n) r.plugins[i] = r.plugins[i] || [], r.plugins[i].push([e, n[i]])
            },
            call: function(t, e, n) {
                var i, r = t.plugins[e];
                if (r && t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType)
                    for (i = 0; i < r.length; i++) t.options[r[i][0]] && r[i][1].apply(t.element, n)
            }
        },
        hasScroll: function(e, n) {
            if ("hidden" === t(e).css("overflow")) return !1;
            var i = n && "left" === n ? "scrollLeft" : "scrollTop",
                r = !1;
            return e[i] > 0 ? !0 : (e[i] = 1, r = e[i] > 0, e[i] = 0, r)
        }
    })
}(jQuery),
function(t, e) {
    var n = 0,
        i = Array.prototype.slice,
        r = t.cleanData;
    t.cleanData = function(e) {
        for (var n, i = 0; null != (n = e[i]); i++) try {
            t(n).triggerHandler("remove")
        } catch (o) {}
        r(e)
    }, t.widget = function(e, n, i) {
        var r, o, s, a, l = {},
            u = e.split(".")[0];
        e = e.split(".")[1], r = u + "-" + e, i || (i = n, n = t.Widget), t.expr[":"][r.toLowerCase()] = function(e) {
            return !!t.data(e, r)
        }, t[u] = t[u] || {}, o = t[u][e], s = t[u][e] = function(t, e) {
            return this._createWidget ? void(arguments.length && this._createWidget(t, e)) : new s(t, e)
        }, t.extend(s, o, {
            version: i.version,
            _proto: t.extend({}, i),
            _childConstructors: []
        }), a = new n, a.options = t.widget.extend({}, a.options), t.each(i, function(e, i) {
            return t.isFunction(i) ? void(l[e] = function() {
                var t = function() {
                        return n.prototype[e].apply(this, arguments)
                    },
                    r = function(t) {
                        return n.prototype[e].apply(this, t)
                    };
                return function() {
                    var e, n = this._super,
                        o = this._superApply;
                    return this._super = t, this._superApply = r, e = i.apply(this, arguments), this._super = n, this._superApply = o, e
                }
            }()) : void(l[e] = i)
        }), s.prototype = t.widget.extend(a, {
            widgetEventPrefix: o ? a.widgetEventPrefix || e : e
        }, l, {
            constructor: s,
            namespace: u,
            widgetName: e,
            widgetFullName: r
        }), o ? (t.each(o._childConstructors, function(e, n) {
            var i = n.prototype;
            t.widget(i.namespace + "." + i.widgetName, s, n._proto)
        }), delete o._childConstructors) : n._childConstructors.push(s), t.widget.bridge(e, s)
    }, t.widget.extend = function(n) {
        for (var r, o, s = i.call(arguments, 1), a = 0, l = s.length; l > a; a++)
            for (r in s[a]) o = s[a][r], s[a].hasOwnProperty(r) && o !== e && (n[r] = t.isPlainObject(o) ? t.isPlainObject(n[r]) ? t.widget.extend({}, n[r], o) : t.widget.extend({}, o) : o);
        return n
    }, t.widget.bridge = function(n, r) {
        var o = r.prototype.widgetFullName || n;
        t.fn[n] = function(s) {
            var a = "string" == typeof s,
                l = i.call(arguments, 1),
                u = this;
            return s = !a && l.length ? t.widget.extend.apply(null, [s].concat(l)) : s, this.each(a ? function() {
                var i, r = t.data(this, o);
                return r ? t.isFunction(r[s]) && "_" !== s.charAt(0) ? (i = r[s].apply(r, l), i !== r && i !== e ? (u = i && i.jquery ? u.pushStack(i.get()) : i, !1) : void 0) : t.error("no such method '" + s + "' for " + n + " widget instance") : t.error("cannot call methods on " + n + " prior to initialization; attempted to call method '" + s + "'")
            } : function() {
                var e = t.data(this, o);
                e ? e.option(s || {})._init() : t.data(this, o, new r(s, this))
            }), u
        }
    }, t.Widget = function() {}, t.Widget._childConstructors = [], t.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {
            disabled: !1,
            create: null
        },
        _createWidget: function(e, i) {
            i = t(i || this.defaultElement || this)[0], this.element = t(i), this.uuid = n++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this.bindings = t(), this.hoverable = t(), this.focusable = t(), i !== this && (t.data(i, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function(t) {
                    t.target === i && this.destroy()
                }
            }), this.document = t(i.style ? i.ownerDocument : i.document || i), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
        },
        _getCreateOptions: t.noop,
        _getCreateEventData: t.noop,
        _create: t.noop,
        _init: t.noop,
        destroy: function() {
            this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
        },
        _destroy: t.noop,
        widget: function() {
            return this.element
        },
        option: function(n, i) {
            var r, o, s, a = n;
            if (0 === arguments.length) return t.widget.extend({}, this.options);
            if ("string" == typeof n)
                if (a = {}, r = n.split("."), n = r.shift(), r.length) {
                    for (o = a[n] = t.widget.extend({}, this.options[n]), s = 0; s < r.length - 1; s++) o[r[s]] = o[r[s]] || {}, o = o[r[s]];
                    if (n = r.pop(), 1 === arguments.length) return o[n] === e ? null : o[n];
                    o[n] = i
                } else {
                    if (1 === arguments.length) return this.options[n] === e ? null : this.options[n];
                    a[n] = i
                } return this._setOptions(a), this
        },
        _setOptions: function(t) {
            var e;
            for (e in t) this._setOption(e, t[e]);
            return this
        },
        _setOption: function(t, e) {
            return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!e).attr("aria-disabled", e), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
        },
        enable: function() {
            return this._setOption("disabled", !1)
        },
        disable: function() {
            return this._setOption("disabled", !0)
        },
        _on: function(e, n, i) {
            var r, o = this;
            "boolean" != typeof e && (i = n, n = e, e = !1), i ? (n = r = t(n), this.bindings = this.bindings.add(n)) : (i = n, n = this.element, r = this.widget()), t.each(i, function(i, s) {
                function a() {
                    return e || o.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof s ? o[s] : s).apply(o, arguments) : void 0
                }
                "string" != typeof s && (a.guid = s.guid = s.guid || a.guid || t.guid++);
                var l = i.match(/^(\w+)\s*(.*)$/),
                    u = l[1] + o.eventNamespace,
                    c = l[2];
                c ? r.delegate(c, u, a) : n.bind(u, a)
            })
        },
        _off: function(t, e) {
            e = (e || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(e).undelegate(e)
        },
        _delay: function(t, e) {
            function n() {
                return ("string" == typeof t ? i[t] : t).apply(i, arguments)
            }
            var i = this;
            return setTimeout(n, e || 0)
        },
        _hoverable: function(e) {
            this.hoverable = this.hoverable.add(e), this._on(e, {
                mouseenter: function(e) {
                    t(e.currentTarget).addClass("ui-state-hover")
                },
                mouseleave: function(e) {
                    t(e.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function(e) {
            this.focusable = this.focusable.add(e), this._on(e, {
                focusin: function(e) {
                    t(e.currentTarget).addClass("ui-state-focus")
                },
                focusout: function(e) {
                    t(e.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function(e, n, i) {
            var r, o, s = this.options[e];
            if (i = i || {}, n = t.Event(n), n.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), n.target = this.element[0], o = n.originalEvent)
                for (r in o) r in n || (n[r] = o[r]);
            return this.element.trigger(n, i), !(t.isFunction(s) && s.apply(this.element[0], [n].concat(i)) === !1 || n.isDefaultPrevented())
        }
    }, t.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(e, n) {
        t.Widget.prototype["_" + e] = function(i, r, o) {
            "string" == typeof r && (r = {
                effect: r
            });
            var s, a = r ? r === !0 || "number" == typeof r ? n : r.effect || n : e;
            r = r || {}, "number" == typeof r && (r = {
                duration: r
            }), s = !t.isEmptyObject(r), r.complete = o, r.delay && i.delay(r.delay), s && t.effects && t.effects.effect[a] ? i[e](r) : a !== e && i[a] ? i[a](r.duration, r.easing, o) : i.queue(function(n) {
                t(this)[e](), o && o.call(i[0]), n()
            })
        }
    })
}(jQuery),
function(t) {
    var e = !1;
    t(document).mouseup(function() {
        e = !1
    }), t.widget("ui.mouse", {
        version: "1.10.4",
        options: {
            cancel: "input,textarea,button,select,option",
            distance: 1,
            delay: 0
        },
        _mouseInit: function() {
            var e = this;
            this.element.bind("mousedown." + this.widgetName, function(t) {
                return e._mouseDown(t)
            }).bind("click." + this.widgetName, function(n) {
                return !0 === t.data(n.target, e.widgetName + ".preventClickEvent") ? (t.removeData(n.target, e.widgetName + ".preventClickEvent"), n.stopImmediatePropagation(), !1) : void 0
            }), this.started = !1
        },
        _mouseDestroy: function() {
            this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
        },
        _mouseDown: function(n) {
            if (!e) {
                this._mouseStarted && this._mouseUp(n), this._mouseDownEvent = n;
                var i = this,
                    r = 1 === n.which,
                    o = "string" == typeof this.options.cancel && n.target.nodeName ? t(n.target).closest(this.options.cancel).length : !1;
                return r && !o && this._mouseCapture(n) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                    i.mouseDelayMet = !0
                }, this.options.delay)), this._mouseDistanceMet(n) && this._mouseDelayMet(n) && (this._mouseStarted = this._mouseStart(n) !== !1, !this._mouseStarted) ? (n.preventDefault(), !0) : (!0 === t.data(n.target, this.widgetName + ".preventClickEvent") && t.removeData(n.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function(t) {
                    return i._mouseMove(t)
                }, this._mouseUpDelegate = function(t) {
                    return i._mouseUp(t)
                }, t(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), n.preventDefault(), e = !0, !0)) : !0
            }
        },
        _mouseMove: function(e) {
            return t.ui.ie && (!document.documentMode || document.documentMode < 9) && !e.button ? this._mouseUp(e) : this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== !1, this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted)
        },
        _mouseUp: function(e) {
            return t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(e)), !1
        },
        _mouseDistanceMet: function(t) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance
        },
        _mouseDelayMet: function() {
            return this.mouseDelayMet
        },
        _mouseStart: function() {},
        _mouseDrag: function() {},
        _mouseStop: function() {},
        _mouseCapture: function() {
            return !0
        }
    })
}(jQuery),
function(t) {
    t.widget("ui.draggable", t.ui.mouse, {
        version: "1.10.4",
        widgetEventPrefix: "drag",
        options: {
            addClasses: !0,
            appendTo: "parent",
            axis: !1,
            connectToSortable: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            iframeFix: !1,
            opacity: !1,
            refreshPositions: !1,
            revert: !1,
            revertDuration: 500,
            scope: "default",
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: !1,
            snapMode: "both",
            snapTolerance: 20,
            stack: !1,
            zIndex: !1,
            drag: null,
            start: null,
            stop: null
        },
        _create: function() {
            "original" !== this.options.helper || /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative"), this.options.addClasses && this.element.addClass("ui-draggable"), this.options.disabled && this.element.addClass("ui-draggable-disabled"), this._mouseInit()
        },
        _destroy: function() {
            this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"), this._mouseDestroy()
        },
        _mouseCapture: function(e) {
            var n = this.options;
            return this.helper || n.disabled || t(e.target).closest(".ui-resizable-handle").length > 0 ? !1 : (this.handle = this._getHandle(e), this.handle ? (t(n.iframeFix === !0 ? "iframe" : n.iframeFix).each(function() {
                t("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>").css({
                    width: this.offsetWidth + "px",
                    height: this.offsetHeight + "px",
                    position: "absolute",
                    opacity: "0.001",
                    zIndex: 1e3
                }).css(t(this).offset()).appendTo("body")
            }), !0) : !1)
        },
        _mouseStart: function(e) {
            var n = this.options;
            return this.helper = this._createHelper(e), this.helper.addClass("ui-draggable-dragging"), this._cacheHelperProportions(), t.ui.ddmanager && (t.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(), this.offsetParent = this.helper.offsetParent(), this.offsetParentCssPosition = this.offsetParent.css("position"), this.offset = this.positionAbs = this.element.offset(), this.offset = {
                top: this.offset.top - this.margins.top,
                left: this.offset.left - this.margins.left
            }, this.offset.scroll = !1, t.extend(this.offset, {
                click: {
                    left: e.pageX - this.offset.left,
                    top: e.pageY - this.offset.top
                },
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            }), this.originalPosition = this.position = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, n.cursorAt && this._adjustOffsetFromHelper(n.cursorAt), this._setContainment(), this._trigger("start", e) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), t.ui.ddmanager && !n.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this._mouseDrag(e, !0), t.ui.ddmanager && t.ui.ddmanager.dragStart(this, e), !0)
        },
        _mouseDrag: function(e, n) {
            if ("fixed" === this.offsetParentCssPosition && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), !n) {
                var i = this._uiHash();
                if (this._trigger("drag", e, i) === !1) return this._mouseUp({}), !1;
                this.position = i.position
            }
            return this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), !1
        },
        _mouseStop: function(e) {
            var n = this,
                i = !1;
            return t.ui.ddmanager && !this.options.dropBehaviour && (i = t.ui.ddmanager.drop(this, e)), this.dropped && (i = this.dropped, this.dropped = !1), "original" !== this.options.helper || t.contains(this.element[0].ownerDocument, this.element[0]) ? ("invalid" === this.options.revert && !i || "valid" === this.options.revert && i || this.options.revert === !0 || t.isFunction(this.options.revert) && this.options.revert.call(this.element, i) ? t(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
                n._trigger("stop", e) !== !1 && n._clear()
            }) : this._trigger("stop", e) !== !1 && this._clear(), !1) : !1
        },
        _mouseUp: function(e) {
            return t("div.ui-draggable-iframeFix").each(function() {
                this.parentNode.removeChild(this)
            }), t.ui.ddmanager && t.ui.ddmanager.dragStop(this, e), t.ui.mouse.prototype._mouseUp.call(this, e)
        },
        cancel: function() {
            return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), this
        },
        _getHandle: function(e) {
            return this.options.handle ? !!t(e.target).closest(this.element.find(this.options.handle)).length : !0
        },
        _createHelper: function(e) {
            var n = this.options,
                i = t.isFunction(n.helper) ? t(n.helper.apply(this.element[0], [e])) : "clone" === n.helper ? this.element.clone().removeAttr("id") : this.element;
            return i.parents("body").length || i.appendTo("parent" === n.appendTo ? this.element[0].parentNode : n.appendTo), i[0] === this.element[0] || /(fixed|absolute)/.test(i.css("position")) || i.css("position", "absolute"), i
        },
        _adjustOffsetFromHelper: function(e) {
            "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
                left: +e[0],
                top: +e[1] || 0
            }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
        },
        _getParentOffset: function() {
            var e = this.offsetParent.offset();
            return "absolute" === this.cssPosition && this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
                top: 0,
                left: 0
            }), {
                top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function() {
            if ("relative" === this.cssPosition) {
                var t = this.element.position();
                return {
                    top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            }
            return {
                top: 0,
                left: 0
            }
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0
            }
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            }
        },
        _setContainment: function() {
            var e, n, i, r = this.options;
            return r.containment ? "window" === r.containment ? void(this.containment = [t(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, t(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, t(window).scrollLeft() + t(window).width() - this.helperProportions.width - this.margins.left, t(window).scrollTop() + (t(window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === r.containment ? void(this.containment = [0, 0, t(document).width() - this.helperProportions.width - this.margins.left, (t(document).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : r.containment.constructor === Array ? void(this.containment = r.containment) : ("parent" === r.containment && (r.containment = this.helper[0].parentNode), n = t(r.containment), i = n[0], void(i && (e = "hidden" !== n.css("overflow"), this.containment = [(parseInt(n.css("borderLeftWidth"), 10) || 0) + (parseInt(n.css("paddingLeft"), 10) || 0), (parseInt(n.css("borderTopWidth"), 10) || 0) + (parseInt(n.css("paddingTop"), 10) || 0), (e ? Math.max(i.scrollWidth, i.offsetWidth) : i.offsetWidth) - (parseInt(n.css("borderRightWidth"), 10) || 0) - (parseInt(n.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (e ? Math.max(i.scrollHeight, i.offsetHeight) : i.offsetHeight) - (parseInt(n.css("borderBottomWidth"), 10) || 0) - (parseInt(n.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relative_container = n))) : void(this.containment = null)
        },
        _convertPositionTo: function(e, n) {
            n || (n = this.position);
            var i = "absolute" === e ? 1 : -1,
                r = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent;
            return this.offset.scroll || (this.offset.scroll = {
                top: r.scrollTop(),
                left: r.scrollLeft()
            }), {
                top: n.top + this.offset.relative.top * i + this.offset.parent.top * i - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : this.offset.scroll.top) * i,
                left: n.left + this.offset.relative.left * i + this.offset.parent.left * i - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : this.offset.scroll.left) * i
            }
        },
        _generatePosition: function(e) {
            var n, i, r, o, s = this.options,
                a = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                l = e.pageX,
                u = e.pageY;
            return this.offset.scroll || (this.offset.scroll = {
                top: a.scrollTop(),
                left: a.scrollLeft()
            }), this.originalPosition && (this.containment && (this.relative_container ? (i = this.relative_container.offset(), n = [this.containment[0] + i.left, this.containment[1] + i.top, this.containment[2] + i.left, this.containment[3] + i.top]) : n = this.containment, e.pageX - this.offset.click.left < n[0] && (l = n[0] + this.offset.click.left), e.pageY - this.offset.click.top < n[1] && (u = n[1] + this.offset.click.top), e.pageX - this.offset.click.left > n[2] && (l = n[2] + this.offset.click.left), e.pageY - this.offset.click.top > n[3] && (u = n[3] + this.offset.click.top)), s.grid && (r = s.grid[1] ? this.originalPageY + Math.round((u - this.originalPageY) / s.grid[1]) * s.grid[1] : this.originalPageY, u = n ? r - this.offset.click.top >= n[1] || r - this.offset.click.top > n[3] ? r : r - this.offset.click.top >= n[1] ? r - s.grid[1] : r + s.grid[1] : r, o = s.grid[0] ? this.originalPageX + Math.round((l - this.originalPageX) / s.grid[0]) * s.grid[0] : this.originalPageX, l = n ? o - this.offset.click.left >= n[0] || o - this.offset.click.left > n[2] ? o : o - this.offset.click.left >= n[0] ? o - s.grid[0] : o + s.grid[0] : o)), {
                top: u - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : this.offset.scroll.top),
                left: l - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : this.offset.scroll.left)
            }
        },
        _clear: function() {
            this.helper.removeClass("ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1
        },
        _trigger: function(e, n, i) {
            return i = i || this._uiHash(), t.ui.plugin.call(this, e, [n, i]), "drag" === e && (this.positionAbs = this._convertPositionTo("absolute")), t.Widget.prototype._trigger.call(this, e, n, i)
        },
        plugins: {},
        _uiHash: function() {
            return {
                helper: this.helper,
                position: this.position,
                originalPosition: this.originalPosition,
                offset: this.positionAbs
            }
        }
    }), t.ui.plugin.add("draggable", "connectToSortable", {
        start: function(e, n) {
            var i = t(this).data("ui-draggable"),
                r = i.options,
                o = t.extend({}, n, {
                    item: i.element
                });
            i.sortables = [], t(r.connectToSortable).each(function() {
                var n = t.data(this, "ui-sortable");
                n && !n.options.disabled && (i.sortables.push({
                    instance: n,
                    shouldRevert: n.options.revert
                }), n.refreshPositions(), n._trigger("activate", e, o))
            })
        },
        stop: function(e, n) {
            var i = t(this).data("ui-draggable"),
                r = t.extend({}, n, {
                    item: i.element
                });
            t.each(i.sortables, function() {
                this.instance.isOver ? (this.instance.isOver = 0, i.cancelHelperRemoval = !0, this.instance.cancelHelperRemoval = !1, this.shouldRevert && (this.instance.options.revert = this.shouldRevert), this.instance._mouseStop(e), this.instance.options.helper = this.instance.options._helper, "original" === i.options.helper && this.instance.currentItem.css({
                    top: "auto",
                    left: "auto"
                })) : (this.instance.cancelHelperRemoval = !1, this.instance._trigger("deactivate", e, r))
            })
        },
        drag: function(e, n) {
            var i = t(this).data("ui-draggable"),
                r = this;
            t.each(i.sortables, function() {
                var o = !1,
                    s = this;
                this.instance.positionAbs = i.positionAbs, this.instance.helperProportions = i.helperProportions, this.instance.offset.click = i.offset.click, this.instance._intersectsWith(this.instance.containerCache) && (o = !0, t.each(i.sortables, function() {
                    return this.instance.positionAbs = i.positionAbs, this.instance.helperProportions = i.helperProportions, this.instance.offset.click = i.offset.click, this !== s && this.instance._intersectsWith(this.instance.containerCache) && t.contains(s.instance.element[0], this.instance.element[0]) && (o = !1), o
                })), o ? (this.instance.isOver || (this.instance.isOver = 1, this.instance.currentItem = t(r).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item", !0), this.instance.options._helper = this.instance.options.helper, this.instance.options.helper = function() {
                    return n.helper[0]
                }, e.target = this.instance.currentItem[0], this.instance._mouseCapture(e, !0), this.instance._mouseStart(e, !0, !0), this.instance.offset.click.top = i.offset.click.top, this.instance.offset.click.left = i.offset.click.left, this.instance.offset.parent.left -= i.offset.parent.left - this.instance.offset.parent.left, this.instance.offset.parent.top -= i.offset.parent.top - this.instance.offset.parent.top, i._trigger("toSortable", e), i.dropped = this.instance.element, i.currentItem = i.element, this.instance.fromOutside = i), this.instance.currentItem && this.instance._mouseDrag(e)) : this.instance.isOver && (this.instance.isOver = 0, this.instance.cancelHelperRemoval = !0, this.instance.options.revert = !1, this.instance._trigger("out", e, this.instance._uiHash(this.instance)), this.instance._mouseStop(e, !0), this.instance.options.helper = this.instance.options._helper, this.instance.currentItem.remove(), this.instance.placeholder && this.instance.placeholder.remove(), i._trigger("fromSortable", e), i.dropped = !1)
            })
        }
    }), t.ui.plugin.add("draggable", "cursor", {
        start: function() {
            var e = t("body"),
                n = t(this).data("ui-draggable").options;
            e.css("cursor") && (n._cursor = e.css("cursor")), e.css("cursor", n.cursor)
        },
        stop: function() {
            var e = t(this).data("ui-draggable").options;
            e._cursor && t("body").css("cursor", e._cursor)
        }
    }), t.ui.plugin.add("draggable", "opacity", {
        start: function(e, n) {
            var i = t(n.helper),
                r = t(this).data("ui-draggable").options;
            i.css("opacity") && (r._opacity = i.css("opacity")), i.css("opacity", r.opacity)
        },
        stop: function(e, n) {
            var i = t(this).data("ui-draggable").options;
            i._opacity && t(n.helper).css("opacity", i._opacity)
        }
    }), t.ui.plugin.add("draggable", "scroll", {
        start: function() {
            var e = t(this).data("ui-draggable");
            e.scrollParent[0] !== document && "HTML" !== e.scrollParent[0].tagName && (e.overflowOffset = e.scrollParent.offset())
        },
        drag: function(e) {
            var n = t(this).data("ui-draggable"),
                i = n.options,
                r = !1;
            n.scrollParent[0] !== document && "HTML" !== n.scrollParent[0].tagName ? (i.axis && "x" === i.axis || (n.overflowOffset.top + n.scrollParent[0].offsetHeight - e.pageY < i.scrollSensitivity ? n.scrollParent[0].scrollTop = r = n.scrollParent[0].scrollTop + i.scrollSpeed : e.pageY - n.overflowOffset.top < i.scrollSensitivity && (n.scrollParent[0].scrollTop = r = n.scrollParent[0].scrollTop - i.scrollSpeed)), i.axis && "y" === i.axis || (n.overflowOffset.left + n.scrollParent[0].offsetWidth - e.pageX < i.scrollSensitivity ? n.scrollParent[0].scrollLeft = r = n.scrollParent[0].scrollLeft + i.scrollSpeed : e.pageX - n.overflowOffset.left < i.scrollSensitivity && (n.scrollParent[0].scrollLeft = r = n.scrollParent[0].scrollLeft - i.scrollSpeed))) : (i.axis && "x" === i.axis || (e.pageY - t(document).scrollTop() < i.scrollSensitivity ? r = t(document).scrollTop(t(document).scrollTop() - i.scrollSpeed) : t(window).height() - (e.pageY - t(document).scrollTop()) < i.scrollSensitivity && (r = t(document).scrollTop(t(document).scrollTop() + i.scrollSpeed))), i.axis && "y" === i.axis || (e.pageX - t(document).scrollLeft() < i.scrollSensitivity ? r = t(document).scrollLeft(t(document).scrollLeft() - i.scrollSpeed) : t(window).width() - (e.pageX - t(document).scrollLeft()) < i.scrollSensitivity && (r = t(document).scrollLeft(t(document).scrollLeft() + i.scrollSpeed)))), r !== !1 && t.ui.ddmanager && !i.dropBehaviour && t.ui.ddmanager.prepareOffsets(n, e)
        }
    }), t.ui.plugin.add("draggable", "snap", {
        start: function() {
            var e = t(this).data("ui-draggable"),
                n = e.options;
            e.snapElements = [], t(n.snap.constructor !== String ? n.snap.items || ":data(ui-draggable)" : n.snap).each(function() {
                var n = t(this),
                    i = n.offset();
                this !== e.element[0] && e.snapElements.push({
                    item: this,
                    width: n.outerWidth(),
                    height: n.outerHeight(),
                    top: i.top,
                    left: i.left
                })
            })
        },
        drag: function(e, n) {
            var i, r, o, s, a, l, u, c, h, d, p = t(this).data("ui-draggable"),
                f = p.options,
                m = f.snapTolerance,
                g = n.offset.left,
                v = g + p.helperProportions.width,
                y = n.offset.top,
                _ = y + p.helperProportions.height;
            for (h = p.snapElements.length - 1; h >= 0; h--) a = p.snapElements[h].left, l = a + p.snapElements[h].width, u = p.snapElements[h].top, c = u + p.snapElements[h].height, a - m > v || g > l + m || u - m > _ || y > c + m || !t.contains(p.snapElements[h].item.ownerDocument, p.snapElements[h].item) ? (p.snapElements[h].snapping && p.options.snap.release && p.options.snap.release.call(p.element, e, t.extend(p._uiHash(), {
                snapItem: p.snapElements[h].item
            })), p.snapElements[h].snapping = !1) : ("inner" !== f.snapMode && (i = Math.abs(u - _) <= m, r = Math.abs(c - y) <= m, o = Math.abs(a - v) <= m, s = Math.abs(l - g) <= m, i && (n.position.top = p._convertPositionTo("relative", {
                top: u - p.helperProportions.height,
                left: 0
            }).top - p.margins.top), r && (n.position.top = p._convertPositionTo("relative", {
                top: c,
                left: 0
            }).top - p.margins.top), o && (n.position.left = p._convertPositionTo("relative", {
                top: 0,
                left: a - p.helperProportions.width
            }).left - p.margins.left), s && (n.position.left = p._convertPositionTo("relative", {
                top: 0,
                left: l
            }).left - p.margins.left)), d = i || r || o || s, "outer" !== f.snapMode && (i = Math.abs(u - y) <= m, r = Math.abs(c - _) <= m, o = Math.abs(a - g) <= m, s = Math.abs(l - v) <= m, i && (n.position.top = p._convertPositionTo("relative", {
                top: u,
                left: 0
            }).top - p.margins.top), r && (n.position.top = p._convertPositionTo("relative", {
                top: c - p.helperProportions.height,
                left: 0
            }).top - p.margins.top), o && (n.position.left = p._convertPositionTo("relative", {
                top: 0,
                left: a
            }).left - p.margins.left), s && (n.position.left = p._convertPositionTo("relative", {
                top: 0,
                left: l - p.helperProportions.width
            }).left - p.margins.left)), !p.snapElements[h].snapping && (i || r || o || s || d) && p.options.snap.snap && p.options.snap.snap.call(p.element, e, t.extend(p._uiHash(), {
                snapItem: p.snapElements[h].item
            })), p.snapElements[h].snapping = i || r || o || s || d)
        }
    }), t.ui.plugin.add("draggable", "stack", {
        start: function() {
            var e, n = this.data("ui-draggable").options,
                i = t.makeArray(t(n.stack)).sort(function(e, n) {
                    return (parseInt(t(e).css("zIndex"), 10) || 0) - (parseInt(t(n).css("zIndex"), 10) || 0)
                });
            i.length && (e = parseInt(t(i[0]).css("zIndex"), 10) || 0, t(i).each(function(n) {
                t(this).css("zIndex", e + n)
            }), this.css("zIndex", e + i.length))
        }
    }), t.ui.plugin.add("draggable", "zIndex", {
        start: function(e, n) {
            var i = t(n.helper),
                r = t(this).data("ui-draggable").options;
            i.css("zIndex") && (r._zIndex = i.css("zIndex")), i.css("zIndex", r.zIndex)
        },
        stop: function(e, n) {
            var i = t(this).data("ui-draggable").options;
            i._zIndex && t(n.helper).css("zIndex", i._zIndex)
        }
    })
}(jQuery),
function(t) {
    function e(t, e, n) {
        return t > e && e + n > t
    }
    t.widget("ui.droppable", {
        version: "1.10.4",
        widgetEventPrefix: "drop",
        options: {
            accept: "*",
            activeClass: !1,
            addClasses: !0,
            greedy: !1,
            hoverClass: !1,
            scope: "default",
            tolerance: "intersect",
            activate: null,
            deactivate: null,
            drop: null,
            out: null,
            over: null
        },
        _create: function() {
            var e, n = this.options,
                i = n.accept;
            this.isover = !1, this.isout = !0, this.accept = t.isFunction(i) ? i : function(t) {
                return t.is(i)
            }, this.proportions = function() {
                return arguments.length ? void(e = arguments[0]) : e ? e : e = {
                    width: this.element[0].offsetWidth,
                    height: this.element[0].offsetHeight
                }
            }, t.ui.ddmanager.droppables[n.scope] = t.ui.ddmanager.droppables[n.scope] || [], t.ui.ddmanager.droppables[n.scope].push(this), n.addClasses && this.element.addClass("ui-droppable")
        },
        _destroy: function() {
            for (var e = 0, n = t.ui.ddmanager.droppables[this.options.scope]; e < n.length; e++) n[e] === this && n.splice(e, 1);
            this.element.removeClass("ui-droppable ui-droppable-disabled")
        },
        _setOption: function(e, n) {
            "accept" === e && (this.accept = t.isFunction(n) ? n : function(t) {
                return t.is(n)
            }), t.Widget.prototype._setOption.apply(this, arguments)
        },
        _activate: function(e) {
            var n = t.ui.ddmanager.current;
            this.options.activeClass && this.element.addClass(this.options.activeClass), n && this._trigger("activate", e, this.ui(n))
        },
        _deactivate: function(e) {
            var n = t.ui.ddmanager.current;
            this.options.activeClass && this.element.removeClass(this.options.activeClass), n && this._trigger("deactivate", e, this.ui(n))
        },
        _over: function(e) {
            var n = t.ui.ddmanager.current;
            n && (n.currentItem || n.element)[0] !== this.element[0] && this.accept.call(this.element[0], n.currentItem || n.element) && (this.options.hoverClass && this.element.addClass(this.options.hoverClass), this._trigger("over", e, this.ui(n)))
        },
        _out: function(e) {
            var n = t.ui.ddmanager.current;
            n && (n.currentItem || n.element)[0] !== this.element[0] && this.accept.call(this.element[0], n.currentItem || n.element) && (this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("out", e, this.ui(n)))
        },
        _drop: function(e, n) {
            var i = n || t.ui.ddmanager.current,
                r = !1;
            return i && (i.currentItem || i.element)[0] !== this.element[0] ? (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
                var e = t.data(this, "ui-droppable");
                return e.options.greedy && !e.options.disabled && e.options.scope === i.options.scope && e.accept.call(e.element[0], i.currentItem || i.element) && t.ui.intersect(i, t.extend(e, {
                    offset: e.element.offset()
                }), e.options.tolerance) ? (r = !0, !1) : void 0
            }), r ? !1 : this.accept.call(this.element[0], i.currentItem || i.element) ? (this.options.activeClass && this.element.removeClass(this.options.activeClass), this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("drop", e, this.ui(i)), this.element) : !1) : !1
        },
        ui: function(t) {
            return {
                draggable: t.currentItem || t.element,
                helper: t.helper,
                position: t.position,
                offset: t.positionAbs
            }
        }
    }), t.ui.intersect = function(t, n, i) {
        if (!n.offset) return !1;
        var r, o, s = (t.positionAbs || t.position.absolute).left,
            a = (t.positionAbs || t.position.absolute).top,
            l = s + t.helperProportions.width,
            u = a + t.helperProportions.height,
            c = n.offset.left,
            h = n.offset.top,
            d = c + n.proportions().width,
            p = h + n.proportions().height;
        switch (i) {
            case "fit":
                return s >= c && d >= l && a >= h && p >= u;
            case "intersect":
                return c < s + t.helperProportions.width / 2 && l - t.helperProportions.width / 2 < d && h < a + t.helperProportions.height / 2 && u - t.helperProportions.height / 2 < p;
            case "pointer":
                return r = (t.positionAbs || t.position.absolute).left + (t.clickOffset || t.offset.click).left, o = (t.positionAbs || t.position.absolute).top + (t.clickOffset || t.offset.click).top, e(o, h, n.proportions().height) && e(r, c, n.proportions().width);
            case "touch":
                return (a >= h && p >= a || u >= h && p >= u || h > a && u > p) && (s >= c && d >= s || l >= c && d >= l || c > s && l > d);
            default:
                return !1
        }
    }, t.ui.ddmanager = {
        current: null,
        droppables: {
            "default": []
        },
        prepareOffsets: function(e, n) {
            var i, r, o = t.ui.ddmanager.droppables[e.options.scope] || [],
                s = n ? n.type : null,
                a = (e.currentItem || e.element).find(":data(ui-droppable)").addBack();
            t: for (i = 0; i < o.length; i++)
                if (!(o[i].options.disabled || e && !o[i].accept.call(o[i].element[0], e.currentItem || e.element))) {
                    for (r = 0; r < a.length; r++)
                        if (a[r] === o[i].element[0]) {
                            o[i].proportions().height = 0;
                            continue t
                        } o[i].visible = "none" !== o[i].element.css("display"), o[i].visible && ("mousedown" === s && o[i]._activate.call(o[i], n), o[i].offset = o[i].element.offset(), o[i].proportions({
                        width: o[i].element[0].offsetWidth,
                        height: o[i].element[0].offsetHeight
                    }))
                }
        },
        drop: function(e, n) {
            var i = !1;
            return t.each((t.ui.ddmanager.droppables[e.options.scope] || []).slice(), function() {
                this.options && (!this.options.disabled && this.visible && t.ui.intersect(e, this, this.options.tolerance) && (i = this._drop.call(this, n) || i), !this.options.disabled && this.visible && this.accept.call(this.element[0], e.currentItem || e.element) && (this.isout = !0, this.isover = !1, this._deactivate.call(this, n)))
            }), i
        },
        dragStart: function(e, n) {
            e.element.parentsUntil("body").bind("scroll.droppable", function() {
                e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, n)
            })
        },
        drag: function(e, n) {
            e.options.refreshPositions && t.ui.ddmanager.prepareOffsets(e, n), t.each(t.ui.ddmanager.droppables[e.options.scope] || [], function() {
                if (!this.options.disabled && !this.greedyChild && this.visible) {
                    var i, r, o, s = t.ui.intersect(e, this, this.options.tolerance),
                        a = !s && this.isover ? "isout" : s && !this.isover ? "isover" : null;
                    a && (this.options.greedy && (r = this.options.scope, o = this.element.parents(":data(ui-droppable)").filter(function() {
                        return t.data(this, "ui-droppable").options.scope === r
                    }), o.length && (i = t.data(o[0], "ui-droppable"), i.greedyChild = "isover" === a)), i && "isover" === a && (i.isover = !1, i.isout = !0, i._out.call(i, n)), this[a] = !0, this["isout" === a ? "isover" : "isout"] = !1, this["isover" === a ? "_over" : "_out"].call(this, n), i && "isout" === a && (i.isout = !1, i.isover = !0, i._over.call(i, n)))
                }
            })
        },
        dragStop: function(e, n) {
            e.element.parentsUntil("body").unbind("scroll.droppable"), e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, n)
        }
    }
}(jQuery),
function(t) {
    function e(t) {
        return parseInt(t, 10) || 0
    }

    function n(t) {
        return !isNaN(parseInt(t, 10))
    }
    t.widget("ui.resizable", t.ui.mouse, {
        version: "1.10.4",
        widgetEventPrefix: "resize",
        options: {
            alsoResize: !1,
            animate: !1,
            animateDuration: "slow",
            animateEasing: "swing",
            aspectRatio: !1,
            autoHide: !1,
            containment: !1,
            ghost: !1,
            grid: !1,
            handles: "e,s,se",
            helper: !1,
            maxHeight: null,
            maxWidth: null,
            minHeight: 10,
            minWidth: 10,
            zIndex: 90,
            resize: null,
            start: null,
            stop: null
        },
        _create: function() {
            var e, n, i, r, o, s = this,
                a = this.options;
            if (this.element.addClass("ui-resizable"), t.extend(this, {
                    _aspectRatio: !!a.aspectRatio,
                    aspectRatio: a.aspectRatio,
                    originalElement: this.element,
                    _proportionallyResizeElements: [],
                    _helper: a.helper || a.ghost || a.animate ? a.helper || "ui-resizable-helper" : null
                }), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap(t("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
                    position: this.element.css("position"),
                    width: this.element.outerWidth(),
                    height: this.element.outerHeight(),
                    top: this.element.css("top"),
                    left: this.element.css("left")
                })), this.element = this.element.parent().data("ui-resizable", this.element.data("ui-resizable")), this.elementIsWrapper = !0, this.element.css({
                    marginLeft: this.originalElement.css("marginLeft"),
                    marginTop: this.originalElement.css("marginTop"),
                    marginRight: this.originalElement.css("marginRight"),
                    marginBottom: this.originalElement.css("marginBottom")
                }), this.originalElement.css({
                    marginLeft: 0,
                    marginTop: 0,
                    marginRight: 0,
                    marginBottom: 0
                }), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
                    position: "static",
                    zoom: 1,
                    display: "block"
                })), this.originalElement.css({
                    margin: this.originalElement.css("margin")
                }), this._proportionallyResize()), this.handles = a.handles || (t(".ui-resizable-handle", this.element).length ? {
                    n: ".ui-resizable-n",
                    e: ".ui-resizable-e",
                    s: ".ui-resizable-s",
                    w: ".ui-resizable-w",
                    se: ".ui-resizable-se",
                    sw: ".ui-resizable-sw",
                    ne: ".ui-resizable-ne",
                    nw: ".ui-resizable-nw"
                } : "e,s,se"), this.handles.constructor === String)
                for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), e = this.handles.split(","), this.handles = {}, n = 0; n < e.length; n++) i = t.trim(e[n]), o = "ui-resizable-" + i, r = t("<div class='ui-resizable-handle " + o + "'></div>"), r.css({
                    zIndex: a.zIndex
                }), "se" === i && r.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[i] = ".ui-resizable-" + i, this.element.append(r);
            this._renderAxis = function(e) {
                var n, i, r, o;
                e = e || this.element;
                for (n in this.handles) this.handles[n].constructor === String && (this.handles[n] = t(this.handles[n], this.element).show()), this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i) && (i = t(this.handles[n], this.element), o = /sw|ne|nw|se|n|s/.test(n) ? i.outerHeight() : i.outerWidth(), r = ["padding", /ne|nw|n/.test(n) ? "Top" : /se|sw|s/.test(n) ? "Bottom" : /^e$/.test(n) ? "Right" : "Left"].join(""), e.css(r, o), this._proportionallyResize()), t(this.handles[n]).length
            }, this._renderAxis(this.element), this._handles = t(".ui-resizable-handle", this.element).disableSelection(), this._handles.mouseover(function() {
                s.resizing || (this.className && (r = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), s.axis = r && r[1] ? r[1] : "se")
            }), a.autoHide && (this._handles.hide(), t(this.element).addClass("ui-resizable-autohide").mouseenter(function() {
                a.disabled || (t(this).removeClass("ui-resizable-autohide"), s._handles.show())
            }).mouseleave(function() {
                a.disabled || s.resizing || (t(this).addClass("ui-resizable-autohide"), s._handles.hide())
            })), this._mouseInit()
        },
        _destroy: function() {
            this._mouseDestroy();
            var e, n = function(e) {
                t(e).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
            };
            return this.elementIsWrapper && (n(this.element), e = this.element, this.originalElement.css({
                position: e.css("position"),
                width: e.outerWidth(),
                height: e.outerHeight(),
                top: e.css("top"),
                left: e.css("left")
            }).insertAfter(e), e.remove()), this.originalElement.css("resize", this.originalResizeStyle), n(this.originalElement), this
        },
        _mouseCapture: function(e) {
            var n, i, r = !1;
            for (n in this.handles) i = t(this.handles[n])[0], (i === e.target || t.contains(i, e.target)) && (r = !0);
            return !this.options.disabled && r
        },
        _mouseStart: function(n) {
            var i, r, o, s = this.options,
                a = this.element.position(),
                l = this.element;
            return this.resizing = !0, /absolute/.test(l.css("position")) ? l.css({
                position: "absolute",
                top: l.css("top"),
                left: l.css("left")
            }) : l.is(".ui-draggable") && l.css({
                position: "absolute",
                top: a.top,
                left: a.left
            }), this._renderProxy(), i = e(this.helper.css("left")), r = e(this.helper.css("top")), s.containment && (i += t(s.containment).scrollLeft() || 0, r += t(s.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
                left: i,
                top: r
            }, this.size = this._helper ? {
                width: this.helper.width(),
                height: this.helper.height()
            } : {
                width: l.width(),
                height: l.height()
            }, this.originalSize = this._helper ? {
                width: l.outerWidth(),
                height: l.outerHeight()
            } : {
                width: l.width(),
                height: l.height()
            }, this.originalPosition = {
                left: i,
                top: r
            }, this.sizeDiff = {
                width: l.outerWidth() - l.width(),
                height: l.outerHeight() - l.height()
            }, this.originalMousePosition = {
                left: n.pageX,
                top: n.pageY
            }, this.aspectRatio = "number" == typeof s.aspectRatio ? s.aspectRatio : this.originalSize.width / this.originalSize.height || 1, o = t(".ui-resizable-" + this.axis).css("cursor"), t("body").css("cursor", "auto" === o ? this.axis + "-resize" : o), l.addClass("ui-resizable-resizing"), this._propagate("start", n), !0
        },
        _mouseDrag: function(e) {
            var n, i = this.helper,
                r = {},
                o = this.originalMousePosition,
                s = this.axis,
                a = this.position.top,
                l = this.position.left,
                u = this.size.width,
                c = this.size.height,
                h = e.pageX - o.left || 0,
                d = e.pageY - o.top || 0,
                p = this._change[s];
            return p ? (n = p.apply(this, [e, h, d]), this._updateVirtualBoundaries(e.shiftKey), (this._aspectRatio || e.shiftKey) && (n = this._updateRatio(n, e)), n = this._respectSize(n, e), this._updateCache(n), this._propagate("resize", e), this.position.top !== a && (r.top = this.position.top + "px"), this.position.left !== l && (r.left = this.position.left + "px"), this.size.width !== u && (r.width = this.size.width + "px"), this.size.height !== c && (r.height = this.size.height + "px"), i.css(r), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), t.isEmptyObject(r) || this._trigger("resize", e, this.ui()), !1) : !1
        },
        _mouseStop: function(e) {
            this.resizing = !1;
            var n, i, r, o, s, a, l, u = this.options,
                c = this;
            return this._helper && (n = this._proportionallyResizeElements, i = n.length && /textarea/i.test(n[0].nodeName), r = i && t.ui.hasScroll(n[0], "left") ? 0 : c.sizeDiff.height, o = i ? 0 : c.sizeDiff.width, s = {
                width: c.helper.width() - o,
                height: c.helper.height() - r
            }, a = parseInt(c.element.css("left"), 10) + (c.position.left - c.originalPosition.left) || null, l = parseInt(c.element.css("top"), 10) + (c.position.top - c.originalPosition.top) || null, u.animate || this.element.css(t.extend(s, {
                top: l,
                left: a
            })), c.helper.height(c.size.height), c.helper.width(c.size.width), this._helper && !u.animate && this._proportionallyResize()), t("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), this._propagate("stop", e), this._helper && this.helper.remove(), !1
        },
        _updateVirtualBoundaries: function(t) {
            var e, i, r, o, s, a = this.options;
            s = {
                minWidth: n(a.minWidth) ? a.minWidth : 0,
                maxWidth: n(a.maxWidth) ? a.maxWidth : 1 / 0,
                minHeight: n(a.minHeight) ? a.minHeight : 0,
                maxHeight: n(a.maxHeight) ? a.maxHeight : 1 / 0
            }, (this._aspectRatio || t) && (e = s.minHeight * this.aspectRatio, r = s.minWidth / this.aspectRatio, i = s.maxHeight * this.aspectRatio, o = s.maxWidth / this.aspectRatio, e > s.minWidth && (s.minWidth = e), r > s.minHeight && (s.minHeight = r), i < s.maxWidth && (s.maxWidth = i), o < s.maxHeight && (s.maxHeight = o)), this._vBoundaries = s
        },
        _updateCache: function(t) {
            this.offset = this.helper.offset(), n(t.left) && (this.position.left = t.left), n(t.top) && (this.position.top = t.top), n(t.height) && (this.size.height = t.height), n(t.width) && (this.size.width = t.width)
        },
        _updateRatio: function(t) {
            var e = this.position,
                i = this.size,
                r = this.axis;
            return n(t.height) ? t.width = t.height * this.aspectRatio : n(t.width) && (t.height = t.width / this.aspectRatio), "sw" === r && (t.left = e.left + (i.width - t.width), t.top = null), "nw" === r && (t.top = e.top + (i.height - t.height), t.left = e.left + (i.width - t.width)), t
        },
        _respectSize: function(t) {
            var e = this._vBoundaries,
                i = this.axis,
                r = n(t.width) && e.maxWidth && e.maxWidth < t.width,
                o = n(t.height) && e.maxHeight && e.maxHeight < t.height,
                s = n(t.width) && e.minWidth && e.minWidth > t.width,
                a = n(t.height) && e.minHeight && e.minHeight > t.height,
                l = this.originalPosition.left + this.originalSize.width,
                u = this.position.top + this.size.height,
                c = /sw|nw|w/.test(i),
                h = /nw|ne|n/.test(i);
            return s && (t.width = e.minWidth), a && (t.height = e.minHeight), r && (t.width = e.maxWidth), o && (t.height = e.maxHeight), s && c && (t.left = l - e.minWidth), r && c && (t.left = l - e.maxWidth), a && h && (t.top = u - e.minHeight), o && h && (t.top = u - e.maxHeight), t.width || t.height || t.left || !t.top ? t.width || t.height || t.top || !t.left || (t.left = null) : t.top = null, t
        },
        _proportionallyResize: function() {
            if (this._proportionallyResizeElements.length) {
                var t, e, n, i, r, o = this.helper || this.element;
                for (t = 0; t < this._proportionallyResizeElements.length; t++) {
                    if (r = this._proportionallyResizeElements[t], !this.borderDif)
                        for (this.borderDif = [], n = [r.css("borderTopWidth"), r.css("borderRightWidth"), r.css("borderBottomWidth"), r.css("borderLeftWidth")], i = [r.css("paddingTop"), r.css("paddingRight"), r.css("paddingBottom"), r.css("paddingLeft")], e = 0; e < n.length; e++) this.borderDif[e] = (parseInt(n[e], 10) || 0) + (parseInt(i[e], 10) || 0);
                    r.css({
                        height: o.height() - this.borderDif[0] - this.borderDif[2] || 0,
                        width: o.width() - this.borderDif[1] - this.borderDif[3] || 0
                    })
                }
            }
        },
        _renderProxy: function() {
            var e = this.element,
                n = this.options;
            this.elementOffset = e.offset(), this._helper ? (this.helper = this.helper || t("<div style='overflow:hidden;'></div>"), this.helper.addClass(this._helper).css({
                width: this.element.outerWidth() - 1,
                height: this.element.outerHeight() - 1,
                position: "absolute",
                left: this.elementOffset.left + "px",
                top: this.elementOffset.top + "px",
                zIndex: ++n.zIndex
            }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element
        },
        _change: {
            e: function(t, e) {
                return {
                    width: this.originalSize.width + e
                }
            },
            w: function(t, e) {
                var n = this.originalSize,
                    i = this.originalPosition;
                return {
                    left: i.left + e,
                    width: n.width - e
                }
            },
            n: function(t, e, n) {
                var i = this.originalSize,
                    r = this.originalPosition;
                return {
                    top: r.top + n,
                    height: i.height - n
                }
            },
            s: function(t, e, n) {
                return {
                    height: this.originalSize.height + n
                }
            },
            se: function(e, n, i) {
                return t.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [e, n, i]))
            },
            sw: function(e, n, i) {
                return t.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [e, n, i]))
            },
            ne: function(e, n, i) {
                return t.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [e, n, i]))
            },
            nw: function(e, n, i) {
                return t.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [e, n, i]))
            }
        },
        _propagate: function(e, n) {
            t.ui.plugin.call(this, e, [n, this.ui()]), "resize" !== e && this._trigger(e, n, this.ui())
        },
        plugins: {},
        ui: function() {
            return {
                originalElement: this.originalElement,
                element: this.element,
                helper: this.helper,
                position: this.position,
                size: this.size,
                originalSize: this.originalSize,
                originalPosition: this.originalPosition
            }
        }
    }), t.ui.plugin.add("resizable", "animate", {
        stop: function(e) {
            var n = t(this).data("ui-resizable"),
                i = n.options,
                r = n._proportionallyResizeElements,
                o = r.length && /textarea/i.test(r[0].nodeName),
                s = o && t.ui.hasScroll(r[0], "left") ? 0 : n.sizeDiff.height,
                a = o ? 0 : n.sizeDiff.width,
                l = {
                    width: n.size.width - a,
                    height: n.size.height - s
                },
                u = parseInt(n.element.css("left"), 10) + (n.position.left - n.originalPosition.left) || null,
                c = parseInt(n.element.css("top"), 10) + (n.position.top - n.originalPosition.top) || null;
            n.element.animate(t.extend(l, c && u ? {
                top: c,
                left: u
            } : {}), {
                duration: i.animateDuration,
                easing: i.animateEasing,
                step: function() {
                    var i = {
                        width: parseInt(n.element.css("width"), 10),
                        height: parseInt(n.element.css("height"), 10),
                        top: parseInt(n.element.css("top"), 10),
                        left: parseInt(n.element.css("left"), 10)
                    };
                    r && r.length && t(r[0]).css({
                        width: i.width,
                        height: i.height
                    }), n._updateCache(i), n._propagate("resize", e)
                }
            })
        }
    }), t.ui.plugin.add("resizable", "containment", {
        start: function() {
            var n, i, r, o, s, a, l, u = t(this).data("ui-resizable"),
                c = u.options,
                h = u.element,
                d = c.containment,
                p = d instanceof t ? d.get(0) : /parent/.test(d) ? h.parent().get(0) : d;
            p && (u.containerElement = t(p), /document/.test(d) || d === document ? (u.containerOffset = {
                left: 0,
                top: 0
            }, u.containerPosition = {
                left: 0,
                top: 0
            }, u.parentData = {
                element: t(document),
                left: 0,
                top: 0,
                width: t(document).width(),
                height: t(document).height() || document.body.parentNode.scrollHeight
            }) : (n = t(p), i = [], t(["Top", "Right", "Left", "Bottom"]).each(function(t, r) {
                i[t] = e(n.css("padding" + r))
            }), u.containerOffset = n.offset(), u.containerPosition = n.position(), u.containerSize = {
                height: n.innerHeight() - i[3],
                width: n.innerWidth() - i[1]
            }, r = u.containerOffset, o = u.containerSize.height, s = u.containerSize.width, a = t.ui.hasScroll(p, "left") ? p.scrollWidth : s, l = t.ui.hasScroll(p) ? p.scrollHeight : o, u.parentData = {
                element: p,
                left: r.left,
                top: r.top,
                width: a,
                height: l
            }))
        },
        resize: function(e) {
            var n, i, r, o, s = t(this).data("ui-resizable"),
                a = s.options,
                l = s.containerOffset,
                u = s.position,
                c = s._aspectRatio || e.shiftKey,
                h = {
                    top: 0,
                    left: 0
                },
                d = s.containerElement;
            d[0] !== document && /static/.test(d.css("position")) && (h = l), u.left < (s._helper ? l.left : 0) && (s.size.width = s.size.width + (s._helper ? s.position.left - l.left : s.position.left - h.left), c && (s.size.height = s.size.width / s.aspectRatio), s.position.left = a.helper ? l.left : 0), u.top < (s._helper ? l.top : 0) && (s.size.height = s.size.height + (s._helper ? s.position.top - l.top : s.position.top), c && (s.size.width = s.size.height * s.aspectRatio), s.position.top = s._helper ? l.top : 0), s.offset.left = s.parentData.left + s.position.left, s.offset.top = s.parentData.top + s.position.top, n = Math.abs((s._helper ? s.offset.left - h.left : s.offset.left - h.left) + s.sizeDiff.width), i = Math.abs((s._helper ? s.offset.top - h.top : s.offset.top - l.top) + s.sizeDiff.height), r = s.containerElement.get(0) === s.element.parent().get(0), o = /relative|absolute/.test(s.containerElement.css("position")), r && o && (n -= Math.abs(s.parentData.left)), n + s.size.width >= s.parentData.width && (s.size.width = s.parentData.width - n, c && (s.size.height = s.size.width / s.aspectRatio)), i + s.size.height >= s.parentData.height && (s.size.height = s.parentData.height - i, c && (s.size.width = s.size.height * s.aspectRatio))
        },
        stop: function() {
            var e = t(this).data("ui-resizable"),
                n = e.options,
                i = e.containerOffset,
                r = e.containerPosition,
                o = e.containerElement,
                s = t(e.helper),
                a = s.offset(),
                l = s.outerWidth() - e.sizeDiff.width,
                u = s.outerHeight() - e.sizeDiff.height;
            e._helper && !n.animate && /relative/.test(o.css("position")) && t(this).css({
                left: a.left - r.left - i.left,
                width: l,
                height: u
            }), e._helper && !n.animate && /static/.test(o.css("position")) && t(this).css({
                left: a.left - r.left - i.left,
                width: l,
                height: u
            })
        }
    }), t.ui.plugin.add("resizable", "alsoResize", {
        start: function() {
            var e = t(this).data("ui-resizable"),
                n = e.options,
                i = function(e) {
                    t(e).each(function() {
                        var e = t(this);
                        e.data("ui-resizable-alsoresize", {
                            width: parseInt(e.width(), 10),
                            height: parseInt(e.height(), 10),
                            left: parseInt(e.css("left"), 10),
                            top: parseInt(e.css("top"), 10)
                        })
                    })
                };
            "object" != typeof n.alsoResize || n.alsoResize.parentNode ? i(n.alsoResize) : n.alsoResize.length ? (n.alsoResize = n.alsoResize[0], i(n.alsoResize)) : t.each(n.alsoResize, function(t) {
                i(t)
            })
        },
        resize: function(e, n) {
            var i = t(this).data("ui-resizable"),
                r = i.options,
                o = i.originalSize,
                s = i.originalPosition,
                a = {
                    height: i.size.height - o.height || 0,
                    width: i.size.width - o.width || 0,
                    top: i.position.top - s.top || 0,
                    left: i.position.left - s.left || 0
                },
                l = function(e, i) {
                    t(e).each(function() {
                        var e = t(this),
                            r = t(this).data("ui-resizable-alsoresize"),
                            o = {},
                            s = i && i.length ? i : e.parents(n.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
                        t.each(s, function(t, e) {
                            var n = (r[e] || 0) + (a[e] || 0);
                            n && n >= 0 && (o[e] = n || null)
                        }), e.css(o)
                    })
                };
            "object" != typeof r.alsoResize || r.alsoResize.nodeType ? l(r.alsoResize) : t.each(r.alsoResize, function(t, e) {
                l(t, e)
            })
        },
        stop: function() {
            t(this).removeData("resizable-alsoresize")
        }
    }), t.ui.plugin.add("resizable", "ghost", {
        start: function() {
            var e = t(this).data("ui-resizable"),
                n = e.options,
                i = e.size;
            e.ghost = e.originalElement.clone(), e.ghost.css({
                opacity: .25,
                display: "block",
                position: "relative",
                height: i.height,
                width: i.width,
                margin: 0,
                left: 0,
                top: 0
            }).addClass("ui-resizable-ghost").addClass("string" == typeof n.ghost ? n.ghost : ""), e.ghost.appendTo(e.helper)
        },
        resize: function() {
            var e = t(this).data("ui-resizable");
            e.ghost && e.ghost.css({
                position: "relative",
                height: e.size.height,
                width: e.size.width
            })
        },
        stop: function() {
            var e = t(this).data("ui-resizable");
            e.ghost && e.helper && e.helper.get(0).removeChild(e.ghost.get(0))
        }
    }), t.ui.plugin.add("resizable", "grid", {
        resize: function() {
            var e = t(this).data("ui-resizable"),
                n = e.options,
                i = e.size,
                r = e.originalSize,
                o = e.originalPosition,
                s = e.axis,
                a = "number" == typeof n.grid ? [n.grid, n.grid] : n.grid,
                l = a[0] || 1,
                u = a[1] || 1,
                c = Math.round((i.width - r.width) / l) * l,
                h = Math.round((i.height - r.height) / u) * u,
                d = r.width + c,
                p = r.height + h,
                f = n.maxWidth && n.maxWidth < d,
                m = n.maxHeight && n.maxHeight < p,
                g = n.minWidth && n.minWidth > d,
                v = n.minHeight && n.minHeight > p;
            n.grid = a, g && (d += l), v && (p += u), f && (d -= l), m && (p -= u), /^(se|s|e)$/.test(s) ? (e.size.width = d, e.size.height = p) : /^(ne)$/.test(s) ? (e.size.width = d, e.size.height = p, e.position.top = o.top - h) : /^(sw)$/.test(s) ? (e.size.width = d, e.size.height = p, e.position.left = o.left - c) : (p - u > 0 ? (e.size.height = p, e.position.top = o.top - h) : (e.size.height = u, e.position.top = o.top + r.height - u), d - l > 0 ? (e.size.width = d, e.position.left = o.left - c) : (e.size.width = l, e.position.left = o.left + r.width - l))
        }
    })
}(jQuery),
function(t) {
    t.widget("ui.selectable", t.ui.mouse, {
        version: "1.10.4",
        options: {
            appendTo: "body",
            autoRefresh: !0,
            distance: 0,
            filter: "*",
            tolerance: "touch",
            selected: null,
            selecting: null,
            start: null,
            stop: null,
            unselected: null,
            unselecting: null
        },
        _create: function() {
            var e, n = this;
            this.element.addClass("ui-selectable"), this.dragged = !1, this.refresh = function() {
                e = t(n.options.filter, n.element[0]), e.addClass("ui-selectee"), e.each(function() {
                    var e = t(this),
                        n = e.offset();
                    t.data(this, "selectable-item", {
                        element: this,
                        $element: e,
                        left: n.left,
                        top: n.top,
                        right: n.left + e.outerWidth(),
                        bottom: n.top + e.outerHeight(),
                        startselected: !1,
                        selected: e.hasClass("ui-selected"),
                        selecting: e.hasClass("ui-selecting"),
                        unselecting: e.hasClass("ui-unselecting")
                    })
                })
            }, this.refresh(), this.selectees = e.addClass("ui-selectee"), this._mouseInit(), this.helper = t("<div class='ui-selectable-helper'></div>")
        },
        _destroy: function() {
            this.selectees.removeClass("ui-selectee").removeData("selectable-item"), this.element.removeClass("ui-selectable ui-selectable-disabled"), this._mouseDestroy()
        },
        _mouseStart: function(e) {
            var n = this,
                i = this.options;
            this.opos = [e.pageX, e.pageY], this.options.disabled || (this.selectees = t(i.filter, this.element[0]), this._trigger("start", e), t(i.appendTo).append(this.helper), this.helper.css({
                left: e.pageX,
                top: e.pageY,
                width: 0,
                height: 0
            }), i.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function() {
                var i = t.data(this, "selectable-item");
                i.startselected = !0, e.metaKey || e.ctrlKey || (i.$element.removeClass("ui-selected"), i.selected = !1, i.$element.addClass("ui-unselecting"), i.unselecting = !0, n._trigger("unselecting", e, {
                    unselecting: i.element
                }))
            }), t(e.target).parents().addBack().each(function() {
                var i, r = t.data(this, "selectable-item");
                return r ? (i = !e.metaKey && !e.ctrlKey || !r.$element.hasClass("ui-selected"), r.$element.removeClass(i ? "ui-unselecting" : "ui-selected").addClass(i ? "ui-selecting" : "ui-unselecting"), r.unselecting = !i, r.selecting = i, r.selected = i, i ? n._trigger("selecting", e, {
                    selecting: r.element
                }) : n._trigger("unselecting", e, {
                    unselecting: r.element
                }), !1) : void 0
            }))
        },
        _mouseDrag: function(e) {
            if (this.dragged = !0, !this.options.disabled) {
                var n, i = this,
                    r = this.options,
                    o = this.opos[0],
                    s = this.opos[1],
                    a = e.pageX,
                    l = e.pageY;
                return o > a && (n = a, a = o, o = n), s > l && (n = l, l = s, s = n), this.helper.css({
                    left: o,
                    top: s,
                    width: a - o,
                    height: l - s
                }), this.selectees.each(function() {
                    var n = t.data(this, "selectable-item"),
                        u = !1;
                    n && n.element !== i.element[0] && ("touch" === r.tolerance ? u = !(n.left > a || n.right < o || n.top > l || n.bottom < s) : "fit" === r.tolerance && (u = n.left > o && n.right < a && n.top > s && n.bottom < l), u ? (n.selected && (n.$element.removeClass("ui-selected"), n.selected = !1), n.unselecting && (n.$element.removeClass("ui-unselecting"), n.unselecting = !1), n.selecting || (n.$element.addClass("ui-selecting"), n.selecting = !0, i._trigger("selecting", e, {
                        selecting: n.element
                    }))) : (n.selecting && ((e.metaKey || e.ctrlKey) && n.startselected ? (n.$element.removeClass("ui-selecting"), n.selecting = !1, n.$element.addClass("ui-selected"), n.selected = !0) : (n.$element.removeClass("ui-selecting"), n.selecting = !1, n.startselected && (n.$element.addClass("ui-unselecting"), n.unselecting = !0), i._trigger("unselecting", e, {
                        unselecting: n.element
                    }))), n.selected && (e.metaKey || e.ctrlKey || n.startselected || (n.$element.removeClass("ui-selected"), n.selected = !1, n.$element.addClass("ui-unselecting"), n.unselecting = !0, i._trigger("unselecting", e, {
                        unselecting: n.element
                    })))))
                }), !1
            }
        },
        _mouseStop: function(e) {
            var n = this;
            return this.dragged = !1, t(".ui-unselecting", this.element[0]).each(function() {
                var i = t.data(this, "selectable-item");
                i.$element.removeClass("ui-unselecting"), i.unselecting = !1, i.startselected = !1, n._trigger("unselected", e, {
                    unselected: i.element
                })
            }), t(".ui-selecting", this.element[0]).each(function() {
                var i = t.data(this, "selectable-item");
                i.$element.removeClass("ui-selecting").addClass("ui-selected"), i.selecting = !1, i.selected = !0, i.startselected = !0, n._trigger("selected", e, {
                    selected: i.element
                })
            }), this._trigger("stop", e), this.helper.remove(), !1
        }
    })
}(jQuery),
function(t) {
    function e(t, e, n) {
        return t > e && e + n > t
    }

    function n(t) {
        return /left|right/.test(t.css("float")) || /inline|table-cell/.test(t.css("display"))
    }
    t.widget("ui.sortable", t.ui.mouse, {
        version: "1.10.4",
        widgetEventPrefix: "sort",
        ready: !1,
        options: {
            appendTo: "parent",
            axis: !1,
            connectWith: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            dropOnEmpty: !0,
            forcePlaceholderSize: !1,
            forceHelperSize: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            items: "> *",
            opacity: !1,
            placeholder: !1,
            revert: !1,
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            scope: "default",
            tolerance: "intersect",
            zIndex: 1e3,
            activate: null,
            beforeStop: null,
            change: null,
            deactivate: null,
            out: null,
            over: null,
            receive: null,
            remove: null,
            sort: null,
            start: null,
            stop: null,
            update: null
        },
        _create: function() {
            var t = this.options;
            this.containerCache = {}, this.element.addClass("ui-sortable"), this.refresh(), this.floating = this.items.length ? "x" === t.axis || n(this.items[0].item) : !1, this.offset = this.element.offset(), this._mouseInit(), this.ready = !0
        },
        _destroy: function() {
            this.element.removeClass("ui-sortable ui-sortable-disabled"), this._mouseDestroy();
            for (var t = this.items.length - 1; t >= 0; t--) this.items[t].item.removeData(this.widgetName + "-item");
            return this
        },
        _setOption: function(e, n) {
            "disabled" === e ? (this.options[e] = n, this.widget().toggleClass("ui-sortable-disabled", !!n)) : t.Widget.prototype._setOption.apply(this, arguments)
        },
        _mouseCapture: function(e, n) {
            var i = null,
                r = !1,
                o = this;
            return this.reverting ? !1 : this.options.disabled || "static" === this.options.type ? !1 : (this._refreshItems(e), t(e.target).parents().each(function() {
                return t.data(this, o.widgetName + "-item") === o ? (i = t(this), !1) : void 0
            }), t.data(e.target, o.widgetName + "-item") === o && (i = t(e.target)), i && (!this.options.handle || n || (t(this.options.handle, i).find("*").addBack().each(function() {
                this === e.target && (r = !0)
            }), r)) ? (this.currentItem = i, this._removeCurrentsFromItems(), !0) : !1)
        },
        _mouseStart: function(e, n, i) {
            var r, o, s = this.options;
            if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(e), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {
                    top: this.offset.top - this.margins.top,
                    left: this.offset.left - this.margins.left
                }, t.extend(this.offset, {
                    click: {
                        left: e.pageX - this.offset.left,
                        top: e.pageY - this.offset.top
                    },
                    parent: this._getParentOffset(),
                    relative: this._getRelativeOffset()
                }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, s.cursorAt && this._adjustOffsetFromHelper(s.cursorAt), this.domPosition = {
                    prev: this.currentItem.prev()[0],
                    parent: this.currentItem.parent()[0]
                }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), s.containment && this._setContainment(), s.cursor && "auto" !== s.cursor && (o = this.document.find("body"), this.storedCursor = o.css("cursor"), o.css("cursor", s.cursor), this.storedStylesheet = t("<style>*{ cursor: " + s.cursor + " !important; }</style>").appendTo(o)), s.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", s.opacity)), s.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", s.zIndex)), this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", e, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !i)
                for (r = this.containers.length - 1; r >= 0; r--) this.containers[r]._trigger("activate", e, this._uiHash(this));
            return t.ui.ddmanager && (t.ui.ddmanager.current = this), t.ui.ddmanager && !s.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(e), !0
        },
        _mouseDrag: function(e) {
            var n, i, r, o, s = this.options,
                a = !1;
            for (this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - e.pageY < s.scrollSensitivity ? this.scrollParent[0].scrollTop = a = this.scrollParent[0].scrollTop + s.scrollSpeed : e.pageY - this.overflowOffset.top < s.scrollSensitivity && (this.scrollParent[0].scrollTop = a = this.scrollParent[0].scrollTop - s.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - e.pageX < s.scrollSensitivity ? this.scrollParent[0].scrollLeft = a = this.scrollParent[0].scrollLeft + s.scrollSpeed : e.pageX - this.overflowOffset.left < s.scrollSensitivity && (this.scrollParent[0].scrollLeft = a = this.scrollParent[0].scrollLeft - s.scrollSpeed)) : (e.pageY - t(document).scrollTop() < s.scrollSensitivity ? a = t(document).scrollTop(t(document).scrollTop() - s.scrollSpeed) : t(window).height() - (e.pageY - t(document).scrollTop()) < s.scrollSensitivity && (a = t(document).scrollTop(t(document).scrollTop() + s.scrollSpeed)), e.pageX - t(document).scrollLeft() < s.scrollSensitivity ? a = t(document).scrollLeft(t(document).scrollLeft() - s.scrollSpeed) : t(window).width() - (e.pageX - t(document).scrollLeft()) < s.scrollSensitivity && (a = t(document).scrollLeft(t(document).scrollLeft() + s.scrollSpeed))), a !== !1 && t.ui.ddmanager && !s.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e)), this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), n = this.items.length - 1; n >= 0; n--)
                if (i = this.items[n], r = i.item[0], o = this._intersectsWithPointer(i), o && i.instance === this.currentContainer && r !== this.currentItem[0] && this.placeholder[1 === o ? "next" : "prev"]()[0] !== r && !t.contains(this.placeholder[0], r) && ("semi-dynamic" === this.options.type ? !t.contains(this.element[0], r) : !0)) {
                    if (this.direction = 1 === o ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(i)) break;
                    this._rearrange(e, i), this._trigger("change", e, this._uiHash());
                    break
                } return this._contactContainers(e), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), this._trigger("sort", e, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
        },
        _mouseStop: function(e, n) {
            if (e) {
                if (t.ui.ddmanager && !this.options.dropBehaviour && t.ui.ddmanager.drop(this, e), this.options.revert) {
                    var i = this,
                        r = this.placeholder.offset(),
                        o = this.options.axis,
                        s = {};
                    o && "x" !== o || (s.left = r.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft)), o && "y" !== o || (s.top = r.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop)), this.reverting = !0, t(this.helper).animate(s, parseInt(this.options.revert, 10) || 500, function() {
                        i._clear(e)
                    })
                } else this._clear(e, n);
                return !1
            }
        },
        cancel: function() {
            if (this.dragging) {
                this._mouseUp({
                    target: null
                }), "original" === this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
                for (var e = this.containers.length - 1; e >= 0; e--) this.containers[e]._trigger("deactivate", null, this._uiHash(this)), this.containers[e].containerCache.over && (this.containers[e]._trigger("out", null, this._uiHash(this)), this.containers[e].containerCache.over = 0)
            }
            return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), t.extend(this, {
                helper: null,
                dragging: !1,
                reverting: !1,
                _noFinalSort: null
            }), this.domPosition.prev ? t(this.domPosition.prev).after(this.currentItem) : t(this.domPosition.parent).prepend(this.currentItem)), this
        },
        serialize: function(e) {
            var n = this._getItemsAsjQuery(e && e.connected),
                i = [];
            return e = e || {}, t(n).each(function() {
                var n = (t(e.item || this).attr(e.attribute || "id") || "").match(e.expression || /(.+)[\-=_](.+)/);
                n && i.push((e.key || n[1] + "[]") + "=" + (e.key && e.expression ? n[1] : n[2]))
            }), !i.length && e.key && i.push(e.key + "="), i.join("&")
        },
        toArray: function(e) {
            var n = this._getItemsAsjQuery(e && e.connected),
                i = [];
            return e = e || {}, n.each(function() {
                i.push(t(e.item || this).attr(e.attribute || "id") || "")
            }), i
        },
        _intersectsWith: function(t) {
            var e = this.positionAbs.left,
                n = e + this.helperProportions.width,
                i = this.positionAbs.top,
                r = i + this.helperProportions.height,
                o = t.left,
                s = o + t.width,
                a = t.top,
                l = a + t.height,
                u = this.offset.click.top,
                c = this.offset.click.left,
                h = "x" === this.options.axis || i + u > a && l > i + u,
                d = "y" === this.options.axis || e + c > o && s > e + c,
                p = h && d;
            return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > t[this.floating ? "width" : "height"] ? p : o < e + this.helperProportions.width / 2 && n - this.helperProportions.width / 2 < s && a < i + this.helperProportions.height / 2 && r - this.helperProportions.height / 2 < l
        },
        _intersectsWithPointer: function(t) {
            var n = "x" === this.options.axis || e(this.positionAbs.top + this.offset.click.top, t.top, t.height),
                i = "y" === this.options.axis || e(this.positionAbs.left + this.offset.click.left, t.left, t.width),
                r = n && i,
                o = this._getDragVerticalDirection(),
                s = this._getDragHorizontalDirection();
            return r ? this.floating ? s && "right" === s || "down" === o ? 2 : 1 : o && ("down" === o ? 2 : 1) : !1
        },
        _intersectsWithSides: function(t) {
            var n = e(this.positionAbs.top + this.offset.click.top, t.top + t.height / 2, t.height),
                i = e(this.positionAbs.left + this.offset.click.left, t.left + t.width / 2, t.width),
                r = this._getDragVerticalDirection(),
                o = this._getDragHorizontalDirection();
            return this.floating && o ? "right" === o && i || "left" === o && !i : r && ("down" === r && n || "up" === r && !n)
        },
        _getDragVerticalDirection: function() {
            var t = this.positionAbs.top - this.lastPositionAbs.top;
            return 0 !== t && (t > 0 ? "down" : "up")
        },
        _getDragHorizontalDirection: function() {
            var t = this.positionAbs.left - this.lastPositionAbs.left;
            return 0 !== t && (t > 0 ? "right" : "left")
        },
        refresh: function(t) {
            return this._refreshItems(t), this.refreshPositions(), this
        },
        _connectWith: function() {
            var t = this.options;
            return t.connectWith.constructor === String ? [t.connectWith] : t.connectWith
        },
        _getItemsAsjQuery: function(e) {
            function n() {
                a.push(this)
            }
            var i, r, o, s, a = [],
                l = [],
                u = this._connectWith();
            if (u && e)
                for (i = u.length - 1; i >= 0; i--)
                    for (o = t(u[i]), r = o.length - 1; r >= 0; r--) s = t.data(o[r], this.widgetFullName), s && s !== this && !s.options.disabled && l.push([t.isFunction(s.options.items) ? s.options.items.call(s.element) : t(s.options.items, s.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), s]);
            for (l.push([t.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
                    options: this.options,
                    item: this.currentItem
                }) : t(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]), i = l.length - 1; i >= 0; i--) l[i][0].each(n);
            return t(a)
        },
        _removeCurrentsFromItems: function() {
            var e = this.currentItem.find(":data(" + this.widgetName + "-item)");
            this.items = t.grep(this.items, function(t) {
                for (var n = 0; n < e.length; n++)
                    if (e[n] === t.item[0]) return !1;
                return !0
            })
        },
        _refreshItems: function(e) {
            this.items = [], this.containers = [this];
            var n, i, r, o, s, a, l, u, c = this.items,
                h = [
                    [t.isFunction(this.options.items) ? this.options.items.call(this.element[0], e, {
                        item: this.currentItem
                    }) : t(this.options.items, this.element), this]
                ],
                d = this._connectWith();
            if (d && this.ready)
                for (n = d.length - 1; n >= 0; n--)
                    for (r = t(d[n]), i = r.length - 1; i >= 0; i--) o = t.data(r[i], this.widgetFullName), o && o !== this && !o.options.disabled && (h.push([t.isFunction(o.options.items) ? o.options.items.call(o.element[0], e, {
                        item: this.currentItem
                    }) : t(o.options.items, o.element), o]), this.containers.push(o));
            for (n = h.length - 1; n >= 0; n--)
                for (s = h[n][1], a = h[n][0], i = 0, u = a.length; u > i; i++) l = t(a[i]), l.data(this.widgetName + "-item", s), c.push({
                    item: l,
                    instance: s,
                    width: 0,
                    height: 0,
                    left: 0,
                    top: 0
                })
        },
        refreshPositions: function(e) {
            this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
            var n, i, r, o;
            for (n = this.items.length - 1; n >= 0; n--) i = this.items[n], i.instance !== this.currentContainer && this.currentContainer && i.item[0] !== this.currentItem[0] || (r = this.options.toleranceElement ? t(this.options.toleranceElement, i.item) : i.item, e || (i.width = r.outerWidth(), i.height = r.outerHeight()), o = r.offset(), i.left = o.left, i.top = o.top);
            if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this);
            else
                for (n = this.containers.length - 1; n >= 0; n--) o = this.containers[n].element.offset(), this.containers[n].containerCache.left = o.left, this.containers[n].containerCache.top = o.top, this.containers[n].containerCache.width = this.containers[n].element.outerWidth(), this.containers[n].containerCache.height = this.containers[n].element.outerHeight();
            return this
        },
        _createPlaceholder: function(e) {
            e = e || this;
            var n, i = e.options;
            i.placeholder && i.placeholder.constructor !== String || (n = i.placeholder, i.placeholder = {
                element: function() {
                    var i = e.currentItem[0].nodeName.toLowerCase(),
                        r = t("<" + i + ">", e.document[0]).addClass(n || e.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
                    return "tr" === i ? e.currentItem.children().each(function() {
                        t("<td>&#160;</td>", e.document[0]).attr("colspan", t(this).attr("colspan") || 1).appendTo(r)
                    }) : "img" === i && r.attr("src", e.currentItem.attr("src")), n || r.css("visibility", "hidden"), r
                },
                update: function(t, r) {
                    (!n || i.forcePlaceholderSize) && (r.height() || r.height(e.currentItem.innerHeight() - parseInt(e.currentItem.css("paddingTop") || 0, 10) - parseInt(e.currentItem.css("paddingBottom") || 0, 10)), r.width() || r.width(e.currentItem.innerWidth() - parseInt(e.currentItem.css("paddingLeft") || 0, 10) - parseInt(e.currentItem.css("paddingRight") || 0, 10)))
                }
            }), e.placeholder = t(i.placeholder.element.call(e.element, e.currentItem)), e.currentItem.after(e.placeholder), i.placeholder.update(e, e.placeholder)
        },
        _contactContainers: function(i) {
            var r, o, s, a, l, u, c, h, d, p, f = null,
                m = null;
            for (r = this.containers.length - 1; r >= 0; r--)
                if (!t.contains(this.currentItem[0], this.containers[r].element[0]))
                    if (this._intersectsWith(this.containers[r].containerCache)) {
                        if (f && t.contains(this.containers[r].element[0], f.element[0])) continue;
                        f = this.containers[r], m = r
                    } else this.containers[r].containerCache.over && (this.containers[r]._trigger("out", i, this._uiHash(this)), this.containers[r].containerCache.over = 0);
            if (f)
                if (1 === this.containers.length) this.containers[m].containerCache.over || (this.containers[m]._trigger("over", i, this._uiHash(this)), this.containers[m].containerCache.over = 1);
                else {
                    for (s = 1e4, a = null, p = f.floating || n(this.currentItem), l = p ? "left" : "top", u = p ? "width" : "height", c = this.positionAbs[l] + this.offset.click[l], o = this.items.length - 1; o >= 0; o--) t.contains(this.containers[m].element[0], this.items[o].item[0]) && this.items[o].item[0] !== this.currentItem[0] && (!p || e(this.positionAbs.top + this.offset.click.top, this.items[o].top, this.items[o].height)) && (h = this.items[o].item.offset()[l], d = !1, Math.abs(h - c) > Math.abs(h + this.items[o][u] - c) && (d = !0, h += this.items[o][u]), Math.abs(h - c) < s && (s = Math.abs(h - c), a = this.items[o], this.direction = d ? "up" : "down"));
                    if (!a && !this.options.dropOnEmpty) return;
                    if (this.currentContainer === this.containers[m]) return;
                    a ? this._rearrange(i, a, null, !0) : this._rearrange(i, null, this.containers[m].element, !0), this._trigger("change", i, this._uiHash()), this.containers[m]._trigger("change", i, this._uiHash(this)), this.currentContainer = this.containers[m], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[m]._trigger("over", i, this._uiHash(this)), this.containers[m].containerCache.over = 1
                }
        },
        _createHelper: function(e) {
            var n = this.options,
                i = t.isFunction(n.helper) ? t(n.helper.apply(this.element[0], [e, this.currentItem])) : "clone" === n.helper ? this.currentItem.clone() : this.currentItem;
            return i.parents("body").length || t("parent" !== n.appendTo ? n.appendTo : this.currentItem[0].parentNode)[0].appendChild(i[0]), i[0] === this.currentItem[0] && (this._storedCSS = {
                width: this.currentItem[0].style.width,
                height: this.currentItem[0].style.height,
                position: this.currentItem.css("position"),
                top: this.currentItem.css("top"),
                left: this.currentItem.css("left")
            }), (!i[0].style.width || n.forceHelperSize) && i.width(this.currentItem.width()), (!i[0].style.height || n.forceHelperSize) && i.height(this.currentItem.height()), i
        },
        _adjustOffsetFromHelper: function(e) {
            "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
                left: +e[0],
                top: +e[1] || 0
            }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
        },
        _getParentOffset: function() {
            this.offsetParent = this.helper.offsetParent();
            var e = this.offsetParent.offset();
            return "absolute" === this.cssPosition && this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
                top: 0,
                left: 0
            }), {
                top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function() {
            if ("relative" === this.cssPosition) {
                var t = this.currentItem.position();
                return {
                    top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            }
            return {
                top: 0,
                left: 0
            }
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
                top: parseInt(this.currentItem.css("marginTop"), 10) || 0
            }
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            }
        },
        _setContainment: function() {
            var e, n, i, r = this.options;
            "parent" === r.containment && (r.containment = this.helper[0].parentNode), ("document" === r.containment || "window" === r.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, t("document" === r.containment ? document : window).width() - this.helperProportions.width - this.margins.left, (t("document" === r.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(r.containment) || (e = t(r.containment)[0], n = t(r.containment).offset(), i = "hidden" !== t(e).css("overflow"), this.containment = [n.left + (parseInt(t(e).css("borderLeftWidth"), 10) || 0) + (parseInt(t(e).css("paddingLeft"), 10) || 0) - this.margins.left, n.top + (parseInt(t(e).css("borderTopWidth"), 10) || 0) + (parseInt(t(e).css("paddingTop"), 10) || 0) - this.margins.top, n.left + (i ? Math.max(e.scrollWidth, e.offsetWidth) : e.offsetWidth) - (parseInt(t(e).css("borderLeftWidth"), 10) || 0) - (parseInt(t(e).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, n.top + (i ? Math.max(e.scrollHeight, e.offsetHeight) : e.offsetHeight) - (parseInt(t(e).css("borderTopWidth"), 10) || 0) - (parseInt(t(e).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top])
        },
        _convertPositionTo: function(e, n) {
            n || (n = this.position);
            var i = "absolute" === e ? 1 : -1,
                r = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                o = /(html|body)/i.test(r[0].tagName);
            return {
                top: n.top + this.offset.relative.top * i + this.offset.parent.top * i - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : o ? 0 : r.scrollTop()) * i,
                left: n.left + this.offset.relative.left * i + this.offset.parent.left * i - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : o ? 0 : r.scrollLeft()) * i
            }
        },
        _generatePosition: function(e) {
            var n, i, r = this.options,
                o = e.pageX,
                s = e.pageY,
                a = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                l = /(html|body)/i.test(a[0].tagName);
            return "relative" !== this.cssPosition || this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (e.pageX - this.offset.click.left < this.containment[0] && (o = this.containment[0] + this.offset.click.left), e.pageY - this.offset.click.top < this.containment[1] && (s = this.containment[1] + this.offset.click.top), e.pageX - this.offset.click.left > this.containment[2] && (o = this.containment[2] + this.offset.click.left), e.pageY - this.offset.click.top > this.containment[3] && (s = this.containment[3] + this.offset.click.top)), r.grid && (n = this.originalPageY + Math.round((s - this.originalPageY) / r.grid[1]) * r.grid[1], s = this.containment ? n - this.offset.click.top >= this.containment[1] && n - this.offset.click.top <= this.containment[3] ? n : n - this.offset.click.top >= this.containment[1] ? n - r.grid[1] : n + r.grid[1] : n, i = this.originalPageX + Math.round((o - this.originalPageX) / r.grid[0]) * r.grid[0], o = this.containment ? i - this.offset.click.left >= this.containment[0] && i - this.offset.click.left <= this.containment[2] ? i : i - this.offset.click.left >= this.containment[0] ? i - r.grid[0] : i + r.grid[0] : i)), {
                top: s - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : l ? 0 : a.scrollTop()),
                left: o - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : l ? 0 : a.scrollLeft())
            }
        },
        _rearrange: function(t, e, n, i) {
            n ? n[0].appendChild(this.placeholder[0]) : e.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? e.item[0] : e.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
            var r = this.counter;
            this._delay(function() {
                r === this.counter && this.refreshPositions(!i)
            })
        },
        _clear: function(t, e) {
            function n(t, e, n) {
                return function(i) {
                    n._trigger(t, i, e._uiHash(e))
                }
            }
            this.reverting = !1;
            var i, r = [];
            if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
                for (i in this._storedCSS)("auto" === this._storedCSS[i] || "static" === this._storedCSS[i]) && (this._storedCSS[i] = "");
                this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
            } else this.currentItem.show();
            for (this.fromOutside && !e && r.push(function(t) {
                    this._trigger("receive", t, this._uiHash(this.fromOutside))
                }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || e || r.push(function(t) {
                    this._trigger("update", t, this._uiHash())
                }), this !== this.currentContainer && (e || (r.push(function(t) {
                    this._trigger("remove", t, this._uiHash())
                }), r.push(function(t) {
                    return function(e) {
                        t._trigger("receive", e, this._uiHash(this))
                    }
                }.call(this, this.currentContainer)), r.push(function(t) {
                    return function(e) {
                        t._trigger("update", e, this._uiHash(this))
                    }
                }.call(this, this.currentContainer)))), i = this.containers.length - 1; i >= 0; i--) e || r.push(n("deactivate", this, this.containers[i])), this.containers[i].containerCache.over && (r.push(n("out", this, this.containers[i])), this.containers[i].containerCache.over = 0);
            if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, this.cancelHelperRemoval) {
                if (!e) {
                    for (this._trigger("beforeStop", t, this._uiHash()), i = 0; i < r.length; i++) r[i].call(this, t);
                    this._trigger("stop", t, this._uiHash())
                }
                return this.fromOutside = !1, !1
            }
            if (e || this._trigger("beforeStop", t, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null, !e) {
                for (i = 0; i < r.length; i++) r[i].call(this, t);
                this._trigger("stop", t, this._uiHash())
            }
            return this.fromOutside = !1, !0
        },
        _trigger: function() {
            t.Widget.prototype._trigger.apply(this, arguments) === !1 && this.cancel()
        },
        _uiHash: function(e) {
            var n = e || this;
            return {
                helper: n.helper,
                placeholder: n.placeholder || t([]),
                position: n.position,
                originalPosition: n.originalPosition,
                offset: n.positionAbs,
                item: n.currentItem,
                sender: e ? e.element : null
            }
        }
    })
}(jQuery),
function(t, e) {
    var n = "ui-effects-";
    t.effects = {
            effect: {}
        },
        function(t, e) {
            function n(t, e, n) {
                var i = h[e.type] || {};
                return null == t ? n || !e.def ? null : e.def : (t = i.floor ? ~~t : parseFloat(t), isNaN(t) ? e.def : i.mod ? (t + i.mod) % i.mod : 0 > t ? 0 : i.max < t ? i.max : t)
            }

            function i(e) {
                var n = u(),
                    i = n._rgba = [];
                return e = e.toLowerCase(), f(l, function(t, r) {
                    var o, s = r.re.exec(e),
                        a = s && r.parse(s),
                        l = r.space || "rgba";
                    return a ? (o = n[l](a), n[c[l].cache] = o[c[l].cache], i = n._rgba = o._rgba, !1) : void 0
                }), i.length ? ("0,0,0,0" === i.join() && t.extend(i, o.transparent), n) : o[e]
            }

            function r(t, e, n) {
                return n = (n + 1) % 1, 1 > 6 * n ? t + (e - t) * n * 6 : 1 > 2 * n ? e : 2 > 3 * n ? t + (e - t) * (2 / 3 - n) * 6 : t
            }
            var o, s = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",
                a = /^([\-+])=\s*(\d+\.?\d*)/,
                l = [{
                    re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                    parse: function(t) {
                        return [t[1], t[2], t[3], t[4]]
                    }
                }, {
                    re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                    parse: function(t) {
                        return [2.55 * t[1], 2.55 * t[2], 2.55 * t[3], t[4]]
                    }
                }, {
                    re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
                    parse: function(t) {
                        return [parseInt(t[1], 16), parseInt(t[2], 16), parseInt(t[3], 16)]
                    }
                }, {
                    re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
                    parse: function(t) {
                        return [parseInt(t[1] + t[1], 16), parseInt(t[2] + t[2], 16), parseInt(t[3] + t[3], 16)]
                    }
                }, {
                    re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                    space: "hsla",
                    parse: function(t) {
                        return [t[1], t[2] / 100, t[3] / 100, t[4]]
                    }
                }],
                u = t.Color = function(e, n, i, r) {
                    return new t.Color.fn.parse(e, n, i, r)
                },
                c = {
                    rgba: {
                        props: {
                            red: {
                                idx: 0,
                                type: "byte"
                            },
                            green: {
                                idx: 1,
                                type: "byte"
                            },
                            blue: {
                                idx: 2,
                                type: "byte"
                            }
                        }
                    },
                    hsla: {
                        props: {
                            hue: {
                                idx: 0,
                                type: "degrees"
                            },
                            saturation: {
                                idx: 1,
                                type: "percent"
                            },
                            lightness: {
                                idx: 2,
                                type: "percent"
                            }
                        }
                    }
                },
                h = {
                    "byte": {
                        floor: !0,
                        max: 255
                    },
                    percent: {
                        max: 1
                    },
                    degrees: {
                        mod: 360,
                        floor: !0
                    }
                },
                d = u.support = {},
                p = t("<p>")[0],
                f = t.each;
            p.style.cssText = "background-color:rgba(1,1,1,.5)", d.rgba = p.style.backgroundColor.indexOf("rgba") > -1, f(c, function(t, e) {
                e.cache = "_" + t, e.props.alpha = {
                    idx: 3,
                    type: "percent",
                    def: 1
                }
            }), u.fn = t.extend(u.prototype, {
                parse: function(r, s, a, l) {
                    if (r === e) return this._rgba = [null, null, null, null], this;
                    (r.jquery || r.nodeType) && (r = t(r).css(s), s = e);
                    var h = this,
                        d = t.type(r),
                        p = this._rgba = [];
                    return s !== e && (r = [r, s, a, l], d = "array"), "string" === d ? this.parse(i(r) || o._default) : "array" === d ? (f(c.rgba.props, function(t, e) {
                        p[e.idx] = n(r[e.idx], e)
                    }), this) : "object" === d ? (r instanceof u ? f(c, function(t, e) {
                        r[e.cache] && (h[e.cache] = r[e.cache].slice())
                    }) : f(c, function(e, i) {
                        var o = i.cache;
                        f(i.props, function(t, e) {
                            if (!h[o] && i.to) {
                                if ("alpha" === t || null == r[t]) return;
                                h[o] = i.to(h._rgba)
                            }
                            h[o][e.idx] = n(r[t], e, !0)
                        }), h[o] && t.inArray(null, h[o].slice(0, 3)) < 0 && (h[o][3] = 1, i.from && (h._rgba = i.from(h[o])))
                    }), this) : void 0
                },
                is: function(t) {
                    var e = u(t),
                        n = !0,
                        i = this;
                    return f(c, function(t, r) {
                        var o, s = e[r.cache];
                        return s && (o = i[r.cache] || r.to && r.to(i._rgba) || [], f(r.props, function(t, e) {
                            return null != s[e.idx] ? n = s[e.idx] === o[e.idx] : void 0
                        })), n
                    }), n
                },
                _space: function() {
                    var t = [],
                        e = this;
                    return f(c, function(n, i) {
                        e[i.cache] && t.push(n)
                    }), t.pop()
                },
                transition: function(t, e) {
                    var i = u(t),
                        r = i._space(),
                        o = c[r],
                        s = 0 === this.alpha() ? u("transparent") : this,
                        a = s[o.cache] || o.to(s._rgba),
                        l = a.slice();
                    return i = i[o.cache], f(o.props, function(t, r) {
                        var o = r.idx,
                            s = a[o],
                            u = i[o],
                            c = h[r.type] || {};
                        null !== u && (null === s ? l[o] = u : (c.mod && (u - s > c.mod / 2 ? s += c.mod : s - u > c.mod / 2 && (s -= c.mod)), l[o] = n((u - s) * e + s, r)))
                    }), this[r](l)
                },
                blend: function(e) {
                    if (1 === this._rgba[3]) return this;
                    var n = this._rgba.slice(),
                        i = n.pop(),
                        r = u(e)._rgba;
                    return u(t.map(n, function(t, e) {
                        return (1 - i) * r[e] + i * t
                    }))
                },
                toRgbaString: function() {
                    var e = "rgba(",
                        n = t.map(this._rgba, function(t, e) {
                            return null == t ? e > 2 ? 1 : 0 : t
                        });
                    return 1 === n[3] && (n.pop(), e = "rgb("), e + n.join() + ")"
                },
                toHslaString: function() {
                    var e = "hsla(",
                        n = t.map(this.hsla(), function(t, e) {
                            return null == t && (t = e > 2 ? 1 : 0), e && 3 > e && (t = Math.round(100 * t) + "%"), t
                        });
                    return 1 === n[3] && (n.pop(), e = "hsl("), e + n.join() + ")"
                },
                toHexString: function(e) {
                    var n = this._rgba.slice(),
                        i = n.pop();
                    return e && n.push(~~(255 * i)), "#" + t.map(n, function(t) {
                        return t = (t || 0).toString(16), 1 === t.length ? "0" + t : t
                    }).join("")
                },
                toString: function() {
                    return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
                }
            }), u.fn.parse.prototype = u.fn, c.hsla.to = function(t) {
                if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
                var e, n, i = t[0] / 255,
                    r = t[1] / 255,
                    o = t[2] / 255,
                    s = t[3],
                    a = Math.max(i, r, o),
                    l = Math.min(i, r, o),
                    u = a - l,
                    c = a + l,
                    h = .5 * c;
                return e = l === a ? 0 : i === a ? 60 * (r - o) / u + 360 : r === a ? 60 * (o - i) / u + 120 : 60 * (i - r) / u + 240, n = 0 === u ? 0 : .5 >= h ? u / c : u / (2 - c), [Math.round(e) % 360, n, h, null == s ? 1 : s]
            }, c.hsla.from = function(t) {
                if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
                var e = t[0] / 360,
                    n = t[1],
                    i = t[2],
                    o = t[3],
                    s = .5 >= i ? i * (1 + n) : i + n - i * n,
                    a = 2 * i - s;
                return [Math.round(255 * r(a, s, e + 1 / 3)), Math.round(255 * r(a, s, e)), Math.round(255 * r(a, s, e - 1 / 3)), o]
            }, f(c, function(i, r) {
                var o = r.props,
                    s = r.cache,
                    l = r.to,
                    c = r.from;
                u.fn[i] = function(i) {
                    if (l && !this[s] && (this[s] = l(this._rgba)), i === e) return this[s].slice();
                    var r, a = t.type(i),
                        h = "array" === a || "object" === a ? i : arguments,
                        d = this[s].slice();
                    return f(o, function(t, e) {
                        var i = h["object" === a ? t : e.idx];
                        null == i && (i = d[e.idx]), d[e.idx] = n(i, e)
                    }), c ? (r = u(c(d)), r[s] = d, r) : u(d)
                }, f(o, function(e, n) {
                    u.fn[e] || (u.fn[e] = function(r) {
                        var o, s = t.type(r),
                            l = "alpha" === e ? this._hsla ? "hsla" : "rgba" : i,
                            u = this[l](),
                            c = u[n.idx];
                        return "undefined" === s ? c : ("function" === s && (r = r.call(this, c), s = t.type(r)), null == r && n.empty ? this : ("string" === s && (o = a.exec(r), o && (r = c + parseFloat(o[2]) * ("+" === o[1] ? 1 : -1))), u[n.idx] = r, this[l](u)))
                    })
                })
            }), u.hook = function(e) {
                var n = e.split(" ");
                f(n, function(e, n) {
                    t.cssHooks[n] = {
                        set: function(e, r) {
                            var o, s, a = "";
                            if ("transparent" !== r && ("string" !== t.type(r) || (o = i(r)))) {
                                if (r = u(o || r), !d.rgba && 1 !== r._rgba[3]) {
                                    for (s = "backgroundColor" === n ? e.parentNode : e;
                                        ("" === a || "transparent" === a) && s && s.style;) try {
                                        a = t.css(s, "backgroundColor"), s = s.parentNode
                                    } catch (l) {}
                                    r = r.blend(a && "transparent" !== a ? a : "_default")
                                }
                                r = r.toRgbaString()
                            }
                            try {
                                e.style[n] = r
                            } catch (l) {}
                        }
                    }, t.fx.step[n] = function(e) {
                        e.colorInit || (e.start = u(e.elem, n), e.end = u(e.end), e.colorInit = !0), t.cssHooks[n].set(e.elem, e.start.transition(e.end, e.pos))
                    }
                })
            }, u.hook(s), t.cssHooks.borderColor = {
                expand: function(t) {
                    var e = {};
                    return f(["Top", "Right", "Bottom", "Left"], function(n, i) {
                        e["border" + i + "Color"] = t
                    }), e
                }
            }, o = t.Color.names = {
                aqua: "#00ffff",
                black: "#000000",
                blue: "#0000ff",
                fuchsia: "#ff00ff",
                gray: "#808080",
                green: "#008000",
                lime: "#00ff00",
                maroon: "#800000",
                navy: "#000080",
                olive: "#808000",
                purple: "#800080",
                red: "#ff0000",
                silver: "#c0c0c0",
                teal: "#008080",
                white: "#ffffff",
                yellow: "#ffff00",
                transparent: [null, null, null, 0],
                _default: "#ffffff"
            }
        }(jQuery),
        function() {
            function n(e) {
                var n, i, r = e.ownerDocument.defaultView ? e.ownerDocument.defaultView.getComputedStyle(e, null) : e.currentStyle,
                    o = {};
                if (r && r.length && r[0] && r[r[0]])
                    for (i = r.length; i--;) n = r[i], "string" == typeof r[n] && (o[t.camelCase(n)] = r[n]);
                else
                    for (n in r) "string" == typeof r[n] && (o[n] = r[n]);
                return o
            }

            function i(e, n) {
                var i, r, s = {};
                for (i in n) r = n[i], e[i] !== r && (o[i] || (t.fx.step[i] || !isNaN(parseFloat(r))) && (s[i] = r));
                return s
            }
            var r = ["add", "remove", "toggle"],
                o = {
                    border: 1,
                    borderBottom: 1,
                    borderColor: 1,
                    borderLeft: 1,
                    borderRight: 1,
                    borderTop: 1,
                    borderWidth: 1,
                    margin: 1,
                    padding: 1
                };
            t.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(e, n) {
                t.fx.step[n] = function(t) {
                    ("none" !== t.end && !t.setAttr || 1 === t.pos && !t.setAttr) && (jQuery.style(t.elem, n, t.end), t.setAttr = !0)
                }
            }), t.fn.addBack || (t.fn.addBack = function(t) {
                return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
            }), t.effects.animateClass = function(e, o, s, a) {
                var l = t.speed(o, s, a);
                return this.queue(function() {
                    var o, s = t(this),
                        a = s.attr("class") || "",
                        u = l.children ? s.find("*").addBack() : s;
                    u = u.map(function() {
                        var e = t(this);
                        return {
                            el: e,
                            start: n(this)
                        }
                    }), o = function() {
                        t.each(r, function(t, n) {
                            e[n] && s[n + "Class"](e[n])
                        })
                    }, o(), u = u.map(function() {
                        return this.end = n(this.el[0]), this.diff = i(this.start, this.end), this
                    }), s.attr("class", a), u = u.map(function() {
                        var e = this,
                            n = t.Deferred(),
                            i = t.extend({}, l, {
                                queue: !1,
                                complete: function() {
                                    n.resolve(e)
                                }
                            });
                        return this.el.animate(this.diff, i), n.promise()
                    }), t.when.apply(t, u.get()).done(function() {
                        o(), t.each(arguments, function() {
                            var e = this.el;
                            t.each(this.diff, function(t) {
                                e.css(t, "")
                            })
                        }), l.complete.call(s[0])
                    })
                })
            }, t.fn.extend({
                addClass: function(e) {
                    return function(n, i, r, o) {
                        return i ? t.effects.animateClass.call(this, {
                            add: n
                        }, i, r, o) : e.apply(this, arguments)
                    }
                }(t.fn.addClass),
                removeClass: function(e) {
                    return function(n, i, r, o) {
                        return arguments.length > 1 ? t.effects.animateClass.call(this, {
                            remove: n
                        }, i, r, o) : e.apply(this, arguments)
                    }
                }(t.fn.removeClass),
                toggleClass: function(n) {
                    return function(i, r, o, s, a) {
                        return "boolean" == typeof r || r === e ? o ? t.effects.animateClass.call(this, r ? {
                            add: i
                        } : {
                            remove: i
                        }, o, s, a) : n.apply(this, arguments) : t.effects.animateClass.call(this, {
                            toggle: i
                        }, r, o, s)
                    }
                }(t.fn.toggleClass),
                switchClass: function(e, n, i, r, o) {
                    return t.effects.animateClass.call(this, {
                        add: n,
                        remove: e
                    }, i, r, o)
                }
            })
        }(),
        function() {
            function i(e, n, i, r) {
                return t.isPlainObject(e) && (n = e, e = e.effect), e = {
                    effect: e
                }, null == n && (n = {}), t.isFunction(n) && (r = n, i = null, n = {}), ("number" == typeof n || t.fx.speeds[n]) && (r = i, i = n, n = {}), t.isFunction(i) && (r = i, i = null), n && t.extend(e, n), i = i || n.duration, e.duration = t.fx.off ? 0 : "number" == typeof i ? i : i in t.fx.speeds ? t.fx.speeds[i] : t.fx.speeds._default, e.complete = r || n.complete, e
            }

            function r(e) {
                return !e || "number" == typeof e || t.fx.speeds[e] ? !0 : "string" != typeof e || t.effects.effect[e] ? t.isFunction(e) ? !0 : "object" != typeof e || e.effect ? !1 : !0 : !0
            }
            t.extend(t.effects, {
                version: "1.10.4",
                save: function(t, e) {
                    for (var i = 0; i < e.length; i++) null !== e[i] && t.data(n + e[i], t[0].style[e[i]])
                },
                restore: function(t, i) {
                    var r, o;
                    for (o = 0; o < i.length; o++) null !== i[o] && (r = t.data(n + i[o]), r === e && (r = ""), t.css(i[o], r))
                },
                setMode: function(t, e) {
                    return "toggle" === e && (e = t.is(":hidden") ? "show" : "hide"), e
                },
                getBaseline: function(t, e) {
                    var n, i;
                    switch (t[0]) {
                        case "top":
                            n = 0;
                            break;
                        case "middle":
                            n = .5;
                            break;
                        case "bottom":
                            n = 1;
                            break;
                        default:
                            n = t[0] / e.height
                    }
                    switch (t[1]) {
                        case "left":
                            i = 0;
                            break;
                        case "center":
                            i = .5;
                            break;
                        case "right":
                            i = 1;
                            break;
                        default:
                            i = t[1] / e.width
                    }
                    return {
                        x: i,
                        y: n
                    }
                },
                createWrapper: function(e) {
                    if (e.parent().is(".ui-effects-wrapper")) return e.parent();
                    var n = {
                            width: e.outerWidth(!0),
                            height: e.outerHeight(!0),
                            "float": e.css("float")
                        },
                        i = t("<div></div>").addClass("ui-effects-wrapper").css({
                            fontSize: "100%",
                            background: "transparent",
                            border: "none",
                            margin: 0,
                            padding: 0
                        }),
                        r = {
                            width: e.width(),
                            height: e.height()
                        },
                        o = document.activeElement;
                    try {
                        o.id
                    } catch (s) {
                        o = document.body
                    }
                    return e.wrap(i), (e[0] === o || t.contains(e[0], o)) && t(o).focus(), i = e.parent(), "static" === e.css("position") ? (i.css({
                        position: "relative"
                    }), e.css({
                        position: "relative"
                    })) : (t.extend(n, {
                        position: e.css("position"),
                        zIndex: e.css("z-index")
                    }), t.each(["top", "left", "bottom", "right"], function(t, i) {
                        n[i] = e.css(i), isNaN(parseInt(n[i], 10)) && (n[i] = "auto")
                    }), e.css({
                        position: "relative",
                        top: 0,
                        left: 0,
                        right: "auto",
                        bottom: "auto"
                    })), e.css(r), i.css(n).show()
                },
                removeWrapper: function(e) {
                    var n = document.activeElement;
                    return e.parent().is(".ui-effects-wrapper") && (e.parent().replaceWith(e), (e[0] === n || t.contains(e[0], n)) && t(n).focus()), e
                },
                setTransition: function(e, n, i, r) {
                    return r = r || {}, t.each(n, function(t, n) {
                        var o = e.cssUnit(n);
                        o[0] > 0 && (r[n] = o[0] * i + o[1])
                    }), r
                }
            }), t.fn.extend({
                effect: function() {
                    function e(e) {
                        function i() {
                            t.isFunction(o) && o.call(r[0]), t.isFunction(e) && e()
                        }
                        var r = t(this),
                            o = n.complete,
                            a = n.mode;
                        (r.is(":hidden") ? "hide" === a : "show" === a) ? (r[a](), i()) : s.call(r[0], n, i)
                    }
                    var n = i.apply(this, arguments),
                        r = n.mode,
                        o = n.queue,
                        s = t.effects.effect[n.effect];
                    return t.fx.off || !s ? r ? this[r](n.duration, n.complete) : this.each(function() {
                        n.complete && n.complete.call(this)
                    }) : o === !1 ? this.each(e) : this.queue(o || "fx", e)
                },
                show: function(t) {
                    return function(e) {
                        if (r(e)) return t.apply(this, arguments);
                        var n = i.apply(this, arguments);
                        return n.mode = "show", this.effect.call(this, n)
                    }
                }(t.fn.show),
                hide: function(t) {
                    return function(e) {
                        if (r(e)) return t.apply(this, arguments);
                        var n = i.apply(this, arguments);
                        return n.mode = "hide", this.effect.call(this, n)
                    }
                }(t.fn.hide),
                toggle: function(t) {
                    return function(e) {
                        if (r(e) || "boolean" == typeof e) return t.apply(this, arguments);
                        var n = i.apply(this, arguments);
                        return n.mode = "toggle", this.effect.call(this, n)
                    }
                }(t.fn.toggle),
                cssUnit: function(e) {
                    var n = this.css(e),
                        i = [];
                    return t.each(["em", "px", "%", "pt"], function(t, e) {
                        n.indexOf(e) > 0 && (i = [parseFloat(n), e])
                    }), i
                }
            })
        }(),
        function() {
            var e = {};
            t.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(t, n) {
                e[n] = function(e) {
                    return Math.pow(e, t + 2)
                }
            }), t.extend(e, {
                Sine: function(t) {
                    return 1 - Math.cos(t * Math.PI / 2)
                },
                Circ: function(t) {
                    return 1 - Math.sqrt(1 - t * t)
                },
                Elastic: function(t) {
                    return 0 === t || 1 === t ? t : -Math.pow(2, 8 * (t - 1)) * Math.sin((80 * (t - 1) - 7.5) * Math.PI / 15)
                },
                Back: function(t) {
                    return t * t * (3 * t - 2)
                },
                Bounce: function(t) {
                    for (var e, n = 4; t < ((e = Math.pow(2, --n)) - 1) / 11;);
                    return 1 / Math.pow(4, 3 - n) - 7.5625 * Math.pow((3 * e - 2) / 22 - t, 2)
                }
            }), t.each(e, function(e, n) {
                t.easing["easeIn" + e] = n, t.easing["easeOut" + e] = function(t) {
                    return 1 - n(1 - t)
                }, t.easing["easeInOut" + e] = function(t) {
                    return .5 > t ? n(2 * t) / 2 : 1 - n(-2 * t + 2) / 2
                }
            })
        }()
}(jQuery),
function(t) {
    var e = 0,
        n = {},
        i = {};
    n.height = n.paddingTop = n.paddingBottom = n.borderTopWidth = n.borderBottomWidth = "hide", i.height = i.paddingTop = i.paddingBottom = i.borderTopWidth = i.borderBottomWidth = "show", t.widget("ui.accordion", {
        version: "1.10.4",
        options: {
            active: 0,
            animate: {},
            collapsible: !1,
            event: "click",
            header: "> li > :first-child,> :not(li):even",
            heightStyle: "auto",
            icons: {
                activeHeader: "ui-icon-triangle-1-s",
                header: "ui-icon-triangle-1-e"
            },
            activate: null,
            beforeActivate: null
        },
        _create: function() {
            var e = this.options;
            this.prevShow = this.prevHide = t(), this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role", "tablist"), e.collapsible || e.active !== !1 && null != e.active || (e.active = 0), this._processPanels(), e.active < 0 && (e.active += this.headers.length), this._refresh()
        },
        _getCreateEventData: function() {
            return {
                header: this.active,
                panel: this.active.length ? this.active.next() : t(),
                content: this.active.length ? this.active.next() : t()
            }
        },
        _createIcons: function() {
            var e = this.options.icons;
            e && (t("<span>").addClass("ui-accordion-header-icon ui-icon " + e.header).prependTo(this.headers), this.active.children(".ui-accordion-header-icon").removeClass(e.header).addClass(e.activeHeader), this.headers.addClass("ui-accordion-icons"))
        },
        _destroyIcons: function() {
            this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()
        },
        _destroy: function() {
            var t;
            this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"), this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function() {
                /^ui-accordion/.test(this.id) && this.removeAttribute("id")
            }), this._destroyIcons(), t = this.headers.next().css("display", "").removeAttr("role").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function() {
                /^ui-accordion/.test(this.id) && this.removeAttribute("id")
            }), "content" !== this.options.heightStyle && t.css("height", "")
        },
        _setOption: function(t, e) {
            return "active" === t ? void this._activate(e) : ("event" === t && (this.options.event && this._off(this.headers, this.options.event), this._setupEvents(e)), this._super(t, e), "collapsible" !== t || e || this.options.active !== !1 || this._activate(0), "icons" === t && (this._destroyIcons(), e && this._createIcons()), void("disabled" === t && this.headers.add(this.headers.next()).toggleClass("ui-state-disabled", !!e)))
        },
        _keydown: function(e) {
            if (!e.altKey && !e.ctrlKey) {
                var n = t.ui.keyCode,
                    i = this.headers.length,
                    r = this.headers.index(e.target),
                    o = !1;
                switch (e.keyCode) {
                    case n.RIGHT:
                    case n.DOWN:
                        o = this.headers[(r + 1) % i];
                        break;
                    case n.LEFT:
                    case n.UP:
                        o = this.headers[(r - 1 + i) % i];
                        break;
                    case n.SPACE:
                    case n.ENTER:
                        this._eventHandler(e);
                        break;
                    case n.HOME:
                        o = this.headers[0];
                        break;
                    case n.END:
                        o = this.headers[i - 1]
                }
                o && (t(e.target).attr("tabIndex", -1), t(o).attr("tabIndex", 0), o.focus(), e.preventDefault())
            }
        },
        _panelKeyDown: function(e) {
            e.keyCode === t.ui.keyCode.UP && e.ctrlKey && t(e.currentTarget).prev().focus()
        },
        refresh: function() {
            var e = this.options;
            this._processPanels(), e.active === !1 && e.collapsible === !0 || !this.headers.length ? (e.active = !1, this.active = t()) : e.active === !1 ? this._activate(0) : this.active.length && !t.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (e.active = !1, this.active = t()) : this._activate(Math.max(0, e.active - 1)) : e.active = this.headers.index(this.active), this._destroyIcons(), this._refresh()
        },
        _processPanels: function() {
            this.headers = this.element.find(this.options.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"), this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide()
        },
        _refresh: function() {
            var n, i = this.options,
                r = i.heightStyle,
                o = this.element.parent(),
                s = this.accordionId = "ui-accordion-" + (this.element.attr("id") || ++e);
            this.active = this._findActive(i.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all"), this.active.next().addClass("ui-accordion-content-active").show(), this.headers.attr("role", "tab").each(function(e) {
                var n = t(this),
                    i = n.attr("id"),
                    r = n.next(),
                    o = r.attr("id");
                i || (i = s + "-header-" + e, n.attr("id", i)), o || (o = s + "-panel-" + e, r.attr("id", o)), n.attr("aria-controls", o), r.attr("aria-labelledby", i)
            }).next().attr("role", "tabpanel"), this.headers.not(this.active).attr({
                "aria-selected": "false",
                "aria-expanded": "false",
                tabIndex: -1
            }).next().attr({
                "aria-hidden": "true"
            }).hide(), this.active.length ? this.active.attr({
                "aria-selected": "true",
                "aria-expanded": "true",
                tabIndex: 0
            }).next().attr({
                "aria-hidden": "false"
            }) : this.headers.eq(0).attr("tabIndex", 0), this._createIcons(), this._setupEvents(i.event), "fill" === r ? (n = o.height(), this.element.siblings(":visible").each(function() {
                var e = t(this),
                    i = e.css("position");
                "absolute" !== i && "fixed" !== i && (n -= e.outerHeight(!0))
            }), this.headers.each(function() {
                n -= t(this).outerHeight(!0)
            }), this.headers.next().each(function() {
                t(this).height(Math.max(0, n - t(this).innerHeight() + t(this).height()))
            }).css("overflow", "auto")) : "auto" === r && (n = 0, this.headers.next().each(function() {
                n = Math.max(n, t(this).css("height", "").height())
            }).height(n))
        },
        _activate: function(e) {
            var n = this._findActive(e)[0];
            n !== this.active[0] && (n = n || this.active[0], this._eventHandler({
                target: n,
                currentTarget: n,
                preventDefault: t.noop
            }))
        },
        _findActive: function(e) {
            return "number" == typeof e ? this.headers.eq(e) : t()
        },
        _setupEvents: function(e) {
            var n = {
                keydown: "_keydown"
            };
            e && t.each(e.split(" "), function(t, e) {
                n[e] = "_eventHandler"
            }), this._off(this.headers.add(this.headers.next())), this._on(this.headers, n), this._on(this.headers.next(), {
                keydown: "_panelKeyDown"
            }), this._hoverable(this.headers), this._focusable(this.headers)
        },
        _eventHandler: function(e) {
            var n = this.options,
                i = this.active,
                r = t(e.currentTarget),
                o = r[0] === i[0],
                s = o && n.collapsible,
                a = s ? t() : r.next(),
                l = i.next(),
                u = {
                    oldHeader: i,
                    oldPanel: l,
                    newHeader: s ? t() : r,
                    newPanel: a
                };
            e.preventDefault(), o && !n.collapsible || this._trigger("beforeActivate", e, u) === !1 || (n.active = s ? !1 : this.headers.index(r), this.active = o ? t() : r, this._toggle(u), i.removeClass("ui-accordion-header-active ui-state-active"), n.icons && i.children(".ui-accordion-header-icon").removeClass(n.icons.activeHeader).addClass(n.icons.header), o || (r.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"), n.icons && r.children(".ui-accordion-header-icon").removeClass(n.icons.header).addClass(n.icons.activeHeader), r.next().addClass("ui-accordion-content-active")))
        },
        _toggle: function(e) {
            var n = e.newPanel,
                i = this.prevShow.length ? this.prevShow : e.oldPanel;
            this.prevShow.add(this.prevHide).stop(!0, !0), this.prevShow = n, this.prevHide = i, this.options.animate ? this._animate(n, i, e) : (i.hide(), n.show(), this._toggleComplete(e)), i.attr({
                "aria-hidden": "true"
            }), i.prev().attr("aria-selected", "false"), n.length && i.length ? i.prev().attr({
                tabIndex: -1,
                "aria-expanded": "false"
            }) : n.length && this.headers.filter(function() {
                return 0 === t(this).attr("tabIndex")
            }).attr("tabIndex", -1), n.attr("aria-hidden", "false").prev().attr({
                "aria-selected": "true",
                tabIndex: 0,
                "aria-expanded": "true"
            })
        },
        _animate: function(t, e, r) {
            var o, s, a, l = this,
                u = 0,
                c = t.length && (!e.length || t.index() < e.index()),
                h = this.options.animate || {},
                d = c && h.down || h,
                p = function() {
                    l._toggleComplete(r)
                };
            return "number" == typeof d && (a = d), "string" == typeof d && (s = d), s = s || d.easing || h.easing, a = a || d.duration || h.duration, e.length ? t.length ? (o = t.show().outerHeight(), e.animate(n, {
                duration: a,
                easing: s,
                step: function(t, e) {
                    e.now = Math.round(t)
                }
            }), void t.hide().animate(i, {
                duration: a,
                easing: s,
                complete: p,
                step: function(t, n) {
                    n.now = Math.round(t), "height" !== n.prop ? u += n.now : "content" !== l.options.heightStyle && (n.now = Math.round(o - e.outerHeight() - u), u = 0)
                }
            })) : e.animate(n, a, s, p) : t.animate(i, a, s, p)
        },
        _toggleComplete: function(t) {
            var e = t.oldPanel;
            e.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"), e.length && (e.parent()[0].className = e.parent()[0].className), this._trigger("activate", null, t)
        }
    })
}(jQuery),
function(t) {
    t.widget("ui.autocomplete", {
        version: "1.10.4",
        defaultElement: "<input>",
        options: {
            appendTo: null,
            autoFocus: !1,
            delay: 300,
            minLength: 1,
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            source: null,
            change: null,
            close: null,
            focus: null,
            open: null,
            response: null,
            search: null,
            select: null
        },
        requestIndex: 0,
        pending: 0,
        _create: function() {
            var e, n, i, r = this.element[0].nodeName.toLowerCase(),
                o = "textarea" === r,
                s = "input" === r;
            this.isMultiLine = o ? !0 : s ? !1 : this.element.prop("isContentEditable"), this.valueMethod = this.element[o || s ? "val" : "text"], this.isNewMenu = !0, this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off"), this._on(this.element, {
                keydown: function(r) {
                    if (this.element.prop("readOnly")) return e = !0, i = !0, void(n = !0);
                    e = !1, i = !1, n = !1;
                    var o = t.ui.keyCode;
                    switch (r.keyCode) {
                        case o.PAGE_UP:
                            e = !0, this._move("previousPage", r);
                            break;
                        case o.PAGE_DOWN:
                            e = !0, this._move("nextPage", r);
                            break;
                        case o.UP:
                            e = !0, this._keyEvent("previous", r);
                            break;
                        case o.DOWN:
                            e = !0, this._keyEvent("next", r);
                            break;
                        case o.ENTER:
                        case o.NUMPAD_ENTER:
                            this.menu.active && (e = !0, r.preventDefault(), this.menu.select(r));
                            break;
                        case o.TAB:
                            this.menu.active && this.menu.select(r);
                            break;
                        case o.ESCAPE:
                            this.menu.element.is(":visible") && (this._value(this.term), this.close(r), r.preventDefault());
                            break;
                        default:
                            n = !0, this._searchTimeout(r)
                    }
                },
                keypress: function(i) {
                    if (e) return e = !1, void((!this.isMultiLine || this.menu.element.is(":visible")) && i.preventDefault());
                    if (!n) {
                        var r = t.ui.keyCode;
                        switch (i.keyCode) {
                            case r.PAGE_UP:
                                this._move("previousPage", i);
                                break;
                            case r.PAGE_DOWN:
                                this._move("nextPage", i);
                                break;
                            case r.UP:
                                this._keyEvent("previous", i);
                                break;
                            case r.DOWN:
                                this._keyEvent("next", i)
                        }
                    }
                },
                input: function(t) {
                    return i ? (i = !1, void t.preventDefault()) : void this._searchTimeout(t)
                },
                focus: function() {
                    this.selectedItem = null, this.previous = this._value()
                },
                blur: function(t) {
                    return this.cancelBlur ? void delete this.cancelBlur : (clearTimeout(this.searching), this.close(t), void this._change(t))
                }
            }), this._initSource(), this.menu = t("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({
                role: null
            }).hide().data("ui-menu"), this._on(this.menu.element, {
                mousedown: function(e) {
                    e.preventDefault(), this.cancelBlur = !0, this._delay(function() {
                        delete this.cancelBlur
                    });
                    var n = this.menu.element[0];
                    t(e.target).closest(".ui-menu-item").length || this._delay(function() {
                        var e = this;
                        this.document.one("mousedown", function(i) {
                            i.target === e.element[0] || i.target === n || t.contains(n, i.target) || e.close()
                        })
                    })
                },
                menufocus: function(e, n) {
                    if (this.isNewMenu && (this.isNewMenu = !1, e.originalEvent && /^mouse/.test(e.originalEvent.type))) return this.menu.blur(), void this.document.one("mousemove", function() {
                        t(e.target).trigger(e.originalEvent)
                    });
                    var i = n.item.data("ui-autocomplete-item");
                    !1 !== this._trigger("focus", e, {
                        item: i
                    }) ? e.originalEvent && /^key/.test(e.originalEvent.type) && this._value(i.value) : this.liveRegion.text(i.value)
                },
                menuselect: function(t, e) {
                    var n = e.item.data("ui-autocomplete-item"),
                        i = this.previous;
                    this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = i, this._delay(function() {
                        this.previous = i, this.selectedItem = n
                    })), !1 !== this._trigger("select", t, {
                        item: n
                    }) && this._value(n.value), this.term = this._value(), this.close(t), this.selectedItem = n
                }
            }), this.liveRegion = t("<span>", {
                role: "status",
                "aria-live": "polite"
            }).addClass("ui-helper-hidden-accessible").insertBefore(this.element), this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete")
                }
            })
        },
        _destroy: function() {
            clearTimeout(this.searching), this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"), this.menu.element.remove(), this.liveRegion.remove()
        },
        _setOption: function(t, e) {
            this._super(t, e), "source" === t && this._initSource(), "appendTo" === t && this.menu.element.appendTo(this._appendTo()), "disabled" === t && e && this.xhr && this.xhr.abort()
        },
        _appendTo: function() {
            var e = this.options.appendTo;
            return e && (e = e.jquery || e.nodeType ? t(e) : this.document.find(e).eq(0)), e || (e = this.element.closest(".ui-front")), e.length || (e = this.document[0].body), e
        },
        _initSource: function() {
            var e, n, i = this;
            t.isArray(this.options.source) ? (e = this.options.source, this.source = function(n, i) {
                i(t.ui.autocomplete.filter(e, n.term))
            }) : "string" == typeof this.options.source ? (n = this.options.source, this.source = function(e, r) {
                i.xhr && i.xhr.abort(), i.xhr = t.ajax({
                    url: n,
                    data: e,
                    dataType: "json",
                    success: function(t) {
                        r(t)
                    },
                    error: function() {
                        r([])
                    }
                })
            }) : this.source = this.options.source
        },
        _searchTimeout: function(t) {
            clearTimeout(this.searching), this.searching = this._delay(function() {
                this.term !== this._value() && (this.selectedItem = null, this.search(null, t))
            }, this.options.delay)
        },
        search: function(t, e) {
            return t = null != t ? t : this._value(), this.term = this._value(), t.length < this.options.minLength ? this.close(e) : this._trigger("search", e) !== !1 ? this._search(t) : void 0
        },
        _search: function(t) {
            this.pending++, this.element.addClass("ui-autocomplete-loading"), this.cancelSearch = !1, this.source({
                term: t
            }, this._response())
        },
        _response: function() {
            var e = ++this.requestIndex;
            return t.proxy(function(t) {
                e === this.requestIndex && this.__response(t), this.pending--, this.pending || this.element.removeClass("ui-autocomplete-loading")
            }, this)
        },
        __response: function(t) {
            t && (t = this._normalize(t)), this._trigger("response", null, {
                content: t
            }), !this.options.disabled && t && t.length && !this.cancelSearch ? (this._suggest(t), this._trigger("open")) : this._close()
        },
        close: function(t) {
            this.cancelSearch = !0, this._close(t)
        },
        _close: function(t) {
            this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", t))
        },
        _change: function(t) {
            this.previous !== this._value() && this._trigger("change", t, {
                item: this.selectedItem
            })
        },
        _normalize: function(e) {
            return e.length && e[0].label && e[0].value ? e : t.map(e, function(e) {
                return "string" == typeof e ? {
                    label: e,
                    value: e
                } : t.extend({
                    label: e.label || e.value,
                    value: e.value || e.label
                }, e)
            })
        },
        _suggest: function(e) {
            var n = this.menu.element.empty();
            this._renderMenu(n, e), this.isNewMenu = !0, this.menu.refresh(), n.show(), this._resizeMenu(), n.position(t.extend({
                of: this.element
            }, this.options.position)), this.options.autoFocus && this.menu.next()
        },
        _resizeMenu: function() {
            var t = this.menu.element;
            t.outerWidth(Math.max(t.width("").outerWidth() + 1, this.element.outerWidth()))
        },
        _renderMenu: function(e, n) {
            var i = this;
            t.each(n, function(t, n) {
                i._renderItemData(e, n)
            })
        },
        _renderItemData: function(t, e) {
            return this._renderItem(t, e).data("ui-autocomplete-item", e)
        },
        _renderItem: function(e, n) {
            return t("<li>").append(t("<a>").text(n.label)).appendTo(e)
        },
        _move: function(t, e) {
            return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(t) || this.menu.isLastItem() && /^next/.test(t) ? (this._value(this.term), void this.menu.blur()) : void this.menu[t](e) : void this.search(null, e)
        },
        widget: function() {
            return this.menu.element
        },
        _value: function() {
            return this.valueMethod.apply(this.element, arguments)
        },
        _keyEvent: function(t, e) {
            (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(t, e), e.preventDefault())
        }
    }), t.extend(t.ui.autocomplete, {
        escapeRegex: function(t) {
            return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
        },
        filter: function(e, n) {
            var i = new RegExp(t.ui.autocomplete.escapeRegex(n), "i");
            return t.grep(e, function(t) {
                return i.test(t.label || t.value || t)
            })
        }
    }), t.widget("ui.autocomplete", t.ui.autocomplete, {
        options: {
            messages: {
                noResults: "No search results.",
                results: function(t) {
                    return t + (t > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate."
                }
            }
        },
        __response: function(t) {
            var e;
            this._superApply(arguments), this.options.disabled || this.cancelSearch || (e = t && t.length ? this.options.messages.results(t.length) : this.options.messages.noResults, this.liveRegion.text(e))
        }
    })
}(jQuery),
function(t) {
    var e, n = "ui-button ui-widget ui-state-default ui-corner-all",
        i = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",
        r = function() {
            var e = t(this);
            setTimeout(function() {
                e.find(":ui-button").button("refresh")
            }, 1)
        },
        o = function(e) {
            var n = e.name,
                i = e.form,
                r = t([]);
            return n && (n = n.replace(/'/g, "\\'"), r = i ? t(i).find("[name='" + n + "']") : t("[name='" + n + "']", e.ownerDocument).filter(function() {
                return !this.form
            })), r
        };
    t.widget("ui.button", {
        version: "1.10.4",
        defaultElement: "<button>",
        options: {
            disabled: null,
            text: !0,
            label: null,
            icons: {
                primary: null,
                secondary: null
            }
        },
        _create: function() {
            this.element.closest("form").unbind("reset" + this.eventNamespace).bind("reset" + this.eventNamespace, r), "boolean" != typeof this.options.disabled ? this.options.disabled = !!this.element.prop("disabled") : this.element.prop("disabled", this.options.disabled), this._determineButtonType(), this.hasTitle = !!this.buttonElement.attr("title");
            var i = this,
                s = this.options,
                a = "checkbox" === this.type || "radio" === this.type,
                l = a ? "" : "ui-state-active";
            null === s.label && (s.label = "input" === this.type ? this.buttonElement.val() : this.buttonElement.html()), this._hoverable(this.buttonElement), this.buttonElement.addClass(n).attr("role", "button").bind("mouseenter" + this.eventNamespace, function() {
                s.disabled || this === e && t(this).addClass("ui-state-active")
            }).bind("mouseleave" + this.eventNamespace, function() {
                s.disabled || t(this).removeClass(l)
            }).bind("click" + this.eventNamespace, function(t) {
                s.disabled && (t.preventDefault(), t.stopImmediatePropagation())
            }), this._on({
                focus: function() {
                    this.buttonElement.addClass("ui-state-focus")
                },
                blur: function() {
                    this.buttonElement.removeClass("ui-state-focus")
                }
            }), a && this.element.bind("change" + this.eventNamespace, function() {
                i.refresh()
            }), "checkbox" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
                return s.disabled ? !1 : void 0
            }) : "radio" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
                if (s.disabled) return !1;
                t(this).addClass("ui-state-active"), i.buttonElement.attr("aria-pressed", "true");
                var e = i.element[0];
                o(e).not(e).map(function() {
                    return t(this).button("widget")[0]
                }).removeClass("ui-state-active").attr("aria-pressed", "false")
            }) : (this.buttonElement.bind("mousedown" + this.eventNamespace, function() {
                return s.disabled ? !1 : (t(this).addClass("ui-state-active"), e = this, void i.document.one("mouseup", function() {
                    e = null
                }))
            }).bind("mouseup" + this.eventNamespace, function() {
                return s.disabled ? !1 : void t(this).removeClass("ui-state-active")
            }).bind("keydown" + this.eventNamespace, function(e) {
                return s.disabled ? !1 : void((e.keyCode === t.ui.keyCode.SPACE || e.keyCode === t.ui.keyCode.ENTER) && t(this).addClass("ui-state-active"))
            }).bind("keyup" + this.eventNamespace + " blur" + this.eventNamespace, function() {
                t(this).removeClass("ui-state-active")
            }), this.buttonElement.is("a") && this.buttonElement.keyup(function(e) {
                e.keyCode === t.ui.keyCode.SPACE && t(this).click()
            })), this._setOption("disabled", s.disabled), this._resetButton()
        },
        _determineButtonType: function() {
            var t, e, n;
            this.type = this.element.is("[type=checkbox]") ? "checkbox" : this.element.is("[type=radio]") ? "radio" : this.element.is("input") ? "input" : "button", "checkbox" === this.type || "radio" === this.type ? (t = this.element.parents().last(), e = "label[for='" + this.element.attr("id") + "']", this.buttonElement = t.find(e), this.buttonElement.length || (t = t.length ? t.siblings() : this.element.siblings(), this.buttonElement = t.filter(e), this.buttonElement.length || (this.buttonElement = t.find(e))), this.element.addClass("ui-helper-hidden-accessible"), n = this.element.is(":checked"), n && this.buttonElement.addClass("ui-state-active"), this.buttonElement.prop("aria-pressed", n)) : this.buttonElement = this.element
        },
        widget: function() {
            return this.buttonElement
        },
        _destroy: function() {
            this.element.removeClass("ui-helper-hidden-accessible"), this.buttonElement.removeClass(n + " ui-state-active " + i).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()), this.hasTitle || this.buttonElement.removeAttr("title")
        },
        _setOption: function(t, e) {
            return this._super(t, e), "disabled" === t ? (this.element.prop("disabled", !!e), void(e && this.buttonElement.removeClass("ui-state-focus"))) : void this._resetButton()
        },
        refresh: function() {
            var e = this.element.is("input, button") ? this.element.is(":disabled") : this.element.hasClass("ui-button-disabled");
            e !== this.options.disabled && this._setOption("disabled", e), "radio" === this.type ? o(this.element[0]).each(function() {
                t(this).is(":checked") ? t(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true") : t(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false")
            }) : "checkbox" === this.type && (this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true") : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false"))
        },
        _resetButton: function() {
            if ("input" === this.type) return void(this.options.label && this.element.val(this.options.label));
            var e = this.buttonElement.removeClass(i),
                n = t("<span></span>", this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(e.empty()).text(),
                r = this.options.icons,
                o = r.primary && r.secondary,
                s = [];
            r.primary || r.secondary ? (this.options.text && s.push("ui-button-text-icon" + (o ? "s" : r.primary ? "-primary" : "-secondary")), r.primary && e.prepend("<span class='ui-button-icon-primary ui-icon " + r.primary + "'></span>"), r.secondary && e.append("<span class='ui-button-icon-secondary ui-icon " + r.secondary + "'></span>"), this.options.text || (s.push(o ? "ui-button-icons-only" : "ui-button-icon-only"), this.hasTitle || e.attr("title", t.trim(n)))) : s.push("ui-button-text-only"), e.addClass(s.join(" "))
        }
    }), t.widget("ui.buttonset", {
        version: "1.10.4",
        options: {
            items: "button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"
        },
        _create: function() {
            this.element.addClass("ui-buttonset")
        },
        _init: function() {
            this.refresh()
        },
        _setOption: function(t, e) {
            "disabled" === t && this.buttons.button("option", t, e), this._super(t, e)
        },
        refresh: function() {
            var e = "rtl" === this.element.css("direction");
            this.buttons = this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function() {
                return t(this).button("widget")[0]
            }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(e ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(e ? "ui-corner-left" : "ui-corner-right").end().end()
        },
        _destroy: function() {
            this.element.removeClass("ui-buttonset"), this.buttons.map(function() {
                return t(this).button("widget")[0]
            }).removeClass("ui-corner-left ui-corner-right").end().button("destroy")
        }
    })
}(jQuery),
function(t, e) {
    function n() {
        this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
            closeText: "Done",
            prevText: "Prev",
            nextText: "Next",
            currentText: "Today",
            monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            weekHeader: "Wk",
            dateFormat: "mm/dd/yy",
            firstDay: 0,
            isRTL: !1,
            showMonthAfterYear: !1,
            yearSuffix: ""
        }, this._defaults = {
            showOn: "focus",
            showAnim: "fadeIn",
            showOptions: {},
            defaultDate: null,
            appendText: "",
            buttonText: "...",
            buttonImage: "",
            buttonImageOnly: !1,
            hideIfNoPrevNext: !1,
            navigationAsDateFormat: !1,
            gotoCurrent: !1,
            changeMonth: !1,
            changeYear: !1,
            yearRange: "c-10:c+10",
            showOtherMonths: !1,
            selectOtherMonths: !1,
            showWeek: !1,
            calculateWeek: this.iso8601Week,
            shortYearCutoff: "+10",
            minDate: null,
            maxDate: null,
            duration: "fast",
            beforeShowDay: null,
            beforeShow: null,
            onSelect: null,
            onChangeMonthYear: null,
            onClose: null,
            numberOfMonths: 1,
            showCurrentAtPos: 0,
            stepMonths: 1,
            stepBigMonths: 12,
            altField: "",
            altFormat: "",
            constrainInput: !0,
            showButtonPanel: !1,
            autoSize: !1,
            disabled: !1
        }, t.extend(this._defaults, this.regional[""]), this.dpDiv = i(t("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))
    }

    function i(e) {
        var n = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
        return e.delegate(n, "mouseout", function() {
            t(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).removeClass("ui-datepicker-next-hover")
        }).delegate(n, "mouseover", function() {
            t.datepicker._isDisabledDatepicker(o.inline ? e.parent()[0] : o.input[0]) || (t(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), t(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).addClass("ui-datepicker-next-hover"))
        })
    }

    function r(e, n) {
        t.extend(e, n);
        for (var i in n) null == n[i] && (e[i] = n[i]);
        return e
    }
    t.extend(t.ui, {
        datepicker: {
            version: "1.10.4"
        }
    });
    var o, s = "datepicker";
    t.extend(n.prototype, {
        markerClassName: "hasDatepicker",
        maxRows: 4,
        _widgetDatepicker: function() {
            return this.dpDiv
        },
        setDefaults: function(t) {
            return r(this._defaults, t || {}), this
        },
        _attachDatepicker: function(e, n) {
            var i, r, o;
            i = e.nodeName.toLowerCase(), r = "div" === i || "span" === i, e.id || (this.uuid += 1, e.id = "dp" + this.uuid), o = this._newInst(t(e), r), o.settings = t.extend({}, n || {}), "input" === i ? this._connectDatepicker(e, o) : r && this._inlineDatepicker(e, o)
        },
        _newInst: function(e, n) {
            var r = e[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1");
            return {
                id: r,
                input: e,
                selectedDay: 0,
                selectedMonth: 0,
                selectedYear: 0,
                drawMonth: 0,
                drawYear: 0,
                inline: n,
                dpDiv: n ? i(t("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
            }
        },
        _connectDatepicker: function(e, n) {
            var i = t(e);
            n.append = t([]), n.trigger = t([]), i.hasClass(this.markerClassName) || (this._attachments(i, n), i.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp), this._autoSize(n), t.data(e, s, n), n.settings.disabled && this._disableDatepicker(e))
        },
        _attachments: function(e, n) {
            var i, r, o, s = this._get(n, "appendText"),
                a = this._get(n, "isRTL");
            n.append && n.append.remove(), s && (n.append = t("<span class='" + this._appendClass + "'>" + s + "</span>"), e[a ? "before" : "after"](n.append)), e.unbind("focus", this._showDatepicker), n.trigger && n.trigger.remove(), i = this._get(n, "showOn"), ("focus" === i || "both" === i) && e.focus(this._showDatepicker), ("button" === i || "both" === i) && (r = this._get(n, "buttonText"), o = this._get(n, "buttonImage"), n.trigger = t(this._get(n, "buttonImageOnly") ? t("<img/>").addClass(this._triggerClass).attr({
                src: o,
                alt: r,
                title: r
            }) : t("<button type='button'></button>").addClass(this._triggerClass).html(o ? t("<img/>").attr({
                src: o,
                alt: r,
                title: r
            }) : r)), e[a ? "before" : "after"](n.trigger), n.trigger.click(function() {
                return t.datepicker._datepickerShowing && t.datepicker._lastInput === e[0] ? t.datepicker._hideDatepicker() : t.datepicker._datepickerShowing && t.datepicker._lastInput !== e[0] ? (t.datepicker._hideDatepicker(), t.datepicker._showDatepicker(e[0])) : t.datepicker._showDatepicker(e[0]), !1
            }))
        },
        _autoSize: function(t) {
            if (this._get(t, "autoSize") && !t.inline) {
                var e, n, i, r, o = new Date(2009, 11, 20),
                    s = this._get(t, "dateFormat");
                s.match(/[DM]/) && (e = function(t) {
                    for (n = 0, i = 0, r = 0; r < t.length; r++) t[r].length > n && (n = t[r].length, i = r);
                    return i
                }, o.setMonth(e(this._get(t, s.match(/MM/) ? "monthNames" : "monthNamesShort"))), o.setDate(e(this._get(t, s.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - o.getDay())), t.input.attr("size", this._formatDate(t, o).length)
            }
        },
        _inlineDatepicker: function(e, n) {
            var i = t(e);
            i.hasClass(this.markerClassName) || (i.addClass(this.markerClassName).append(n.dpDiv), t.data(e, s, n), this._setDate(n, this._getDefaultDate(n), !0), this._updateDatepicker(n), this._updateAlternate(n), n.settings.disabled && this._disableDatepicker(e), n.dpDiv.css("display", "block"))
        },
        _dialogDatepicker: function(e, n, i, o, a) {
            var l, u, c, h, d, p = this._dialogInst;
            return p || (this.uuid += 1, l = "dp" + this.uuid, this._dialogInput = t("<input type='text' id='" + l + "' style='position: absolute; top: -100px; width: 0px;'/>"), this._dialogInput.keydown(this._doKeyDown), t("body").append(this._dialogInput), p = this._dialogInst = this._newInst(this._dialogInput, !1), p.settings = {}, t.data(this._dialogInput[0], s, p)), r(p.settings, o || {}), n = n && n.constructor === Date ? this._formatDate(p, n) : n, this._dialogInput.val(n), this._pos = a ? a.length ? a : [a.pageX, a.pageY] : null, this._pos || (u = document.documentElement.clientWidth, c = document.documentElement.clientHeight, h = document.documentElement.scrollLeft || document.body.scrollLeft, d = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [u / 2 - 100 + h, c / 2 - 150 + d]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), p.settings.onSelect = i, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), t.blockUI && t.blockUI(this.dpDiv), t.data(this._dialogInput[0], s, p), this
        },
        _destroyDatepicker: function(e) {
            var n, i = t(e),
                r = t.data(e, s);
            i.hasClass(this.markerClassName) && (n = e.nodeName.toLowerCase(), t.removeData(e, s), "input" === n ? (r.append.remove(), r.trigger.remove(), i.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : ("div" === n || "span" === n) && i.removeClass(this.markerClassName).empty())
        },
        _enableDatepicker: function(e) {
            var n, i, r = t(e),
                o = t.data(e, s);
            r.hasClass(this.markerClassName) && (n = e.nodeName.toLowerCase(), "input" === n ? (e.disabled = !1, o.trigger.filter("button").each(function() {
                this.disabled = !1
            }).end().filter("img").css({
                opacity: "1.0",
                cursor: ""
            })) : ("div" === n || "span" === n) && (i = r.children("." + this._inlineClass), i.children().removeClass("ui-state-disabled"), i.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = t.map(this._disabledInputs, function(t) {
                return t === e ? null : t
            }))
        },
        _disableDatepicker: function(e) {
            var n, i, r = t(e),
                o = t.data(e, s);
            r.hasClass(this.markerClassName) && (n = e.nodeName.toLowerCase(), "input" === n ? (e.disabled = !0, o.trigger.filter("button").each(function() {
                this.disabled = !0
            }).end().filter("img").css({
                opacity: "0.5",
                cursor: "default"
            })) : ("div" === n || "span" === n) && (i = r.children("." + this._inlineClass), i.children().addClass("ui-state-disabled"), i.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = t.map(this._disabledInputs, function(t) {
                return t === e ? null : t
            }), this._disabledInputs[this._disabledInputs.length] = e)
        },
        _isDisabledDatepicker: function(t) {
            if (!t) return !1;
            for (var e = 0; e < this._disabledInputs.length; e++)
                if (this._disabledInputs[e] === t) return !0;
            return !1
        },
        _getInst: function(e) {
            try {
                return t.data(e, s)
            } catch (n) {
                throw "Missing instance data for this datepicker"
            }
        },
        _optionDatepicker: function(n, i, o) {
            var s, a, l, u, c = this._getInst(n);
            return 2 === arguments.length && "string" == typeof i ? "defaults" === i ? t.extend({}, t.datepicker._defaults) : c ? "all" === i ? t.extend({}, c.settings) : this._get(c, i) : null : (s = i || {}, "string" == typeof i && (s = {}, s[i] = o), void(c && (this._curInst === c && this._hideDatepicker(), a = this._getDateDatepicker(n, !0), l = this._getMinMaxDate(c, "min"), u = this._getMinMaxDate(c, "max"), r(c.settings, s), null !== l && s.dateFormat !== e && s.minDate === e && (c.settings.minDate = this._formatDate(c, l)), null !== u && s.dateFormat !== e && s.maxDate === e && (c.settings.maxDate = this._formatDate(c, u)), "disabled" in s && (s.disabled ? this._disableDatepicker(n) : this._enableDatepicker(n)), this._attachments(t(n), c), this._autoSize(c), this._setDate(c, a), this._updateAlternate(c), this._updateDatepicker(c))))
        },
        _changeDatepicker: function(t, e, n) {
            this._optionDatepicker(t, e, n)
        },
        _refreshDatepicker: function(t) {
            var e = this._getInst(t);
            e && this._updateDatepicker(e)
        },
        _setDateDatepicker: function(t, e) {
            var n = this._getInst(t);
            n && (this._setDate(n, e), this._updateDatepicker(n), this._updateAlternate(n))
        },
        _getDateDatepicker: function(t, e) {
            var n = this._getInst(t);
            return n && !n.inline && this._setDateFromField(n, e), n ? this._getDate(n) : null
        },
        _doKeyDown: function(e) {
            var n, i, r, o = t.datepicker._getInst(e.target),
                s = !0,
                a = o.dpDiv.is(".ui-datepicker-rtl");
            if (o._keyEvent = !0, t.datepicker._datepickerShowing) switch (e.keyCode) {
                case 9:
                    t.datepicker._hideDatepicker(), s = !1;
                    break;
                case 13:
                    return r = t("td." + t.datepicker._dayOverClass + ":not(." + t.datepicker._currentClass + ")", o.dpDiv), r[0] && t.datepicker._selectDay(e.target, o.selectedMonth, o.selectedYear, r[0]), n = t.datepicker._get(o, "onSelect"), n ? (i = t.datepicker._formatDate(o), n.apply(o.input ? o.input[0] : null, [i, o])) : t.datepicker._hideDatepicker(), !1;
                case 27:
                    t.datepicker._hideDatepicker();
                    break;
                case 33:
                    t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(o, "stepBigMonths") : -t.datepicker._get(o, "stepMonths"), "M");
                    break;
                case 34:
                    t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(o, "stepBigMonths") : +t.datepicker._get(o, "stepMonths"), "M");
                    break;
                case 35:
                    (e.ctrlKey || e.metaKey) && t.datepicker._clearDate(e.target), s = e.ctrlKey || e.metaKey;
                    break;
                case 36:
                    (e.ctrlKey || e.metaKey) && t.datepicker._gotoToday(e.target), s = e.ctrlKey || e.metaKey;
                    break;
                case 37:
                    (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, a ? 1 : -1, "D"), s = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(o, "stepBigMonths") : -t.datepicker._get(o, "stepMonths"), "M");
                    break;
                case 38:
                    (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, -7, "D"), s = e.ctrlKey || e.metaKey;
                    break;
                case 39:
                    (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, a ? -1 : 1, "D"), s = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(o, "stepBigMonths") : +t.datepicker._get(o, "stepMonths"), "M");
                    break;
                case 40:
                    (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, 7, "D"), s = e.ctrlKey || e.metaKey;
                    break;
                default:
                    s = !1
            } else 36 === e.keyCode && e.ctrlKey ? t.datepicker._showDatepicker(this) : s = !1;
            s && (e.preventDefault(), e.stopPropagation())
        },
        _doKeyPress: function(e) {
            var n, i, r = t.datepicker._getInst(e.target);
            return t.datepicker._get(r, "constrainInput") ? (n = t.datepicker._possibleChars(t.datepicker._get(r, "dateFormat")), i = String.fromCharCode(null == e.charCode ? e.keyCode : e.charCode), e.ctrlKey || e.metaKey || " " > i || !n || n.indexOf(i) > -1) : void 0
        },
        _doKeyUp: function(e) {
            var n, i = t.datepicker._getInst(e.target);
            if (i.input.val() !== i.lastVal) try {
                n = t.datepicker.parseDate(t.datepicker._get(i, "dateFormat"), i.input ? i.input.val() : null, t.datepicker._getFormatConfig(i)), n && (t.datepicker._setDateFromField(i), t.datepicker._updateAlternate(i), t.datepicker._updateDatepicker(i))
            } catch (r) {}
            return !0
        },
        _showDatepicker: function(e) {
            if (e = e.target || e, "input" !== e.nodeName.toLowerCase() && (e = t("input", e.parentNode)[0]), !t.datepicker._isDisabledDatepicker(e) && t.datepicker._lastInput !== e) {
                var n, i, o, s, a, l, u;
                n = t.datepicker._getInst(e), t.datepicker._curInst && t.datepicker._curInst !== n && (t.datepicker._curInst.dpDiv.stop(!0, !0), n && t.datepicker._datepickerShowing && t.datepicker._hideDatepicker(t.datepicker._curInst.input[0])), i = t.datepicker._get(n, "beforeShow"), o = i ? i.apply(e, [e, n]) : {}, o !== !1 && (r(n.settings, o), n.lastVal = null, t.datepicker._lastInput = e, t.datepicker._setDateFromField(n), t.datepicker._inDialog && (e.value = ""), t.datepicker._pos || (t.datepicker._pos = t.datepicker._findPos(e), t.datepicker._pos[1] += e.offsetHeight), s = !1, t(e).parents().each(function() {
                    return s |= "fixed" === t(this).css("position"), !s
                }), a = {
                    left: t.datepicker._pos[0],
                    top: t.datepicker._pos[1]
                }, t.datepicker._pos = null, n.dpDiv.empty(), n.dpDiv.css({
                    position: "absolute",
                    display: "block",
                    top: "-1000px"
                }), t.datepicker._updateDatepicker(n), a = t.datepicker._checkOffset(n, a, s), n.dpDiv.css({
                    position: t.datepicker._inDialog && t.blockUI ? "static" : s ? "fixed" : "absolute",
                    display: "none",
                    left: a.left + "px",
                    top: a.top + "px"
                }), n.inline || (l = t.datepicker._get(n, "showAnim"), u = t.datepicker._get(n, "duration"), n.dpDiv.zIndex(t(e).zIndex() + 1), t.datepicker._datepickerShowing = !0, t.effects && t.effects.effect[l] ? n.dpDiv.show(l, t.datepicker._get(n, "showOptions"), u) : n.dpDiv[l || "show"](l ? u : null), t.datepicker._shouldFocusInput(n) && n.input.focus(), t.datepicker._curInst = n))
            }
        },
        _updateDatepicker: function(e) {
            this.maxRows = 4, o = e, e.dpDiv.empty().append(this._generateHTML(e)), this._attachHandlers(e), e.dpDiv.find("." + this._dayOverClass + " a").mouseover();
            var n, i = this._getNumberOfMonths(e),
                r = i[1],
                s = 17;
            e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), r > 1 && e.dpDiv.addClass("ui-datepicker-multi-" + r).css("width", s * r + "em"), e.dpDiv[(1 !== i[0] || 1 !== i[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), e.dpDiv[(this._get(e, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), e === t.datepicker._curInst && t.datepicker._datepickerShowing && t.datepicker._shouldFocusInput(e) && e.input.focus(), e.yearshtml && (n = e.yearshtml, setTimeout(function() {
                n === e.yearshtml && e.yearshtml && e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml), n = e.yearshtml = null
            }, 0))
        },
        _shouldFocusInput: function(t) {
            return t.input && t.input.is(":visible") && !t.input.is(":disabled") && !t.input.is(":focus")
        },
        _checkOffset: function(e, n, i) {
            var r = e.dpDiv.outerWidth(),
                o = e.dpDiv.outerHeight(),
                s = e.input ? e.input.outerWidth() : 0,
                a = e.input ? e.input.outerHeight() : 0,
                l = document.documentElement.clientWidth + (i ? 0 : t(document).scrollLeft()),
                u = document.documentElement.clientHeight + (i ? 0 : t(document).scrollTop());
            return n.left -= this._get(e, "isRTL") ? r - s : 0, n.left -= i && n.left === e.input.offset().left ? t(document).scrollLeft() : 0, n.top -= i && n.top === e.input.offset().top + a ? t(document).scrollTop() : 0, n.left -= Math.min(n.left, n.left + r > l && l > r ? Math.abs(n.left + r - l) : 0), n.top -= Math.min(n.top, n.top + o > u && u > o ? Math.abs(o + a) : 0), n
        },
        _findPos: function(e) {
            for (var n, i = this._getInst(e), r = this._get(i, "isRTL"); e && ("hidden" === e.type || 1 !== e.nodeType || t.expr.filters.hidden(e));) e = e[r ? "previousSibling" : "nextSibling"];
            return n = t(e).offset(), [n.left, n.top]
        },
        _hideDatepicker: function(e) {
            var n, i, r, o, a = this._curInst;
            !a || e && a !== t.data(e, s) || this._datepickerShowing && (n = this._get(a, "showAnim"), i = this._get(a, "duration"), r = function() {
                t.datepicker._tidyDialog(a)
            }, t.effects && (t.effects.effect[n] || t.effects[n]) ? a.dpDiv.hide(n, t.datepicker._get(a, "showOptions"), i, r) : a.dpDiv["slideDown" === n ? "slideUp" : "fadeIn" === n ? "fadeOut" : "hide"](n ? i : null, r), n || r(), this._datepickerShowing = !1, o = this._get(a, "onClose"), o && o.apply(a.input ? a.input[0] : null, [a.input ? a.input.val() : "", a]), this._lastInput = null, this._inDialog && (this._dialogInput.css({
                position: "absolute",
                left: "0",
                top: "-100px"
            }), t.blockUI && (t.unblockUI(), t("body").append(this.dpDiv))), this._inDialog = !1)
        },
        _tidyDialog: function(t) {
            t.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
        },
        _checkExternalClick: function(e) {
            if (t.datepicker._curInst) {
                var n = t(e.target),
                    i = t.datepicker._getInst(n[0]);
                (n[0].id !== t.datepicker._mainDivId && 0 === n.parents("#" + t.datepicker._mainDivId).length && !n.hasClass(t.datepicker.markerClassName) && !n.closest("." + t.datepicker._triggerClass).length && t.datepicker._datepickerShowing && (!t.datepicker._inDialog || !t.blockUI) || n.hasClass(t.datepicker.markerClassName) && t.datepicker._curInst !== i) && t.datepicker._hideDatepicker()
            }
        },
        _adjustDate: function(e, n, i) {
            var r = t(e),
                o = this._getInst(r[0]);
            this._isDisabledDatepicker(r[0]) || (this._adjustInstDate(o, n + ("M" === i ? this._get(o, "showCurrentAtPos") : 0), i), this._updateDatepicker(o))
        },
        _gotoToday: function(e) {
            var n, i = t(e),
                r = this._getInst(i[0]);
            this._get(r, "gotoCurrent") && r.currentDay ? (r.selectedDay = r.currentDay, r.drawMonth = r.selectedMonth = r.currentMonth, r.drawYear = r.selectedYear = r.currentYear) : (n = new Date, r.selectedDay = n.getDate(), r.drawMonth = r.selectedMonth = n.getMonth(), r.drawYear = r.selectedYear = n.getFullYear()), this._notifyChange(r), this._adjustDate(i)
        },
        _selectMonthYear: function(e, n, i) {
            var r = t(e),
                o = this._getInst(r[0]);
            o["selected" + ("M" === i ? "Month" : "Year")] = o["draw" + ("M" === i ? "Month" : "Year")] = parseInt(n.options[n.selectedIndex].value, 10), this._notifyChange(o), this._adjustDate(r)
        },
        _selectDay: function(e, n, i, r) {
            var o, s = t(e);
            t(r).hasClass(this._unselectableClass) || this._isDisabledDatepicker(s[0]) || (o = this._getInst(s[0]), o.selectedDay = o.currentDay = t("a", r).html(), o.selectedMonth = o.currentMonth = n, o.selectedYear = o.currentYear = i, this._selectDate(e, this._formatDate(o, o.currentDay, o.currentMonth, o.currentYear)))
        },
        _clearDate: function(e) {
            var n = t(e);
            this._selectDate(n, "")
        },
        _selectDate: function(e, n) {
            var i, r = t(e),
                o = this._getInst(r[0]);
            n = null != n ? n : this._formatDate(o), o.input && o.input.val(n), this._updateAlternate(o), i = this._get(o, "onSelect"), i ? i.apply(o.input ? o.input[0] : null, [n, o]) : o.input && o.input.trigger("change"), o.inline ? this._updateDatepicker(o) : (this._hideDatepicker(), this._lastInput = o.input[0], "object" != typeof o.input[0] && o.input.focus(), this._lastInput = null)
        },
        _updateAlternate: function(e) {
            var n, i, r, o = this._get(e, "altField");
            o && (n = this._get(e, "altFormat") || this._get(e, "dateFormat"), i = this._getDate(e), r = this.formatDate(n, i, this._getFormatConfig(e)), t(o).each(function() {
                t(this).val(r)
            }))
        },
        noWeekends: function(t) {
            var e = t.getDay();
            return [e > 0 && 6 > e, ""]
        },
        iso8601Week: function(t) {
            var e, n = new Date(t.getTime());
            return n.setDate(n.getDate() + 4 - (n.getDay() || 7)), e = n.getTime(), n.setMonth(0), n.setDate(1), Math.floor(Math.round((e - n) / 864e5) / 7) + 1
        },
        parseDate: function(e, n, i) {
            if (null == e || null == n) throw "Invalid arguments";
            if (n = "object" == typeof n ? n.toString() : n + "", "" === n) return null;
            var r, o, s, a, l = 0,
                u = (i ? i.shortYearCutoff : null) || this._defaults.shortYearCutoff,
                c = "string" != typeof u ? u : (new Date).getFullYear() % 100 + parseInt(u, 10),
                h = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort,
                d = (i ? i.dayNames : null) || this._defaults.dayNames,
                p = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort,
                f = (i ? i.monthNames : null) || this._defaults.monthNames,
                m = -1,
                g = -1,
                v = -1,
                y = -1,
                _ = !1,
                b = function(t) {
                    var n = r + 1 < e.length && e.charAt(r + 1) === t;
                    return n && r++, n
                },
                w = function(t) {
                    var e = b(t),
                        i = "@" === t ? 14 : "!" === t ? 20 : "y" === t && e ? 4 : "o" === t ? 3 : 2,
                        r = new RegExp("^\\d{1," + i + "}"),
                        o = n.substring(l).match(r);
                    if (!o) throw "Missing number at position " + l;
                    return l += o[0].length, parseInt(o[0], 10)
                },
                L = function(e, i, r) {
                    var o = -1,
                        s = t.map(b(e) ? r : i, function(t, e) {
                            return [
                                [e, t]
                            ]
                        }).sort(function(t, e) {
                            return -(t[1].length - e[1].length)
                        });
                    if (t.each(s, function(t, e) {
                            var i = e[1];
                            return n.substr(l, i.length).toLowerCase() === i.toLowerCase() ? (o = e[0], l += i.length, !1) : void 0
                        }), -1 !== o) return o + 1;
                    throw "Unknown name at position " + l
                },
                x = function() {
                    if (n.charAt(l) !== e.charAt(r)) throw "Unexpected literal at position " + l;
                    l++
                };
            for (r = 0; r < e.length; r++)
                if (_) "'" !== e.charAt(r) || b("'") ? x() : _ = !1;
                else switch (e.charAt(r)) {
                    case "d":
                        v = w("d");
                        break;
                    case "D":
                        L("D", h, d);
                        break;
                    case "o":
                        y = w("o");
                        break;
                    case "m":
                        g = w("m");
                        break;
                    case "M":
                        g = L("M", p, f);
                        break;
                    case "y":
                        m = w("y");
                        break;
                    case "@":
                        a = new Date(w("@")), m = a.getFullYear(), g = a.getMonth() + 1, v = a.getDate();
                        break;
                    case "!":
                        a = new Date((w("!") - this._ticksTo1970) / 1e4), m = a.getFullYear(), g = a.getMonth() + 1, v = a.getDate();
                        break;
                    case "'":
                        b("'") ? x() : _ = !0;
                        break;
                    default:
                        x()
                }
            if (l < n.length && (s = n.substr(l), !/^\s+/.test(s))) throw "Extra/unparsed characters found in date: " + s;
            if (-1 === m ? m = (new Date).getFullYear() : 100 > m && (m += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (c >= m ? 0 : -100)), y > -1)
                for (g = 1, v = y;;) {
                    if (o = this._getDaysInMonth(m, g - 1), o >= v) break;
                    g++, v -= o
                }
            if (a = this._daylightSavingAdjust(new Date(m, g - 1, v)), a.getFullYear() !== m || a.getMonth() + 1 !== g || a.getDate() !== v) throw "Invalid date";
            return a
        },
        ATOM: "yy-mm-dd",
        COOKIE: "D, dd M yy",
        ISO_8601: "yy-mm-dd",
        RFC_822: "D, d M y",
        RFC_850: "DD, dd-M-y",
        RFC_1036: "D, d M y",
        RFC_1123: "D, d M yy",
        RFC_2822: "D, d M yy",
        RSS: "D, d M y",
        TICKS: "!",
        TIMESTAMP: "@",
        W3C: "yy-mm-dd",
        _ticksTo1970: 24 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)) * 60 * 60 * 1e7,
        formatDate: function(t, e, n) {
            if (!e) return "";
            var i, r = (n ? n.dayNamesShort : null) || this._defaults.dayNamesShort,
                o = (n ? n.dayNames : null) || this._defaults.dayNames,
                s = (n ? n.monthNamesShort : null) || this._defaults.monthNamesShort,
                a = (n ? n.monthNames : null) || this._defaults.monthNames,
                l = function(e) {
                    var n = i + 1 < t.length && t.charAt(i + 1) === e;
                    return n && i++, n
                },
                u = function(t, e, n) {
                    var i = "" + e;
                    if (l(t))
                        for (; i.length < n;) i = "0" + i;
                    return i
                },
                c = function(t, e, n, i) {
                    return l(t) ? i[e] : n[e]
                },
                h = "",
                d = !1;
            if (e)
                for (i = 0; i < t.length; i++)
                    if (d) "'" !== t.charAt(i) || l("'") ? h += t.charAt(i) : d = !1;
                    else switch (t.charAt(i)) {
                        case "d":
                            h += u("d", e.getDate(), 2);
                            break;
                        case "D":
                            h += c("D", e.getDay(), r, o);
                            break;
                        case "o":
                            h += u("o", Math.round((new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime() - new Date(e.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                            break;
                        case "m":
                            h += u("m", e.getMonth() + 1, 2);
                            break;
                        case "M":
                            h += c("M", e.getMonth(), s, a);
                            break;
                        case "y":
                            h += l("y") ? e.getFullYear() : (e.getYear() % 100 < 10 ? "0" : "") + e.getYear() % 100;
                            break;
                        case "@":
                            h += e.getTime();
                            break;
                        case "!":
                            h += 1e4 * e.getTime() + this._ticksTo1970;
                            break;
                        case "'":
                            l("'") ? h += "'" : d = !0;
                            break;
                        default:
                            h += t.charAt(i)
                    }
            return h
        },
        _possibleChars: function(t) {
            var e, n = "",
                i = !1,
                r = function(n) {
                    var i = e + 1 < t.length && t.charAt(e + 1) === n;
                    return i && e++, i
                };
            for (e = 0; e < t.length; e++)
                if (i) "'" !== t.charAt(e) || r("'") ? n += t.charAt(e) : i = !1;
                else switch (t.charAt(e)) {
                    case "d":
                    case "m":
                    case "y":
                    case "@":
                        n += "0123456789";
                        break;
                    case "D":
                    case "M":
                        return null;
                    case "'":
                        r("'") ? n += "'" : i = !0;
                        break;
                    default:
                        n += t.charAt(e)
                }
            return n
        },
        _get: function(t, n) {
            return t.settings[n] !== e ? t.settings[n] : this._defaults[n]
        },
        _setDateFromField: function(t, e) {
            if (t.input.val() !== t.lastVal) {
                var n = this._get(t, "dateFormat"),
                    i = t.lastVal = t.input ? t.input.val() : null,
                    r = this._getDefaultDate(t),
                    o = r,
                    s = this._getFormatConfig(t);
                try {
                    o = this.parseDate(n, i, s) || r
                } catch (a) {
                    i = e ? "" : i
                }
                t.selectedDay = o.getDate(), t.drawMonth = t.selectedMonth = o.getMonth(), t.drawYear = t.selectedYear = o.getFullYear(), t.currentDay = i ? o.getDate() : 0, t.currentMonth = i ? o.getMonth() : 0, t.currentYear = i ? o.getFullYear() : 0, this._adjustInstDate(t)
            }
        },
        _getDefaultDate: function(t) {
            return this._restrictMinMax(t, this._determineDate(t, this._get(t, "defaultDate"), new Date))
        },
        _determineDate: function(e, n, i) {
            var r = function(t) {
                    var e = new Date;
                    return e.setDate(e.getDate() + t), e
                },
                o = function(n) {
                    try {
                        return t.datepicker.parseDate(t.datepicker._get(e, "dateFormat"), n, t.datepicker._getFormatConfig(e))
                    } catch (i) {}
                    for (var r = (n.toLowerCase().match(/^c/) ? t.datepicker._getDate(e) : null) || new Date, o = r.getFullYear(), s = r.getMonth(), a = r.getDate(), l = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, u = l.exec(n); u;) {
                        switch (u[2] || "d") {
                            case "d":
                            case "D":
                                a += parseInt(u[1], 10);
                                break;
                            case "w":
                            case "W":
                                a += 7 * parseInt(u[1], 10);
                                break;
                            case "m":
                            case "M":
                                s += parseInt(u[1], 10), a = Math.min(a, t.datepicker._getDaysInMonth(o, s));
                                break;
                            case "y":
                            case "Y":
                                o += parseInt(u[1], 10), a = Math.min(a, t.datepicker._getDaysInMonth(o, s))
                        }
                        u = l.exec(n)
                    }
                    return new Date(o, s, a)
                },
                s = null == n || "" === n ? i : "string" == typeof n ? o(n) : "number" == typeof n ? isNaN(n) ? i : r(n) : new Date(n.getTime());
            return s = s && "Invalid Date" === s.toString() ? i : s, s && (s.setHours(0), s.setMinutes(0), s.setSeconds(0), s.setMilliseconds(0)), this._daylightSavingAdjust(s)
        },
        _daylightSavingAdjust: function(t) {
            return t ? (t.setHours(t.getHours() > 12 ? t.getHours() + 2 : 0), t) : null
        },
        _setDate: function(t, e, n) {
            var i = !e,
                r = t.selectedMonth,
                o = t.selectedYear,
                s = this._restrictMinMax(t, this._determineDate(t, e, new Date));
            t.selectedDay = t.currentDay = s.getDate(), t.drawMonth = t.selectedMonth = t.currentMonth = s.getMonth(), t.drawYear = t.selectedYear = t.currentYear = s.getFullYear(), r === t.selectedMonth && o === t.selectedYear || n || this._notifyChange(t), this._adjustInstDate(t), t.input && t.input.val(i ? "" : this._formatDate(t))
        },
        _getDate: function(t) {
            var e = !t.currentYear || t.input && "" === t.input.val() ? null : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
            return e
        },
        _attachHandlers: function(e) {
            var n = this._get(e, "stepMonths"),
                i = "#" + e.id.replace(/\\\\/g, "\\");
            e.dpDiv.find("[data-handler]").map(function() {
                var e = {
                    prev: function() {
                        t.datepicker._adjustDate(i, -n, "M")
                    },
                    next: function() {
                        t.datepicker._adjustDate(i, +n, "M")
                    },
                    hide: function() {
                        t.datepicker._hideDatepicker()
                    },
                    today: function() {
                        t.datepicker._gotoToday(i)
                    },
                    selectDay: function() {
                        return t.datepicker._selectDay(i, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                    },
                    selectMonth: function() {
                        return t.datepicker._selectMonthYear(i, this, "M"), !1
                    },
                    selectYear: function() {
                        return t.datepicker._selectMonthYear(i, this, "Y"), !1
                    }
                };
                t(this).bind(this.getAttribute("data-event"), e[this.getAttribute("data-handler")])
            })
        },
        _generateHTML: function(t) {
            var e, n, i, r, o, s, a, l, u, c, h, d, p, f, m, g, v, y, _, b, w, L, x, $, C, k, S, P, E, T, M, D, O, A, I, N, R, U, B, j = new Date,
                z = this._daylightSavingAdjust(new Date(j.getFullYear(), j.getMonth(), j.getDate())),
                F = this._get(t, "isRTL"),
                H = this._get(t, "showButtonPanel"),
                q = this._get(t, "hideIfNoPrevNext"),
                V = this._get(t, "navigationAsDateFormat"),
                W = this._getNumberOfMonths(t),
                G = this._get(t, "showCurrentAtPos"),
                Z = this._get(t, "stepMonths"),
                Y = 1 !== W[0] || 1 !== W[1],
                J = this._daylightSavingAdjust(t.currentDay ? new Date(t.currentYear, t.currentMonth, t.currentDay) : new Date(9999, 9, 9)),
                K = this._getMinMaxDate(t, "min"),
                Q = this._getMinMaxDate(t, "max"),
                X = t.drawMonth - G,
                te = t.drawYear;
            if (0 > X && (X += 12, te--), Q)
                for (e = this._daylightSavingAdjust(new Date(Q.getFullYear(), Q.getMonth() - W[0] * W[1] + 1, Q.getDate())), e = K && K > e ? K : e; this._daylightSavingAdjust(new Date(te, X, 1)) > e;) X--, 0 > X && (X = 11, te--);
            for (t.drawMonth = X, t.drawYear = te, n = this._get(t, "prevText"), n = V ? this.formatDate(n, this._daylightSavingAdjust(new Date(te, X - Z, 1)), this._getFormatConfig(t)) : n, i = this._canAdjustMonth(t, -1, te, X) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (F ? "e" : "w") + "'>" + n + "</span></a>" : q ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (F ? "e" : "w") + "'>" + n + "</span></a>", r = this._get(t, "nextText"), r = V ? this.formatDate(r, this._daylightSavingAdjust(new Date(te, X + Z, 1)), this._getFormatConfig(t)) : r, o = this._canAdjustMonth(t, 1, te, X) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + r + "'><span class='ui-icon ui-icon-circle-triangle-" + (F ? "w" : "e") + "'>" + r + "</span></a>" : q ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + r + "'><span class='ui-icon ui-icon-circle-triangle-" + (F ? "w" : "e") + "'>" + r + "</span></a>", s = this._get(t, "currentText"), a = this._get(t, "gotoCurrent") && t.currentDay ? J : z, s = V ? this.formatDate(s, a, this._getFormatConfig(t)) : s, l = t.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(t, "closeText") + "</button>", u = H ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (F ? l : "") + (this._isInRange(t, a) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + s + "</button>" : "") + (F ? "" : l) + "</div>" : "", c = parseInt(this._get(t, "firstDay"), 10), c = isNaN(c) ? 0 : c, h = this._get(t, "showWeek"), d = this._get(t, "dayNames"), p = this._get(t, "dayNamesMin"), f = this._get(t, "monthNames"), m = this._get(t, "monthNamesShort"), g = this._get(t, "beforeShowDay"), v = this._get(t, "showOtherMonths"), y = this._get(t, "selectOtherMonths"), _ = this._getDefaultDate(t), b = "", L = 0; L < W[0]; L++) {
                for (x = "", this.maxRows = 4, $ = 0; $ < W[1]; $++) {
                    if (C = this._daylightSavingAdjust(new Date(te, X, t.selectedDay)), k = " ui-corner-all", S = "", Y) {
                        if (S += "<div class='ui-datepicker-group", W[1] > 1) switch ($) {
                            case 0:
                                S += " ui-datepicker-group-first", k = " ui-corner-" + (F ? "right" : "left");
                                break;
                            case W[1] - 1:
                                S += " ui-datepicker-group-last", k = " ui-corner-" + (F ? "left" : "right");
                                break;
                            default:
                                S += " ui-datepicker-group-middle", k = ""
                        }
                        S += "'>"
                    }
                    for (S += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + k + "'>" + (/all|left/.test(k) && 0 === L ? F ? o : i : "") + (/all|right/.test(k) && 0 === L ? F ? i : o : "") + this._generateMonthYearHeader(t, X, te, K, Q, L > 0 || $ > 0, f, m) + "</div><table class='ui-datepicker-calendar'><thead><tr>", P = h ? "<th class='ui-datepicker-week-col'>" + this._get(t, "weekHeader") + "</th>" : "", w = 0; 7 > w; w++) E = (w + c) % 7, P += "<th" + ((w + c + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + "><span title='" + d[E] + "'>" + p[E] + "</span></th>";
                    for (S += P + "</tr></thead><tbody>", T = this._getDaysInMonth(te, X), te === t.selectedYear && X === t.selectedMonth && (t.selectedDay = Math.min(t.selectedDay, T)), M = (this._getFirstDayOfMonth(te, X) - c + 7) % 7, D = Math.ceil((M + T) / 7), O = Y && this.maxRows > D ? this.maxRows : D, this.maxRows = O, A = this._daylightSavingAdjust(new Date(te, X, 1 - M)), I = 0; O > I; I++) {
                        for (S += "<tr>", N = h ? "<td class='ui-datepicker-week-col'>" + this._get(t, "calculateWeek")(A) + "</td>" : "", w = 0; 7 > w; w++) R = g ? g.apply(t.input ? t.input[0] : null, [A]) : [!0, ""], U = A.getMonth() !== X, B = U && !y || !R[0] || K && K > A || Q && A > Q, N += "<td class='" + ((w + c + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (U ? " ui-datepicker-other-month" : "") + (A.getTime() === C.getTime() && X === t.selectedMonth && t._keyEvent || _.getTime() === A.getTime() && _.getTime() === C.getTime() ? " " + this._dayOverClass : "") + (B ? " " + this._unselectableClass + " ui-state-disabled" : "") + (U && !v ? "" : " " + R[1] + (A.getTime() === J.getTime() ? " " + this._currentClass : "") + (A.getTime() === z.getTime() ? " ui-datepicker-today" : "")) + "'" + (U && !v || !R[2] ? "" : " title='" + R[2].replace(/'/g, "&#39;") + "'") + (B ? "" : " data-handler='selectDay' data-event='click' data-month='" + A.getMonth() + "' data-year='" + A.getFullYear() + "'") + ">" + (U && !v ? "&#xa0;" : B ? "<span class='ui-state-default'>" + A.getDate() + "</span>" : "<a class='ui-state-default" + (A.getTime() === z.getTime() ? " ui-state-highlight" : "") + (A.getTime() === J.getTime() ? " ui-state-active" : "") + (U ? " ui-priority-secondary" : "") + "' href='#'>" + A.getDate() + "</a>") + "</td>", A.setDate(A.getDate() + 1), A = this._daylightSavingAdjust(A);
                        S += N + "</tr>"
                    }
                    X++, X > 11 && (X = 0, te++), S += "</tbody></table>" + (Y ? "</div>" + (W[0] > 0 && $ === W[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : ""), x += S
                }
                b += x
            }
            return b += u, t._keyEvent = !1, b
        },
        _generateMonthYearHeader: function(t, e, n, i, r, o, s, a) {
            var l, u, c, h, d, p, f, m, g = this._get(t, "changeMonth"),
                v = this._get(t, "changeYear"),
                y = this._get(t, "showMonthAfterYear"),
                _ = "<div class='ui-datepicker-title'>",
                b = "";
            if (o || !g) b += "<span class='ui-datepicker-month'>" + s[e] + "</span>";
            else {
                for (l = i && i.getFullYear() === n, u = r && r.getFullYear() === n, b += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", c = 0; 12 > c; c++)(!l || c >= i.getMonth()) && (!u || c <= r.getMonth()) && (b += "<option value='" + c + "'" + (c === e ? " selected='selected'" : "") + ">" + a[c] + "</option>");
                b += "</select>"
            }
            if (y || (_ += b + (!o && g && v ? "" : "&#xa0;")), !t.yearshtml)
                if (t.yearshtml = "", o || !v) _ += "<span class='ui-datepicker-year'>" + n + "</span>";
                else {
                    for (h = this._get(t, "yearRange").split(":"), d = (new Date).getFullYear(), p = function(t) {
                            var e = t.match(/c[+\-].*/) ? n + parseInt(t.substring(1), 10) : t.match(/[+\-].*/) ? d + parseInt(t, 10) : parseInt(t, 10);
                            return isNaN(e) ? d : e
                        }, f = p(h[0]), m = Math.max(f, p(h[1] || "")), f = i ? Math.max(f, i.getFullYear()) : f, m = r ? Math.min(m, r.getFullYear()) : m, t.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; m >= f; f++) t.yearshtml += "<option value='" + f + "'" + (f === n ? " selected='selected'" : "") + ">" + f + "</option>";
                    t.yearshtml += "</select>", _ += t.yearshtml, t.yearshtml = null
                } return _ += this._get(t, "yearSuffix"), y && (_ += (!o && g && v ? "" : "&#xa0;") + b), _ += "</div>"
        },
        _adjustInstDate: function(t, e, n) {
            var i = t.drawYear + ("Y" === n ? e : 0),
                r = t.drawMonth + ("M" === n ? e : 0),
                o = Math.min(t.selectedDay, this._getDaysInMonth(i, r)) + ("D" === n ? e : 0),
                s = this._restrictMinMax(t, this._daylightSavingAdjust(new Date(i, r, o)));
            t.selectedDay = s.getDate(), t.drawMonth = t.selectedMonth = s.getMonth(), t.drawYear = t.selectedYear = s.getFullYear(), ("M" === n || "Y" === n) && this._notifyChange(t)
        },
        _restrictMinMax: function(t, e) {
            var n = this._getMinMaxDate(t, "min"),
                i = this._getMinMaxDate(t, "max"),
                r = n && n > e ? n : e;
            return i && r > i ? i : r
        },
        _notifyChange: function(t) {
            var e = this._get(t, "onChangeMonthYear");
            e && e.apply(t.input ? t.input[0] : null, [t.selectedYear, t.selectedMonth + 1, t])
        },
        _getNumberOfMonths: function(t) {
            var e = this._get(t, "numberOfMonths");
            return null == e ? [1, 1] : "number" == typeof e ? [1, e] : e
        },
        _getMinMaxDate: function(t, e) {
            return this._determineDate(t, this._get(t, e + "Date"), null)
        },
        _getDaysInMonth: function(t, e) {
            return 32 - this._daylightSavingAdjust(new Date(t, e, 32)).getDate()
        },
        _getFirstDayOfMonth: function(t, e) {
            return new Date(t, e, 1).getDay()
        },
        _canAdjustMonth: function(t, e, n, i) {
            var r = this._getNumberOfMonths(t),
                o = this._daylightSavingAdjust(new Date(n, i + (0 > e ? e : r[0] * r[1]), 1));
            return 0 > e && o.setDate(this._getDaysInMonth(o.getFullYear(), o.getMonth())), this._isInRange(t, o)
        },
        _isInRange: function(t, e) {
            var n, i, r = this._getMinMaxDate(t, "min"),
                o = this._getMinMaxDate(t, "max"),
                s = null,
                a = null,
                l = this._get(t, "yearRange");
            return l && (n = l.split(":"), i = (new Date).getFullYear(), s = parseInt(n[0], 10), a = parseInt(n[1], 10), n[0].match(/[+\-].*/) && (s += i), n[1].match(/[+\-].*/) && (a += i)), (!r || e.getTime() >= r.getTime()) && (!o || e.getTime() <= o.getTime()) && (!s || e.getFullYear() >= s) && (!a || e.getFullYear() <= a)
        },
        _getFormatConfig: function(t) {
            var e = this._get(t, "shortYearCutoff");
            return e = "string" != typeof e ? e : (new Date).getFullYear() % 100 + parseInt(e, 10), {
                shortYearCutoff: e,
                dayNamesShort: this._get(t, "dayNamesShort"),
                dayNames: this._get(t, "dayNames"),
                monthNamesShort: this._get(t, "monthNamesShort"),
                monthNames: this._get(t, "monthNames")
            }
        },
        _formatDate: function(t, e, n, i) {
            e || (t.currentDay = t.selectedDay, t.currentMonth = t.selectedMonth, t.currentYear = t.selectedYear);
            var r = e ? "object" == typeof e ? e : this._daylightSavingAdjust(new Date(i, n, e)) : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
            return this.formatDate(this._get(t, "dateFormat"), r, this._getFormatConfig(t))
        }
    }), t.fn.datepicker = function(e) {
        if (!this.length) return this;
        t.datepicker.initialized || (t(document).mousedown(t.datepicker._checkExternalClick), t.datepicker.initialized = !0), 0 === t("#" + t.datepicker._mainDivId).length && t("body").append(t.datepicker.dpDiv);
        var n = Array.prototype.slice.call(arguments, 1);
        return "string" != typeof e || "isDisabled" !== e && "getDate" !== e && "widget" !== e ? "option" === e && 2 === arguments.length && "string" == typeof arguments[1] ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(n)) : this.each(function() {
            "string" == typeof e ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this].concat(n)) : t.datepicker._attachDatepicker(this, e)
        }) : t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(n))
    }, t.datepicker = new n, t.datepicker.initialized = !1, t.datepicker.uuid = (new Date).getTime(), t.datepicker.version = "1.10.4"
}(jQuery),
function(t) {
    var e = {
            buttons: !0,
            height: !0,
            maxHeight: !0,
            maxWidth: !0,
            minHeight: !0,
            minWidth: !0,
            width: !0
        },
        n = {
            maxHeight: !0,
            maxWidth: !0,
            minHeight: !0,
            minWidth: !0
        };
    t.widget("ui.dialog", {
        version: "1.10.4",
        options: {
            appendTo: "body",
            autoOpen: !0,
            buttons: [],
            closeOnEscape: !0,
            closeText: "close",
            dialogClass: "",
            draggable: !0,
            hide: null,
            height: "auto",
            maxHeight: null,
            maxWidth: null,
            minHeight: 150,
            minWidth: 150,
            modal: !1,
            position: {
                my: "center",
                at: "center",
                of: window,
                collision: "fit",
                using: function(e) {
                    var n = t(this).css(e).offset().top;
                    0 > n && t(this).css("top", e.top - n)
                }
            },
            resizable: !0,
            show: null,
            title: null,
            width: 300,
            beforeClose: null,
            close: null,
            drag: null,
            dragStart: null,
            dragStop: null,
            focus: null,
            open: null,
            resize: null,
            resizeStart: null,
            resizeStop: null
        },
        _create: function() {
            this.originalCss = {
                display: this.element[0].style.display,
                width: this.element[0].style.width,
                minHeight: this.element[0].style.minHeight,
                maxHeight: this.element[0].style.maxHeight,
                height: this.element[0].style.height
            }, this.originalPosition = {
                parent: this.element.parent(),
                index: this.element.parent().children().index(this.element)
            }, this.originalTitle = this.element.attr("title"), this.options.title = this.options.title || this.originalTitle, this._createWrapper(), this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog), this._createTitlebar(), this._createButtonPane(), this.options.draggable && t.fn.draggable && this._makeDraggable(), this.options.resizable && t.fn.resizable && this._makeResizable(), this._isOpen = !1
        },
        _init: function() {
            this.options.autoOpen && this.open()
        },
        _appendTo: function() {
            var e = this.options.appendTo;
            return e && (e.jquery || e.nodeType) ? t(e) : this.document.find(e || "body").eq(0)
        },
        _destroy: function() {
            var t, e = this.originalPosition;
            this._destroyOverlay(), this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach(), this.uiDialog.stop(!0, !0).remove(), this.originalTitle && this.element.attr("title", this.originalTitle), t = e.parent.children().eq(e.index), t.length && t[0] !== this.element[0] ? t.before(this.element) : e.parent.append(this.element)
        },
        widget: function() {
            return this.uiDialog
        },
        disable: t.noop,
        enable: t.noop,
        close: function(e) {
            var n, i = this;
            if (this._isOpen && this._trigger("beforeClose", e) !== !1) {
                if (this._isOpen = !1, this._destroyOverlay(), !this.opener.filter(":focusable").focus().length) try {
                    n = this.document[0].activeElement, n && "body" !== n.nodeName.toLowerCase() && t(n).blur()
                } catch (r) {}
                this._hide(this.uiDialog, this.options.hide, function() {
                    i._trigger("close", e)
                })
            }
        },
        isOpen: function() {
            return this._isOpen
        },
        moveToTop: function() {
            this._moveToTop()
        },
        _moveToTop: function(t, e) {
            var n = !!this.uiDialog.nextAll(":visible").insertBefore(this.uiDialog).length;
            return n && !e && this._trigger("focus", t), n
        },
        open: function() {
            var e = this;
            return this._isOpen ? void(this._moveToTop() && this._focusTabbable()) : (this._isOpen = !0, this.opener = t(this.document[0].activeElement), this._size(), this._position(), this._createOverlay(), this._moveToTop(null, !0), this._show(this.uiDialog, this.options.show, function() {
                e._focusTabbable(), e._trigger("focus")
            }), void this._trigger("open"))
        },
        _focusTabbable: function() {
            var t = this.element.find("[autofocus]");
            t.length || (t = this.element.find(":tabbable")), t.length || (t = this.uiDialogButtonPane.find(":tabbable")), t.length || (t = this.uiDialogTitlebarClose.filter(":tabbable")), t.length || (t = this.uiDialog), t.eq(0).focus()
        },
        _keepFocus: function(e) {
            function n() {
                var e = this.document[0].activeElement,
                    n = this.uiDialog[0] === e || t.contains(this.uiDialog[0], e);
                n || this._focusTabbable()
            }
            e.preventDefault(), n.call(this), this._delay(n)
        },
        _createWrapper: function() {
            this.uiDialog = t("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front " + this.options.dialogClass).hide().attr({
                tabIndex: -1,
                role: "dialog"
            }).appendTo(this._appendTo()), this._on(this.uiDialog, {
                keydown: function(e) {
                    if (this.options.closeOnEscape && !e.isDefaultPrevented() && e.keyCode && e.keyCode === t.ui.keyCode.ESCAPE) return e.preventDefault(), void this.close(e);
                    if (e.keyCode === t.ui.keyCode.TAB) {
                        var n = this.uiDialog.find(":tabbable"),
                            i = n.filter(":first"),
                            r = n.filter(":last");
                        e.target !== r[0] && e.target !== this.uiDialog[0] || e.shiftKey ? e.target !== i[0] && e.target !== this.uiDialog[0] || !e.shiftKey || (r.focus(1), e.preventDefault()) : (i.focus(1), e.preventDefault())
                    }
                },
                mousedown: function(t) {
                    this._moveToTop(t) && this._focusTabbable()
                }
            }), this.element.find("[aria-describedby]").length || this.uiDialog.attr({
                "aria-describedby": this.element.uniqueId().attr("id")
            })
        },
        _createTitlebar: function() {
            var e;
            this.uiDialogTitlebar = t("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog), this._on(this.uiDialogTitlebar, {
                mousedown: function(e) {
                    t(e.target).closest(".ui-dialog-titlebar-close") || this.uiDialog.focus()
                }
            }), this.uiDialogTitlebarClose = t("<button type='button'></button>").button({
                label: this.options.closeText,
                icons: {
                    primary: "ui-icon-closethick"
                },
                text: !1
            }).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar), this._on(this.uiDialogTitlebarClose, {
                click: function(t) {
                    t.preventDefault(), this.close(t)
                }
            }), e = t("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar), this._title(e), this.uiDialog.attr({
                "aria-labelledby": e.attr("id")
            })
        },
        _title: function(t) {
            this.options.title || t.html("&#160;"), t.text(this.options.title)
        },
        _createButtonPane: function() {
            this.uiDialogButtonPane = t("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"), this.uiButtonSet = t("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane), this._createButtons()
        },
        _createButtons: function() {
            var e = this,
                n = this.options.buttons;
            return this.uiDialogButtonPane.remove(), this.uiButtonSet.empty(), t.isEmptyObject(n) || t.isArray(n) && !n.length ? void this.uiDialog.removeClass("ui-dialog-buttons") : (t.each(n, function(n, i) {
                var r, o;
                i = t.isFunction(i) ? {
                    click: i,
                    text: n
                } : i, i = t.extend({
                    type: "button"
                }, i), r = i.click, i.click = function() {
                    r.apply(e.element[0], arguments)
                }, o = {
                    icons: i.icons,
                    text: i.showText
                }, delete i.icons, delete i.showText, t("<button></button>", i).button(o).appendTo(e.uiButtonSet)
            }), this.uiDialog.addClass("ui-dialog-buttons"), void this.uiDialogButtonPane.appendTo(this.uiDialog))
        },
        _makeDraggable: function() {
            function e(t) {
                return {
                    position: t.position,
                    offset: t.offset
                }
            }
            var n = this,
                i = this.options;
            this.uiDialog.draggable({
                cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
                handle: ".ui-dialog-titlebar",
                containment: "document",
                start: function(i, r) {
                    t(this).addClass("ui-dialog-dragging"), n._blockFrames(), n._trigger("dragStart", i, e(r))
                },
                drag: function(t, i) {
                    n._trigger("drag", t, e(i))
                },
                stop: function(r, o) {
                    i.position = [o.position.left - n.document.scrollLeft(), o.position.top - n.document.scrollTop()], t(this).removeClass("ui-dialog-dragging"), n._unblockFrames(), n._trigger("dragStop", r, e(o))
                }
            })
        },
        _makeResizable: function() {
            function e(t) {
                return {
                    originalPosition: t.originalPosition,
                    originalSize: t.originalSize,
                    position: t.position,
                    size: t.size
                }
            }
            var n = this,
                i = this.options,
                r = i.resizable,
                o = this.uiDialog.css("position"),
                s = "string" == typeof r ? r : "n,e,s,w,se,sw,ne,nw";
            this.uiDialog.resizable({
                cancel: ".ui-dialog-content",
                containment: "document",
                alsoResize: this.element,
                maxWidth: i.maxWidth,
                maxHeight: i.maxHeight,
                minWidth: i.minWidth,
                minHeight: this._minHeight(),
                handles: s,
                start: function(i, r) {
                    t(this).addClass("ui-dialog-resizing"), n._blockFrames(), n._trigger("resizeStart", i, e(r))
                },
                resize: function(t, i) {
                    n._trigger("resize", t, e(i))
                },
                stop: function(r, o) {
                    i.height = t(this).height(), i.width = t(this).width(), t(this).removeClass("ui-dialog-resizing"), n._unblockFrames(), n._trigger("resizeStop", r, e(o))
                }
            }).css("position", o)
        },
        _minHeight: function() {
            var t = this.options;
            return "auto" === t.height ? t.minHeight : Math.min(t.minHeight, t.height)
        },
        _position: function() {
            var t = this.uiDialog.is(":visible");
            t || this.uiDialog.show(), this.uiDialog.position(this.options.position), t || this.uiDialog.hide()
        },
        _setOptions: function(i) {
            var r = this,
                o = !1,
                s = {};
            t.each(i, function(t, i) {
                r._setOption(t, i), t in e && (o = !0), t in n && (s[t] = i)
            }), o && (this._size(), this._position()), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", s)
        },
        _setOption: function(t, e) {
            var n, i, r = this.uiDialog;
            "dialogClass" === t && r.removeClass(this.options.dialogClass).addClass(e), "disabled" !== t && (this._super(t, e), "appendTo" === t && this.uiDialog.appendTo(this._appendTo()), "buttons" === t && this._createButtons(), "closeText" === t && this.uiDialogTitlebarClose.button({
                label: "" + e
            }), "draggable" === t && (n = r.is(":data(ui-draggable)"), n && !e && r.draggable("destroy"), !n && e && this._makeDraggable()), "position" === t && this._position(), "resizable" === t && (i = r.is(":data(ui-resizable)"), i && !e && r.resizable("destroy"), i && "string" == typeof e && r.resizable("option", "handles", e), i || e === !1 || this._makeResizable()), "title" === t && this._title(this.uiDialogTitlebar.find(".ui-dialog-title")))
        },
        _size: function() {
            var t, e, n, i = this.options;
            this.element.show().css({
                width: "auto",
                minHeight: 0,
                maxHeight: "none",
                height: 0
            }), i.minWidth > i.width && (i.width = i.minWidth), t = this.uiDialog.css({
                height: "auto",
                width: i.width
            }).outerHeight(), e = Math.max(0, i.minHeight - t), n = "number" == typeof i.maxHeight ? Math.max(0, i.maxHeight - t) : "none", "auto" === i.height ? this.element.css({
                minHeight: e,
                maxHeight: n,
                height: "auto"
            }) : this.element.height(Math.max(0, i.height - t)), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight())
        },
        _blockFrames: function() {
            this.iframeBlocks = this.document.find("iframe").map(function() {
                var e = t(this);
                return t("<div>").css({
                    position: "absolute",
                    width: e.outerWidth(),
                    height: e.outerHeight()
                }).appendTo(e.parent()).offset(e.offset())[0]
            })
        },
        _unblockFrames: function() {
            this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks)
        },
        _allowInteraction: function(e) {
            return t(e.target).closest(".ui-dialog").length ? !0 : !!t(e.target).closest(".ui-datepicker").length
        },
        _createOverlay: function() {
            if (this.options.modal) {
                var e = this,
                    n = this.widgetFullName;
                t.ui.dialog.overlayInstances || this._delay(function() {
                    t.ui.dialog.overlayInstances && this.document.bind("focusin.dialog", function(i) {
                        e._allowInteraction(i) || (i.preventDefault(), t(".ui-dialog:visible:last .ui-dialog-content").data(n)._focusTabbable())
                    })
                }), this.overlay = t("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo()), this._on(this.overlay, {
                    mousedown: "_keepFocus"
                }), t.ui.dialog.overlayInstances++
            }
        },
        _destroyOverlay: function() {
            this.options.modal && this.overlay && (t.ui.dialog.overlayInstances--, t.ui.dialog.overlayInstances || this.document.unbind("focusin.dialog"), this.overlay.remove(), this.overlay = null)
        }
    }), t.ui.dialog.overlayInstances = 0, t.uiBackCompat !== !1 && t.widget("ui.dialog", t.ui.dialog, {
        _position: function() {
            var e, n = this.options.position,
                i = [],
                r = [0, 0];
            n ? (("string" == typeof n || "object" == typeof n && "0" in n) && (i = n.split ? n.split(" ") : [n[0], n[1]], 1 === i.length && (i[1] = i[0]), t.each(["left", "top"], function(t, e) {
                +i[t] === i[t] && (r[t] = i[t], i[t] = e)
            }), n = {
                my: i[0] + (r[0] < 0 ? r[0] : "+" + r[0]) + " " + i[1] + (r[1] < 0 ? r[1] : "+" + r[1]),
                at: i.join(" ")
            }), n = t.extend({}, t.ui.dialog.prototype.options.position, n)) : n = t.ui.dialog.prototype.options.position, e = this.uiDialog.is(":visible"), e || this.uiDialog.show(), this.uiDialog.position(n), e || this.uiDialog.hide()
        }
    })
}(jQuery),
function(t) {
    var e = /up|down|vertical/,
        n = /up|left|vertical|horizontal/;
    t.effects.effect.blind = function(i, r) {
        var o, s, a, l = t(this),
            u = ["position", "top", "bottom", "left", "right", "height", "width"],
            c = t.effects.setMode(l, i.mode || "hide"),
            h = i.direction || "up",
            d = e.test(h),
            p = d ? "height" : "width",
            f = d ? "top" : "left",
            m = n.test(h),
            g = {},
            v = "show" === c;
        l.parent().is(".ui-effects-wrapper") ? t.effects.save(l.parent(), u) : t.effects.save(l, u), l.show(), o = t.effects.createWrapper(l).css({
            overflow: "hidden"
        }), s = o[p](), a = parseFloat(o.css(f)) || 0, g[p] = v ? s : 0, m || (l.css(d ? "bottom" : "right", 0).css(d ? "top" : "left", "auto").css({
            position: "absolute"
        }), g[f] = v ? a : s + a), v && (o.css(p, 0), m || o.css(f, a + s)), o.animate(g, {
            duration: i.duration,
            easing: i.easing,
            queue: !1,
            complete: function() {
                "hide" === c && l.hide(), t.effects.restore(l, u), t.effects.removeWrapper(l), r()
            }
        })
    }
}(jQuery),
function(t) {
    t.effects.effect.bounce = function(e, n) {
        var i, r, o, s = t(this),
            a = ["position", "top", "bottom", "left", "right", "height", "width"],
            l = t.effects.setMode(s, e.mode || "effect"),
            u = "hide" === l,
            c = "show" === l,
            h = e.direction || "up",
            d = e.distance,
            p = e.times || 5,
            f = 2 * p + (c || u ? 1 : 0),
            m = e.duration / f,
            g = e.easing,
            v = "up" === h || "down" === h ? "top" : "left",
            y = "up" === h || "left" === h,
            _ = s.queue(),
            b = _.length;
        for ((c || u) && a.push("opacity"), t.effects.save(s, a), s.show(), t.effects.createWrapper(s), d || (d = s["top" === v ? "outerHeight" : "outerWidth"]() / 3), c && (o = {
                opacity: 1
            }, o[v] = 0, s.css("opacity", 0).css(v, y ? 2 * -d : 2 * d).animate(o, m, g)), u && (d /= Math.pow(2, p - 1)), o = {}, o[v] = 0, i = 0; p > i; i++) r = {}, r[v] = (y ? "-=" : "+=") + d, s.animate(r, m, g).animate(o, m, g), d = u ? 2 * d : d / 2;
        u && (r = {
            opacity: 0
        }, r[v] = (y ? "-=" : "+=") + d, s.animate(r, m, g)), s.queue(function() {
            u && s.hide(), t.effects.restore(s, a), t.effects.removeWrapper(s), n()
        }), b > 1 && _.splice.apply(_, [1, 0].concat(_.splice(b, f + 1))), s.dequeue()
    }
}(jQuery),
function(t) {
    t.effects.effect.clip = function(e, n) {
        var i, r, o, s = t(this),
            a = ["position", "top", "bottom", "left", "right", "height", "width"],
            l = t.effects.setMode(s, e.mode || "hide"),
            u = "show" === l,
            c = e.direction || "vertical",
            h = "vertical" === c,
            d = h ? "height" : "width",
            p = h ? "top" : "left",
            f = {};
        t.effects.save(s, a), s.show(), i = t.effects.createWrapper(s).css({
            overflow: "hidden"
        }), r = "IMG" === s[0].tagName ? i : s, o = r[d](), u && (r.css(d, 0), r.css(p, o / 2)), f[d] = u ? o : 0, f[p] = u ? 0 : o / 2, r.animate(f, {
            queue: !1,
            duration: e.duration,
            easing: e.easing,
            complete: function() {
                u || s.hide(), t.effects.restore(s, a), t.effects.removeWrapper(s), n()
            }
        })
    }
}(jQuery),
function(t) {
    t.effects.effect.drop = function(e, n) {
        var i, r = t(this),
            o = ["position", "top", "bottom", "left", "right", "opacity", "height", "width"],
            s = t.effects.setMode(r, e.mode || "hide"),
            a = "show" === s,
            l = e.direction || "left",
            u = "up" === l || "down" === l ? "top" : "left",
            c = "up" === l || "left" === l ? "pos" : "neg",
            h = {
                opacity: a ? 1 : 0
            };
        t.effects.save(r, o), r.show(), t.effects.createWrapper(r), i = e.distance || r["top" === u ? "outerHeight" : "outerWidth"](!0) / 2, a && r.css("opacity", 0).css(u, "pos" === c ? -i : i), h[u] = (a ? "pos" === c ? "+=" : "-=" : "pos" === c ? "-=" : "+=") + i, r.animate(h, {
            queue: !1,
            duration: e.duration,
            easing: e.easing,
            complete: function() {
                "hide" === s && r.hide(), t.effects.restore(r, o), t.effects.removeWrapper(r), n()
            }
        })
    }
}(jQuery),
function(t) {
    t.effects.effect.explode = function(e, n) {
        function i() {
            _.push(this), _.length === h * d && r()
        }

        function r() {
            p.css({
                visibility: "visible"
            }), t(_).remove(), m || p.hide(), n()
        }
        var o, s, a, l, u, c, h = e.pieces ? Math.round(Math.sqrt(e.pieces)) : 3,
            d = h,
            p = t(this),
            f = t.effects.setMode(p, e.mode || "hide"),
            m = "show" === f,
            g = p.show().css("visibility", "hidden").offset(),
            v = Math.ceil(p.outerWidth() / d),
            y = Math.ceil(p.outerHeight() / h),
            _ = [];
        for (o = 0; h > o; o++)
            for (l = g.top + o * y, c = o - (h - 1) / 2, s = 0; d > s; s++) a = g.left + s * v, u = s - (d - 1) / 2, p.clone().appendTo("body").wrap("<div></div>").css({
                position: "absolute",
                visibility: "visible",
                left: -s * v,
                top: -o * y
            }).parent().addClass("ui-effects-explode").css({
                position: "absolute",
                overflow: "hidden",
                width: v,
                height: y,
                left: a + (m ? u * v : 0),
                top: l + (m ? c * y : 0),
                opacity: m ? 0 : 1
            }).animate({
                left: a + (m ? 0 : u * v),
                top: l + (m ? 0 : c * y),
                opacity: m ? 1 : 0
            }, e.duration || 500, e.easing, i)
    }
}(jQuery),
function(t) {
    t.effects.effect.fade = function(e, n) {
        var i = t(this),
            r = t.effects.setMode(i, e.mode || "toggle");
        i.animate({
            opacity: r
        }, {
            queue: !1,
            duration: e.duration,
            easing: e.easing,
            complete: n
        })
    }
}(jQuery),
function(t) {
    t.effects.effect.fold = function(e, n) {
        var i, r, o = t(this),
            s = ["position", "top", "bottom", "left", "right", "height", "width"],
            a = t.effects.setMode(o, e.mode || "hide"),
            l = "show" === a,
            u = "hide" === a,
            c = e.size || 15,
            h = /([0-9]+)%/.exec(c),
            d = !!e.horizFirst,
            p = l !== d,
            f = p ? ["width", "height"] : ["height", "width"],
            m = e.duration / 2,
            g = {},
            v = {};
        t.effects.save(o, s), o.show(), i = t.effects.createWrapper(o).css({
            overflow: "hidden"
        }), r = p ? [i.width(), i.height()] : [i.height(), i.width()], h && (c = parseInt(h[1], 10) / 100 * r[u ? 0 : 1]), l && i.css(d ? {
            height: 0,
            width: c
        } : {
            height: c,
            width: 0
        }), g[f[0]] = l ? r[0] : c, v[f[1]] = l ? r[1] : 0, i.animate(g, m, e.easing).animate(v, m, e.easing, function() {
            u && o.hide(), t.effects.restore(o, s), t.effects.removeWrapper(o), n()
        })
    }
}(jQuery),
function(t) {
    t.effects.effect.highlight = function(e, n) {
        var i = t(this),
            r = ["backgroundImage", "backgroundColor", "opacity"],
            o = t.effects.setMode(i, e.mode || "show"),
            s = {
                backgroundColor: i.css("backgroundColor")
            };
        "hide" === o && (s.opacity = 0), t.effects.save(i, r), i.show().css({
            backgroundImage: "none",
            backgroundColor: e.color || "#ffff99"
        }).animate(s, {
            queue: !1,
            duration: e.duration,
            easing: e.easing,
            complete: function() {
                "hide" === o && i.hide(), t.effects.restore(i, r), n()
            }
        })
    }
}(jQuery),
function(t) {
    t.effects.effect.pulsate = function(e, n) {
        var i, r = t(this),
            o = t.effects.setMode(r, e.mode || "show"),
            s = "show" === o,
            a = "hide" === o,
            l = s || "hide" === o,
            u = 2 * (e.times || 5) + (l ? 1 : 0),
            c = e.duration / u,
            h = 0,
            d = r.queue(),
            p = d.length;
        for ((s || !r.is(":visible")) && (r.css("opacity", 0).show(), h = 1), i = 1; u > i; i++) r.animate({
            opacity: h
        }, c, e.easing), h = 1 - h;
        r.animate({
            opacity: h
        }, c, e.easing), r.queue(function() {
            a && r.hide(), n()
        }), p > 1 && d.splice.apply(d, [1, 0].concat(d.splice(p, u + 1))), r.dequeue()
    }
}(jQuery),
function(t) {
    t.effects.effect.puff = function(e, n) {
        var i = t(this),
            r = t.effects.setMode(i, e.mode || "hide"),
            o = "hide" === r,
            s = parseInt(e.percent, 10) || 150,
            a = s / 100,
            l = {
                height: i.height(),
                width: i.width(),
                outerHeight: i.outerHeight(),
                outerWidth: i.outerWidth()
            };
        t.extend(e, {
            effect: "scale",
            queue: !1,
            fade: !0,
            mode: r,
            complete: n,
            percent: o ? s : 100,
            from: o ? l : {
                height: l.height * a,
                width: l.width * a,
                outerHeight: l.outerHeight * a,
                outerWidth: l.outerWidth * a
            }
        }), i.effect(e)
    }, t.effects.effect.scale = function(e, n) {
        var i = t(this),
            r = t.extend(!0, {}, e),
            o = t.effects.setMode(i, e.mode || "effect"),
            s = parseInt(e.percent, 10) || (0 === parseInt(e.percent, 10) ? 0 : "hide" === o ? 0 : 100),
            a = e.direction || "both",
            l = e.origin,
            u = {
                height: i.height(),
                width: i.width(),
                outerHeight: i.outerHeight(),
                outerWidth: i.outerWidth()
            },
            c = {
                y: "horizontal" !== a ? s / 100 : 1,
                x: "vertical" !== a ? s / 100 : 1
            };
        r.effect = "size", r.queue = !1, r.complete = n, "effect" !== o && (r.origin = l || ["middle", "center"], r.restore = !0), r.from = e.from || ("show" === o ? {
            height: 0,
            width: 0,
            outerHeight: 0,
            outerWidth: 0
        } : u), r.to = {
            height: u.height * c.y,
            width: u.width * c.x,
            outerHeight: u.outerHeight * c.y,
            outerWidth: u.outerWidth * c.x
        }, r.fade && ("show" === o && (r.from.opacity = 0, r.to.opacity = 1), "hide" === o && (r.from.opacity = 1, r.to.opacity = 0)), i.effect(r)
    }, t.effects.effect.size = function(e, n) {
        var i, r, o, s = t(this),
            a = ["position", "top", "bottom", "left", "right", "width", "height", "overflow", "opacity"],
            l = ["position", "top", "bottom", "left", "right", "overflow", "opacity"],
            u = ["width", "height", "overflow"],
            c = ["fontSize"],
            h = ["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"],
            d = ["borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight"],
            p = t.effects.setMode(s, e.mode || "effect"),
            f = e.restore || "effect" !== p,
            m = e.scale || "both",
            g = e.origin || ["middle", "center"],
            v = s.css("position"),
            y = f ? a : l,
            _ = {
                height: 0,
                width: 0,
                outerHeight: 0,
                outerWidth: 0
            };
        "show" === p && s.show(), i = {
            height: s.height(),
            width: s.width(),
            outerHeight: s.outerHeight(),
            outerWidth: s.outerWidth()
        }, "toggle" === e.mode && "show" === p ? (s.from = e.to || _, s.to = e.from || i) : (s.from = e.from || ("show" === p ? _ : i), s.to = e.to || ("hide" === p ? _ : i)), o = {
            from: {
                y: s.from.height / i.height,
                x: s.from.width / i.width
            },
            to: {
                y: s.to.height / i.height,
                x: s.to.width / i.width
            }
        }, ("box" === m || "both" === m) && (o.from.y !== o.to.y && (y = y.concat(h), s.from = t.effects.setTransition(s, h, o.from.y, s.from), s.to = t.effects.setTransition(s, h, o.to.y, s.to)), o.from.x !== o.to.x && (y = y.concat(d), s.from = t.effects.setTransition(s, d, o.from.x, s.from), s.to = t.effects.setTransition(s, d, o.to.x, s.to))), ("content" === m || "both" === m) && o.from.y !== o.to.y && (y = y.concat(c).concat(u), s.from = t.effects.setTransition(s, c, o.from.y, s.from), s.to = t.effects.setTransition(s, c, o.to.y, s.to)), t.effects.save(s, y), s.show(), t.effects.createWrapper(s), s.css("overflow", "hidden").css(s.from), g && (r = t.effects.getBaseline(g, i), s.from.top = (i.outerHeight - s.outerHeight()) * r.y, s.from.left = (i.outerWidth - s.outerWidth()) * r.x, s.to.top = (i.outerHeight - s.to.outerHeight) * r.y, s.to.left = (i.outerWidth - s.to.outerWidth) * r.x), s.css(s.from), ("content" === m || "both" === m) && (h = h.concat(["marginTop", "marginBottom"]).concat(c), d = d.concat(["marginLeft", "marginRight"]), u = a.concat(h).concat(d), s.find("*[width]").each(function() {
            var n = t(this),
                i = {
                    height: n.height(),
                    width: n.width(),
                    outerHeight: n.outerHeight(),
                    outerWidth: n.outerWidth()
                };
            f && t.effects.save(n, u), n.from = {
                height: i.height * o.from.y,
                width: i.width * o.from.x,
                outerHeight: i.outerHeight * o.from.y,
                outerWidth: i.outerWidth * o.from.x
            }, n.to = {
                height: i.height * o.to.y,
                width: i.width * o.to.x,
                outerHeight: i.height * o.to.y,
                outerWidth: i.width * o.to.x
            }, o.from.y !== o.to.y && (n.from = t.effects.setTransition(n, h, o.from.y, n.from), n.to = t.effects.setTransition(n, h, o.to.y, n.to)), o.from.x !== o.to.x && (n.from = t.effects.setTransition(n, d, o.from.x, n.from), n.to = t.effects.setTransition(n, d, o.to.x, n.to)), n.css(n.from), n.animate(n.to, e.duration, e.easing, function() {
                f && t.effects.restore(n, u)
            })
        })), s.animate(s.to, {
            queue: !1,
            duration: e.duration,
            easing: e.easing,
            complete: function() {
                0 === s.to.opacity && s.css("opacity", s.from.opacity), "hide" === p && s.hide(), t.effects.restore(s, y), f || ("static" === v ? s.css({
                    position: "relative",
                    top: s.to.top,
                    left: s.to.left
                }) : t.each(["top", "left"], function(t, e) {
                    s.css(e, function(e, n) {
                        var i = parseInt(n, 10),
                            r = t ? s.to.left : s.to.top;
                        return "auto" === n ? r + "px" : i + r + "px"
                    })
                })), t.effects.removeWrapper(s), n()
            }
        })
    }
}(jQuery),
function(t) {
    t.effects.effect.shake = function(e, n) {
        var i, r = t(this),
            o = ["position", "top", "bottom", "left", "right", "height", "width"],
            s = t.effects.setMode(r, e.mode || "effect"),
            a = e.direction || "left",
            l = e.distance || 20,
            u = e.times || 3,
            c = 2 * u + 1,
            h = Math.round(e.duration / c),
            d = "up" === a || "down" === a ? "top" : "left",
            p = "up" === a || "left" === a,
            f = {},
            m = {},
            g = {},
            v = r.queue(),
            y = v.length;
        for (t.effects.save(r, o), r.show(), t.effects.createWrapper(r), f[d] = (p ? "-=" : "+=") + l, m[d] = (p ? "+=" : "-=") + 2 * l, g[d] = (p ? "-=" : "+=") + 2 * l, r.animate(f, h, e.easing), i = 1; u > i; i++) r.animate(m, h, e.easing).animate(g, h, e.easing);
        r.animate(m, h, e.easing).animate(f, h / 2, e.easing).queue(function() {
            "hide" === s && r.hide(), t.effects.restore(r, o), t.effects.removeWrapper(r), n()
        }), y > 1 && v.splice.apply(v, [1, 0].concat(v.splice(y, c + 1))), r.dequeue()
    }
}(jQuery),
function(t) {
    t.effects.effect.slide = function(e, n) {
        var i, r = t(this),
            o = ["position", "top", "bottom", "left", "right", "width", "height"],
            s = t.effects.setMode(r, e.mode || "show"),
            a = "show" === s,
            l = e.direction || "left",
            u = "up" === l || "down" === l ? "top" : "left",
            c = "up" === l || "left" === l,
            h = {};
        t.effects.save(r, o), r.show(), i = e.distance || r["top" === u ? "outerHeight" : "outerWidth"](!0), t.effects.createWrapper(r).css({
            overflow: "hidden"
        }), a && r.css(u, c ? isNaN(i) ? "-" + i : -i : i), h[u] = (a ? c ? "+=" : "-=" : c ? "-=" : "+=") + i, r.animate(h, {
            queue: !1,
            duration: e.duration,
            easing: e.easing,
            complete: function() {
                "hide" === s && r.hide(), t.effects.restore(r, o), t.effects.removeWrapper(r), n()
            }
        })
    }
}(jQuery),
function(t) {
    t.effects.effect.transfer = function(e, n) {
        var i = t(this),
            r = t(e.to),
            o = "fixed" === r.css("position"),
            s = t("body"),
            a = o ? s.scrollTop() : 0,
            l = o ? s.scrollLeft() : 0,
            u = r.offset(),
            c = {
                top: u.top - a,
                left: u.left - l,
                height: r.innerHeight(),
                width: r.innerWidth()
            },
            h = i.offset(),
            d = t("<div class='ui-effects-transfer'></div>").appendTo(document.body).addClass(e.className).css({
                top: h.top - a,
                left: h.left - l,
                height: i.innerHeight(),
                width: i.innerWidth(),
                position: o ? "fixed" : "absolute"
            }).animate(c, e.duration, e.easing, function() {
                d.remove(), n()
            })
    }
}(jQuery),
function(t) {
    t.widget("ui.menu", {
        version: "1.10.4",
        defaultElement: "<ul>",
        delay: 300,
        options: {
            icons: {
                submenu: "ui-icon-carat-1-e"
            },
            menus: "ul",
            position: {
                my: "left top",
                at: "right top"
            },
            role: "menu",
            blur: null,
            focus: null,
            select: null
        },
        _create: function() {
            this.activeMenu = this.element, this.mouseHandled = !1, this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({
                role: this.options.role,
                tabIndex: 0
            }).bind("click" + this.eventNamespace, t.proxy(function(t) {
                this.options.disabled && t.preventDefault()
            }, this)), this.options.disabled && this.element.addClass("ui-state-disabled").attr("aria-disabled", "true"), this._on({
                "mousedown .ui-menu-item > a": function(t) {
                    t.preventDefault()
                },
                "click .ui-state-disabled > a": function(t) {
                    t.preventDefault()
                },
                "click .ui-menu-item:has(a)": function(e) {
                    var n = t(e.target).closest(".ui-menu-item");
                    !this.mouseHandled && n.not(".ui-state-disabled").length && (this.select(e), e.isPropagationStopped() || (this.mouseHandled = !0), n.has(".ui-menu").length ? this.expand(e) : !this.element.is(":focus") && t(this.document[0].activeElement).closest(".ui-menu").length && (this.element.trigger("focus", [!0]), this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)))
                },
                "mouseenter .ui-menu-item": function(e) {
                    var n = t(e.currentTarget);
                    n.siblings().children(".ui-state-active").removeClass("ui-state-active"), this.focus(e, n)
                },
                mouseleave: "collapseAll",
                "mouseleave .ui-menu": "collapseAll",
                focus: function(t, e) {
                    var n = this.active || this.element.children(".ui-menu-item").eq(0);
                    e || this.focus(t, n)
                },
                blur: function(e) {
                    this._delay(function() {
                        t.contains(this.element[0], this.document[0].activeElement) || this.collapseAll(e)
                    })
                },
                keydown: "_keydown"
            }), this.refresh(), this._on(this.document, {
                click: function(e) {
                    t(e.target).closest(".ui-menu").length || this.collapseAll(e), this.mouseHandled = !1
                }
            })
        },
        _destroy: function() {
            this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(), this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function() {
                var e = t(this);
                e.data("ui-menu-submenu-carat") && e.remove()
            }), this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")
        },
        _keydown: function(e) {
            function n(t) {
                return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
            }
            var i, r, o, s, a, l = !0;
            switch (e.keyCode) {
                case t.ui.keyCode.PAGE_UP:
                    this.previousPage(e);
                    break;
                case t.ui.keyCode.PAGE_DOWN:
                    this.nextPage(e);
                    break;
                case t.ui.keyCode.HOME:
                    this._move("first", "first", e);
                    break;
                case t.ui.keyCode.END:
                    this._move("last", "last", e);
                    break;
                case t.ui.keyCode.UP:
                    this.previous(e);
                    break;
                case t.ui.keyCode.DOWN:
                    this.next(e);
                    break;
                case t.ui.keyCode.LEFT:
                    this.collapse(e);
                    break;
                case t.ui.keyCode.RIGHT:
                    this.active && !this.active.is(".ui-state-disabled") && this.expand(e);
                    break;
                case t.ui.keyCode.ENTER:
                case t.ui.keyCode.SPACE:
                    this._activate(e);
                    break;
                case t.ui.keyCode.ESCAPE:
                    this.collapse(e);
                    break;
                default:
                    l = !1, r = this.previousFilter || "", o = String.fromCharCode(e.keyCode), s = !1, clearTimeout(this.filterTimer), o === r ? s = !0 : o = r + o, a = new RegExp("^" + n(o), "i"), i = this.activeMenu.children(".ui-menu-item").filter(function() {
                        return a.test(t(this).children("a").text())
                    }), i = s && -1 !== i.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : i, i.length || (o = String.fromCharCode(e.keyCode), a = new RegExp("^" + n(o), "i"), i = this.activeMenu.children(".ui-menu-item").filter(function() {
                        return a.test(t(this).children("a").text())
                    })), i.length ? (this.focus(e, i), i.length > 1 ? (this.previousFilter = o, this.filterTimer = this._delay(function() {
                        delete this.previousFilter
                    }, 1e3)) : delete this.previousFilter) : delete this.previousFilter
            }
            l && e.preventDefault()
        },
        _activate: function(t) {
            this.active.is(".ui-state-disabled") || (this.active.children("a[aria-haspopup='true']").length ? this.expand(t) : this.select(t))
        },
        refresh: function() {
            var e, n = this.options.icons.submenu,
                i = this.element.find(this.options.menus);
            this.element.toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length), i.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({
                role: this.options.role,
                "aria-hidden": "true",
                "aria-expanded": "false"
            }).each(function() {
                var e = t(this),
                    i = e.prev("a"),
                    r = t("<span>").addClass("ui-menu-icon ui-icon " + n).data("ui-menu-submenu-carat", !0);
                i.attr("aria-haspopup", "true").prepend(r), e.attr("aria-labelledby", i.attr("id"))
            }), e = i.add(this.element), e.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role", "presentation").children("a").uniqueId().addClass("ui-corner-all").attr({
                tabIndex: -1,
                role: this._itemRole()
            }), e.children(":not(.ui-menu-item)").each(function() {
                var e = t(this);
                /[^\-\u2014\u2013\s]/.test(e.text()) || e.addClass("ui-widget-content ui-menu-divider")
            }), e.children(".ui-state-disabled").attr("aria-disabled", "true"), this.active && !t.contains(this.element[0], this.active[0]) && this.blur()
        },
        _itemRole: function() {
            return {
                menu: "menuitem",
                listbox: "option"
            } [this.options.role]
        },
        _setOption: function(t, e) {
            "icons" === t && this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(e.submenu), this._super(t, e)
        },
        focus: function(t, e) {
            var n, i;
            this.blur(t, t && "focus" === t.type), this._scrollIntoView(e), this.active = e.first(), i = this.active.children("a").addClass("ui-state-focus"), this.options.role && this.element.attr("aria-activedescendant", i.attr("id")), this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active"), t && "keydown" === t.type ? this._close() : this.timer = this._delay(function() {
                this._close()
            }, this.delay), n = e.children(".ui-menu"), n.length && t && /^mouse/.test(t.type) && this._startOpening(n), this.activeMenu = e.parent(), this._trigger("focus", t, {
                item: e
            })
        },
        _scrollIntoView: function(e) {
            var n, i, r, o, s, a;
            this._hasScroll() && (n = parseFloat(t.css(this.activeMenu[0], "borderTopWidth")) || 0, i = parseFloat(t.css(this.activeMenu[0], "paddingTop")) || 0, r = e.offset().top - this.activeMenu.offset().top - n - i, o = this.activeMenu.scrollTop(), s = this.activeMenu.height(), a = e.height(), 0 > r ? this.activeMenu.scrollTop(o + r) : r + a > s && this.activeMenu.scrollTop(o + r - s + a))
        },
        blur: function(t, e) {
            e || clearTimeout(this.timer), this.active && (this.active.children("a").removeClass("ui-state-focus"), this.active = null, this._trigger("blur", t, {
                item: this.active
            }))
        },
        _startOpening: function(t) {
            clearTimeout(this.timer), "true" === t.attr("aria-hidden") && (this.timer = this._delay(function() {
                this._close(), this._open(t)
            }, this.delay))
        },
        _open: function(e) {
            var n = t.extend({
                of: this.active
            }, this.options.position);
            clearTimeout(this.timer), this.element.find(".ui-menu").not(e.parents(".ui-menu")).hide().attr("aria-hidden", "true"), e.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(n)
        },
        collapseAll: function(e, n) {
            clearTimeout(this.timer), this.timer = this._delay(function() {
                var i = n ? this.element : t(e && e.target).closest(this.element.find(".ui-menu"));
                i.length || (i = this.element), this._close(i), this.blur(e), this.activeMenu = i
            }, this.delay)
        },
        _close: function(t) {
            t || (t = this.active ? this.active.parent() : this.element), t.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find("a.ui-state-active").removeClass("ui-state-active")
        },
        collapse: function(t) {
            var e = this.active && this.active.parent().closest(".ui-menu-item", this.element);
            e && e.length && (this._close(), this.focus(t, e))
        },
        expand: function(t) {
            var e = this.active && this.active.children(".ui-menu ").children(".ui-menu-item").first();
            e && e.length && (this._open(e.parent()), this._delay(function() {
                this.focus(t, e)
            }))
        },
        next: function(t) {
            this._move("next", "first", t)
        },
        previous: function(t) {
            this._move("prev", "last", t)
        },
        isFirstItem: function() {
            return this.active && !this.active.prevAll(".ui-menu-item").length
        },
        isLastItem: function() {
            return this.active && !this.active.nextAll(".ui-menu-item").length
        },
        _move: function(t, e, n) {
            var i;
            this.active && (i = "first" === t || "last" === t ? this.active["first" === t ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[t + "All"](".ui-menu-item").eq(0)), i && i.length && this.active || (i = this.activeMenu.children(".ui-menu-item")[e]()), this.focus(n, i)
        },
        nextPage: function(e) {
            var n, i, r;
            return this.active ? void(this.isLastItem() || (this._hasScroll() ? (i = this.active.offset().top, r = this.element.height(), this.active.nextAll(".ui-menu-item").each(function() {
                return n = t(this), n.offset().top - i - r < 0
            }), this.focus(e, n)) : this.focus(e, this.activeMenu.children(".ui-menu-item")[this.active ? "last" : "first"]()))) : void this.next(e)
        },
        previousPage: function(e) {
            var n, i, r;
            return this.active ? void(this.isFirstItem() || (this._hasScroll() ? (i = this.active.offset().top, r = this.element.height(), this.active.prevAll(".ui-menu-item").each(function() {
                return n = t(this), n.offset().top - i + r > 0
            }), this.focus(e, n)) : this.focus(e, this.activeMenu.children(".ui-menu-item").first()))) : void this.next(e)
        },
        _hasScroll: function() {
            return this.element.outerHeight() < this.element.prop("scrollHeight")
        },
        select: function(e) {
            this.active = this.active || t(e.target).closest(".ui-menu-item");
            var n = {
                item: this.active
            };
            this.active.has(".ui-menu").length || this.collapseAll(e, !0), this._trigger("select", e, n)
        }
    })
}(jQuery),
function(t, e) {
    function n(t, e, n) {
        return [parseFloat(t[0]) * (p.test(t[0]) ? e / 100 : 1), parseFloat(t[1]) * (p.test(t[1]) ? n / 100 : 1)]
    }

    function i(e, n) {
        return parseInt(t.css(e, n), 10) || 0
    }

    function r(e) {
        var n = e[0];
        return 9 === n.nodeType ? {
            width: e.width(),
            height: e.height(),
            offset: {
                top: 0,
                left: 0
            }
        } : t.isWindow(n) ? {
            width: e.width(),
            height: e.height(),
            offset: {
                top: e.scrollTop(),
                left: e.scrollLeft()
            }
        } : n.preventDefault ? {
            width: 0,
            height: 0,
            offset: {
                top: n.pageY,
                left: n.pageX
            }
        } : {
            width: e.outerWidth(),
            height: e.outerHeight(),
            offset: e.offset()
        }
    }
    t.ui = t.ui || {};
    var o, s = Math.max,
        a = Math.abs,
        l = Math.round,
        u = /left|center|right/,
        c = /top|center|bottom/,
        h = /[\+\-]\d+(\.[\d]+)?%?/,
        d = /^\w+/,
        p = /%$/,
        f = t.fn.position;
    t.position = {
            scrollbarWidth: function() {
                if (o !== e) return o;
                var n, i, r = t("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
                    s = r.children()[0];
                return t("body").append(r), n = s.offsetWidth, r.css("overflow", "scroll"), i = s.offsetWidth, n === i && (i = r[0].clientWidth), r.remove(), o = n - i
            },
            getScrollInfo: function(e) {
                var n = e.isWindow || e.isDocument ? "" : e.element.css("overflow-x"),
                    i = e.isWindow || e.isDocument ? "" : e.element.css("overflow-y"),
                    r = "scroll" === n || "auto" === n && e.width < e.element[0].scrollWidth,
                    o = "scroll" === i || "auto" === i && e.height < e.element[0].scrollHeight;
                return {
                    width: o ? t.position.scrollbarWidth() : 0,
                    height: r ? t.position.scrollbarWidth() : 0
                }
            },
            getWithinInfo: function(e) {
                var n = t(e || window),
                    i = t.isWindow(n[0]),
                    r = !!n[0] && 9 === n[0].nodeType;
                return {
                    element: n,
                    isWindow: i,
                    isDocument: r,
                    offset: n.offset() || {
                        left: 0,
                        top: 0
                    },
                    scrollLeft: n.scrollLeft(),
                    scrollTop: n.scrollTop(),
                    width: i ? n.width() : n.outerWidth(),
                    height: i ? n.height() : n.outerHeight()
                }
            }
        }, t.fn.position = function(e) {
            if (!e || !e.of) return f.apply(this, arguments);
            e = t.extend({}, e);
            var o, p, m, g, v, y, _ = t(e.of),
                b = t.position.getWithinInfo(e.within),
                w = t.position.getScrollInfo(b),
                L = (e.collision || "flip").split(" "),
                x = {};
            return y = r(_), _[0].preventDefault && (e.at = "left top"), p = y.width, m = y.height, g = y.offset, v = t.extend({}, g), t.each(["my", "at"], function() {
                var t, n, i = (e[this] || "").split(" ");
                1 === i.length && (i = u.test(i[0]) ? i.concat(["center"]) : c.test(i[0]) ? ["center"].concat(i) : ["center", "center"]), i[0] = u.test(i[0]) ? i[0] : "center", i[1] = c.test(i[1]) ? i[1] : "center", t = h.exec(i[0]), n = h.exec(i[1]), x[this] = [t ? t[0] : 0, n ? n[0] : 0], e[this] = [d.exec(i[0])[0], d.exec(i[1])[0]]
            }), 1 === L.length && (L[1] = L[0]), "right" === e.at[0] ? v.left += p : "center" === e.at[0] && (v.left += p / 2), "bottom" === e.at[1] ? v.top += m : "center" === e.at[1] && (v.top += m / 2), o = n(x.at, p, m), v.left += o[0], v.top += o[1], this.each(function() {
                var r, u, c = t(this),
                    h = c.outerWidth(),
                    d = c.outerHeight(),
                    f = i(this, "marginLeft"),
                    y = i(this, "marginTop"),
                    $ = h + f + i(this, "marginRight") + w.width,
                    C = d + y + i(this, "marginBottom") + w.height,
                    k = t.extend({}, v),
                    S = n(x.my, c.outerWidth(), c.outerHeight());
                "right" === e.my[0] ? k.left -= h : "center" === e.my[0] && (k.left -= h / 2), "bottom" === e.my[1] ? k.top -= d : "center" === e.my[1] && (k.top -= d / 2), k.left += S[0], k.top += S[1], t.support.offsetFractions || (k.left = l(k.left), k.top = l(k.top)), r = {
                    marginLeft: f,
                    marginTop: y
                }, t.each(["left", "top"], function(n, i) {
                    t.ui.position[L[n]] && t.ui.position[L[n]][i](k, {
                        targetWidth: p,
                        targetHeight: m,
                        elemWidth: h,
                        elemHeight: d,
                        collisionPosition: r,
                        collisionWidth: $,
                        collisionHeight: C,
                        offset: [o[0] + S[0], o[1] + S[1]],
                        my: e.my,
                        at: e.at,
                        within: b,
                        elem: c
                    })
                }), e.using && (u = function(t) {
                    var n = g.left - k.left,
                        i = n + p - h,
                        r = g.top - k.top,
                        o = r + m - d,
                        l = {
                            target: {
                                element: _,
                                left: g.left,
                                top: g.top,
                                width: p,
                                height: m
                            },
                            element: {
                                element: c,
                                left: k.left,
                                top: k.top,
                                width: h,
                                height: d
                            },
                            horizontal: 0 > i ? "left" : n > 0 ? "right" : "center",
                            vertical: 0 > o ? "top" : r > 0 ? "bottom" : "middle"
                        };
                    h > p && a(n + i) < p && (l.horizontal = "center"), d > m && a(r + o) < m && (l.vertical = "middle"), l.important = s(a(n), a(i)) > s(a(r), a(o)) ? "horizontal" : "vertical", e.using.call(this, t, l)
                }), c.offset(t.extend(k, {
                    using: u
                }))
            })
        }, t.ui.position = {
            fit: {
                left: function(t, e) {
                    var n, i = e.within,
                        r = i.isWindow ? i.scrollLeft : i.offset.left,
                        o = i.width,
                        a = t.left - e.collisionPosition.marginLeft,
                        l = r - a,
                        u = a + e.collisionWidth - o - r;
                    e.collisionWidth > o ? l > 0 && 0 >= u ? (n = t.left + l + e.collisionWidth - o - r, t.left += l - n) : t.left = u > 0 && 0 >= l ? r : l > u ? r + o - e.collisionWidth : r : l > 0 ? t.left += l : u > 0 ? t.left -= u : t.left = s(t.left - a, t.left)
                },
                top: function(t, e) {
                    var n, i = e.within,
                        r = i.isWindow ? i.scrollTop : i.offset.top,
                        o = e.within.height,
                        a = t.top - e.collisionPosition.marginTop,
                        l = r - a,
                        u = a + e.collisionHeight - o - r;
                    e.collisionHeight > o ? l > 0 && 0 >= u ? (n = t.top + l + e.collisionHeight - o - r, t.top += l - n) : t.top = u > 0 && 0 >= l ? r : l > u ? r + o - e.collisionHeight : r : l > 0 ? t.top += l : u > 0 ? t.top -= u : t.top = s(t.top - a, t.top)
                }
            },
            flip: {
                left: function(t, e) {
                    var n, i, r = e.within,
                        o = r.offset.left + r.scrollLeft,
                        s = r.width,
                        l = r.isWindow ? r.scrollLeft : r.offset.left,
                        u = t.left - e.collisionPosition.marginLeft,
                        c = u - l,
                        h = u + e.collisionWidth - s - l,
                        d = "left" === e.my[0] ? -e.elemWidth : "right" === e.my[0] ? e.elemWidth : 0,
                        p = "left" === e.at[0] ? e.targetWidth : "right" === e.at[0] ? -e.targetWidth : 0,
                        f = -2 * e.offset[0];
                    0 > c ? (n = t.left + d + p + f + e.collisionWidth - s - o, (0 > n || n < a(c)) && (t.left += d + p + f)) : h > 0 && (i = t.left - e.collisionPosition.marginLeft + d + p + f - l, (i > 0 || a(i) < h) && (t.left += d + p + f))
                },
                top: function(t, e) {
                    var n, i, r = e.within,
                        o = r.offset.top + r.scrollTop,
                        s = r.height,
                        l = r.isWindow ? r.scrollTop : r.offset.top,
                        u = t.top - e.collisionPosition.marginTop,
                        c = u - l,
                        h = u + e.collisionHeight - s - l,
                        d = "top" === e.my[1],
                        p = d ? -e.elemHeight : "bottom" === e.my[1] ? e.elemHeight : 0,
                        f = "top" === e.at[1] ? e.targetHeight : "bottom" === e.at[1] ? -e.targetHeight : 0,
                        m = -2 * e.offset[1];
                    0 > c ? (i = t.top + p + f + m + e.collisionHeight - s - o, t.top + p + f + m > c && (0 > i || i < a(c)) && (t.top += p + f + m)) : h > 0 && (n = t.top - e.collisionPosition.marginTop + p + f + m - l, t.top + p + f + m > h && (n > 0 || a(n) < h) && (t.top += p + f + m))
                }
            },
            flipfit: {
                left: function() {
                    t.ui.position.flip.left.apply(this, arguments), t.ui.position.fit.left.apply(this, arguments)
                },
                top: function() {
                    t.ui.position.flip.top.apply(this, arguments), t.ui.position.fit.top.apply(this, arguments)
                }
            }
        },
        function() {
            var e, n, i, r, o, s = document.getElementsByTagName("body")[0],
                a = document.createElement("div");
            e = document.createElement(s ? "div" : "body"), i = {
                visibility: "hidden",
                width: 0,
                height: 0,
                border: 0,
                margin: 0,
                background: "none"
            }, s && t.extend(i, {
                position: "absolute",
                left: "-1000px",
                top: "-1000px"
            });
            for (o in i) e.style[o] = i[o];
            e.appendChild(a), n = s || document.documentElement, n.insertBefore(e, n.firstChild), a.style.cssText = "position: absolute; left: 10.7432222px;", r = t(a).offset().left, t.support.offsetFractions = r > 10 && 11 > r, e.innerHTML = "", n.removeChild(e)
        }()
}(jQuery),
function(t, e) {
    t.widget("ui.progressbar", {
        version: "1.10.4",
        options: {
            max: 100,
            value: 0,
            change: null,
            complete: null
        },
        min: 0,
        _create: function() {
            this.oldValue = this.options.value = this._constrainedValue(), this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({
                role: "progressbar",
                "aria-valuemin": this.min
            }), this.valueDiv = t("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element), this._refreshValue()
        },
        _destroy: function() {
            this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), this.valueDiv.remove()
        },
        value: function(t) {
            return t === e ? this.options.value : (this.options.value = this._constrainedValue(t), void this._refreshValue())
        },
        _constrainedValue: function(t) {
            return t === e && (t = this.options.value), this.indeterminate = t === !1, "number" != typeof t && (t = 0), this.indeterminate ? !1 : Math.min(this.options.max, Math.max(this.min, t))
        },
        _setOptions: function(t) {
            var e = t.value;
            delete t.value, this._super(t), this.options.value = this._constrainedValue(e), this._refreshValue()
        },
        _setOption: function(t, e) {
            "max" === t && (e = Math.max(this.min, e)), this._super(t, e)
        },
        _percentage: function() {
            return this.indeterminate ? 100 : 100 * (this.options.value - this.min) / (this.options.max - this.min)
        },
        _refreshValue: function() {
            var e = this.options.value,
                n = this._percentage();
            this.valueDiv.toggle(this.indeterminate || e > this.min).toggleClass("ui-corner-right", e === this.options.max).width(n.toFixed(0) + "%"), this.element.toggleClass("ui-progressbar-indeterminate", this.indeterminate), this.indeterminate ? (this.element.removeAttr("aria-valuenow"), this.overlayDiv || (this.overlayDiv = t("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv))) : (this.element.attr({
                "aria-valuemax": this.options.max,
                "aria-valuenow": e
            }), this.overlayDiv && (this.overlayDiv.remove(), this.overlayDiv = null)), this.oldValue !== e && (this.oldValue = e, this._trigger("change")), e === this.options.max && this._trigger("complete")
        }
    })
}(jQuery),
function(t) {
    var e = 5;
    t.widget("ui.slider", t.ui.mouse, {
        version: "1.10.4",
        widgetEventPrefix: "slide",
        options: {
            animate: !1,
            distance: 0,
            max: 100,
            min: 0,
            orientation: "horizontal",
            range: !1,
            step: 1,
            value: 0,
            values: null,
            change: null,
            slide: null,
            start: null,
            stop: null
        },
        _create: function() {
            this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget ui-widget-content ui-corner-all"), this._refresh(), this._setOption("disabled", this.options.disabled), this._animateOff = !1
        },
        _refresh: function() {
            this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue()
        },
        _createHandles: function() {
            var e, n, i = this.options,
                r = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),
                o = "<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>",
                s = [];
            for (n = i.values && i.values.length || 1, r.length > n && (r.slice(n).remove(), r = r.slice(0, n)), e = r.length; n > e; e++) s.push(o);
            this.handles = r.add(t(s.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), this.handles.each(function(e) {
                t(this).data("ui-slider-handle-index", e)
            })
        },
        _createRange: function() {
            var e = this.options,
                n = "";
            e.range ? (e.range === !0 && (e.values ? e.values.length && 2 !== e.values.length ? e.values = [e.values[0], e.values[0]] : t.isArray(e.values) && (e.values = e.values.slice(0)) : e.values = [this._valueMin(), this._valueMin()]), this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
                left: "",
                bottom: ""
            }) : (this.range = t("<div></div>").appendTo(this.element), n = "ui-slider-range ui-widget-header ui-corner-all"), this.range.addClass(n + ("min" === e.range || "max" === e.range ? " ui-slider-range-" + e.range : ""))) : (this.range && this.range.remove(), this.range = null)
        },
        _setupEvents: function() {
            var t = this.handles.add(this.range).filter("a");
            this._off(t), this._on(t, this._handleEvents), this._hoverable(t), this._focusable(t)
        },
        _destroy: function() {
            this.handles.remove(), this.range && this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"), this._mouseDestroy()
        },
        _mouseCapture: function(e) {
            var n, i, r, o, s, a, l, u, c = this,
                h = this.options;
            return h.disabled ? !1 : (this.elementSize = {
                width: this.element.outerWidth(),
                height: this.element.outerHeight()
            }, this.elementOffset = this.element.offset(), n = {
                x: e.pageX,
                y: e.pageY
            }, i = this._normValueFromMouse(n), r = this._valueMax() - this._valueMin() + 1, this.handles.each(function(e) {
                var n = Math.abs(i - c.values(e));
                (r > n || r === n && (e === c._lastChangedValue || c.values(e) === h.min)) && (r = n, o = t(this), s = e)
            }), a = this._start(e, s), a === !1 ? !1 : (this._mouseSliding = !0, this._handleIndex = s, o.addClass("ui-state-active").focus(), l = o.offset(), u = !t(e.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = u ? {
                left: 0,
                top: 0
            } : {
                left: e.pageX - l.left - o.width() / 2,
                top: e.pageY - l.top - o.height() / 2 - (parseInt(o.css("borderTopWidth"), 10) || 0) - (parseInt(o.css("borderBottomWidth"), 10) || 0) + (parseInt(o.css("marginTop"), 10) || 0)
            }, this.handles.hasClass("ui-state-hover") || this._slide(e, s, i), this._animateOff = !0, !0))
        },
        _mouseStart: function() {
            return !0
        },
        _mouseDrag: function(t) {
            var e = {
                    x: t.pageX,
                    y: t.pageY
                },
                n = this._normValueFromMouse(e);
            return this._slide(t, this._handleIndex, n), !1
        },
        _mouseStop: function(t) {
            return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(t, this._handleIndex), this._change(t, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1
        },
        _detectOrientation: function() {
            this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal"
        },
        _normValueFromMouse: function(t) {
            var e, n, i, r, o;
            return "horizontal" === this.orientation ? (e = this.elementSize.width, n = t.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (e = this.elementSize.height, n = t.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), i = n / e, i > 1 && (i = 1), 0 > i && (i = 0), "vertical" === this.orientation && (i = 1 - i), r = this._valueMax() - this._valueMin(), o = this._valueMin() + i * r, this._trimAlignValue(o)
        },
        _start: function(t, e) {
            var n = {
                handle: this.handles[e],
                value: this.value()
            };
            return this.options.values && this.options.values.length && (n.value = this.values(e), n.values = this.values()), this._trigger("start", t, n)
        },
        _slide: function(t, e, n) {
            var i, r, o;
            this.options.values && this.options.values.length ? (i = this.values(e ? 0 : 1), 2 === this.options.values.length && this.options.range === !0 && (0 === e && n > i || 1 === e && i > n) && (n = i), n !== this.values(e) && (r = this.values(), r[e] = n, o = this._trigger("slide", t, {
                handle: this.handles[e],
                value: n,
                values: r
            }), i = this.values(e ? 0 : 1), o !== !1 && this.values(e, n))) : n !== this.value() && (o = this._trigger("slide", t, {
                handle: this.handles[e],
                value: n
            }), o !== !1 && this.value(n))
        },
        _stop: function(t, e) {
            var n = {
                handle: this.handles[e],
                value: this.value()
            };
            this.options.values && this.options.values.length && (n.value = this.values(e), n.values = this.values()), this._trigger("stop", t, n)
        },
        _change: function(t, e) {
            if (!this._keySliding && !this._mouseSliding) {
                var n = {
                    handle: this.handles[e],
                    value: this.value()
                };
                this.options.values && this.options.values.length && (n.value = this.values(e), n.values = this.values()), this._lastChangedValue = e, this._trigger("change", t, n)
            }
        },
        value: function(t) {
            return arguments.length ? (this.options.value = this._trimAlignValue(t), this._refreshValue(), void this._change(null, 0)) : this._value()
        },
        values: function(e, n) {
            var i, r, o;
            if (arguments.length > 1) return this.options.values[e] = this._trimAlignValue(n), this._refreshValue(), void this._change(null, e);
            if (!arguments.length) return this._values();
            if (!t.isArray(arguments[0])) return this.options.values && this.options.values.length ? this._values(e) : this.value();
            for (i = this.options.values, r = arguments[0], o = 0; o < i.length; o += 1) i[o] = this._trimAlignValue(r[o]), this._change(null, o);
            this._refreshValue()
        },
        _setOption: function(e, n) {
            var i, r = 0;
            switch ("range" === e && this.options.range === !0 && ("min" === n ? (this.options.value = this._values(0), this.options.values = null) : "max" === n && (this.options.value = this._values(this.options.values.length - 1), this.options.values = null)), t.isArray(this.options.values) && (r = this.options.values.length), t.Widget.prototype._setOption.apply(this, arguments), e) {
                case "orientation":
                    this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), this._refreshValue();
                    break;
                case "value":
                    this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
                    break;
                case "values":
                    for (this._animateOff = !0, this._refreshValue(), i = 0; r > i; i += 1) this._change(null, i);
                    this._animateOff = !1;
                    break;
                case "min":
                case "max":
                    this._animateOff = !0, this._refreshValue(), this._animateOff = !1;
                    break;
                case "range":
                    this._animateOff = !0, this._refresh(), this._animateOff = !1
            }
        },
        _value: function() {
            var t = this.options.value;
            return t = this._trimAlignValue(t)
        },
        _values: function(t) {
            var e, n, i;
            if (arguments.length) return e = this.options.values[t], e = this._trimAlignValue(e);
            if (this.options.values && this.options.values.length) {
                for (n = this.options.values.slice(), i = 0; i < n.length; i += 1) n[i] = this._trimAlignValue(n[i]);
                return n
            }
            return []
        },
        _trimAlignValue: function(t) {
            if (t <= this._valueMin()) return this._valueMin();
            if (t >= this._valueMax()) return this._valueMax();
            var e = this.options.step > 0 ? this.options.step : 1,
                n = (t - this._valueMin()) % e,
                i = t - n;
            return 2 * Math.abs(n) >= e && (i += n > 0 ? e : -e), parseFloat(i.toFixed(5))
        },
        _valueMin: function() {
            return this.options.min
        },
        _valueMax: function() {
            return this.options.max
        },
        _refreshValue: function() {
            var e, n, i, r, o, s = this.options.range,
                a = this.options,
                l = this,
                u = this._animateOff ? !1 : a.animate,
                c = {};
            this.options.values && this.options.values.length ? this.handles.each(function(i) {
                n = (l.values(i) - l._valueMin()) / (l._valueMax() - l._valueMin()) * 100, c["horizontal" === l.orientation ? "left" : "bottom"] = n + "%", t(this).stop(1, 1)[u ? "animate" : "css"](c, a.animate), l.options.range === !0 && ("horizontal" === l.orientation ? (0 === i && l.range.stop(1, 1)[u ? "animate" : "css"]({
                    left: n + "%"
                }, a.animate), 1 === i && l.range[u ? "animate" : "css"]({
                    width: n - e + "%"
                }, {
                    queue: !1,
                    duration: a.animate
                })) : (0 === i && l.range.stop(1, 1)[u ? "animate" : "css"]({
                    bottom: n + "%"
                }, a.animate), 1 === i && l.range[u ? "animate" : "css"]({
                    height: n - e + "%"
                }, {
                    queue: !1,
                    duration: a.animate
                }))), e = n
            }) : (i = this.value(), r = this._valueMin(), o = this._valueMax(), n = o !== r ? (i - r) / (o - r) * 100 : 0, c["horizontal" === this.orientation ? "left" : "bottom"] = n + "%", this.handle.stop(1, 1)[u ? "animate" : "css"](c, a.animate), "min" === s && "horizontal" === this.orientation && this.range.stop(1, 1)[u ? "animate" : "css"]({
                width: n + "%"
            }, a.animate), "max" === s && "horizontal" === this.orientation && this.range[u ? "animate" : "css"]({
                width: 100 - n + "%"
            }, {
                queue: !1,
                duration: a.animate
            }), "min" === s && "vertical" === this.orientation && this.range.stop(1, 1)[u ? "animate" : "css"]({
                height: n + "%"
            }, a.animate), "max" === s && "vertical" === this.orientation && this.range[u ? "animate" : "css"]({
                height: 100 - n + "%"
            }, {
                queue: !1,
                duration: a.animate
            }))
        },
        _handleEvents: {
            keydown: function(n) {
                var i, r, o, s, a = t(n.target).data("ui-slider-handle-index");
                switch (n.keyCode) {
                    case t.ui.keyCode.HOME:
                    case t.ui.keyCode.END:
                    case t.ui.keyCode.PAGE_UP:
                    case t.ui.keyCode.PAGE_DOWN:
                    case t.ui.keyCode.UP:
                    case t.ui.keyCode.RIGHT:
                    case t.ui.keyCode.DOWN:
                    case t.ui.keyCode.LEFT:
                        if (n.preventDefault(), !this._keySliding && (this._keySliding = !0, t(n.target).addClass("ui-state-active"), i = this._start(n, a), i === !1)) return
                }
                switch (s = this.options.step, r = o = this.options.values && this.options.values.length ? this.values(a) : this.value(), n.keyCode) {
                    case t.ui.keyCode.HOME:
                        o = this._valueMin();
                        break;
                    case t.ui.keyCode.END:
                        o = this._valueMax();
                        break;
                    case t.ui.keyCode.PAGE_UP:
                        o = this._trimAlignValue(r + (this._valueMax() - this._valueMin()) / e);
                        break;
                    case t.ui.keyCode.PAGE_DOWN:
                        o = this._trimAlignValue(r - (this._valueMax() - this._valueMin()) / e);
                        break;
                    case t.ui.keyCode.UP:
                    case t.ui.keyCode.RIGHT:
                        if (r === this._valueMax()) return;
                        o = this._trimAlignValue(r + s);
                        break;
                    case t.ui.keyCode.DOWN:
                    case t.ui.keyCode.LEFT:
                        if (r === this._valueMin()) return;
                        o = this._trimAlignValue(r - s)
                }
                this._slide(n, a, o)
            },
            click: function(t) {
                t.preventDefault()
            },
            keyup: function(e) {
                var n = t(e.target).data("ui-slider-handle-index");
                this._keySliding && (this._keySliding = !1, this._stop(e, n), this._change(e, n), t(e.target).removeClass("ui-state-active"))
            }
        }
    })
}(jQuery),
function(t) {
    function e(t) {
        return function() {
            var e = this.element.val();
            t.apply(this, arguments), this._refresh(), e !== this.element.val() && this._trigger("change")
        }
    }
    t.widget("ui.spinner", {
        version: "1.10.4",
        defaultElement: "<input>",
        widgetEventPrefix: "spin",
        options: {
            culture: null,
            icons: {
                down: "ui-icon-triangle-1-s",
                up: "ui-icon-triangle-1-n"
            },
            incremental: !0,
            max: null,
            min: null,
            numberFormat: null,
            page: 10,
            step: 1,
            change: null,
            spin: null,
            start: null,
            stop: null
        },
        _create: function() {
            this._setOption("max", this.options.max), this._setOption("min", this.options.min), this._setOption("step", this.options.step), "" !== this.value() && this._value(this.element.val(), !0), this._draw(), this._on(this._events), this._refresh(), this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete")
                }
            })
        },
        _getCreateOptions: function() {
            var e = {},
                n = this.element;
            return t.each(["min", "max", "step"], function(t, i) {
                var r = n.attr(i);
                void 0 !== r && r.length && (e[i] = r)
            }), e
        },
        _events: {
            keydown: function(t) {
                this._start(t) && this._keydown(t) && t.preventDefault()
            },
            keyup: "_stop",
            focus: function() {
                this.previous = this.element.val()
            },
            blur: function(t) {
                return this.cancelBlur ? void delete this.cancelBlur : (this._stop(), this._refresh(), void(this.previous !== this.element.val() && this._trigger("change", t)))
            },
            mousewheel: function(t, e) {
                if (e) {
                    if (!this.spinning && !this._start(t)) return !1;
                    this._spin((e > 0 ? 1 : -1) * this.options.step, t), clearTimeout(this.mousewheelTimer), this.mousewheelTimer = this._delay(function() {
                        this.spinning && this._stop(t)
                    }, 100), t.preventDefault()
                }
            },
            "mousedown .ui-spinner-button": function(e) {
                function n() {
                    var t = this.element[0] === this.document[0].activeElement;
                    t || (this.element.focus(), this.previous = i, this._delay(function() {
                        this.previous = i
                    }))
                }
                var i;
                i = this.element[0] === this.document[0].activeElement ? this.previous : this.element.val(), e.preventDefault(), n.call(this), this.cancelBlur = !0, this._delay(function() {
                    delete this.cancelBlur, n.call(this)
                }), this._start(e) !== !1 && this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e)
            },
            "mouseup .ui-spinner-button": "_stop",
            "mouseenter .ui-spinner-button": function(e) {
                return t(e.currentTarget).hasClass("ui-state-active") ? this._start(e) === !1 ? !1 : void this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e) : void 0
            },
            "mouseleave .ui-spinner-button": "_stop"
        },
        _draw: function() {
            var t = this.uiSpinner = this.element.addClass("ui-spinner-input").attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());
            this.element.attr("role", "spinbutton"), this.buttons = t.find(".ui-spinner-button").attr("tabIndex", -1).button().removeClass("ui-corner-all"), this.buttons.height() > Math.ceil(.5 * t.height()) && t.height() > 0 && t.height(t.height()), this.options.disabled && this.disable()
        },
        _keydown: function(e) {
            var n = this.options,
                i = t.ui.keyCode;
            switch (e.keyCode) {
                case i.UP:
                    return this._repeat(null, 1, e), !0;
                case i.DOWN:
                    return this._repeat(null, -1, e), !0;
                case i.PAGE_UP:
                    return this._repeat(null, n.page, e), !0;
                case i.PAGE_DOWN:
                    return this._repeat(null, -n.page, e), !0
            }
            return !1
        },
        _uiSpinnerHtml: function() {
            return "<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"
        },
        _buttonHtml: function() {
            return "<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon " + this.options.icons.up + "'>&#9650;</span></a><a class='ui-spinner-button ui-spinner-down ui-corner-br'><span class='ui-icon " + this.options.icons.down + "'>&#9660;</span></a>"
        },
        _start: function(t) {
            return this.spinning || this._trigger("start", t) !== !1 ? (this.counter || (this.counter = 1), this.spinning = !0, !0) : !1
        },
        _repeat: function(t, e, n) {
            t = t || 500, clearTimeout(this.timer), this.timer = this._delay(function() {
                this._repeat(40, e, n)
            }, t), this._spin(e * this.options.step, n)
        },
        _spin: function(t, e) {
            var n = this.value() || 0;
            this.counter || (this.counter = 1), n = this._adjustValue(n + t * this._increment(this.counter)), this.spinning && this._trigger("spin", e, {
                value: n
            }) === !1 || (this._value(n), this.counter++)
        },
        _increment: function(e) {
            var n = this.options.incremental;
            return n ? t.isFunction(n) ? n(e) : Math.floor(e * e * e / 5e4 - e * e / 500 + 17 * e / 200 + 1) : 1
        },
        _precision: function() {
            var t = this._precisionOf(this.options.step);
            return null !== this.options.min && (t = Math.max(t, this._precisionOf(this.options.min))), t
        },
        _precisionOf: function(t) {
            var e = t.toString(),
                n = e.indexOf(".");
            return -1 === n ? 0 : e.length - n - 1
        },
        _adjustValue: function(t) {
            var e, n, i = this.options;
            return e = null !== i.min ? i.min : 0, n = t - e, n = Math.round(n / i.step) * i.step, t = e + n, t = parseFloat(t.toFixed(this._precision())), null !== i.max && t > i.max ? i.max : null !== i.min && t < i.min ? i.min : t
        },
        _stop: function(t) {
            this.spinning && (clearTimeout(this.timer), clearTimeout(this.mousewheelTimer), this.counter = 0, this.spinning = !1, this._trigger("stop", t))
        },
        _setOption: function(t, e) {
            if ("culture" === t || "numberFormat" === t) {
                var n = this._parse(this.element.val());
                return this.options[t] = e, void this.element.val(this._format(n))
            }("max" === t || "min" === t || "step" === t) && "string" == typeof e && (e = this._parse(e)), "icons" === t && (this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(e.up), this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(e.down)), this._super(t, e), "disabled" === t && (e ? (this.element.prop("disabled", !0), this.buttons.button("disable")) : (this.element.prop("disabled", !1), this.buttons.button("enable")))
        },
        _setOptions: e(function(t) {
            this._super(t), this._value(this.element.val())
        }),
        _parse: function(t) {
            return "string" == typeof t && "" !== t && (t = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(t, 10, this.options.culture) : +t), "" === t || isNaN(t) ? null : t
        },
        _format: function(t) {
            return "" === t ? "" : window.Globalize && this.options.numberFormat ? Globalize.format(t, this.options.numberFormat, this.options.culture) : t
        },
        _refresh: function() {
            this.element.attr({
                "aria-valuemin": this.options.min,
                "aria-valuemax": this.options.max,
                "aria-valuenow": this._parse(this.element.val())
            })
        },
        _value: function(t, e) {
            var n;
            "" !== t && (n = this._parse(t), null !== n && (e || (n = this._adjustValue(n)), t = this._format(n))), this.element.val(t), this._refresh()
        },
        _destroy: function() {
            this.element.removeClass("ui-spinner-input").prop("disabled", !1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), this.uiSpinner.replaceWith(this.element)
        },
        stepUp: e(function(t) {
            this._stepUp(t)
        }),
        _stepUp: function(t) {
            this._start() && (this._spin((t || 1) * this.options.step), this._stop())
        },
        stepDown: e(function(t) {
            this._stepDown(t)
        }),
        _stepDown: function(t) {
            this._start() && (this._spin((t || 1) * -this.options.step), this._stop())
        },
        pageUp: e(function(t) {
            this._stepUp((t || 1) * this.options.page)
        }),
        pageDown: e(function(t) {
            this._stepDown((t || 1) * this.options.page)
        }),
        value: function(t) {
            return arguments.length ? void e(this._value).call(this, t) : this._parse(this.element.val())
        },
        widget: function() {
            return this.uiSpinner
        }
    })
}(jQuery),
function(t, e) {
    function n() {
        return ++r
    }

    function i(t) {
        return t = t.cloneNode(!1), t.hash.length > 1 && decodeURIComponent(t.href.replace(o, "")) === decodeURIComponent(location.href.replace(o, ""))
    }
    var r = 0,
        o = /#.*$/;
    t.widget("ui.tabs", {
        version: "1.10.4",
        delay: 300,
        options: {
            active: null,
            collapsible: !1,
            event: "click",
            heightStyle: "content",
            hide: null,
            show: null,
            activate: null,
            beforeActivate: null,
            beforeLoad: null,
            load: null
        },
        _create: function() {
            var e = this,
                n = this.options;
            this.running = !1, this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible", n.collapsible).delegate(".ui-tabs-nav > li", "mousedown" + this.eventNamespace, function(e) {
                t(this).is(".ui-state-disabled") && e.preventDefault()
            }).delegate(".ui-tabs-anchor", "focus" + this.eventNamespace, function() {
                t(this).closest("li").is(".ui-state-disabled") && this.blur()
            }), this._processTabs(), n.active = this._initialActive(), t.isArray(n.disabled) && (n.disabled = t.unique(n.disabled.concat(t.map(this.tabs.filter(".ui-state-disabled"), function(t) {
                return e.tabs.index(t)
            }))).sort()), this.active = this.options.active !== !1 && this.anchors.length ? this._findActive(n.active) : t(), this._refresh(), this.active.length && this.load(n.active)
        },
        _initialActive: function() {
            var e = this.options.active,
                n = this.options.collapsible,
                i = location.hash.substring(1);
            return null === e && (i && this.tabs.each(function(n, r) {
                return t(r).attr("aria-controls") === i ? (e = n, !1) : void 0
            }), null === e && (e = this.tabs.index(this.tabs.filter(".ui-tabs-active"))), (null === e || -1 === e) && (e = this.tabs.length ? 0 : !1)), e !== !1 && (e = this.tabs.index(this.tabs.eq(e)), -1 === e && (e = n ? !1 : 0)), !n && e === !1 && this.anchors.length && (e = 0), e
        },
        _getCreateEventData: function() {
            return {
                tab: this.active,
                panel: this.active.length ? this._getPanelForTab(this.active) : t()
            }
        },
        _tabKeydown: function(e) {
            var n = t(this.document[0].activeElement).closest("li"),
                i = this.tabs.index(n),
                r = !0;
            if (!this._handlePageNav(e)) {
                switch (e.keyCode) {
                    case t.ui.keyCode.RIGHT:
                    case t.ui.keyCode.DOWN:
                        i++;
                        break;
                    case t.ui.keyCode.UP:
                    case t.ui.keyCode.LEFT:
                        r = !1, i--;
                        break;
                    case t.ui.keyCode.END:
                        i = this.anchors.length - 1;
                        break;
                    case t.ui.keyCode.HOME:
                        i = 0;
                        break;
                    case t.ui.keyCode.SPACE:
                        return e.preventDefault(), clearTimeout(this.activating), void this._activate(i);
                    case t.ui.keyCode.ENTER:
                        return e.preventDefault(), clearTimeout(this.activating), void this._activate(i === this.options.active ? !1 : i);
                    default:
                        return
                }
                e.preventDefault(), clearTimeout(this.activating), i = this._focusNextTab(i, r), e.ctrlKey || (n.attr("aria-selected", "false"), this.tabs.eq(i).attr("aria-selected", "true"), this.activating = this._delay(function() {
                    this.option("active", i)
                }, this.delay))
            }
        },
        _panelKeydown: function(e) {
            this._handlePageNav(e) || e.ctrlKey && e.keyCode === t.ui.keyCode.UP && (e.preventDefault(), this.active.focus())
        },
        _handlePageNav: function(e) {
            return e.altKey && e.keyCode === t.ui.keyCode.PAGE_UP ? (this._activate(this._focusNextTab(this.options.active - 1, !1)), !0) : e.altKey && e.keyCode === t.ui.keyCode.PAGE_DOWN ? (this._activate(this._focusNextTab(this.options.active + 1, !0)), !0) : void 0
        },
        _findNextTab: function(e, n) {
            function i() {
                return e > r && (e = 0), 0 > e && (e = r), e
            }
            for (var r = this.tabs.length - 1; - 1 !== t.inArray(i(), this.options.disabled);) e = n ? e + 1 : e - 1;
            return e
        },
        _focusNextTab: function(t, e) {
            return t = this._findNextTab(t, e), this.tabs.eq(t).focus(), t
        },
        _setOption: function(t, e) {
            return "active" === t ? void this._activate(e) : "disabled" === t ? void this._setupDisabled(e) : (this._super(t, e), "collapsible" === t && (this.element.toggleClass("ui-tabs-collapsible", e), e || this.options.active !== !1 || this._activate(0)), "event" === t && this._setupEvents(e), void("heightStyle" === t && this._setupHeightStyle(e)))
        },
        _tabId: function(t) {
            return t.attr("aria-controls") || "ui-tabs-" + n()
        },
        _sanitizeSelector: function(t) {
            return t ? t.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : ""
        },
        refresh: function() {
            var e = this.options,
                n = this.tablist.children(":has(a[href])");
            e.disabled = t.map(n.filter(".ui-state-disabled"), function(t) {
                return n.index(t)
            }), this._processTabs(), e.active !== !1 && this.anchors.length ? this.active.length && !t.contains(this.tablist[0], this.active[0]) ? this.tabs.length === e.disabled.length ? (e.active = !1, this.active = t()) : this._activate(this._findNextTab(Math.max(0, e.active - 1), !1)) : e.active = this.tabs.index(this.active) : (e.active = !1, this.active = t()), this._refresh()
        },
        _refresh: function() {
            this._setupDisabled(this.options.disabled), this._setupEvents(this.options.event), this._setupHeightStyle(this.options.heightStyle), this.tabs.not(this.active).attr({
                "aria-selected": "false",
                tabIndex: -1
            }), this.panels.not(this._getPanelForTab(this.active)).hide().attr({
                "aria-expanded": "false",
                "aria-hidden": "true"
            }), this.active.length ? (this.active.addClass("ui-tabs-active ui-state-active").attr({
                "aria-selected": "true",
                tabIndex: 0
            }), this._getPanelForTab(this.active).show().attr({
                "aria-expanded": "true",
                "aria-hidden": "false"
            })) : this.tabs.eq(0).attr("tabIndex", 0)
        },
        _processTabs: function() {
            var e = this;
            this.tablist = this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role", "tablist"), this.tabs = this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({
                role: "tab",
                tabIndex: -1
            }), this.anchors = this.tabs.map(function() {
                return t("a", this)[0]
            }).addClass("ui-tabs-anchor").attr({
                role: "presentation",
                tabIndex: -1
            }), this.panels = t(), this.anchors.each(function(n, r) {
                var o, s, a, l = t(r).uniqueId().attr("id"),
                    u = t(r).closest("li"),
                    c = u.attr("aria-controls");
                i(r) ? (o = r.hash, s = e.element.find(e._sanitizeSelector(o))) : (a = e._tabId(u), o = "#" + a, s = e.element.find(o), s.length || (s = e._createPanel(a), s.insertAfter(e.panels[n - 1] || e.tablist)), s.attr("aria-live", "polite")), s.length && (e.panels = e.panels.add(s)), c && u.data("ui-tabs-aria-controls", c), u.attr({
                    "aria-controls": o.substring(1),
                    "aria-labelledby": l
                }), s.attr("aria-labelledby", l)
            }), this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role", "tabpanel")
        },
        _getList: function() {
            return this.tablist || this.element.find("ol,ul").eq(0)
        },
        _createPanel: function(e) {
            return t("<div>").attr("id", e).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy", !0)
        },
        _setupDisabled: function(e) {
            t.isArray(e) && (e.length ? e.length === this.anchors.length && (e = !0) : e = !1);
            for (var n, i = 0; n = this.tabs[i]; i++) e === !0 || -1 !== t.inArray(i, e) ? t(n).addClass("ui-state-disabled").attr("aria-disabled", "true") : t(n).removeClass("ui-state-disabled").removeAttr("aria-disabled");
            this.options.disabled = e
        },
        _setupEvents: function(e) {
            var n = {
                click: function(t) {
                    t.preventDefault()
                }
            };
            e && t.each(e.split(" "), function(t, e) {
                n[e] = "_eventHandler"
            }), this._off(this.anchors.add(this.tabs).add(this.panels)), this._on(this.anchors, n), this._on(this.tabs, {
                keydown: "_tabKeydown"
            }), this._on(this.panels, {
                keydown: "_panelKeydown"
            }), this._focusable(this.tabs), this._hoverable(this.tabs)
        },
        _setupHeightStyle: function(e) {
            var n, i = this.element.parent();
            "fill" === e ? (n = i.height(), n -= this.element.outerHeight() - this.element.height(), this.element.siblings(":visible").each(function() {
                var e = t(this),
                    i = e.css("position");
                "absolute" !== i && "fixed" !== i && (n -= e.outerHeight(!0))
            }), this.element.children().not(this.panels).each(function() {
                n -= t(this).outerHeight(!0)
            }), this.panels.each(function() {
                t(this).height(Math.max(0, n - t(this).innerHeight() + t(this).height()))
            }).css("overflow", "auto")) : "auto" === e && (n = 0, this.panels.each(function() {
                n = Math.max(n, t(this).height("").height())
            }).height(n))
        },
        _eventHandler: function(e) {
            var n = this.options,
                i = this.active,
                r = t(e.currentTarget),
                o = r.closest("li"),
                s = o[0] === i[0],
                a = s && n.collapsible,
                l = a ? t() : this._getPanelForTab(o),
                u = i.length ? this._getPanelForTab(i) : t(),
                c = {
                    oldTab: i,
                    oldPanel: u,
                    newTab: a ? t() : o,
                    newPanel: l
                };
            e.preventDefault(), o.hasClass("ui-state-disabled") || o.hasClass("ui-tabs-loading") || this.running || s && !n.collapsible || this._trigger("beforeActivate", e, c) === !1 || (n.active = a ? !1 : this.tabs.index(o), this.active = s ? t() : o, this.xhr && this.xhr.abort(), u.length || l.length || t.error("jQuery UI Tabs: Mismatching fragment identifier."), l.length && this.load(this.tabs.index(o), e), this._toggle(e, c))
        },
        _toggle: function(e, n) {
            function i() {
                o.running = !1, o._trigger("activate", e, n)
            }

            function r() {
                n.newTab.closest("li").addClass("ui-tabs-active ui-state-active"), s.length && o.options.show ? o._show(s, o.options.show, i) : (s.show(), i())
            }
            var o = this,
                s = n.newPanel,
                a = n.oldPanel;
            this.running = !0, a.length && this.options.hide ? this._hide(a, this.options.hide, function() {
                n.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), r()
            }) : (n.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), a.hide(), r()), a.attr({
                "aria-expanded": "false",
                "aria-hidden": "true"
            }), n.oldTab.attr("aria-selected", "false"), s.length && a.length ? n.oldTab.attr("tabIndex", -1) : s.length && this.tabs.filter(function() {
                return 0 === t(this).attr("tabIndex")
            }).attr("tabIndex", -1), s.attr({
                "aria-expanded": "true",
                "aria-hidden": "false"
            }), n.newTab.attr({
                "aria-selected": "true",
                tabIndex: 0
            })
        },
        _activate: function(e) {
            var n, i = this._findActive(e);
            i[0] !== this.active[0] && (i.length || (i = this.active), n = i.find(".ui-tabs-anchor")[0], this._eventHandler({
                target: n,
                currentTarget: n,
                preventDefault: t.noop
            }))
        },
        _findActive: function(e) {
            return e === !1 ? t() : this.tabs.eq(e)
        },
        _getIndex: function(t) {
            return "string" == typeof t && (t = this.anchors.index(this.anchors.filter("[href$='" + t + "']"))), t
        },
        _destroy: function() {
            this.xhr && this.xhr.abort(), this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"), this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"), this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId(), this.tabs.add(this.panels).each(function() {
                t.data(this, "ui-tabs-destroy") ? t(this).remove() : t(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")
            }), this.tabs.each(function() {
                var e = t(this),
                    n = e.data("ui-tabs-aria-controls");
                n ? e.attr("aria-controls", n).removeData("ui-tabs-aria-controls") : e.removeAttr("aria-controls")
            }), this.panels.show(), "content" !== this.options.heightStyle && this.panels.css("height", "")
        },
        enable: function(n) {
            var i = this.options.disabled;
            i !== !1 && (n === e ? i = !1 : (n = this._getIndex(n), i = t.isArray(i) ? t.map(i, function(t) {
                return t !== n ? t : null
            }) : t.map(this.tabs, function(t, e) {
                return e !== n ? e : null
            })), this._setupDisabled(i))
        },
        disable: function(n) {
            var i = this.options.disabled;
            if (i !== !0) {
                if (n === e) i = !0;
                else {
                    if (n = this._getIndex(n), -1 !== t.inArray(n, i)) return;
                    i = t.isArray(i) ? t.merge([n], i).sort() : [n]
                }
                this._setupDisabled(i)
            }
        },
        load: function(e, n) {
            e = this._getIndex(e);
            var r = this,
                o = this.tabs.eq(e),
                s = o.find(".ui-tabs-anchor"),
                a = this._getPanelForTab(o),
                l = {
                    tab: o,
                    panel: a
                };
            i(s[0]) || (this.xhr = t.ajax(this._ajaxSettings(s, n, l)), this.xhr && "canceled" !== this.xhr.statusText && (o.addClass("ui-tabs-loading"), a.attr("aria-busy", "true"), this.xhr.success(function(t) {
                setTimeout(function() {
                    a.html(t), r._trigger("load", n, l)
                }, 1)
            }).complete(function(t, e) {
                setTimeout(function() {
                    "abort" === e && r.panels.stop(!1, !0), o.removeClass("ui-tabs-loading"), a.removeAttr("aria-busy"), t === r.xhr && delete r.xhr
                }, 1)
            })))
        },
        _ajaxSettings: function(e, n, i) {
            var r = this;
            return {
                url: e.attr("href"),
                beforeSend: function(e, o) {
                    return r._trigger("beforeLoad", n, t.extend({
                        jqXHR: e,
                        ajaxSettings: o
                    }, i))
                }
            }
        },
        _getPanelForTab: function(e) {
            var n = t(e).attr("aria-controls");
            return this.element.find(this._sanitizeSelector("#" + n))
        }
    })
}(jQuery),
function(t) {
    function e(e, n) {
        var i = (e.attr("aria-describedby") || "").split(/\s+/);
        i.push(n), e.data("ui-tooltip-id", n).attr("aria-describedby", t.trim(i.join(" ")))
    }

    function n(e) {
        var n = e.data("ui-tooltip-id"),
            i = (e.attr("aria-describedby") || "").split(/\s+/),
            r = t.inArray(n, i); - 1 !== r && i.splice(r, 1), e.removeData("ui-tooltip-id"), i = t.trim(i.join(" ")), i ? e.attr("aria-describedby", i) : e.removeAttr("aria-describedby")
    }
    var i = 0;
    t.widget("ui.tooltip", {
        version: "1.10.4",
        options: {
            content: function() {
                var e = t(this).attr("title") || "";
                return t("<a>").text(e).html()
            },
            hide: !0,
            items: "[title]:not([disabled])",
            position: {
                my: "left top+15",
                at: "left bottom",
                collision: "flipfit flip"
            },
            show: !0,
            tooltipClass: null,
            track: !1,
            close: null,
            open: null
        },
        _create: function() {
            this._on({
                mouseover: "open",
                focusin: "open"
            }), this.tooltips = {}, this.parents = {}, this.options.disabled && this._disable()
        },
        _setOption: function(e, n) {
            var i = this;
            return "disabled" === e ? (this[n ? "_disable" : "_enable"](), void(this.options[e] = n)) : (this._super(e, n), void("content" === e && t.each(this.tooltips, function(t, e) {
                i._updateContent(e)
            })))
        },
        _disable: function() {
            var e = this;
            t.each(this.tooltips, function(n, i) {
                var r = t.Event("blur");
                r.target = r.currentTarget = i[0], e.close(r, !0)
            }), this.element.find(this.options.items).addBack().each(function() {
                var e = t(this);
                e.is("[title]") && e.data("ui-tooltip-title", e.attr("title")).attr("title", "")
            })
        },
        _enable: function() {
            this.element.find(this.options.items).addBack().each(function() {
                var e = t(this);
                e.data("ui-tooltip-title") && e.attr("title", e.data("ui-tooltip-title"))
            })
        },
        open: function(e) {
            var n = this,
                i = t(e ? e.target : this.element).closest(this.options.items);
            i.length && !i.data("ui-tooltip-id") && (i.attr("title") && i.data("ui-tooltip-title", i.attr("title")), i.data("ui-tooltip-open", !0), e && "mouseover" === e.type && i.parents().each(function() {
                var e, i = t(this);
                i.data("ui-tooltip-open") && (e = t.Event("blur"), e.target = e.currentTarget = this, n.close(e, !0)), i.attr("title") && (i.uniqueId(), n.parents[this.id] = {
                    element: this,
                    title: i.attr("title")
                }, i.attr("title", ""))
            }), this._updateContent(i, e))
        },
        _updateContent: function(t, e) {
            var n, i = this.options.content,
                r = this,
                o = e ? e.type : null;
            return "string" == typeof i ? this._open(e, t, i) : (n = i.call(t[0], function(n) {
                t.data("ui-tooltip-open") && r._delay(function() {
                    e && (e.type = o), this._open(e, t, n)
                })
            }), void(n && this._open(e, t, n)))
        },
        _open: function(n, i, r) {
            function o(t) {
                u.of = t, s.is(":hidden") || s.position(u)
            }
            var s, a, l, u = t.extend({}, this.options.position);
            if (r) {
                if (s = this._find(i), s.length) return void s.find(".ui-tooltip-content").html(r);
                i.is("[title]") && (n && "mouseover" === n.type ? i.attr("title", "") : i.removeAttr("title")), s = this._tooltip(i), e(i, s.attr("id")), s.find(".ui-tooltip-content").html(r), this.options.track && n && /^mouse/.test(n.type) ? (this._on(this.document, {
                    mousemove: o
                }), o(n)) : s.position(t.extend({
                    of: i
                }, this.options.position)), s.hide(), this._show(s, this.options.show), this.options.show && this.options.show.delay && (l = this.delayedShow = setInterval(function() {
                    s.is(":visible") && (o(u.of), clearInterval(l))
                }, t.fx.interval)), this._trigger("open", n, {
                    tooltip: s
                }), a = {
                    keyup: function(e) {
                        if (e.keyCode === t.ui.keyCode.ESCAPE) {
                            var n = t.Event(e);
                            n.currentTarget = i[0], this.close(n, !0)
                        }
                    },
                    remove: function() {
                        this._removeTooltip(s)
                    }
                }, n && "mouseover" !== n.type || (a.mouseleave = "close"), n && "focusin" !== n.type || (a.focusout = "close"), this._on(!0, i, a)
            }
        },
        close: function(e) {
            var i = this,
                r = t(e ? e.currentTarget : this.element),
                o = this._find(r);
            this.closing || (clearInterval(this.delayedShow), r.data("ui-tooltip-title") && r.attr("title", r.data("ui-tooltip-title")), n(r), o.stop(!0), this._hide(o, this.options.hide, function() {
                i._removeTooltip(t(this))
            }), r.removeData("ui-tooltip-open"), this._off(r, "mouseleave focusout keyup"), r[0] !== this.element[0] && this._off(r, "remove"), this._off(this.document, "mousemove"), e && "mouseleave" === e.type && t.each(this.parents, function(e, n) {
                t(n.element).attr("title", n.title), delete i.parents[e]
            }), this.closing = !0, this._trigger("close", e, {
                tooltip: o
            }), this.closing = !1)
        },
        _tooltip: function(e) {
            var n = "ui-tooltip-" + i++,
                r = t("<div>").attr({
                    id: n,
                    role: "tooltip"
                }).addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content " + (this.options.tooltipClass || ""));
            return t("<div>").addClass("ui-tooltip-content").appendTo(r), r.appendTo(this.document[0].body), this.tooltips[n] = e, r
        },
        _find: function(e) {
            var n = e.data("ui-tooltip-id");
            return n ? t("#" + n) : t()
        },
        _removeTooltip: function(t) {
            t.remove(), delete this.tooltips[t.attr("id")]
        },
        _destroy: function() {
            var e = this;
            t.each(this.tooltips, function(n, i) {
                var r = t.Event("blur");
                r.target = r.currentTarget = i[0], e.close(r, !0), t("#" + n).remove(), i.data("ui-tooltip-title") && (i.attr("title", i.data("ui-tooltip-title")), i.removeData("ui-tooltip-title"))
            })
        }
    })
}(jQuery);
