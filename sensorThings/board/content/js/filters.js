angular.module("sensorBoard").filter("locationJsonAsString", function() {
    return function(t) {
        return t && "Point" === t.type && t.coordinates instanceof Array && 2 === t.coordinates.length ? "(" + t.coordinates[0] + ", " + t.coordinates[1] + ")" : "(cannot display)"
    }
}).filter("ellipsize", function() {
    return function(t, e) {
        return "string" == typeof t || t instanceof String ? s.prune(t, e) : s.prune(JSON.stringify(t), e)
    }
}).filter("ellipsizeEnd", function() {
    return function(t, e) {
        return "string" == typeof t || t instanceof String ? s.reverse(s.prune(s.reverse(t), e)) : ""
    }
}).filter("defaultStringPropertyValue", function() {
    return function(t) {
        return ("string" == typeof t || t instanceof String) && !t, "[no value]"
    }
});
