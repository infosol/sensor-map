! function(t, e, n) {
    var i = t.L,
        o = {};
    o.version = "0.7.7", "object" == typeof module && "object" == typeof module.exports ? module.exports = o : "function" == typeof define && define.amd && define(o), o.noConflict = function() {
            return t.L = i, this
        }, t.L = o, o.Util = {
            extend: function(t) {
                var e, n, i, o, r = Array.prototype.slice.call(arguments, 1);
                for (n = 0, i = r.length; i > n; n++) {
                    o = r[n] || {};
                    for (e in o) o.hasOwnProperty(e) && (t[e] = o[e])
                }
                return t
            },
            bind: function(t, e) {
                var n = arguments.length > 2 ? Array.prototype.slice.call(arguments, 2) : null;
                return function() {
                    return t.apply(e, n || arguments)
                }
            },
            stamp: function() {
                var t = 0,
                    e = "_leaflet_id";
                return function(n) {
                    return n[e] = n[e] || ++t, n[e]
                }
            }(),
            invokeEach: function(t, e, n) {
                var i, o;
                if ("object" == typeof t) {
                    o = Array.prototype.slice.call(arguments, 3);
                    for (i in t) e.apply(n, [i, t[i]].concat(o));
                    return !0
                }
                return !1
            },
            limitExecByInterval: function(t, e, n) {
                var i, o;
                return function r() {
                    var s = arguments;
                    return i ? void(o = !0) : (i = !0, setTimeout(function() {
                        i = !1, o && (r.apply(n, s), o = !1)
                    }, e), void t.apply(n, s))
                }
            },
            falseFn: function() {
                return !1
            },
            formatNum: function(t, e) {
                var n = Math.pow(10, e || 5);
                return Math.round(t * n) / n
            },
            trim: function(t) {
                return t.trim ? t.trim() : t.replace(/^\s+|\s+$/g, "")
            },
            splitWords: function(t) {
                return o.Util.trim(t).split(/\s+/)
            },
            setOptions: function(t, e) {
                return t.options = o.extend({}, t.options, e), t.options
            },
            getParamString: function(t, e, n) {
                var i = [];
                for (var o in t) i.push(encodeURIComponent(n ? o.toUpperCase() : o) + "=" + encodeURIComponent(t[o]));
                return (e && -1 !== e.indexOf("?") ? "&" : "?") + i.join("&")
            },
            template: function(t, e) {
                return t.replace(/\{ *([\w_]+) *\}/g, function(t, i) {
                    var o = e[i];
                    if (o === n) throw new Error("No value provided for variable " + t);
                    return "function" == typeof o && (o = o(e)), o
                })
            },
            isArray: Array.isArray || function(t) {
                return "[object Array]" === Object.prototype.toString.call(t)
            },
            emptyImageUrl: "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
        },
        function() {
            function e(e) {
                var n, i, o = ["webkit", "moz", "o", "ms"];
                for (n = 0; n < o.length && !i; n++) i = t[o[n] + e];
                return i
            }

            function n(e) {
                var n = +new Date,
                    o = Math.max(0, 16 - (n - i));
                return i = n + o, t.setTimeout(e, o)
            }
            var i = 0,
                r = t.requestAnimationFrame || e("RequestAnimationFrame") || n,
                s = t.cancelAnimationFrame || e("CancelAnimationFrame") || e("CancelRequestAnimationFrame") || function(e) {
                    t.clearTimeout(e)
                };
            o.Util.requestAnimFrame = function(e, i, s, a) {
                return e = o.bind(e, i), s && r === n ? void e() : r.call(t, e, a)
            }, o.Util.cancelAnimFrame = function(e) {
                e && s.call(t, e)
            }
        }(), o.extend = o.Util.extend, o.bind = o.Util.bind, o.stamp = o.Util.stamp, o.setOptions = o.Util.setOptions, o.Class = function() {}, o.Class.extend = function(t) {
            var e = function() {
                    this.initialize && this.initialize.apply(this, arguments), this._initHooks && this.callInitHooks()
                },
                n = function() {};
            n.prototype = this.prototype;
            var i = new n;
            i.constructor = e, e.prototype = i;
            for (var r in this) this.hasOwnProperty(r) && "prototype" !== r && (e[r] = this[r]);
            t.statics && (o.extend(e, t.statics), delete t.statics), t.includes && (o.Util.extend.apply(null, [i].concat(t.includes)), delete t.includes), t.options && i.options && (t.options = o.extend({}, i.options, t.options)), o.extend(i, t), i._initHooks = [];
            var s = this;
            return e.__super__ = s.prototype, i.callInitHooks = function() {
                if (!this._initHooksCalled) {
                    s.prototype.callInitHooks && s.prototype.callInitHooks.call(this), this._initHooksCalled = !0;
                    for (var t = 0, e = i._initHooks.length; e > t; t++) i._initHooks[t].call(this)
                }
            }, e
        }, o.Class.include = function(t) {
            o.extend(this.prototype, t)
        }, o.Class.mergeOptions = function(t) {
            o.extend(this.prototype.options, t)
        }, o.Class.addInitHook = function(t) {
            var e = Array.prototype.slice.call(arguments, 1),
                n = "function" == typeof t ? t : function() {
                    this[t].apply(this, e)
                };
            this.prototype._initHooks = this.prototype._initHooks || [], this.prototype._initHooks.push(n)
        };
    var r = "_leaflet_events";
    o.Mixin = {}, o.Mixin.Events = {
            addEventListener: function(t, e, n) {
                if (o.Util.invokeEach(t, this.addEventListener, this, e, n)) return this;
                var i, s, a, u, l, c, h, f = this[r] = this[r] || {},
                    d = n && n !== this && o.stamp(n);
                for (t = o.Util.splitWords(t), i = 0, s = t.length; s > i; i++) a = {
                    action: e,
                    context: n || this
                }, u = t[i], d ? (l = u + "_idx", c = l + "_len", h = f[l] = f[l] || {}, h[d] || (h[d] = [], f[c] = (f[c] || 0) + 1), h[d].push(a)) : (f[u] = f[u] || [], f[u].push(a));
                return this
            },
            hasEventListeners: function(t) {
                var e = this[r];
                return !!e && (t in e && e[t].length > 0 || t + "_idx" in e && e[t + "_idx_len"] > 0)
            },
            removeEventListener: function(t, e, n) {
                if (!this[r]) return this;
                if (!t) return this.clearAllEventListeners();
                if (o.Util.invokeEach(t, this.removeEventListener, this, e, n)) return this;
                var i, s, a, u, l, c, h, f, d, p = this[r],
                    m = n && n !== this && o.stamp(n);
                for (t = o.Util.splitWords(t), i = 0, s = t.length; s > i; i++)
                    if (a = t[i], c = a + "_idx", h = c + "_len", f = p[c], e) {
                        if (u = m && f ? f[m] : p[a]) {
                            for (l = u.length - 1; l >= 0; l--) u[l].action !== e || n && u[l].context !== n || (d = u.splice(l, 1), d[0].action = o.Util.falseFn);
                            n && f && 0 === u.length && (delete f[m], p[h]--)
                        }
                    } else delete p[a], delete p[c], delete p[h];
                return this
            },
            clearAllEventListeners: function() {
                return delete this[r], this
            },
            fireEvent: function(t, e) {
                if (!this.hasEventListeners(t)) return this;
                var n, i, s, a, u, l = o.Util.extend({}, e, {
                        type: t,
                        target: this
                    }),
                    c = this[r];
                if (c[t])
                    for (n = c[t].slice(), i = 0, s = n.length; s > i; i++) n[i].action.call(n[i].context, l);
                a = c[t + "_idx"];
                for (u in a)
                    if (n = a[u].slice())
                        for (i = 0, s = n.length; s > i; i++) n[i].action.call(n[i].context, l);
                return this
            },
            addOneTimeEventListener: function(t, e, n) {
                if (o.Util.invokeEach(t, this.addOneTimeEventListener, this, e, n)) return this;
                var i = o.bind(function() {
                    this.removeEventListener(t, e, n).removeEventListener(t, i, n)
                }, this);
                return this.addEventListener(t, e, n).addEventListener(t, i, n)
            }
        }, o.Mixin.Events.on = o.Mixin.Events.addEventListener, o.Mixin.Events.off = o.Mixin.Events.removeEventListener, o.Mixin.Events.once = o.Mixin.Events.addOneTimeEventListener, o.Mixin.Events.fire = o.Mixin.Events.fireEvent,
        function() {
            var i = "ActiveXObject" in t,
                r = i && !e.addEventListener,
                s = navigator.userAgent.toLowerCase(),
                a = -1 !== s.indexOf("webkit"),
                u = -1 !== s.indexOf("chrome"),
                l = -1 !== s.indexOf("phantom"),
                c = -1 !== s.indexOf("android"),
                h = -1 !== s.search("android [23]"),
                f = -1 !== s.indexOf("gecko"),
                d = typeof orientation != n + "",
                p = !t.PointerEvent && t.MSPointerEvent,
                m = t.PointerEvent && t.navigator.pointerEnabled || p,
                g = "devicePixelRatio" in t && t.devicePixelRatio > 1 || "matchMedia" in t && t.matchMedia("(min-resolution:144dpi)") && t.matchMedia("(min-resolution:144dpi)").matches,
                v = e.documentElement,
                y = i && "transition" in v.style,
                $ = "WebKitCSSMatrix" in t && "m11" in new t.WebKitCSSMatrix && !h,
                _ = "MozPerspective" in v.style,
                b = "OTransition" in v.style,
                x = !t.L_DISABLE_3D && (y || $ || _ || b) && !l,
                w = !t.L_NO_TOUCH && !l && (m || "ontouchstart" in t || t.DocumentTouch && e instanceof t.DocumentTouch);
            o.Browser = {
                ie: i,
                ielt9: r,
                webkit: a,
                gecko: f && !a && !t.opera && !i,
                android: c,
                android23: h,
                chrome: u,
                ie3d: y,
                webkit3d: $,
                gecko3d: _,
                opera3d: b,
                any3d: x,
                mobile: d,
                mobileWebkit: d && a,
                mobileWebkit3d: d && $,
                mobileOpera: d && t.opera,
                touch: w,
                msPointer: p,
                pointer: m,
                retina: g
            }
        }(), o.Point = function(t, e, n) {
            this.x = n ? Math.round(t) : t, this.y = n ? Math.round(e) : e
        }, o.Point.prototype = {
            clone: function() {
                return new o.Point(this.x, this.y)
            },
            add: function(t) {
                return this.clone()._add(o.point(t))
            },
            _add: function(t) {
                return this.x += t.x, this.y += t.y, this
            },
            subtract: function(t) {
                return this.clone()._subtract(o.point(t))
            },
            _subtract: function(t) {
                return this.x -= t.x, this.y -= t.y, this
            },
            divideBy: function(t) {
                return this.clone()._divideBy(t)
            },
            _divideBy: function(t) {
                return this.x /= t, this.y /= t, this
            },
            multiplyBy: function(t) {
                return this.clone()._multiplyBy(t)
            },
            _multiplyBy: function(t) {
                return this.x *= t, this.y *= t, this
            },
            round: function() {
                return this.clone()._round()
            },
            _round: function() {
                return this.x = Math.round(this.x), this.y = Math.round(this.y), this
            },
            floor: function() {
                return this.clone()._floor()
            },
            _floor: function() {
                return this.x = Math.floor(this.x), this.y = Math.floor(this.y), this
            },
            distanceTo: function(t) {
                t = o.point(t);
                var e = t.x - this.x,
                    n = t.y - this.y;
                return Math.sqrt(e * e + n * n)
            },
            equals: function(t) {
                return t = o.point(t), t.x === this.x && t.y === this.y
            },
            contains: function(t) {
                return t = o.point(t), Math.abs(t.x) <= Math.abs(this.x) && Math.abs(t.y) <= Math.abs(this.y)
            },
            toString: function() {
                return "Point(" + o.Util.formatNum(this.x) + ", " + o.Util.formatNum(this.y) + ")"
            }
        }, o.point = function(t, e, i) {
            return t instanceof o.Point ? t : o.Util.isArray(t) ? new o.Point(t[0], t[1]) : t === n || null === t ? t : new o.Point(t, e, i)
        }, o.Bounds = function(t, e) {
            if (t)
                for (var n = e ? [t, e] : t, i = 0, o = n.length; o > i; i++) this.extend(n[i])
        }, o.Bounds.prototype = {
            extend: function(t) {
                return t = o.point(t), this.min || this.max ? (this.min.x = Math.min(t.x, this.min.x), this.max.x = Math.max(t.x, this.max.x), this.min.y = Math.min(t.y, this.min.y), this.max.y = Math.max(t.y, this.max.y)) : (this.min = t.clone(), this.max = t.clone()), this
            },
            getCenter: function(t) {
                return new o.Point((this.min.x + this.max.x) / 2, (this.min.y + this.max.y) / 2, t)
            },
            getBottomLeft: function() {
                return new o.Point(this.min.x, this.max.y)
            },
            getTopRight: function() {
                return new o.Point(this.max.x, this.min.y)
            },
            getSize: function() {
                return this.max.subtract(this.min)
            },
            contains: function(t) {
                var e, n;
                return t = "number" == typeof t[0] || t instanceof o.Point ? o.point(t) : o.bounds(t), t instanceof o.Bounds ? (e = t.min, n = t.max) : e = n = t, e.x >= this.min.x && n.x <= this.max.x && e.y >= this.min.y && n.y <= this.max.y
            },
            intersects: function(t) {
                t = o.bounds(t);
                var e = this.min,
                    n = this.max,
                    i = t.min,
                    r = t.max,
                    s = r.x >= e.x && i.x <= n.x,
                    a = r.y >= e.y && i.y <= n.y;
                return s && a
            },
            isValid: function() {
                return !(!this.min || !this.max)
            }
        }, o.bounds = function(t, e) {
            return !t || t instanceof o.Bounds ? t : new o.Bounds(t, e)
        }, o.Transformation = function(t, e, n, i) {
            this._a = t, this._b = e, this._c = n, this._d = i
        }, o.Transformation.prototype = {
            transform: function(t, e) {
                return this._transform(t.clone(), e)
            },
            _transform: function(t, e) {
                return e = e || 1, t.x = e * (this._a * t.x + this._b), t.y = e * (this._c * t.y + this._d), t
            },
            untransform: function(t, e) {
                return e = e || 1, new o.Point((t.x / e - this._b) / this._a, (t.y / e - this._d) / this._c)
            }
        }, o.DomUtil = {
            get: function(t) {
                return "string" == typeof t ? e.getElementById(t) : t
            },
            getStyle: function(t, n) {
                var i = t.style[n];
                if (!i && t.currentStyle && (i = t.currentStyle[n]), (!i || "auto" === i) && e.defaultView) {
                    var o = e.defaultView.getComputedStyle(t, null);
                    i = o ? o[n] : null
                }
                return "auto" === i ? null : i
            },
            getViewportOffset: function(t) {
                var n, i = 0,
                    r = 0,
                    s = t,
                    a = e.body,
                    u = e.documentElement;
                do {
                    if (i += s.offsetTop || 0, r += s.offsetLeft || 0, i += parseInt(o.DomUtil.getStyle(s, "borderTopWidth"), 10) || 0, r += parseInt(o.DomUtil.getStyle(s, "borderLeftWidth"), 10) || 0, n = o.DomUtil.getStyle(s, "position"), s.offsetParent === a && "absolute" === n) break;
                    if ("fixed" === n) {
                        i += a.scrollTop || u.scrollTop || 0, r += a.scrollLeft || u.scrollLeft || 0;
                        break
                    }
                    if ("relative" === n && !s.offsetLeft) {
                        var l = o.DomUtil.getStyle(s, "width"),
                            c = o.DomUtil.getStyle(s, "max-width"),
                            h = s.getBoundingClientRect();
                        ("none" !== l || "none" !== c) && (r += h.left + s.clientLeft), i += h.top + (a.scrollTop || u.scrollTop || 0);
                        break
                    }
                    s = s.offsetParent
                } while (s);
                s = t;
                do {
                    if (s === a) break;
                    i -= s.scrollTop || 0, r -= s.scrollLeft || 0, s = s.parentNode
                } while (s);
                return new o.Point(r, i)
            },
            documentIsLtr: function() {
                return o.DomUtil._docIsLtrCached || (o.DomUtil._docIsLtrCached = !0, o.DomUtil._docIsLtr = "ltr" === o.DomUtil.getStyle(e.body, "direction")), o.DomUtil._docIsLtr
            },
            create: function(t, n, i) {
                var o = e.createElement(t);
                return o.className = n, i && i.appendChild(o), o
            },
            hasClass: function(t, e) {
                if (t.classList !== n) return t.classList.contains(e);
                var i = o.DomUtil._getClass(t);
                return i.length > 0 && new RegExp("(^|\\s)" + e + "(\\s|$)").test(i)
            },
            addClass: function(t, e) {
                if (t.classList !== n)
                    for (var i = o.Util.splitWords(e), r = 0, s = i.length; s > r; r++) t.classList.add(i[r]);
                else if (!o.DomUtil.hasClass(t, e)) {
                    var a = o.DomUtil._getClass(t);
                    o.DomUtil._setClass(t, (a ? a + " " : "") + e)
                }
            },
            removeClass: function(t, e) {
                t.classList !== n ? t.classList.remove(e) : o.DomUtil._setClass(t, o.Util.trim((" " + o.DomUtil._getClass(t) + " ").replace(" " + e + " ", " ")))
            },
            _setClass: function(t, e) {
                t.className.baseVal === n ? t.className = e : t.className.baseVal = e
            },
            _getClass: function(t) {
                return t.className.baseVal === n ? t.className : t.className.baseVal
            },
            setOpacity: function(t, e) {
                if ("opacity" in t.style) t.style.opacity = e;
                else if ("filter" in t.style) {
                    var n = !1,
                        i = "DXImageTransform.Microsoft.Alpha";
                    try {
                        n = t.filters.item(i)
                    } catch (o) {
                        if (1 === e) return
                    }
                    e = Math.round(100 * e), n ? (n.Enabled = 100 !== e, n.Opacity = e) : t.style.filter += " progid:" + i + "(opacity=" + e + ")"
                }
            },
            testProp: function(t) {
                for (var n = e.documentElement.style, i = 0; i < t.length; i++)
                    if (t[i] in n) return t[i];
                return !1
            },
            getTranslateString: function(t) {
                var e = o.Browser.webkit3d,
                    n = "translate" + (e ? "3d" : "") + "(",
                    i = (e ? ",0" : "") + ")";
                return n + t.x + "px," + t.y + "px" + i
            },
            getScaleString: function(t, e) {
                var n = o.DomUtil.getTranslateString(e.add(e.multiplyBy(-1 * t))),
                    i = " scale(" + t + ") ";
                return n + i
            },
            setPosition: function(t, e, n) {
                t._leaflet_pos = e, !n && o.Browser.any3d ? t.style[o.DomUtil.TRANSFORM] = o.DomUtil.getTranslateString(e) : (t.style.left = e.x + "px", t.style.top = e.y + "px")
            },
            getPosition: function(t) {
                return t._leaflet_pos
            }
        }, o.DomUtil.TRANSFORM = o.DomUtil.testProp(["transform", "WebkitTransform", "OTransform", "MozTransform", "msTransform"]), o.DomUtil.TRANSITION = o.DomUtil.testProp(["webkitTransition", "transition", "OTransition", "MozTransition", "msTransition"]), o.DomUtil.TRANSITION_END = "webkitTransition" === o.DomUtil.TRANSITION || "OTransition" === o.DomUtil.TRANSITION ? o.DomUtil.TRANSITION + "End" : "transitionend",
        function() {
            if ("onselectstart" in e) o.extend(o.DomUtil, {
                disableTextSelection: function() {
                    o.DomEvent.on(t, "selectstart", o.DomEvent.preventDefault)
                },
                enableTextSelection: function() {
                    o.DomEvent.off(t, "selectstart", o.DomEvent.preventDefault)
                }
            });
            else {
                var n = o.DomUtil.testProp(["userSelect", "WebkitUserSelect", "OUserSelect", "MozUserSelect", "msUserSelect"]);
                o.extend(o.DomUtil, {
                    disableTextSelection: function() {
                        if (n) {
                            var t = e.documentElement.style;
                            this._userSelect = t[n], t[n] = "none"
                        }
                    },
                    enableTextSelection: function() {
                        n && (e.documentElement.style[n] = this._userSelect, delete this._userSelect)
                    }
                })
            }
            o.extend(o.DomUtil, {
                disableImageDrag: function() {
                    o.DomEvent.on(t, "dragstart", o.DomEvent.preventDefault)
                },
                enableImageDrag: function() {
                    o.DomEvent.off(t, "dragstart", o.DomEvent.preventDefault)
                }
            })
        }(), o.LatLng = function(t, e, i) {
            if (t = parseFloat(t), e = parseFloat(e), isNaN(t) || isNaN(e)) console.error( "Invalid LatLng object : (" + t + ", " + e + ")" ) // throw new Error("Invalid LatLng object: (" + t + ", " + e + ")"); -- Rac021 
            this.lat = t, this.lng = e, i !== n && (this.alt = parseFloat(i))
        }, o.extend(o.LatLng, {
            DEG_TO_RAD: Math.PI / 180,
            RAD_TO_DEG: 180 / Math.PI,
            MAX_MARGIN: 1e-9
        }), o.LatLng.prototype = {
            equals: function(t) {
                if (!t) return !1;
                t = o.latLng(t);
                var e = Math.max(Math.abs(this.lat - t.lat), Math.abs(this.lng - t.lng));
                return e <= o.LatLng.MAX_MARGIN
            },
            toString: function(t) {
                return "LatLng(" + o.Util.formatNum(this.lat, t) + ", " + o.Util.formatNum(this.lng, t) + ")"
            },
            distanceTo: function(t) {
                t = o.latLng(t);
                var e = 6378137,
                    n = o.LatLng.DEG_TO_RAD,
                    i = (t.lat - this.lat) * n,
                    r = (t.lng - this.lng) * n,
                    s = this.lat * n,
                    a = t.lat * n,
                    u = Math.sin(i / 2),
                    l = Math.sin(r / 2),
                    c = u * u + l * l * Math.cos(s) * Math.cos(a);
                return 2 * e * Math.atan2(Math.sqrt(c), Math.sqrt(1 - c))
            },
            wrap: function(t, e) {
                var n = this.lng;
                return t = t || -180, e = e || 180, n = (n + e) % (e - t) + (t > n || n === e ? e : t), new o.LatLng(this.lat, n)
            }
        }, o.latLng = function(t, e) {
            return t instanceof o.LatLng ? t : o.Util.isArray(t) ? "number" == typeof t[0] || "string" == typeof t[0] ? new o.LatLng(t[0], t[1], t[2]) : null : t === n || null === t ? t : "object" == typeof t && "lat" in t ? new o.LatLng(t.lat, "lng" in t ? t.lng : t.lon) : e === n ? null : new o.LatLng(t, e)
        }, o.LatLngBounds = function(t, e) {
            if (t)
                for (var n = e ? [t, e] : t, i = 0, o = n.length; o > i; i++) this.extend(n[i])
        }, o.LatLngBounds.prototype = {
            extend: function(t) {
                if (!t) return this;
                var e = o.latLng(t);
                return t = null !== e ? e : o.latLngBounds(t), t instanceof o.LatLng ? this._southWest || this._northEast ? (this._southWest.lat = Math.min(t.lat, this._southWest.lat), this._southWest.lng = Math.min(t.lng, this._southWest.lng), this._northEast.lat = Math.max(t.lat, this._northEast.lat), this._northEast.lng = Math.max(t.lng, this._northEast.lng)) : (this._southWest = new o.LatLng(t.lat, t.lng), this._northEast = new o.LatLng(t.lat, t.lng)) : t instanceof o.LatLngBounds && (this.extend(t._southWest), this.extend(t._northEast)), this
            },
            pad: function(t) {
                var e = this._southWest,
                    n = this._northEast,
                    i = Math.abs(e.lat - n.lat) * t,
                    r = Math.abs(e.lng - n.lng) * t;
                return new o.LatLngBounds(new o.LatLng(e.lat - i, e.lng - r), new o.LatLng(n.lat + i, n.lng + r))
            },
            getCenter: function() {
                return new o.LatLng((this._southWest.lat + this._northEast.lat) / 2, (this._southWest.lng + this._northEast.lng) / 2)
            },
            getSouthWest: function() {
                return ( this._southWest == null ) ? "" : this._southWest
            },
            getNorthEast: function() {
                return ( this._northEast == null ) ? "" : this._northEast
            },
            getNorthWest: function() {
                return new o.LatLng(this.getNorth(), this.getWest())
            },
            getSouthEast: function() {
                return new o.LatLng(this.getSouth(), this.getEast())
            },
            getWest: function() {
                return ( this._southWest == null ) ? null : this._southWest.lng
            },
            getSouth: function() {
                return ( this._southWest == null ) ? null : this._southWest.lat
            },
            getEast: function() {
                return ( this._northEast == null ) ? null : this._northEast.lng
            },
            getNorth: function() {
                return ( this._northEast == null ) ? null : this._northEast.lat 
            },
            contains: function(t) {
                t = "number" == typeof t[0] || t instanceof o.LatLng ? o.latLng(t) : o.latLngBounds(t);
                var e, n, i = this._southWest,
                    r = this._northEast;
                return t instanceof o.LatLngBounds ? (e = t.getSouthWest(), n = t.getNorthEast()) : e = n = t, e.lat >= i.lat && n.lat <= r.lat && e.lng >= i.lng && n.lng <= r.lng
            },
            intersects: function(t) {
                t = o.latLngBounds(t);
                var e = this._southWest,
                    n = this._northEast,
                    i = t.getSouthWest(),
                    r = t.getNorthEast(),
                    s = r.lat >= e.lat && i.lat <= n.lat,
                    a = r.lng >= e.lng && i.lng <= n.lng;
                return s && a
            },
            toBBoxString: function() {
                return [this.getWest(), this.getSouth(), this.getEast(), this.getNorth()].join(",")
            },
            equals: function(t) {
                return t ? (t = o.latLngBounds(t), this._southWest.equals(t.getSouthWest()) && this._northEast.equals(t.getNorthEast())) : !1
            },
            isValid: function() {
                return !(!this._southWest || !this._northEast)
            }
        }, o.latLngBounds = function(t, e) {
            return !t || t instanceof o.LatLngBounds ? t : new o.LatLngBounds(t, e)
        }, o.Projection = {}, o.Projection.SphericalMercator = {
            MAX_LATITUDE: 85.0511287798,
            project: function(t) {
                var e = o.LatLng.DEG_TO_RAD,
                    n = this.MAX_LATITUDE,
                    i = Math.max(Math.min( n, ( t == null ) ? 0 : t.lat ), -n ), // Rac021
                    r = ( ( t == null ) ? 0 : t.lng ) * e, // Rac021
                    s = i * e;
                return s = Math.log(Math.tan(Math.PI / 4 + s / 2)), new o.Point(r, s)
            },
            unproject: function(t) {
                var e = o.LatLng.RAD_TO_DEG,
                    n = t.x * e,
                    i = (2 * Math.atan(Math.exp(t.y)) - Math.PI / 2) * e;
                return new o.LatLng(i, n)
            }
        }, o.Projection.LonLat = {
            project: function(t) {
                return new o.Point(t.lng, t.lat)
            },
            unproject: function(t) {
                return new o.LatLng(t.y, t.x)
            }
        }, o.CRS = {
            latLngToPoint: function(t, e) {
                var n = this.projection.project(t),
                    i = this.scale(e);
                return this.transformation._transform(n, i)
            },
            pointToLatLng: function(t, e) {
                var n = this.scale(e),
                    i = this.transformation.untransform(t, n);
                return this.projection.unproject(i)
            },
            project: function(t) {
                return this.projection.project(t)
            },
            scale: function(t) {
                return 256 * Math.pow(2, t)
            },
            getSize: function(t) {
                var e = this.scale(t);
                return o.point(e, e)
            }
        }, o.CRS.Simple = o.extend({}, o.CRS, {
            projection: o.Projection.LonLat,
            transformation: new o.Transformation(1, 0, -1, 0),
            scale: function(t) {
                return Math.pow(2, t)
            }
        }), o.CRS.EPSG3857 = o.extend({}, o.CRS, {
            code: "EPSG:3857",
            projection: o.Projection.SphericalMercator,
            transformation: new o.Transformation(.5 / Math.PI, .5, -.5 / Math.PI, .5),
            project: function(t) {
                var e = this.projection.project(t),
                    n = 6378137;
                return e.multiplyBy(n)
            }
        }), o.CRS.EPSG900913 = o.extend({}, o.CRS.EPSG3857, {
            code: "EPSG:900913"
        }), o.CRS.EPSG4326 = o.extend({}, o.CRS, {
            code: "EPSG:4326",
            projection: o.Projection.LonLat,
            transformation: new o.Transformation(1 / 360, .5, -1 / 360, .5)
        }), o.Map = o.Class.extend({
            includes: o.Mixin.Events,
            options: {
                crs: o.CRS.EPSG3857,
                fadeAnimation: o.DomUtil.TRANSITION && !o.Browser.android23,
                trackResize: !0,
                markerZoomAnimation: o.DomUtil.TRANSITION && o.Browser.any3d
            },
            initialize: function(t, e) {
                e = o.setOptions(this, e), this._initContainer(t), this._initLayout(), this._onResize = o.bind(this._onResize, this), this._initEvents(), e.maxBounds && this.setMaxBounds(e.maxBounds), e.center && e.zoom !== n && this.setView(o.latLng(e.center), e.zoom, {
                    reset: !0
                }), this._handlers = [], this._layers = {}, this._zoomBoundLayers = {}, this._tileLayersNum = 0, this.callInitHooks(), this._addLayers(e.layers)
            },
            setView: function(t, e) {
                return e = e === n ? this.getZoom() : e, this._resetView(o.latLng(t), this._limitZoom(e)), this
            },
            setZoom: function(t, e) {
                return this._loaded ? this.setView(this.getCenter(), t, {
                    zoom: e
                }) : (this._zoom = this._limitZoom(t), this)
            },
            zoomIn: function(t, e) {
                return this.setZoom(this._zoom + (t || 1), e)
            },
            zoomOut: function(t, e) {
                return this.setZoom(this._zoom - (t || 1), e)
            },
            setZoomAround: function(t, e, n) {
                var i = this.getZoomScale(e),
                    r = this.getSize().divideBy(2),
                    s = t instanceof o.Point ? t : this.latLngToContainerPoint(t),
                    a = s.subtract(r).multiplyBy(1 - 1 / i),
                    u = this.containerPointToLatLng(r.add(a));
                return this.setView(u, e, {
                    zoom: n
                })
            },
            fitBounds: function(t, e) {
                e = e || {}, t = t.getBounds ? t.getBounds() : o.latLngBounds(t);
                var n = o.point(e.paddingTopLeft || e.padding || [0, 0]),
                    i = o.point(e.paddingBottomRight || e.padding || [0, 0]),
                    r = this.getBoundsZoom(t, !1, n.add(i));
                r = e.maxZoom ? Math.min(e.maxZoom, r) : r;
                var s = i.subtract(n).divideBy(2),
                    a = this.project(t.getSouthWest(), r),
                    u = this.project(t.getNorthEast(), r),
                    l = this.unproject(a.add(u).divideBy(2).add(s), r);
                return this.setView(l, r, e)
            },
            fitWorld: function(t) {
                return this.fitBounds([
                    [-90, -180],
                    [90, 180]
                ], t)
            },
            panTo: function(t, e) {
                return this.setView(t, this._zoom, {
                    pan: e
                })
            },
            panBy: function(t) {
                return this.fire("movestart"), this._rawPanBy(o.point(t)), this.fire("move"), this.fire("moveend")
            },
            setMaxBounds: function(t) {
                return t = o.latLngBounds(t), this.options.maxBounds = t, t ? (this._loaded && this._panInsideMaxBounds(), this.on("moveend", this._panInsideMaxBounds, this)) : this.off("moveend", this._panInsideMaxBounds, this)
            },
            panInsideBounds: function(t, e) {
                var n = this.getCenter(),
                    i = this._limitCenter(n, this._zoom, t);
                return n.equals(i) ? this : this.panTo(i, e)
            },
            addLayer: function(t) {
                var e = o.stamp(t);
                return this._layers[e] ? this : (this._layers[e] = t, !t.options || isNaN(t.options.maxZoom) && isNaN(t.options.minZoom) || (this._zoomBoundLayers[e] = t, this._updateZoomLevels()), this.options.zoomAnimation && o.TileLayer && t instanceof o.TileLayer && (this._tileLayersNum++, this._tileLayersToLoad++, t.on("load", this._onTileLayerLoad, this)), this._loaded && this._layerAdd(t), this)
            },
            removeLayer: function(t) {
                var e = o.stamp(t);
                return this._layers[e] ? (this._loaded && t.onRemove(this), delete this._layers[e], this._loaded && this.fire("layerremove", {
                    layer: t
                }), this._zoomBoundLayers[e] && (delete this._zoomBoundLayers[e], this._updateZoomLevels()), this.options.zoomAnimation && o.TileLayer && t instanceof o.TileLayer && (this._tileLayersNum--, this._tileLayersToLoad--, t.off("load", this._onTileLayerLoad, this)), this) : this
            },
            hasLayer: function(t) {
                return t ? o.stamp(t) in this._layers : !1
            },
            eachLayer: function(t, e) {
                for (var n in this._layers) t.call(e, this._layers[n]);
                return this
            },
            invalidateSize: function(t) {
                if (!this._loaded) return this;
                t = o.extend({
                    animate: !1,
                    pan: !0
                }, t === !0 ? {
                    animate: !0
                } : t);
                var e = this.getSize();
                this._sizeChanged = !0, this._initialCenter = null;
                var n = this.getSize(),
                    i = e.divideBy(2).round(),
                    r = n.divideBy(2).round(),
                    s = i.subtract(r);
                return s.x || s.y ? (t.animate && t.pan ? this.panBy(s) : (t.pan && this._rawPanBy(s), this.fire("move"), t.debounceMoveend ? (clearTimeout(this._sizeTimer), this._sizeTimer = setTimeout(o.bind(this.fire, this, "moveend"), 200)) : this.fire("moveend")), this.fire("resize", {
                    oldSize: e,
                    newSize: n
                })) : this
            },
            addHandler: function(t, e) {
                if (!e) return this;
                var n = this[t] = new e(this);
                return this._handlers.push(n), this.options[t] && n.enable(), this
            },
            remove: function() {
                this._loaded && this.fire("unload"), this._initEvents("off");
                try {
                    delete this._container._leaflet
                } catch (t) {
                    this._container._leaflet = n
                }
                return this._clearPanes(), this._clearControlPos && this._clearControlPos(), this._clearHandlers(), this
            },
            getCenter: function() {
                return this._checkIfLoaded(), this._initialCenter && !this._moved() ? this._initialCenter : this.layerPointToLatLng(this._getCenterLayerPoint())
            },
            getZoom: function() {
                return this._zoom
            },
            getBounds: function() {
                var t = this.getPixelBounds(),
                    e = this.unproject(t.getBottomLeft()),
                    n = this.unproject(t.getTopRight());
                return new o.LatLngBounds(e, n)
            },
            getMinZoom: function() {
                return this.options.minZoom === n ? this._layersMinZoom === n ? 0 : this._layersMinZoom : this.options.minZoom
            },
            getMaxZoom: function() {
                return this.options.maxZoom === n ? this._layersMaxZoom === n ? 1 / 0 : this._layersMaxZoom : this.options.maxZoom
            },
            getBoundsZoom: function(t, e, n) {
                t = o.latLngBounds(t);
                var i, r = this.getMinZoom() - (e ? 1 : 0),
                    s = this.getMaxZoom(),
                    a = this.getSize(),
                    u = t.getNorthWest(),
                    l = t.getSouthEast(),
                    c = !0;
                n = o.point(n || [0, 0]);
                do r++, i = this.project(l, r).subtract(this.project(u, r)).add(n), c = e ? i.x < a.x || i.y < a.y : a.contains(i); while (c && s >= r);
                return c && e ? null : e ? r : r - 1
            },
            getSize: function() {
                return (!this._size || this._sizeChanged) && (this._size = new o.Point(this._container.clientWidth, this._container.clientHeight), this._sizeChanged = !1), this._size.clone()
            },
            getPixelBounds: function() {
                var t = this._getTopLeftPoint();
                return new o.Bounds(t, t.add(this.getSize()))
            },
            getPixelOrigin: function() {
                return this._checkIfLoaded(), this._initialTopLeftPoint
            },
            getPanes: function() {
                return this._panes
            },
            getContainer: function() {
                return this._container
            },
            getZoomScale: function(t) {
                var e = this.options.crs;
                return e.scale(t) / e.scale(this._zoom)
            },
            getScaleZoom: function(t) {
                return this._zoom + Math.log(t) / Math.LN2
            },
            project: function(t, e) {
                return e = e === n ? this._zoom : e, this.options.crs.latLngToPoint(o.latLng(t), e)
            },
            unproject: function(t, e) {
                return e = e === n ? this._zoom : e, this.options.crs.pointToLatLng(o.point(t), e)
            },
            layerPointToLatLng: function(t) {
                var e = o.point(t).add(this.getPixelOrigin());
                return this.unproject(e)
            },
            latLngToLayerPoint: function(t) {
                var e = this.project(o.latLng(t))._round();
                return e._subtract(this.getPixelOrigin())
            },
            containerPointToLayerPoint: function(t) {
                return o.point(t).subtract(this._getMapPanePos())
            },
            layerPointToContainerPoint: function(t) {
                return o.point(t).add(this._getMapPanePos())
            },
            containerPointToLatLng: function(t) {
                var e = this.containerPointToLayerPoint(o.point(t));
                return this.layerPointToLatLng(e)
            },
            latLngToContainerPoint: function(t) {
                return this.layerPointToContainerPoint(this.latLngToLayerPoint(o.latLng(t)))
            },
            mouseEventToContainerPoint: function(t) {
                return o.DomEvent.getMousePosition(t, this._container)
            },
            mouseEventToLayerPoint: function(t) {
                return this.containerPointToLayerPoint(this.mouseEventToContainerPoint(t))
            },
            mouseEventToLatLng: function(t) {
                return this.layerPointToLatLng(this.mouseEventToLayerPoint(t))
            },
            _initContainer: function(t) {
                var e = this._container = o.DomUtil.get(t);
                if (!e) throw new Error("Map container not found.");
                if (e._leaflet) throw new Error("Map container is already initialized.");
                e._leaflet = !0
            },
            _initLayout: function() {
                var t = this._container;
                o.DomUtil.addClass(t, "leaflet-container" + (o.Browser.touch ? " leaflet-touch" : "") + (o.Browser.retina ? " leaflet-retina" : "") + (o.Browser.ielt9 ? " leaflet-oldie" : "") + (this.options.fadeAnimation ? " leaflet-fade-anim" : ""));
                var e = o.DomUtil.getStyle(t, "position");
                "absolute" !== e && "relative" !== e && "fixed" !== e && (t.style.position = "relative"), this._initPanes(), this._initControlPos && this._initControlPos()
            },
            _initPanes: function() {
                var t = this._panes = {};
                this._mapPane = t.mapPane = this._createPane("leaflet-map-pane", this._container), this._tilePane = t.tilePane = this._createPane("leaflet-tile-pane", this._mapPane), t.objectsPane = this._createPane("leaflet-objects-pane", this._mapPane), t.shadowPane = this._createPane("leaflet-shadow-pane"), t.overlayPane = this._createPane("leaflet-overlay-pane"), t.markerPane = this._createPane("leaflet-marker-pane"), t.popupPane = this._createPane("leaflet-popup-pane");
                var e = " leaflet-zoom-hide";
                this.options.markerZoomAnimation || (o.DomUtil.addClass(t.markerPane, e), o.DomUtil.addClass(t.shadowPane, e), o.DomUtil.addClass(t.popupPane, e))
            },
            _createPane: function(t, e) {
                return o.DomUtil.create("div", t, e || this._panes.objectsPane)
            },
            _clearPanes: function() {
                this._container.removeChild(this._mapPane)
            },
            _addLayers: function(t) {
                t = t ? o.Util.isArray(t) ? t : [t] : [];
                for (var e = 0, n = t.length; n > e; e++) this.addLayer(t[e])
            },
            _resetView: function(t, e, n, i) {
                var r = this._zoom !== e;
                i || (this.fire("movestart"), r && this.fire("zoomstart")), this._zoom = e, this._initialCenter = t, this._initialTopLeftPoint = this._getNewTopLeftPoint(t), n ? this._initialTopLeftPoint._add(this._getMapPanePos()) : o.DomUtil.setPosition(this._mapPane, new o.Point(0, 0)), this._tileLayersToLoad = this._tileLayersNum;
                var s = !this._loaded;
                this._loaded = !0, this.fire("viewreset", {
                    hard: !n
                }), s && (this.fire("load"), this.eachLayer(this._layerAdd, this)), this.fire("move"), (r || i) && this.fire("zoomend"), this.fire("moveend", {
                    hard: !n
                })
            },
            _rawPanBy: function(t) {
                o.DomUtil.setPosition(this._mapPane, this._getMapPanePos().subtract(t))
            },
            _getZoomSpan: function() {
                return this.getMaxZoom() - this.getMinZoom()
            },
            _updateZoomLevels: function() {
                var t, e = 1 / 0,
                    i = -(1 / 0),
                    o = this._getZoomSpan();
                for (t in this._zoomBoundLayers) {
                    var r = this._zoomBoundLayers[t];
                    isNaN(r.options.minZoom) || (e = Math.min(e, r.options.minZoom)), isNaN(r.options.maxZoom) || (i = Math.max(i, r.options.maxZoom))
                }
                t === n ? this._layersMaxZoom = this._layersMinZoom = n : (this._layersMaxZoom = i, this._layersMinZoom = e), o !== this._getZoomSpan() && this.fire("zoomlevelschange")
            },
            _panInsideMaxBounds: function() {
                this.panInsideBounds(this.options.maxBounds)
            },
            _checkIfLoaded: function() {
                if (!this._loaded) throw new Error("Set map center and zoom first.")
            },
            _initEvents: function(e) {
                if (o.DomEvent) {
                    e = e || "on", o.DomEvent[e](this._container, "click", this._onMouseClick, this);
                    var n, i, r = ["dblclick", "mousedown", "mouseup", "mouseenter", "mouseleave", "mousemove", "contextmenu"];
                    for (n = 0, i = r.length; i > n; n++) o.DomEvent[e](this._container, r[n], this._fireMouseEvent, this);
                    this.options.trackResize && o.DomEvent[e](t, "resize", this._onResize, this)
                }
            },
            _onResize: function() {
                o.Util.cancelAnimFrame(this._resizeRequest), this._resizeRequest = o.Util.requestAnimFrame(function() {
                    this.invalidateSize({
                        debounceMoveend: !0
                    })
                }, this, !1, this._container)
            },
            _onMouseClick: function(t) {
                !this._loaded || !t._simulated && (this.dragging && this.dragging.moved() || this.boxZoom && this.boxZoom.moved()) || o.DomEvent._skipped(t) || (this.fire("preclick"), this._fireMouseEvent(t))
            },
            _fireMouseEvent: function(t) {
                if (this._loaded && !o.DomEvent._skipped(t)) {
                    var e = t.type;
                    if (e = "mouseenter" === e ? "mouseover" : "mouseleave" === e ? "mouseout" : e, this.hasEventListeners(e)) {
                        "contextmenu" === e && o.DomEvent.preventDefault(t);
                        var n = this.mouseEventToContainerPoint(t),
                            i = this.containerPointToLayerPoint(n),
                            r = this.layerPointToLatLng(i);
                        this.fire(e, {
                            latlng: r,
                            layerPoint: i,
                            containerPoint: n,
                            originalEvent: t
                        })
                    }
                }
            },
            _onTileLayerLoad: function() {
                this._tileLayersToLoad--, this._tileLayersNum && !this._tileLayersToLoad && this.fire("tilelayersload")
            },
            _clearHandlers: function() {
                for (var t = 0, e = this._handlers.length; e > t; t++) this._handlers[t].disable()
            },
            whenReady: function(t, e) {
                return this._loaded ? t.call(e || this, this) : this.on("load", t, e), this
            },
            _layerAdd: function(t) {
                t.onAdd(this), this.fire("layeradd", {
                    layer: t
                })
            },
            _getMapPanePos: function() {
                return o.DomUtil.getPosition(this._mapPane)
            },
            _moved: function() {
                var t = this._getMapPanePos();
                return t && !t.equals([0, 0])
            },
            _getTopLeftPoint: function() {
                return this.getPixelOrigin().subtract(this._getMapPanePos())
            },
            _getNewTopLeftPoint: function(t, e) {
                var n = this.getSize()._divideBy(2);
                return this.project(t, e)._subtract(n)._round()
            },
            _latLngToNewLayerPoint: function(t, e, n) {
                var i = this._getNewTopLeftPoint(n, e).add(this._getMapPanePos());
                return this.project(t, e)._subtract(i)
            },
            _getCenterLayerPoint: function() {
                return this.containerPointToLayerPoint(this.getSize()._divideBy(2))
            },
            _getCenterOffset: function(t) {
                return this.latLngToLayerPoint(t).subtract(this._getCenterLayerPoint())
            },
            _limitCenter: function(t, e, n) {
                if (!n) return t;
                var i = this.project(t, e),
                    r = this.getSize().divideBy(2),
                    s = new o.Bounds(i.subtract(r), i.add(r)),
                    a = this._getBoundsOffset(s, n, e);
                return this.unproject(i.add(a), e)
            },
            _limitOffset: function(t, e) {
                if (!e) return t;
                var n = this.getPixelBounds(),
                    i = new o.Bounds(n.min.add(t), n.max.add(t));
                return t.add(this._getBoundsOffset(i, e))
            },
            _getBoundsOffset: function(t, e, n) {
                var i = this.project(e.getNorthWest(), n).subtract(t.min),
                    r = this.project(e.getSouthEast(), n).subtract(t.max),
                    s = this._rebound(i.x, -r.x),
                    a = this._rebound(i.y, -r.y);
                return new o.Point(s, a)
            },
            _rebound: function(t, e) {
                return t + e > 0 ? Math.round(t - e) / 2 : Math.max(0, Math.ceil(t)) - Math.max(0, Math.floor(e))
            },
            _limitZoom: function(t) {
                var e = this.getMinZoom(),
                    n = this.getMaxZoom();
                return Math.max(e, Math.min(n, t))
            }
        }), o.map = function(t, e) {
            return new o.Map(t, e)
        }, o.Projection.Mercator = {
            MAX_LATITUDE: 85.0840591556,
            R_MINOR: 6356752.314245179,
            R_MAJOR: 6378137,
            project: function(t) {
                var e = o.LatLng.DEG_TO_RAD,
                    n = this.MAX_LATITUDE,
                    i = Math.max(Math.min(n, t.lat), -n),
                    r = this.R_MAJOR,
                    s = this.R_MINOR,
                    a = t.lng * e * r,
                    u = i * e,
                    l = s / r,
                    c = Math.sqrt(1 - l * l),
                    h = c * Math.sin(u);
                h = Math.pow((1 - h) / (1 + h), .5 * c);
                var f = Math.tan(.5 * (.5 * Math.PI - u)) / h;
                return u = -r * Math.log(f), new o.Point(a, u)
            },
            unproject: function(t) {
                for (var e, n = o.LatLng.RAD_TO_DEG, i = this.R_MAJOR, r = this.R_MINOR, s = t.x * n / i, a = r / i, u = Math.sqrt(1 - a * a), l = Math.exp(-t.y / i), c = Math.PI / 2 - 2 * Math.atan(l), h = 15, f = 1e-7, d = h, p = .1; Math.abs(p) > f && --d > 0;) e = u * Math.sin(c), p = Math.PI / 2 - 2 * Math.atan(l * Math.pow((1 - e) / (1 + e), .5 * u)) - c, c += p;
                return new o.LatLng(c * n, s)
            }
        }, o.CRS.EPSG3395 = o.extend({}, o.CRS, {
            code: "EPSG:3395",
            projection: o.Projection.Mercator,
            transformation: function() {
                var t = o.Projection.Mercator,
                    e = t.R_MAJOR,
                    n = .5 / (Math.PI * e);
                return new o.Transformation(n, .5, -n, .5)
            }()
        }), o.TileLayer = o.Class.extend({
            includes: o.Mixin.Events,
            options: {
                minZoom: 0,
                maxZoom: 18,
                tileSize: 256,
                subdomains: "abc",
                errorTileUrl: "",
                attribution: "",
                zoomOffset: 0,
                opacity: 1,
                unloadInvisibleTiles: o.Browser.mobile,
                updateWhenIdle: o.Browser.mobile
            },
            initialize: function(t, e) {
                e = o.setOptions(this, e), e.detectRetina && o.Browser.retina && e.maxZoom > 0 && (e.tileSize = Math.floor(e.tileSize / 2), e.zoomOffset++, e.minZoom > 0 && e.minZoom--, this.options.maxZoom--), e.bounds && (e.bounds = o.latLngBounds(e.bounds)), this._url = t;
                var n = this.options.subdomains;
                "string" == typeof n && (this.options.subdomains = n.split(""))
            },
            onAdd: function(t) {
                this._map = t, this._animated = t._zoomAnimated, this._initContainer(), t.on({
                    viewreset: this._reset,
                    moveend: this._update
                }, this), this._animated && t.on({
                    zoomanim: this._animateZoom,
                    zoomend: this._endZoomAnim
                }, this), this.options.updateWhenIdle || (this._limitedUpdate = o.Util.limitExecByInterval(this._update, 150, this), t.on("move", this._limitedUpdate, this)), this._reset(), this._update()
            },
            addTo: function(t) {
                return t.addLayer(this), this
            },
            onRemove: function(t) {
                this._container.parentNode.removeChild(this._container), t.off({
                    viewreset: this._reset,
                    moveend: this._update
                }, this), this._animated && t.off({
                    zoomanim: this._animateZoom,
                    zoomend: this._endZoomAnim
                }, this), this.options.updateWhenIdle || t.off("move", this._limitedUpdate, this), this._container = null, this._map = null
            },
            bringToFront: function() {
                var t = this._map._panes.tilePane;
                return this._container && (t.appendChild(this._container), this._setAutoZIndex(t, Math.max)), this
            },
            bringToBack: function() {
                var t = this._map._panes.tilePane;
                return this._container && (t.insertBefore(this._container, t.firstChild), this._setAutoZIndex(t, Math.min)), this
            },
            getAttribution: function() {
                return this.options.attribution
            },
            getContainer: function() {
                return this._container
            },
            setOpacity: function(t) {
                return this.options.opacity = t, this._map && this._updateOpacity(), this
            },
            setZIndex: function(t) {
                return this.options.zIndex = t, this._updateZIndex(), this
            },
            setUrl: function(t, e) {
                return this._url = t, e || this.redraw(), this
            },
            redraw: function() {
                return this._map && (this._reset({
                    hard: !0
                }), this._update()), this
            },
            _updateZIndex: function() {
                this._container && this.options.zIndex !== n && (this._container.style.zIndex = this.options.zIndex)
            },
            _setAutoZIndex: function(t, e) {
                var n, i, o, r = t.children,
                    s = -e(1 / 0, -(1 / 0));
                for (i = 0, o = r.length; o > i; i++) r[i] !== this._container && (n = parseInt(r[i].style.zIndex, 10), isNaN(n) || (s = e(s, n)));
                this.options.zIndex = this._container.style.zIndex = (isFinite(s) ? s : 0) + e(1, -1)
            },
            _updateOpacity: function() {
                var t, e = this._tiles;
                if (o.Browser.ielt9)
                    for (t in e) o.DomUtil.setOpacity(e[t], this.options.opacity);
                else o.DomUtil.setOpacity(this._container, this.options.opacity)
            },
            _initContainer: function() {
                var t = this._map._panes.tilePane;
                if (!this._container) {
                    if (this._container = o.DomUtil.create("div", "leaflet-layer"), this._updateZIndex(), this._animated) {
                        var e = "leaflet-tile-container";
                        this._bgBuffer = o.DomUtil.create("div", e, this._container), this._tileContainer = o.DomUtil.create("div", e, this._container)
                    } else this._tileContainer = this._container;
                    t.appendChild(this._container), this.options.opacity < 1 && this._updateOpacity()
                }
            },
            _reset: function(t) {
                for (var e in this._tiles) this.fire("tileunload", {
                    tile: this._tiles[e]
                });
                this._tiles = {}, this._tilesToLoad = 0, this.options.reuseTiles && (this._unusedTiles = []), this._tileContainer.innerHTML = "", this._animated && t && t.hard && this._clearBgBuffer(), this._initContainer()
            },
            _getTileSize: function() {
                var t = this._map,
                    e = t.getZoom() + this.options.zoomOffset,
                    n = this.options.maxNativeZoom,
                    i = this.options.tileSize;
                return n && e > n && (i = Math.round(t.getZoomScale(e) / t.getZoomScale(n) * i)), i
            },
            _update: function() {
                if (this._map) {
                    var t = this._map,
                        e = t.getPixelBounds(),
                        n = t.getZoom(),
                        i = this._getTileSize();
                    if (!(n > this.options.maxZoom || n < this.options.minZoom)) {
                        var r = o.bounds(e.min.divideBy(i)._floor(), e.max.divideBy(i)._floor());
                        this._addTilesFromCenterOut(r), (this.options.unloadInvisibleTiles || this.options.reuseTiles) && this._removeOtherTiles(r)
                    }
                }
            },
            _addTilesFromCenterOut: function(t) {
                var n, i, r, s = [],
                    a = t.getCenter();
                for (n = t.min.y; n <= t.max.y; n++)
                    for (i = t.min.x; i <= t.max.x; i++) r = new o.Point(i, n), this._tileShouldBeLoaded(r) && s.push(r);
                var u = s.length;
                if (0 !== u) {
                    s.sort(function(t, e) {
                        return t.distanceTo(a) - e.distanceTo(a)
                    });
                    var l = e.createDocumentFragment();
                    for (this._tilesToLoad || this.fire("loading"), this._tilesToLoad += u, i = 0; u > i; i++) this._addTile(s[i], l);
                    this._tileContainer.appendChild(l)
                }
            },
            _tileShouldBeLoaded: function(t) {
                if (t.x + ":" + t.y in this._tiles) return !1;
                var e = this.options;
                if (!e.continuousWorld) {
                    var n = this._getWrapTileNum();
                    if (e.noWrap && (t.x < 0 || t.x >= n.x) || t.y < 0 || t.y >= n.y) return !1
                }
                if (e.bounds) {
                    var i = this._getTileSize(),
                        o = t.multiplyBy(i),
                        r = o.add([i, i]),
                        s = this._map.unproject(o),
                        a = this._map.unproject(r);
                    if (e.continuousWorld || e.noWrap || (s = s.wrap(), a = a.wrap()), !e.bounds.intersects([s, a])) return !1
                }
                return !0
            },
            _removeOtherTiles: function(t) {
                var e, n, i, o;
                for (o in this._tiles) e = o.split(":"), n = parseInt(e[0], 10), i = parseInt(e[1], 10), (n < t.min.x || n > t.max.x || i < t.min.y || i > t.max.y) && this._removeTile(o)
            },
            _removeTile: function(t) {
                var e = this._tiles[t];
                this.fire("tileunload", {
                    tile: e,
                    url: e.src
                }), this.options.reuseTiles ? (o.DomUtil.removeClass(e, "leaflet-tile-loaded"), this._unusedTiles.push(e)) : e.parentNode === this._tileContainer && this._tileContainer.removeChild(e), o.Browser.android || (e.onload = null, e.src = o.Util.emptyImageUrl), delete this._tiles[t]
            },
            _addTile: function(t, e) {
                var n = this._getTilePos(t),
                    i = this._getTile();
                o.DomUtil.setPosition(i, n, o.Browser.chrome), this._tiles[t.x + ":" + t.y] = i, this._loadTile(i, t), i.parentNode !== this._tileContainer && e.appendChild(i)
            },
            _getZoomForUrl: function() {
                var t = this.options,
                    e = this._map.getZoom();
                return t.zoomReverse && (e = t.maxZoom - e), e += t.zoomOffset, t.maxNativeZoom ? Math.min(e, t.maxNativeZoom) : e
            },
            _getTilePos: function(t) {
                var e = this._map.getPixelOrigin(),
                    n = this._getTileSize();
                return t.multiplyBy(n).subtract(e)
            },
            getTileUrl: function(t) {
                return o.Util.template(this._url, o.extend({
                    s: this._getSubdomain(t),
                    z: t.z,
                    x: t.x,
                    y: t.y
                }, this.options))
            },
            _getWrapTileNum: function() {
                var t = this._map.options.crs,
                    e = t.getSize(this._map.getZoom());
                return e.divideBy(this._getTileSize())._floor()
            },
            _adjustTilePoint: function(t) {
                var e = this._getWrapTileNum();
                this.options.continuousWorld || this.options.noWrap || (t.x = (t.x % e.x + e.x) % e.x), this.options.tms && (t.y = e.y - t.y - 1), t.z = this._getZoomForUrl()
            },
            _getSubdomain: function(t) {
                var e = Math.abs(t.x + t.y) % this.options.subdomains.length;
                return this.options.subdomains[e]
            },
            _getTile: function() {
                if (this.options.reuseTiles && this._unusedTiles.length > 0) {
                    var t = this._unusedTiles.pop();
                    return this._resetTile(t), t
                }
                return this._createTile()
            },
            _resetTile: function() {},
            _createTile: function() {
                var t = o.DomUtil.create("img", "leaflet-tile");
                return t.style.width = t.style.height = this._getTileSize() + "px", t.galleryimg = "no", t.onselectstart = t.onmousemove = o.Util.falseFn, o.Browser.ielt9 && this.options.opacity !== n && o.DomUtil.setOpacity(t, this.options.opacity), o.Browser.mobileWebkit3d && (t.style.WebkitBackfaceVisibility = "hidden"), t
            },
            _loadTile: function(t, e) {
                t._layer = this, t.onload = this._tileOnLoad, t.onerror = this._tileOnError, this._adjustTilePoint(e), t.src = this.getTileUrl(e), this.fire("tileloadstart", {
                    tile: t,
                    url: t.src
                })
            },
            _tileLoaded: function() {
                this._tilesToLoad--, this._animated && o.DomUtil.addClass(this._tileContainer, "leaflet-zoom-animated"), this._tilesToLoad || (this.fire("load"), this._animated && (clearTimeout(this._clearBgBufferTimer), this._clearBgBufferTimer = setTimeout(o.bind(this._clearBgBuffer, this), 500)))
            },
            _tileOnLoad: function() {
                var t = this._layer;
                this.src !== o.Util.emptyImageUrl && (o.DomUtil.addClass(this, "leaflet-tile-loaded"), t.fire("tileload", {
                    tile: this,
                    url: this.src
                })), t._tileLoaded()
            },
            _tileOnError: function() {
                var t = this._layer;
                t.fire("tileerror", {
                    tile: this,
                    url: this.src
                });
                var e = t.options.errorTileUrl;
                e && (this.src = e), t._tileLoaded()
            }
        }), o.tileLayer = function(t, e) {
            return new o.TileLayer(t, e)
        }, o.TileLayer.WMS = o.TileLayer.extend({
            defaultWmsParams: {
                service: "WMS",
                request: "GetMap",
                version: "1.1.1",
                layers: "",
                styles: "",
                format: "image/jpeg",
                transparent: !1
            },
            initialize: function(t, e) {
                this._url = t;
                var n = o.extend({}, this.defaultWmsParams),
                    i = e.tileSize || this.options.tileSize;
                n.width = n.height = e.detectRetina && o.Browser.retina ? 2 * i : i;
                for (var r in e) this.options.hasOwnProperty(r) || "crs" === r || (n[r] = e[r]);
                this.wmsParams = n, o.setOptions(this, e)
            },
            onAdd: function(t) {
                this._crs = this.options.crs || t.options.crs, this._wmsVersion = parseFloat(this.wmsParams.version);
                var e = this._wmsVersion >= 1.3 ? "crs" : "srs";
                this.wmsParams[e] = this._crs.code, o.TileLayer.prototype.onAdd.call(this, t)
            },
            getTileUrl: function(t) {
                var e = this._map,
                    n = this.options.tileSize,
                    i = t.multiplyBy(n),
                    r = i.add([n, n]),
                    s = this._crs.project(e.unproject(i, t.z)),
                    a = this._crs.project(e.unproject(r, t.z)),
                    u = this._wmsVersion >= 1.3 && this._crs === o.CRS.EPSG4326 ? [a.y, s.x, s.y, a.x].join(",") : [s.x, a.y, a.x, s.y].join(","),
                    l = o.Util.template(this._url, {
                        s: this._getSubdomain(t)
                    });
                return l + o.Util.getParamString(this.wmsParams, l, !0) + "&BBOX=" + u
            },
            setParams: function(t, e) {
                return o.extend(this.wmsParams, t), e || this.redraw(), this
            }
        }), o.tileLayer.wms = function(t, e) {
            return new o.TileLayer.WMS(t, e)
        }, o.TileLayer.Canvas = o.TileLayer.extend({
            options: {
                async: !1
            },
            initialize: function(t) {
                o.setOptions(this, t)
            },
            redraw: function() {
                this._map && (this._reset({
                    hard: !0
                }), this._update());
                for (var t in this._tiles) this._redrawTile(this._tiles[t]);
                return this
            },
            _redrawTile: function(t) {
                this.drawTile(t, t._tilePoint, this._map._zoom)
            },
            _createTile: function() {
                var t = o.DomUtil.create("canvas", "leaflet-tile");
                return t.width = t.height = this.options.tileSize, t.onselectstart = t.onmousemove = o.Util.falseFn, t
            },
            _loadTile: function(t, e) {
                t._layer = this, t._tilePoint = e, this._redrawTile(t), this.options.async || this.tileDrawn(t)
            },
            drawTile: function() {},
            tileDrawn: function(t) {
                this._tileOnLoad.call(t)
            }
        }), o.tileLayer.canvas = function(t) {
            return new o.TileLayer.Canvas(t)
        }, o.ImageOverlay = o.Class.extend({
            includes: o.Mixin.Events,
            options: {
                opacity: 1
            },
            initialize: function(t, e, n) {
                this._url = t, this._bounds = o.latLngBounds(e), o.setOptions(this, n)
            },
            onAdd: function(t) {
                this._map = t, this._image || this._initImage(), t._panes.overlayPane.appendChild(this._image), t.on("viewreset", this._reset, this), t.options.zoomAnimation && o.Browser.any3d && t.on("zoomanim", this._animateZoom, this), this._reset()
            },
            onRemove: function(t) {
                t.getPanes().overlayPane.removeChild(this._image), t.off("viewreset", this._reset, this), t.options.zoomAnimation && t.off("zoomanim", this._animateZoom, this)
            },
            addTo: function(t) {
                return t.addLayer(this), this
            },
            setOpacity: function(t) {
                return this.options.opacity = t, this._updateOpacity(), this
            },
            bringToFront: function() {
                return this._image && this._map._panes.overlayPane.appendChild(this._image), this
            },
            bringToBack: function() {
                var t = this._map._panes.overlayPane;
                return this._image && t.insertBefore(this._image, t.firstChild), this
            },
            setUrl: function(t) {
                this._url = t, this._image.src = this._url
            },
            getAttribution: function() {
                return this.options.attribution
            },
            _initImage: function() {
                this._image = o.DomUtil.create("img", "leaflet-image-layer"), this._map.options.zoomAnimation && o.Browser.any3d ? o.DomUtil.addClass(this._image, "leaflet-zoom-animated") : o.DomUtil.addClass(this._image, "leaflet-zoom-hide"), this._updateOpacity(), o.extend(this._image, {
                    galleryimg: "no",
                    onselectstart: o.Util.falseFn,
                    onmousemove: o.Util.falseFn,
                    onload: o.bind(this._onImageLoad, this),
                    src: this._url
                })
            },
            _animateZoom: function(t) {
                var e = this._map,
                    n = this._image,
                    i = e.getZoomScale(t.zoom),
                    r = this._bounds.getNorthWest(),
                    s = this._bounds.getSouthEast(),
                    a = e._latLngToNewLayerPoint(r, t.zoom, t.center),
                    u = e._latLngToNewLayerPoint(s, t.zoom, t.center)._subtract(a),
                    l = a._add(u._multiplyBy(.5 * (1 - 1 / i)));
                n.style[o.DomUtil.TRANSFORM] = o.DomUtil.getTranslateString(l) + " scale(" + i + ") "
            },
            _reset: function() {
                var t = this._image,
                    e = this._map.latLngToLayerPoint(this._bounds.getNorthWest()),
                    n = this._map.latLngToLayerPoint(this._bounds.getSouthEast())._subtract(e);
                o.DomUtil.setPosition(t, e), t.style.width = n.x + "px", t.style.height = n.y + "px"
            },
            _onImageLoad: function() {
                this.fire("load")
            },
            _updateOpacity: function() {
                o.DomUtil.setOpacity(this._image, this.options.opacity)
            }
        }), o.imageOverlay = function(t, e, n) {
            return new o.ImageOverlay(t, e, n)
        }, o.Icon = o.Class.extend({
            options: {
                className: ""
            },
            initialize: function(t) {
                o.setOptions(this, t)
            },
            createIcon: function(t) {
                return this._createIcon("icon", t)
            },
            createShadow: function(t) {
                return this._createIcon("shadow", t)
            },
            _createIcon: function(t, e) {
                var n = this._getIconUrl(t);
                if (!n) {
                    if ("icon" === t) throw new Error("iconUrl not set in Icon options (see the docs).");
                    return null
                }
                var i;
                return i = e && "IMG" === e.tagName ? this._createImg(n, e) : this._createImg(n), this._setIconStyles(i, t), i
            },
            _setIconStyles: function(t, e) {
                var n, i = this.options,
                    r = o.point(i[e + "Size"]);
                n = o.point("shadow" === e ? i.shadowAnchor || i.iconAnchor : i.iconAnchor), !n && r && (n = r.divideBy(2, !0)), t.className = "leaflet-marker-" + e + " " + i.className, n && (t.style.marginLeft = -n.x + "px", t.style.marginTop = -n.y + "px"), r && (t.style.width = r.x + "px", t.style.height = r.y + "px")
            },
            _createImg: function(t, n) {
                return n = n || e.createElement("img"), n.src = t, n
            },
            _getIconUrl: function(t) {
                return o.Browser.retina && this.options[t + "RetinaUrl"] ? this.options[t + "RetinaUrl"] : this.options[t + "Url"]
            }
        }), o.icon = function(t) {
            return new o.Icon(t)
        }, o.Icon.Default = o.Icon.extend({
            options: {
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            },
            _getIconUrl: function(t) {
                var e = t + "Url";
                if (this.options[e]) return this.options[e];
                o.Browser.retina && "icon" === t && (t += "-2x");
                var n = o.Icon.Default.imagePath;
                if (!n) throw new Error("Couldn't autodetect L.Icon.Default.imagePath, set it manually.");
                return n + "/marker-" + t + ".png"
            }
        }), o.Icon.Default.imagePath = function() {
            var t, n, i, o, r, s = e.getElementsByTagName("script"),
                a = /[\/^]leaflet[\-\._]?([\w\-\._]*)\.js\??/;
            for (t = 0, n = s.length; n > t; t++)
                if (i = s[t].src, o = i.match(a)) return r = i.split(a)[0], (r ? r + "/" : "") + "images"
        }(), o.Marker = o.Class.extend({
            includes: o.Mixin.Events,
            options: {
                icon: new o.Icon.Default,
                title: "",
                alt: "",
                clickable: !0,
                draggable: !1,
                keyboard: !0,
                zIndexOffset: 0,
                opacity: 1,
                riseOnHover: !1,
                riseOffset: 250
            },
            initialize: function(t, e) {
                o.setOptions(this, e), this._latlng = o.latLng(t)
            },
            onAdd: function(t) {
                this._map = t, t.on("viewreset", this.update, this), this._initIcon(), this.update(), this.fire("add"), t.options.zoomAnimation && t.options.markerZoomAnimation && t.on("zoomanim", this._animateZoom, this)
            },
            addTo: function(t) {
                return t.addLayer(this), this
            },
            onRemove: function(t) {
                this.dragging && this.dragging.disable(), this._removeIcon(), this._removeShadow(), this.fire("remove"), t.off({
                    viewreset: this.update,
                    zoomanim: this._animateZoom
                }, this), this._map = null
            },
            getLatLng: function() {
                return this._latlng
            },
            setLatLng: function(t) {
                return this._latlng = o.latLng(t), this.update(), this.fire("move", {
                    latlng: this._latlng
                })
            },
            setZIndexOffset: function(t) {
                return this.options.zIndexOffset = t, this.update(), this
            },
            setIcon: function(t) {
                return this.options.icon = t, this._map && (this._initIcon(), this.update()), this._popup && this.bindPopup(this._popup), this
            },
            update: function() {
                return this._icon && this._setPos(this._map.latLngToLayerPoint(this._latlng).round()), this
            },
            _initIcon: function() {
                var t = this.options,
                    e = this._map,
                    n = e.options.zoomAnimation && e.options.markerZoomAnimation,
                    i = n ? "leaflet-zoom-animated" : "leaflet-zoom-hide",
                    r = t.icon.createIcon(this._icon),
                    s = !1;
                r !== this._icon && (this._icon && this._removeIcon(), s = !0, t.title && (r.title = t.title), t.alt && (r.alt = t.alt)), o.DomUtil.addClass(r, i), t.keyboard && (r.tabIndex = "0"), this._icon = r, this._initInteraction(), t.riseOnHover && o.DomEvent.on(r, "mouseover", this._bringToFront, this).on(r, "mouseout", this._resetZIndex, this);
                var a = t.icon.createShadow(this._shadow),
                    u = !1;
                a !== this._shadow && (this._removeShadow(), u = !0), a && o.DomUtil.addClass(a, i), this._shadow = a, t.opacity < 1 && this._updateOpacity();
                var l = this._map._panes;
                s && l.markerPane.appendChild(this._icon), a && u && l.shadowPane.appendChild(this._shadow)
            },
            _removeIcon: function() {
                this.options.riseOnHover && o.DomEvent.off(this._icon, "mouseover", this._bringToFront).off(this._icon, "mouseout", this._resetZIndex), this._map._panes.markerPane.removeChild(this._icon), this._icon = null
            },
            _removeShadow: function() {
                this._shadow && this._map._panes.shadowPane.removeChild(this._shadow), this._shadow = null
            },
            _setPos: function(t) {
                o.DomUtil.setPosition(this._icon, t), this._shadow && o.DomUtil.setPosition(this._shadow, t), this._zIndex = t.y + this.options.zIndexOffset, this._resetZIndex()
            },
            _updateZIndex: function(t) {
                this._icon.style.zIndex = this._zIndex + t
            },
            _animateZoom: function(t) {
                var e = this._map._latLngToNewLayerPoint(this._latlng, t.zoom, t.center).round();
                this._setPos(e)
            },
            _initInteraction: function() {
                if (this.options.clickable) {
                    var t = this._icon,
                        e = ["dblclick", "mousedown", "mouseover", "mouseout", "contextmenu"];
                    o.DomUtil.addClass(t, "leaflet-clickable"), o.DomEvent.on(t, "click", this._onMouseClick, this), o.DomEvent.on(t, "keypress", this._onKeyPress, this);
                    for (var n = 0; n < e.length; n++) o.DomEvent.on(t, e[n], this._fireMouseEvent, this);
                    o.Handler.MarkerDrag && (this.dragging = new o.Handler.MarkerDrag(this), this.options.draggable && this.dragging.enable())
                }
            },
            _onMouseClick: function(t) {
                var e = this.dragging && this.dragging.moved();
                (this.hasEventListeners(t.type) || e) && o.DomEvent.stopPropagation(t), e || (this.dragging && this.dragging._enabled || !this._map.dragging || !this._map.dragging.moved()) && this.fire(t.type, {
                    originalEvent: t,
                    latlng: this._latlng
                })
            },
            _onKeyPress: function(t) {
                13 === t.keyCode && this.fire("click", {
                    originalEvent: t,
                    latlng: this._latlng
                })
            },
            _fireMouseEvent: function(t) {
                this.fire(t.type, {
                    originalEvent: t,
                    latlng: this._latlng
                }), "contextmenu" === t.type && this.hasEventListeners(t.type) && o.DomEvent.preventDefault(t), "mousedown" !== t.type ? o.DomEvent.stopPropagation(t) : o.DomEvent.preventDefault(t)
            },
            setOpacity: function(t) {
                return this.options.opacity = t, this._map && this._updateOpacity(), this
            },
            _updateOpacity: function() {
                o.DomUtil.setOpacity(this._icon, this.options.opacity), this._shadow && o.DomUtil.setOpacity(this._shadow, this.options.opacity)
            },
            _bringToFront: function() {
                this._updateZIndex(this.options.riseOffset)
            },
            _resetZIndex: function() {
                this._updateZIndex(0)
            }
        }), o.marker = function(t, e) {
            return new o.Marker(t, e)
        }, o.DivIcon = o.Icon.extend({
            options: {
                iconSize: [12, 12],
                className: "leaflet-div-icon",
                html: !1
            },
            createIcon: function(t) {
                var n = t && "DIV" === t.tagName ? t : e.createElement("div"),
                    i = this.options;
                return n.innerHTML = i.html !== !1 ? i.html : "", i.bgPos && (n.style.backgroundPosition = -i.bgPos.x + "px " + -i.bgPos.y + "px"), this._setIconStyles(n, "icon"), n
            },
            createShadow: function() {
                return null
            }
        }), o.divIcon = function(t) {
            return new o.DivIcon(t)
        }, o.Map.mergeOptions({
            closePopupOnClick: !0
        }), o.Popup = o.Class.extend({
            includes: o.Mixin.Events,
            options: {
                minWidth: 50,
                maxWidth: 300,
                autoPan: !0,
                closeButton: !0,
                offset: [0, 7],
                autoPanPadding: [5, 5],
                keepInView: !1,
                className: "",
                zoomAnimation: !0
            },
            initialize: function(t, e) {
                o.setOptions(this, t), this._source = e, this._animated = o.Browser.any3d && this.options.zoomAnimation, this._isOpen = !1
            },
            onAdd: function(t) {
                this._map = t, this._container || this._initLayout();
                var e = t.options.fadeAnimation;
                e && o.DomUtil.setOpacity(this._container, 0), t._panes.popupPane.appendChild(this._container), t.on(this._getEvents(), this), this.update(), e && o.DomUtil.setOpacity(this._container, 1), this.fire("open"), t.fire("popupopen", {
                    popup: this
                }), this._source && this._source.fire("popupopen", {
                    popup: this
                })
            },
            addTo: function(t) {
                return t.addLayer(this), this
            },
            openOn: function(t) {
                return t.openPopup(this), this
            },
            onRemove: function(t) {
                t._panes.popupPane.removeChild(this._container), o.Util.falseFn(this._container.offsetWidth), t.off(this._getEvents(), this), t.options.fadeAnimation && o.DomUtil.setOpacity(this._container, 0), this._map = null, this.fire("close"), t.fire("popupclose", {
                    popup: this
                }), this._source && this._source.fire("popupclose", {
                    popup: this
                })
            },
            getLatLng: function() {
                return this._latlng
            },
            setLatLng: function(t) {
                return this._latlng = o.latLng(t), this._map && (this._updatePosition(), this._adjustPan()), this
            },
            getContent: function() {
                return this._content
            },
            setContent: function(t) {
                return this._content = t, this.update(), this
            },
            update: function() {
                this._map && (this._container.style.visibility = "hidden", this._updateContent(), this._updateLayout(), this._updatePosition(), this._container.style.visibility = "", this._adjustPan())
            },
            _getEvents: function() {
                var t = {
                    viewreset: this._updatePosition
                };
                return this._animated && (t.zoomanim = this._zoomAnimation), ("closeOnClick" in this.options ? this.options.closeOnClick : this._map.options.closePopupOnClick) && (t.preclick = this._close), this.options.keepInView && (t.moveend = this._adjustPan), t
            },
            _close: function() {
                this._map && this._map.closePopup(this)
            },
            _initLayout: function() {
                var t, e = "leaflet-popup",
                    n = e + " " + this.options.className + " leaflet-zoom-" + (this._animated ? "animated" : "hide"),
                    i = this._container = o.DomUtil.create("div", n);
                this.options.closeButton && (t = this._closeButton = o.DomUtil.create("a", e + "-close-button", i), t.href = "#close", t.innerHTML = "&#215;", o.DomEvent.disableClickPropagation(t), o.DomEvent.on(t, "click", this._onCloseButtonClick, this));
                var r = this._wrapper = o.DomUtil.create("div", e + "-content-wrapper", i);
                o.DomEvent.disableClickPropagation(r), this._contentNode = o.DomUtil.create("div", e + "-content", r), o.DomEvent.disableScrollPropagation(this._contentNode), o.DomEvent.on(r, "contextmenu", o.DomEvent.stopPropagation), this._tipContainer = o.DomUtil.create("div", e + "-tip-container", i), this._tip = o.DomUtil.create("div", e + "-tip", this._tipContainer)
            },
            _updateContent: function() {
                if (this._content) {
                    if ("string" == typeof this._content) this._contentNode.innerHTML = this._content;
                    else {
                        for (; this._contentNode.hasChildNodes();) this._contentNode.removeChild(this._contentNode.firstChild);
                        this._contentNode.appendChild(this._content)
                    }
                    this.fire("contentupdate")
                }
            },
            _updateLayout: function() {
                var t = this._contentNode,
                    e = t.style;
                e.width = "", e.whiteSpace = "nowrap";
                var n = t.offsetWidth;
                n = Math.min(n, this.options.maxWidth), n = Math.max(n, this.options.minWidth), e.width = n + 1 + "px", e.whiteSpace = "", e.height = "";
                var i = t.offsetHeight,
                    r = this.options.maxHeight,
                    s = "leaflet-popup-scrolled";
                r && i > r ? (e.height = r + "px", o.DomUtil.addClass(t, s)) : o.DomUtil.removeClass(t, s), this._containerWidth = this._container.offsetWidth
            },
            _updatePosition: function() {
                if (this._map) {
                    var t = this._map.latLngToLayerPoint(this._latlng),
                        e = this._animated,
                        n = o.point(this.options.offset);
                    e && o.DomUtil.setPosition(this._container, t), this._containerBottom = -n.y - (e ? 0 : t.y), this._containerLeft = -Math.round(this._containerWidth / 2) + n.x + (e ? 0 : t.x), this._container.style.bottom = this._containerBottom + "px", this._container.style.left = this._containerLeft + "px"
                }
            },
            _zoomAnimation: function(t) {
                var e = this._map._latLngToNewLayerPoint(this._latlng, t.zoom, t.center);
                o.DomUtil.setPosition(this._container, e)
            },
            _adjustPan: function() {
                if (this.options.autoPan) {
                    var t = this._map,
                        e = this._container.offsetHeight,
                        n = this._containerWidth,
                        i = new o.Point(this._containerLeft, -e - this._containerBottom);
                    this._animated && i._add(o.DomUtil.getPosition(this._container));
                    var r = t.layerPointToContainerPoint(i),
                        s = o.point(this.options.autoPanPadding),
                        a = o.point(this.options.autoPanPaddingTopLeft || s),
                        u = o.point(this.options.autoPanPaddingBottomRight || s),
                        l = t.getSize(),
                        c = 0,
                        h = 0;
                    r.x + n + u.x > l.x && (c = r.x + n - l.x + u.x), r.x - c - a.x < 0 && (c = r.x - a.x), r.y + e + u.y > l.y && (h = r.y + e - l.y + u.y), r.y - h - a.y < 0 && (h = r.y - a.y), (c || h) && t.fire("autopanstart").panBy([c, h])
                }
            },
            _onCloseButtonClick: function(t) {
                this._close(), o.DomEvent.stop(t)
            }
        }), o.popup = function(t, e) {
            return new o.Popup(t, e)
        }, o.Map.include({
            openPopup: function(t, e, n) {
                if (this.closePopup(), !(t instanceof o.Popup)) {
                    var i = t;
                    t = new o.Popup(n).setLatLng(e).setContent(i)
                }
                return t._isOpen = !0, this._popup = t, this.addLayer(t)
            },
            closePopup: function(t) {
                return t && t !== this._popup || (t = this._popup, this._popup = null), t && (this.removeLayer(t), t._isOpen = !1), this
            }
        }), o.Marker.include({
            openPopup: function() {
                return this._popup && this._map && !this._map.hasLayer(this._popup) && (this._popup.setLatLng(this._latlng), this._map.openPopup(this._popup)), this
            },
            closePopup: function() {
                return this._popup && this._popup._close(), this
            },
            togglePopup: function() {
                return this._popup && (this._popup._isOpen ? this.closePopup() : this.openPopup()), this
            },
            bindPopup: function(t, e) {
                var n = o.point(this.options.icon.options.popupAnchor || [0, 0]);
                return n = n.add(o.Popup.prototype.options.offset), e && e.offset && (n = n.add(e.offset)), e = o.extend({
                    offset: n
                }, e), this._popupHandlersAdded || (this.on("click", this.togglePopup, this).on("remove", this.closePopup, this).on("move", this._movePopup, this), this._popupHandlersAdded = !0), t instanceof o.Popup ? (o.setOptions(t, e), this._popup = t, t._source = this) : this._popup = new o.Popup(e, this).setContent(t), this
            },
            setPopupContent: function(t) {
                return this._popup && this._popup.setContent(t), this
            },
            unbindPopup: function() {
                return this._popup && (this._popup = null, this.off("click", this.togglePopup, this).off("remove", this.closePopup, this).off("move", this._movePopup, this), this._popupHandlersAdded = !1), this
            },
            getPopup: function() {
                return this._popup
            },
            _movePopup: function(t) {
                this._popup.setLatLng(t.latlng)
            }
        }), o.LayerGroup = o.Class.extend({
            initialize: function(t) {
                this._layers = {};
                var e, n;
                if (t)
                    for (e = 0, n = t.length; n > e; e++) this.addLayer(t[e])
            },
            addLayer: function(t) {
                var e = this.getLayerId(t);
                return this._layers[e] = t, this._map && this._map.addLayer(t), this
            },
            removeLayer: function(t) {
                var e = t in this._layers ? t : this.getLayerId(t);
                return this._map && this._layers[e] && this._map.removeLayer(this._layers[e]), delete this._layers[e], this
            },
            hasLayer: function(t) {
                return t ? t in this._layers || this.getLayerId(t) in this._layers : !1
            },
            clearLayers: function() {
                return this.eachLayer(this.removeLayer, this), this
            },
            invoke: function(t) {
                var e, n, i = Array.prototype.slice.call(arguments, 1);
                for (e in this._layers) n = this._layers[e], n[t] && n[t].apply(n, i);
                return this
            },
            onAdd: function(t) {
                this._map = t, this.eachLayer(t.addLayer, t)
            },
            onRemove: function(t) {
                this.eachLayer(t.removeLayer, t), this._map = null
            },
            addTo: function(t) {
                return t.addLayer(this), this
            },
            eachLayer: function(t, e) {
                for (var n in this._layers) t.call(e, this._layers[n]);
                return this
            },
            getLayer: function(t) {
                return this._layers[t]
            },
            getLayers: function() {
                var t = [];
                for (var e in this._layers) t.push(this._layers[e]);
                return t
            },
            setZIndex: function(t) {
                return this.invoke("setZIndex", t)
            },
            getLayerId: function(t) {
                return o.stamp(t)
            }
        }), o.layerGroup = function(t) {
            return new o.LayerGroup(t)
        }, o.FeatureGroup = o.LayerGroup.extend({
            includes: o.Mixin.Events,
            statics: {
                EVENTS: "click dblclick mouseover mouseout mousemove contextmenu popupopen popupclose"
            },
            addLayer: function(t) {
                return this.hasLayer(t) ? this : ("on" in t && t.on(o.FeatureGroup.EVENTS, this._propagateEvent, this), o.LayerGroup.prototype.addLayer.call(this, t), this._popupContent && t.bindPopup && t.bindPopup(this._popupContent, this._popupOptions), this.fire("layeradd", {
                    layer: t
                }))
            },
            removeLayer: function(t) {
                return this.hasLayer(t) ? (t in this._layers && (t = this._layers[t]), "off" in t && t.off(o.FeatureGroup.EVENTS, this._propagateEvent, this), o.LayerGroup.prototype.removeLayer.call(this, t), this._popupContent && this.invoke("unbindPopup"), this.fire("layerremove", {
                    layer: t
                })) : this
            },
            bindPopup: function(t, e) {
                return this._popupContent = t, this._popupOptions = e, this.invoke("bindPopup", t, e)
            },
            openPopup: function(t) {
                for (var e in this._layers) {
                    this._layers[e].openPopup(t);
                    break
                }
                return this
            },
            setStyle: function(t) {
                return this.invoke("setStyle", t)
            },
            bringToFront: function() {
                return this.invoke("bringToFront")
            },
            bringToBack: function() {
                return this.invoke("bringToBack")
            },
            getBounds: function() {
                var t = new o.LatLngBounds;
                return this.eachLayer(function(e) {
                    t.extend(e instanceof o.Marker ? e.getLatLng() : e.getBounds())
                }), t
            },
            _propagateEvent: function(t) {
                t = o.extend({
                    layer: t.target,
                    target: this
                }, t), this.fire(t.type, t)
            }
        }), o.featureGroup = function(t) {
            return new o.FeatureGroup(t)
        }, o.Path = o.Class.extend({
            includes: [o.Mixin.Events],
            statics: {
                CLIP_PADDING: function() {
                    var e = o.Browser.mobile ? 1280 : 2e3,
                        n = (e / Math.max(t.outerWidth, t.outerHeight) - 1) / 2;
                    return Math.max(0, Math.min(.5, n))
                }()
            },
            options: {
                stroke: !0,
                color: "#0033ff",
                dashArray: null,
                lineCap: null,
                lineJoin: null,
                weight: 5,
                opacity: .5,
                fill: !1,
                fillColor: null,
                fillOpacity: .2,
                clickable: !0
            },
            initialize: function(t) {
                o.setOptions(this, t)
            },
            onAdd: function(t) {
                this._map = t, this._container || (this._initElements(), this._initEvents()), this.projectLatlngs(), this._updatePath(), this._container && this._map._pathRoot.appendChild(this._container), this.fire("add"), t.on({
                    viewreset: this.projectLatlngs,
                    moveend: this._updatePath
                }, this)
            },
            addTo: function(t) {
                return t.addLayer(this), this
            },
            onRemove: function(t) {
                t._pathRoot.removeChild(this._container), this.fire("remove"), this._map = null, o.Browser.vml && (this._container = null, this._stroke = null, this._fill = null), t.off({
                    viewreset: this.projectLatlngs,
                    moveend: this._updatePath
                }, this)
            },
            projectLatlngs: function() {},
            setStyle: function(t) {
                return o.setOptions(this, t), this._container && this._updateStyle(), this
            },
            redraw: function() {
                return this._map && (this.projectLatlngs(), this._updatePath()), this
            }
        }), o.Map.include({
            _updatePathViewport: function() {
                var t = o.Path.CLIP_PADDING,
                    e = this.getSize(),
                    n = o.DomUtil.getPosition(this._mapPane),
                    i = n.multiplyBy(-1)._subtract(e.multiplyBy(t)._round()),
                    r = i.add(e.multiplyBy(1 + 2 * t)._round());
                this._pathViewport = new o.Bounds(i, r)
            }
        }), o.Path.SVG_NS = "http://www.w3.org/2000/svg", o.Browser.svg = !(!e.createElementNS || !e.createElementNS(o.Path.SVG_NS, "svg").createSVGRect), o.Path = o.Path.extend({
            statics: {
                SVG: o.Browser.svg
            },
            bringToFront: function() {
                var t = this._map._pathRoot,
                    e = this._container;
                return e && t.lastChild !== e && t.appendChild(e), this
            },
            bringToBack: function() {
                var t = this._map._pathRoot,
                    e = this._container,
                    n = t.firstChild;
                return e && n !== e && t.insertBefore(e, n), this
            },
            getPathString: function() {},
            _createElement: function(t) {
                return e.createElementNS(o.Path.SVG_NS, t)
            },
            _initElements: function() {
                this._map._initPathRoot(), this._initPath(), this._initStyle()
            },
            _initPath: function() {
                this._container = this._createElement("g"), this._path = this._createElement("path"), this.options.className && o.DomUtil.addClass(this._path, this.options.className), this._container.appendChild(this._path)
            },
            _initStyle: function() {
                this.options.stroke && (this._path.setAttribute("stroke-linejoin", "round"), this._path.setAttribute("stroke-linecap", "round")), this.options.fill && this._path.setAttribute("fill-rule", "evenodd"), this.options.pointerEvents && this._path.setAttribute("pointer-events", this.options.pointerEvents), this.options.clickable || this.options.pointerEvents || this._path.setAttribute("pointer-events", "none"), this._updateStyle()
            },
            _updateStyle: function() {
                this.options.stroke ? (this._path.setAttribute("stroke", this.options.color), this._path.setAttribute("stroke-opacity", this.options.opacity), this._path.setAttribute("stroke-width", this.options.weight), this.options.dashArray ? this._path.setAttribute("stroke-dasharray", this.options.dashArray) : this._path.removeAttribute("stroke-dasharray"), this.options.lineCap && this._path.setAttribute("stroke-linecap", this.options.lineCap), this.options.lineJoin && this._path.setAttribute("stroke-linejoin", this.options.lineJoin)) : this._path.setAttribute("stroke", "none"), this.options.fill ? (this._path.setAttribute("fill", this.options.fillColor || this.options.color), this._path.setAttribute("fill-opacity", this.options.fillOpacity)) : this._path.setAttribute("fill", "none")
            },
            _updatePath: function() {
                var t = this.getPathString();
                t || (t = "M0 0"), this._path.setAttribute("d", t)
            },
            _initEvents: function() {
                if (this.options.clickable) {
                    (o.Browser.svg || !o.Browser.vml) && o.DomUtil.addClass(this._path, "leaflet-clickable"), o.DomEvent.on(this._container, "click", this._onMouseClick, this);
                    for (var t = ["dblclick", "mousedown", "mouseover", "mouseout", "mousemove", "contextmenu"], e = 0; e < t.length; e++) o.DomEvent.on(this._container, t[e], this._fireMouseEvent, this)
                }
            },
            _onMouseClick: function(t) {
                this._map.dragging && this._map.dragging.moved() || this._fireMouseEvent(t)
            },
            _fireMouseEvent: function(t) {
                if (this._map && this.hasEventListeners(t.type)) {
                    var e = this._map,
                        n = e.mouseEventToContainerPoint(t),
                        i = e.containerPointToLayerPoint(n),
                        r = e.layerPointToLatLng(i);
                    this.fire(t.type, {
                        latlng: r,
                        layerPoint: i,
                        containerPoint: n,
                        originalEvent: t
                    }), "contextmenu" === t.type && o.DomEvent.preventDefault(t), "mousemove" !== t.type && o.DomEvent.stopPropagation(t)
                }
            }
        }), o.Map.include({
            _initPathRoot: function() {
                this._pathRoot || (this._pathRoot = o.Path.prototype._createElement("svg"), this._panes.overlayPane.appendChild(this._pathRoot), this.options.zoomAnimation && o.Browser.any3d ? (o.DomUtil.addClass(this._pathRoot, "leaflet-zoom-animated"), this.on({
                    zoomanim: this._animatePathZoom,
                    zoomend: this._endPathZoom
                })) : o.DomUtil.addClass(this._pathRoot, "leaflet-zoom-hide"), this.on("moveend", this._updateSvgViewport), this._updateSvgViewport())
            },
            _animatePathZoom: function(t) {
                var e = this.getZoomScale(t.zoom),
                    n = this._getCenterOffset(t.center)._multiplyBy(-e)._add(this._pathViewport.min);
                this._pathRoot.style[o.DomUtil.TRANSFORM] = o.DomUtil.getTranslateString(n) + " scale(" + e + ") ", this._pathZooming = !0
            },
            _endPathZoom: function() {
                this._pathZooming = !1
            },
            _updateSvgViewport: function() {
                if (!this._pathZooming) {
                    this._updatePathViewport();
                    var t = this._pathViewport,
                        e = t.min,
                        n = t.max,
                        i = n.x - e.x,
                        r = n.y - e.y,
                        s = this._pathRoot,
                        a = this._panes.overlayPane;
                    o.Browser.mobileWebkit && a.removeChild(s), o.DomUtil.setPosition(s, e), s.setAttribute("width", i), s.setAttribute("height", r), s.setAttribute("viewBox", [e.x, e.y, i, r].join(" ")), o.Browser.mobileWebkit && a.appendChild(s)
                }
            }
        }), o.Path.include({
            bindPopup: function(t, e) {
                return t instanceof o.Popup ? this._popup = t : ((!this._popup || e) && (this._popup = new o.Popup(e, this)), this._popup.setContent(t)), this._popupHandlersAdded || (this.on("click", this._openPopup, this).on("remove", this.closePopup, this), this._popupHandlersAdded = !0), this
            },
            unbindPopup: function() {
                return this._popup && (this._popup = null, this.off("click", this._openPopup).off("remove", this.closePopup), this._popupHandlersAdded = !1), this
            },
            openPopup: function(t) {
                return this._popup && (t = t || this._latlng || this._latlngs[Math.floor(this._latlngs.length / 2)], this._openPopup({
                    latlng: t
                })), this
            },
            closePopup: function() {
                return this._popup && this._popup._close(), this
            },
            _openPopup: function(t) {
                this._popup.setLatLng(t.latlng), this._map.openPopup(this._popup)
            }
        }), o.Browser.vml = !o.Browser.svg && function() {
            try {
                var t = e.createElement("div");
                t.innerHTML = '<v:shape adj="1"/>';
                var n = t.firstChild;
                return n.style.behavior = "url(#default#VML)", n && "object" == typeof n.adj
            } catch (i) {
                return !1
            }
        }(), o.Path = o.Browser.svg || !o.Browser.vml ? o.Path : o.Path.extend({
            statics: {
                VML: !0,
                CLIP_PADDING: .02
            },
            _createElement: function() {
                try {
                    return e.namespaces.add("lvml", "urn:schemas-microsoft-com:vml"),
                        function(t) {
                            return e.createElement("<lvml:" + t + ' class="lvml">')
                        }
                } catch (t) {
                    return function(t) {
                        return e.createElement("<" + t + ' xmlns="urn:schemas-microsoft.com:vml" class="lvml">')
                    }
                }
            }(),
            _initPath: function() {
                var t = this._container = this._createElement("shape");
                o.DomUtil.addClass(t, "leaflet-vml-shape" + (this.options.className ? " " + this.options.className : "")), this.options.clickable && o.DomUtil.addClass(t, "leaflet-clickable"), t.coordsize = "1 1", this._path = this._createElement("path"), t.appendChild(this._path), this._map._pathRoot.appendChild(t)
            },
            _initStyle: function() {
                this._updateStyle()
            },
            _updateStyle: function() {
                var t = this._stroke,
                    e = this._fill,
                    n = this.options,
                    i = this._container;
                i.stroked = n.stroke, i.filled = n.fill, n.stroke ? (t || (t = this._stroke = this._createElement("stroke"), t.endcap = "round", i.appendChild(t)), t.weight = n.weight + "px", t.color = n.color, t.opacity = n.opacity, t.dashStyle = n.dashArray ? o.Util.isArray(n.dashArray) ? n.dashArray.join(" ") : n.dashArray.replace(/( *, *)/g, " ") : "", n.lineCap && (t.endcap = n.lineCap.replace("butt", "flat")), n.lineJoin && (t.joinstyle = n.lineJoin)) : t && (i.removeChild(t), this._stroke = null), n.fill ? (e || (e = this._fill = this._createElement("fill"), i.appendChild(e)), e.color = n.fillColor || n.color, e.opacity = n.fillOpacity) : e && (i.removeChild(e), this._fill = null)
            },
            _updatePath: function() {
                var t = this._container.style;
                t.display = "none", this._path.v = this.getPathString() + " ", t.display = ""
            }
        }), o.Map.include(o.Browser.svg || !o.Browser.vml ? {} : {
            _initPathRoot: function() {
                if (!this._pathRoot) {
                    var t = this._pathRoot = e.createElement("div");
                    t.className = "leaflet-vml-container", this._panes.overlayPane.appendChild(t), this.on("moveend", this._updatePathViewport), this._updatePathViewport()
                }
            }
        }), o.Browser.canvas = function() {
            return !!e.createElement("canvas").getContext
        }(), o.Path = o.Path.SVG && !t.L_PREFER_CANVAS || !o.Browser.canvas ? o.Path : o.Path.extend({
            statics: {
                CANVAS: !0,
                SVG: !1
            },
            redraw: function() {
                return this._map && (this.projectLatlngs(), this._requestUpdate()), this
            },
            setStyle: function(t) {
                return o.setOptions(this, t), this._map && (this._updateStyle(), this._requestUpdate()), this
            },
            onRemove: function(t) {
                t.off("viewreset", this.projectLatlngs, this).off("moveend", this._updatePath, this), this.options.clickable && (this._map.off("click", this._onClick, this), this._map.off("mousemove", this._onMouseMove, this)), this._requestUpdate(), this.fire("remove"), this._map = null
            },
            _requestUpdate: function() {
                this._map && !o.Path._updateRequest && (o.Path._updateRequest = o.Util.requestAnimFrame(this._fireMapMoveEnd, this._map))
            },
            _fireMapMoveEnd: function() {
                o.Path._updateRequest = null, this.fire("moveend")
            },
            _initElements: function() {
                this._map._initPathRoot(), this._ctx = this._map._canvasCtx
            },
            _updateStyle: function() {
                var t = this.options;
                t.stroke && (this._ctx.lineWidth = t.weight, this._ctx.strokeStyle = t.color), t.fill && (this._ctx.fillStyle = t.fillColor || t.color), t.lineCap && (this._ctx.lineCap = t.lineCap), t.lineJoin && (this._ctx.lineJoin = t.lineJoin)
            },
            _drawPath: function() {
                var t, e, n, i, r, s;
                for (this._ctx.beginPath(), t = 0, n = this._parts.length; n > t; t++) {
                    for (e = 0, i = this._parts[t].length; i > e; e++) r = this._parts[t][e], s = (0 === e ? "move" : "line") + "To", this._ctx[s](r.x, r.y);
                    this instanceof o.Polygon && this._ctx.closePath()
                }
            },
            _checkIfEmpty: function() {
                return !this._parts.length
            },
            _updatePath: function() {
                if (!this._checkIfEmpty()) {
                    var t = this._ctx,
                        e = this.options;
                    this._drawPath(), t.save(), this._updateStyle(), e.fill && (t.globalAlpha = e.fillOpacity, t.fill(e.fillRule || "evenodd")), e.stroke && (t.globalAlpha = e.opacity, t.stroke()), t.restore()
                }
            },
            _initEvents: function() {
                this.options.clickable && (this._map.on("mousemove", this._onMouseMove, this), this._map.on("click dblclick contextmenu", this._fireMouseEvent, this))
            },
            _fireMouseEvent: function(t) {
                this._containsPoint(t.layerPoint) && this.fire(t.type, t)
            },
            _onMouseMove: function(t) {
                this._map && !this._map._animatingZoom && (this._containsPoint(t.layerPoint) ? (this._ctx.canvas.style.cursor = "pointer", this._mouseInside = !0, this.fire("mouseover", t)) : this._mouseInside && (this._ctx.canvas.style.cursor = "", this._mouseInside = !1, this.fire("mouseout", t)))
            }
        }), o.Map.include(o.Path.SVG && !t.L_PREFER_CANVAS || !o.Browser.canvas ? {} : {
            _initPathRoot: function() {
                var t, n = this._pathRoot;
                n || (n = this._pathRoot = e.createElement("canvas"), n.style.position = "absolute", t = this._canvasCtx = n.getContext("2d"), t.lineCap = "round", t.lineJoin = "round", this._panes.overlayPane.appendChild(n), this.options.zoomAnimation && (this._pathRoot.className = "leaflet-zoom-animated", this.on("zoomanim", this._animatePathZoom), this.on("zoomend", this._endPathZoom)), this.on("moveend", this._updateCanvasViewport), this._updateCanvasViewport())
            },
            _updateCanvasViewport: function() {
                if (!this._pathZooming) {
                    this._updatePathViewport();
                    var t = this._pathViewport,
                        e = t.min,
                        n = t.max.subtract(e),
                        i = this._pathRoot;
                    o.DomUtil.setPosition(i, e), i.width = n.x, i.height = n.y, i.getContext("2d").translate(-e.x, -e.y)
                }
            }
        }), o.LineUtil = {
            simplify: function(t, e) {
                if (!e || !t.length) return t.slice();
                var n = e * e;
                return t = this._reducePoints(t, n), t = this._simplifyDP(t, n)
            },
            pointToSegmentDistance: function(t, e, n) {
                return Math.sqrt(this._sqClosestPointOnSegment(t, e, n, !0))
            },
            closestPointOnSegment: function(t, e, n) {
                return this._sqClosestPointOnSegment(t, e, n)
            },
            _simplifyDP: function(t, e) {
                var i = t.length,
                    o = typeof Uint8Array != n + "" ? Uint8Array : Array,
                    r = new o(i);
                r[0] = r[i - 1] = 1, this._simplifyDPStep(t, r, e, 0, i - 1);
                var s, a = [];
                for (s = 0; i > s; s++) r[s] && a.push(t[s]);
                return a
            },
            _simplifyDPStep: function(t, e, n, i, o) {
                var r, s, a, u = 0;
                for (s = i + 1; o - 1 >= s; s++) a = this._sqClosestPointOnSegment(t[s], t[i], t[o], !0), a > u && (r = s, u = a);
                u > n && (e[r] = 1, this._simplifyDPStep(t, e, n, i, r), this._simplifyDPStep(t, e, n, r, o))
            },
            _reducePoints: function(t, e) {
                for (var n = [t[0]], i = 1, o = 0, r = t.length; r > i; i++) this._sqDist(t[i], t[o]) > e && (n.push(t[i]), o = i);
                return r - 1 > o && n.push(t[r - 1]), n
            },
            clipSegment: function(t, e, n, i) {
                var o, r, s, a = i ? this._lastCode : this._getBitCode(t, n),
                    u = this._getBitCode(e, n);
                for (this._lastCode = u;;) {
                    if (!(a | u)) return [t, e];
                    if (a & u) return !1;
                    o = a || u, r = this._getEdgeIntersection(t, e, o, n), s = this._getBitCode(r, n), o === a ? (t = r, a = s) : (e = r, u = s)
                }
            },
            _getEdgeIntersection: function(t, e, n, i) {
                var r = e.x - t.x,
                    s = e.y - t.y,
                    a = i.min,
                    u = i.max;
                return 8 & n ? new o.Point(t.x + r * (u.y - t.y) / s, u.y) : 4 & n ? new o.Point(t.x + r * (a.y - t.y) / s, a.y) : 2 & n ? new o.Point(u.x, t.y + s * (u.x - t.x) / r) : 1 & n ? new o.Point(a.x, t.y + s * (a.x - t.x) / r) : void 0
            },
            _getBitCode: function(t, e) {
                var n = 0;
                return t.x < e.min.x ? n |= 1 : t.x > e.max.x && (n |= 2), t.y < e.min.y ? n |= 4 : t.y > e.max.y && (n |= 8), n
            },
            _sqDist: function(t, e) {
                var n = e.x - t.x,
                    i = e.y - t.y;
                return n * n + i * i
            },
            _sqClosestPointOnSegment: function(t, e, n, i) {
                var r, s = e.x,
                    a = e.y,
                    u = n.x - s,
                    l = n.y - a,
                    c = u * u + l * l;
                return c > 0 && (r = ((t.x - s) * u + (t.y - a) * l) / c, r > 1 ? (s = n.x, a = n.y) : r > 0 && (s += u * r, a += l * r)), u = t.x - s, l = t.y - a, i ? u * u + l * l : new o.Point(s, a)
            }
        }, o.Polyline = o.Path.extend({
            initialize: function(t, e) {
                o.Path.prototype.initialize.call(this, e), this._latlngs = this._convertLatLngs(t)
            },
            options: {
                smoothFactor: 1,
                noClip: !1
            },
            projectLatlngs: function() {
                this._originalPoints = [];
                for (var t = 0, e = this._latlngs.length; e > t; t++) this._originalPoints[t] = this._map.latLngToLayerPoint(this._latlngs[t])
            },
            getPathString: function() {
                for (var t = 0, e = this._parts.length, n = ""; e > t; t++) n += this._getPathPartStr(this._parts[t]);
                return n
            },
            getLatLngs: function() {
                return this._latlngs
            },
            setLatLngs: function(t) {
                return this._latlngs = this._convertLatLngs(t), this.redraw()
            },
            addLatLng: function(t) {
                return this._latlngs.push(o.latLng(t)), this.redraw()
            },
            spliceLatLngs: function() {
                var t = [].splice.apply(this._latlngs, arguments);
                return this._convertLatLngs(this._latlngs, !0), this.redraw(), t
            },
            closestLayerPoint: function(t) {
                for (var e, n, i = 1 / 0, r = this._parts, s = null, a = 0, u = r.length; u > a; a++)
                    for (var l = r[a], c = 1, h = l.length; h > c; c++) {
                        e = l[c - 1], n = l[c];
                        var f = o.LineUtil._sqClosestPointOnSegment(t, e, n, !0);
                        i > f && (i = f, s = o.LineUtil._sqClosestPointOnSegment(t, e, n))
                    }
                return s && (s.distance = Math.sqrt(i)), s
            },
            getBounds: function() {
                return new o.LatLngBounds(this.getLatLngs())
            },
            _convertLatLngs: function(t, e) {
                var n, i, r = e ? t : [];
                for (n = 0, i = t.length; i > n; n++) {
                    if (o.Util.isArray(t[n]) && "number" != typeof t[n][0]) return;
                    r[n] = o.latLng(t[n])
                }
                return r
            },
            _initEvents: function() {
                o.Path.prototype._initEvents.call(this)
            },
            _getPathPartStr: function(t) {
                for (var e, n = o.Path.VML, i = 0, r = t.length, s = ""; r > i; i++) e = t[i], n && e._round(), s += (i ? "L" : "M") + e.x + " " + e.y;
                return s
            },
            _clipPoints: function() {
                var t, e, n, i = this._originalPoints,
                    r = i.length;
                if (this.options.noClip) return void(this._parts = [i]);
                this._parts = [];
                var s = this._parts,
                    a = this._map._pathViewport,
                    u = o.LineUtil;
                for (t = 0, e = 0; r - 1 > t; t++) n = u.clipSegment(i[t], i[t + 1], a, t), n && (s[e] = s[e] || [], s[e].push(n[0]), (n[1] !== i[t + 1] || t === r - 2) && (s[e].push(n[1]), e++))
            },
            _simplifyPoints: function() {
                for (var t = this._parts, e = o.LineUtil, n = 0, i = t.length; i > n; n++) t[n] = e.simplify(t[n], this.options.smoothFactor)
            },
            _updatePath: function() {
                this._map && (this._clipPoints(), this._simplifyPoints(), o.Path.prototype._updatePath.call(this))
            }
        }), o.polyline = function(t, e) {
            return new o.Polyline(t, e)
        }, o.PolyUtil = {}, o.PolyUtil.clipPolygon = function(t, e) {
            var n, i, r, s, a, u, l, c, h, f = [1, 4, 2, 8],
                d = o.LineUtil;
            for (i = 0, l = t.length; l > i; i++) t[i]._code = d._getBitCode(t[i], e);
            for (s = 0; 4 > s; s++) {
                for (c = f[s], n = [], i = 0, l = t.length, r = l - 1; l > i; r = i++) a = t[i], u = t[r], a._code & c ? u._code & c || (h = d._getEdgeIntersection(u, a, c, e), h._code = d._getBitCode(h, e), n.push(h)) : (u._code & c && (h = d._getEdgeIntersection(u, a, c, e), h._code = d._getBitCode(h, e), n.push(h)), n.push(a));
                t = n
            }
            return t
        }, o.Polygon = o.Polyline.extend({
            options: {
                fill: !0
            },
            initialize: function(t, e) {
                o.Polyline.prototype.initialize.call(this, t, e), this._initWithHoles(t)
            },
            _initWithHoles: function(t) {
                var e, n, i;
                if (t && o.Util.isArray(t[0]) && "number" != typeof t[0][0])
                    for (this._latlngs = this._convertLatLngs(t[0]), this._holes = t.slice(1), e = 0, n = this._holes.length; n > e; e++) i = this._holes[e] = this._convertLatLngs(this._holes[e]), i[0].equals(i[i.length - 1]) && i.pop();
                t = this._latlngs, t.length >= 2 && t[0].equals(t[t.length - 1]) && t.pop()
            },
            projectLatlngs: function() {
                if (o.Polyline.prototype.projectLatlngs.call(this), this._holePoints = [], this._holes) {
                    var t, e, n, i;
                    for (t = 0, n = this._holes.length; n > t; t++)
                        for (this._holePoints[t] = [], e = 0, i = this._holes[t].length; i > e; e++) this._holePoints[t][e] = this._map.latLngToLayerPoint(this._holes[t][e])
                }
            },
            setLatLngs: function(t) {
                return t && o.Util.isArray(t[0]) && "number" != typeof t[0][0] ? (this._initWithHoles(t), this.redraw()) : o.Polyline.prototype.setLatLngs.call(this, t)
            },
            _clipPoints: function() {
                var t = this._originalPoints,
                    e = [];
                if (this._parts = [t].concat(this._holePoints), !this.options.noClip) {
                    for (var n = 0, i = this._parts.length; i > n; n++) {
                        var r = o.PolyUtil.clipPolygon(this._parts[n], this._map._pathViewport);
                        r.length && e.push(r)
                    }
                    this._parts = e
                }
            },
            _getPathPartStr: function(t) {
                var e = o.Polyline.prototype._getPathPartStr.call(this, t);
                return e + (o.Browser.svg ? "z" : "x")
            }
        }), o.polygon = function(t, e) {
            return new o.Polygon(t, e)
        },
        function() {
            function t(t) {
                return o.FeatureGroup.extend({
                    initialize: function(t, e) {
                        this._layers = {}, this._options = e, this.setLatLngs(t)
                    },
                    setLatLngs: function(e) {
                        var n = 0,
                            i = e.length;
                        for (this.eachLayer(function(t) {
                                i > n ? t.setLatLngs(e[n++]) : this.removeLayer(t)
                            }, this); i > n;) this.addLayer(new t(e[n++], this._options));
                        return this
                    },
                    getLatLngs: function() {
                        var t = [];
                        return this.eachLayer(function(e) {
                            t.push(e.getLatLngs())
                        }), t
                    }
                })
            }
            o.MultiPolyline = t(o.Polyline), o.MultiPolygon = t(o.Polygon), o.multiPolyline = function(t, e) {
                return new o.MultiPolyline(t, e)
            }, o.multiPolygon = function(t, e) {
                return new o.MultiPolygon(t, e)
            }
        }(), o.Rectangle = o.Polygon.extend({
            initialize: function(t, e) {
                o.Polygon.prototype.initialize.call(this, this._boundsToLatLngs(t), e)
            },
            setBounds: function(t) {
                this.setLatLngs(this._boundsToLatLngs(t))
            },
            _boundsToLatLngs: function(t) {
                return t = o.latLngBounds(t), [t.getSouthWest(), t.getNorthWest(), t.getNorthEast(), t.getSouthEast()]
            }
        }), o.rectangle = function(t, e) {
            return new o.Rectangle(t, e)
        }, o.Circle = o.Path.extend({
            initialize: function(t, e, n) {
                o.Path.prototype.initialize.call(this, n), this._latlng = o.latLng(t), this._mRadius = e
            },
            options: {
                fill: !0
            },
            setLatLng: function(t) {
                return this._latlng = o.latLng(t), this.redraw()
            },
            setRadius: function(t) {
                return this._mRadius = t, this.redraw()
            },
            projectLatlngs: function() {
                var t = this._getLngRadius(),
                    e = this._latlng,
                    n = this._map.latLngToLayerPoint([e.lat, e.lng - t]);
                this._point = this._map.latLngToLayerPoint(e), this._radius = Math.max(this._point.x - n.x, 1)
            },
            getBounds: function() {
                var t = this._getLngRadius(),
                    e = this._mRadius / 40075017 * 360,
                    n = this._latlng;
                return new o.LatLngBounds([n.lat - e, n.lng - t], [n.lat + e, n.lng + t])
            },
            getLatLng: function() {
                return this._latlng
            },
            getPathString: function() {
                var t = this._point,
                    e = this._radius;
                return this._checkIfEmpty() ? "" : o.Browser.svg ? "M" + t.x + "," + (t.y - e) + "A" + e + "," + e + ",0,1,1," + (t.x - .1) + "," + (t.y - e) + " z" : (t._round(), e = Math.round(e), "AL " + t.x + "," + t.y + " " + e + "," + e + " 0,23592600")
            },
            getRadius: function() {
                return this._mRadius
            },
            _getLatRadius: function() {
                return this._mRadius / 40075017 * 360
            },
            _getLngRadius: function() {
                return this._getLatRadius() / Math.cos(o.LatLng.DEG_TO_RAD * this._latlng.lat)
            },
            _checkIfEmpty: function() {
                if (!this._map) return !1;
                var t = this._map._pathViewport,
                    e = this._radius,
                    n = this._point;
                return n.x - e > t.max.x || n.y - e > t.max.y || n.x + e < t.min.x || n.y + e < t.min.y
            }
        }), o.circle = function(t, e, n) {
            return new o.Circle(t, e, n)
        }, o.CircleMarker = o.Circle.extend({
            options: {
                radius: 10,
                weight: 2
            },
            initialize: function(t, e) {
                o.Circle.prototype.initialize.call(this, t, null, e), this._radius = this.options.radius
            },
            projectLatlngs: function() {
                this._point = this._map.latLngToLayerPoint(this._latlng)
            },
            _updateStyle: function() {
                o.Circle.prototype._updateStyle.call(this), this.setRadius(this.options.radius)
            },
            setLatLng: function(t) {
                return o.Circle.prototype.setLatLng.call(this, t), this._popup && this._popup._isOpen && this._popup.setLatLng(t), this
            },
            setRadius: function(t) {
                return this.options.radius = this._radius = t, this.redraw()
            },
            getRadius: function() {
                return this._radius
            }
        }), o.circleMarker = function(t, e) {
            return new o.CircleMarker(t, e)
        }, o.Polyline.include(o.Path.CANVAS ? {
            _containsPoint: function(t, e) {
                var n, i, r, s, a, u, l, c = this.options.weight / 2;
                for (o.Browser.touch && (c += 10), n = 0, s = this._parts.length; s > n; n++)
                    for (l = this._parts[n], i = 0, a = l.length, r = a - 1; a > i; r = i++)
                        if ((e || 0 !== i) && (u = o.LineUtil.pointToSegmentDistance(t, l[r], l[i]), c >= u)) return !0;
                return !1
            }
        } : {}), o.Polygon.include(o.Path.CANVAS ? {
            _containsPoint: function(t) {
                var e, n, i, r, s, a, u, l, c = !1;
                if (o.Polyline.prototype._containsPoint.call(this, t, !0)) return !0;
                for (r = 0, u = this._parts.length; u > r; r++)
                    for (e = this._parts[r], s = 0, l = e.length, a = l - 1; l > s; a = s++) n = e[s], i = e[a], n.y > t.y != i.y > t.y && t.x < (i.x - n.x) * (t.y - n.y) / (i.y - n.y) + n.x && (c = !c);
                return c
            }
        } : {}), o.Circle.include(o.Path.CANVAS ? {
            _drawPath: function() {
                var t = this._point;
                this._ctx.beginPath(), this._ctx.arc(t.x, t.y, this._radius, 0, 2 * Math.PI, !1)
            },
            _containsPoint: function(t) {
                var e = this._point,
                    n = this.options.stroke ? this.options.weight / 2 : 0;
                return t.distanceTo(e) <= this._radius + n
            }
        } : {}), o.CircleMarker.include(o.Path.CANVAS ? {
            _updateStyle: function() {
                o.Path.prototype._updateStyle.call(this)
            }
        } : {}), o.GeoJSON = o.FeatureGroup.extend({
            initialize: function(t, e) {
                o.setOptions(this, e), this._layers = {}, t && this.addData(t)
            },
            addData: function(t) {
                var e, n, i, r = o.Util.isArray(t) ? t : t.features;
                if (r) {
                    for (e = 0, n = r.length; n > e; e++) i = r[e], (i.geometries || i.geometry || i.features || i.coordinates) && this.addData(r[e]);
                    return this
                }
                var s = this.options;
                if (!s.filter || s.filter(t)) {
                    var a = o.GeoJSON.geometryToLayer(t, s.pointToLayer, s.coordsToLatLng, s);
                    return a.feature = o.GeoJSON.asFeature(t), a.defaultOptions = a.options, this.resetStyle(a), s.onEachFeature && s.onEachFeature(t, a), this.addLayer(a)
                }
            },
            resetStyle: function(t) {
                var e = this.options.style;
                e && (o.Util.extend(t.options, t.defaultOptions), this._setLayerStyle(t, e))
            },
            setStyle: function(t) {
                this.eachLayer(function(e) {
                    this._setLayerStyle(e, t)
                }, this)
            },
            _setLayerStyle: function(t, e) {
                "function" == typeof e && (e = e(t.feature)), t.setStyle && t.setStyle(e)
            }
        }), o.extend(o.GeoJSON, {
            geometryToLayer: function(t, e, n, i) {
                var r, s, a, u, l = "Feature" === t.type ? t.geometry : t,
                    c = l.coordinates,
                    h = [];
                switch (n = n || this.coordsToLatLng, l.type) {
                    case "Point":
                        return r = n(c), e ? e(t, r) : new o.Marker(r);
                    case "MultiPoint":
                        for (a = 0, u = c.length; u > a; a++) r = n(c[a]), h.push(e ? e(t, r) : new o.Marker(r));
                        return new o.FeatureGroup(h);
                    case "LineString":
                        return s = this.coordsToLatLngs(c, 0, n), new o.Polyline(s, i);
                    case "Polygon":
                        if (2 === c.length && !c[1].length) throw new Error("Invalid GeoJSON object.");
                        return s = this.coordsToLatLngs(c, 1, n), new o.Polygon(s, i);
                    case "MultiLineString":
                        return s = this.coordsToLatLngs(c, 1, n), new o.MultiPolyline(s, i);
                    case "MultiPolygon":
                        return s = this.coordsToLatLngs(c, 2, n), new o.MultiPolygon(s, i);
                    case "GeometryCollection":
                        for (a = 0, u = l.geometries.length; u > a; a++) h.push(this.geometryToLayer({
                            geometry: l.geometries[a],
                            type: "Feature",
                            properties: t.properties
                        }, e, n, i));
                        return new o.FeatureGroup(h);
                    default:
                        throw new Error("Invalid GeoJSON object.")
                }
            },
            coordsToLatLng: function(t) {
                return new o.LatLng(t[1], t[0], t[2])
            },
            coordsToLatLngs: function(t, e, n) {
                var i, o, r, s = [];
                for (o = 0, r = t.length; r > o; o++) i = e ? this.coordsToLatLngs(t[o], e - 1, n) : (n || this.coordsToLatLng)(t[o]), s.push(i);
                return s
            },
            latLngToCoords: function(t) {
                var e = [t.lng, t.lat];
                return t.alt !== n && e.push(t.alt), e
            },
            latLngsToCoords: function(t) {
                for (var e = [], n = 0, i = t.length; i > n; n++) e.push(o.GeoJSON.latLngToCoords(t[n]));
                return e
            },
            getFeature: function(t, e) {
                return t.feature ? o.extend({}, t.feature, {
                    geometry: e
                }) : o.GeoJSON.asFeature(e)
            },
            asFeature: function(t) {
                return "Feature" === t.type ? t : {
                    type: "Feature",
                    properties: {},
                    geometry: t
                }
            }
        });
    var s = {
        toGeoJSON: function() {
            return o.GeoJSON.getFeature(this, {
                type: "Point",
                coordinates: o.GeoJSON.latLngToCoords(this.getLatLng())
            })
        }
    };
    o.Marker.include(s), o.Circle.include(s), o.CircleMarker.include(s), o.Polyline.include({
            toGeoJSON: function() {
                return o.GeoJSON.getFeature(this, {
                    type: "LineString",
                    coordinates: o.GeoJSON.latLngsToCoords(this.getLatLngs())
                })
            }
        }), o.Polygon.include({
            toGeoJSON: function() {
                var t, e, n, i = [o.GeoJSON.latLngsToCoords(this.getLatLngs())];
                if (i[0].push(i[0][0]), this._holes)
                    for (t = 0, e = this._holes.length; e > t; t++) n = o.GeoJSON.latLngsToCoords(this._holes[t]), n.push(n[0]), i.push(n);
                return o.GeoJSON.getFeature(this, {
                    type: "Polygon",
                    coordinates: i
                })
            }
        }),
        function() {
            function t(t) {
                return function() {
                    var e = [];
                    return this.eachLayer(function(t) {
                        e.push(t.toGeoJSON().geometry.coordinates)
                    }), o.GeoJSON.getFeature(this, {
                        type: t,
                        coordinates: e
                    })
                }
            }
            o.MultiPolyline.include({
                toGeoJSON: t("MultiLineString")
            }), o.MultiPolygon.include({
                toGeoJSON: t("MultiPolygon")
            }), o.LayerGroup.include({
                toGeoJSON: function() {
                    var e, n = this.feature && this.feature.geometry,
                        i = [];
                    if (n && "MultiPoint" === n.type) return t("MultiPoint").call(this);
                    var r = n && "GeometryCollection" === n.type;
                    return this.eachLayer(function(t) {
                        t.toGeoJSON && (e = t.toGeoJSON(), i.push(r ? e.geometry : o.GeoJSON.asFeature(e)))
                    }), r ? o.GeoJSON.getFeature(this, {
                        geometries: i,
                        type: "GeometryCollection"
                    }) : {
                        type: "FeatureCollection",
                        features: i
                    }
                }
            })
        }(), o.geoJson = function(t, e) {
            return new o.GeoJSON(t, e)
        }, o.DomEvent = {
            addListener: function(t, e, n, i) {
                var r, s, a, u = o.stamp(n),
                    l = "_leaflet_" + e + u;
                return t[l] ? this : (r = function(e) {
                    return n.call(i || t, e || o.DomEvent._getEvent())
                }, o.Browser.pointer && 0 === e.indexOf("touch") ? this.addPointerListener(t, e, r, u) : (o.Browser.touch && "dblclick" === e && this.addDoubleTapListener && this.addDoubleTapListener(t, r, u), "addEventListener" in t ? "mousewheel" === e ? (t.addEventListener("DOMMouseScroll", r, !1), t.addEventListener(e, r, !1)) : "mouseenter" === e || "mouseleave" === e ? (s = r, a = "mouseenter" === e ? "mouseover" : "mouseout", r = function(e) {
                    return o.DomEvent._checkMouse(t, e) ? s(e) : void 0
                }, t.addEventListener(a, r, !1)) : "click" === e && o.Browser.android ? (s = r, r = function(t) {
                    return o.DomEvent._filterClick(t, s)
                }, t.addEventListener(e, r, !1)) : t.addEventListener(e, r, !1) : "attachEvent" in t && t.attachEvent("on" + e, r), t[l] = r, this))
            },
            removeListener: function(t, e, n) {
                var i = o.stamp(n),
                    r = "_leaflet_" + e + i,
                    s = t[r];
                return s ? (o.Browser.pointer && 0 === e.indexOf("touch") ? this.removePointerListener(t, e, i) : o.Browser.touch && "dblclick" === e && this.removeDoubleTapListener ? this.removeDoubleTapListener(t, i) : "removeEventListener" in t ? "mousewheel" === e ? (t.removeEventListener("DOMMouseScroll", s, !1), t.removeEventListener(e, s, !1)) : "mouseenter" === e || "mouseleave" === e ? t.removeEventListener("mouseenter" === e ? "mouseover" : "mouseout", s, !1) : t.removeEventListener(e, s, !1) : "detachEvent" in t && t.detachEvent("on" + e, s), t[r] = null, this) : this
            },
            stopPropagation: function(t) {
                return t.stopPropagation ? t.stopPropagation() : t.cancelBubble = !0, o.DomEvent._skipped(t), this
            },
            disableScrollPropagation: function(t) {
                var e = o.DomEvent.stopPropagation;
                return o.DomEvent.on(t, "mousewheel", e).on(t, "MozMousePixelScroll", e)
            },
            disableClickPropagation: function(t) {
                for (var e = o.DomEvent.stopPropagation, n = o.Draggable.START.length - 1; n >= 0; n--) o.DomEvent.on(t, o.Draggable.START[n], e);
                return o.DomEvent.on(t, "click", o.DomEvent._fakeStop).on(t, "dblclick", e)
            },
            preventDefault: function(t) {
                return t.preventDefault ? t.preventDefault() : t.returnValue = !1, this
            },
            stop: function(t) {
                return o.DomEvent.preventDefault(t).stopPropagation(t)
            },
            getMousePosition: function(t, e) {
                if (!e) return new o.Point(t.clientX, t.clientY);
                var n = e.getBoundingClientRect();
                return new o.Point(t.clientX - n.left - e.clientLeft, t.clientY - n.top - e.clientTop)
            },
            getWheelDelta: function(t) {
                var e = 0;
                return t.wheelDelta && (e = t.wheelDelta / 120), t.detail && (e = -t.detail / 3), e
            },
            _skipEvents: {},
            _fakeStop: function(t) {
                o.DomEvent._skipEvents[t.type] = !0
            },
            _skipped: function(t) {
                var e = this._skipEvents[t.type];
                return this._skipEvents[t.type] = !1, e
            },
            _checkMouse: function(t, e) {
                var n = e.relatedTarget;
                if (!n) return !0;
                try {
                    for (; n && n !== t;) n = n.parentNode
                } catch (i) {
                    return !1
                }
                return n !== t
            },
            _getEvent: function() {
                var e = t.event;
                if (!e)
                    for (var n = arguments.callee.caller; n && (e = n.arguments[0], !e || t.Event !== e.constructor);) n = n.caller;
                return e
            },
            _filterClick: function(t, e) {
                var n = t.timeStamp || t.originalEvent.timeStamp,
                    i = o.DomEvent._lastClick && n - o.DomEvent._lastClick;
                return i && i > 100 && 500 > i || t.target._simulatedClick && !t._simulated ? void o.DomEvent.stop(t) : (o.DomEvent._lastClick = n, e(t))
            }
        }, o.DomEvent.on = o.DomEvent.addListener, o.DomEvent.off = o.DomEvent.removeListener, o.Draggable = o.Class.extend({
            includes: o.Mixin.Events,
            statics: {
                START: o.Browser.touch ? ["touchstart", "mousedown"] : ["mousedown"],
                END: {
                    mousedown: "mouseup",
                    touchstart: "touchend",
                    pointerdown: "touchend",
                    MSPointerDown: "touchend"
                },
                MOVE: {
                    mousedown: "mousemove",
                    touchstart: "touchmove",
                    pointerdown: "touchmove",
                    MSPointerDown: "touchmove"
                }
            },
            initialize: function(t, e) {
                this._element = t, this._dragStartTarget = e || t
            },
            enable: function() {
                if (!this._enabled) {
                    for (var t = o.Draggable.START.length - 1; t >= 0; t--) o.DomEvent.on(this._dragStartTarget, o.Draggable.START[t], this._onDown, this);
                    this._enabled = !0
                }
            },
            disable: function() {
                if (this._enabled) {
                    for (var t = o.Draggable.START.length - 1; t >= 0; t--) o.DomEvent.off(this._dragStartTarget, o.Draggable.START[t], this._onDown, this);
                    this._enabled = !1, this._moved = !1
                }
            },
            _onDown: function(t) {
                if (this._moved = !1, !(t.shiftKey || 1 !== t.which && 1 !== t.button && !t.touches || (o.DomEvent.stopPropagation(t), o.Draggable._disabled || (o.DomUtil.disableImageDrag(), o.DomUtil.disableTextSelection(), this._moving)))) {
                    var n = t.touches ? t.touches[0] : t;
                    this._startPoint = new o.Point(n.clientX, n.clientY), this._startPos = this._newPos = o.DomUtil.getPosition(this._element), o.DomEvent.on(e, o.Draggable.MOVE[t.type], this._onMove, this).on(e, o.Draggable.END[t.type], this._onUp, this)
                }
            },
            _onMove: function(t) {
                if (t.touches && t.touches.length > 1) return void(this._moved = !0);
                var n = t.touches && 1 === t.touches.length ? t.touches[0] : t,
                    i = new o.Point(n.clientX, n.clientY),
                    r = i.subtract(this._startPoint);
                (r.x || r.y) && (o.Browser.touch && Math.abs(r.x) + Math.abs(r.y) < 3 || (o.DomEvent.preventDefault(t), this._moved || (this.fire("dragstart"), this._moved = !0, this._startPos = o.DomUtil.getPosition(this._element).subtract(r), o.DomUtil.addClass(e.body, "leaflet-dragging"), this._lastTarget = t.target || t.srcElement, o.DomUtil.addClass(this._lastTarget, "leaflet-drag-target")), this._newPos = this._startPos.add(r), this._moving = !0, o.Util.cancelAnimFrame(this._animRequest), this._animRequest = o.Util.requestAnimFrame(this._updatePosition, this, !0, this._dragStartTarget)))
            },
            _updatePosition: function() {
                this.fire("predrag"), o.DomUtil.setPosition(this._element, this._newPos), this.fire("drag")
            },
            _onUp: function() {
                o.DomUtil.removeClass(e.body, "leaflet-dragging"), this._lastTarget && (o.DomUtil.removeClass(this._lastTarget, "leaflet-drag-target"), this._lastTarget = null);
                for (var t in o.Draggable.MOVE) o.DomEvent.off(e, o.Draggable.MOVE[t], this._onMove).off(e, o.Draggable.END[t], this._onUp);
                o.DomUtil.enableImageDrag(), o.DomUtil.enableTextSelection(), this._moved && this._moving && (o.Util.cancelAnimFrame(this._animRequest), this.fire("dragend", {
                    distance: this._newPos.distanceTo(this._startPos)
                })), this._moving = !1
            }
        }), o.Handler = o.Class.extend({
            initialize: function(t) {
                this._map = t
            },
            enable: function() {
                this._enabled || (this._enabled = !0, this.addHooks())
            },
            disable: function() {
                this._enabled && (this._enabled = !1, this.removeHooks())
            },
            enabled: function() {
                return !!this._enabled
            }
        }), o.Map.mergeOptions({
            dragging: !0,
            inertia: !o.Browser.android23,
            inertiaDeceleration: 3400,
            inertiaMaxSpeed: 1 / 0,
            inertiaThreshold: o.Browser.touch ? 32 : 18,
            easeLinearity: .25,
            worldCopyJump: !1
        }), o.Map.Drag = o.Handler.extend({
            addHooks: function() {
                if (!this._draggable) {
                    var t = this._map;
                    this._draggable = new o.Draggable(t._mapPane, t._container), this._draggable.on({
                        dragstart: this._onDragStart,
                        drag: this._onDrag,
                        dragend: this._onDragEnd
                    }, this), t.options.worldCopyJump && (this._draggable.on("predrag", this._onPreDrag, this), t.on("viewreset", this._onViewReset, this), t.whenReady(this._onViewReset, this))
                }
                this._draggable.enable()
            },
            removeHooks: function() {
                this._draggable.disable()
            },
            moved: function() {
                return this._draggable && this._draggable._moved
            },
            _onDragStart: function() {
                var t = this._map;
                t._panAnim && t._panAnim.stop(), t.fire("movestart").fire("dragstart"), t.options.inertia && (this._positions = [], this._times = [])
            },
            _onDrag: function() {
                if (this._map.options.inertia) {
                    var t = this._lastTime = +new Date,
                        e = this._lastPos = this._draggable._newPos;
                    this._positions.push(e), this._times.push(t), t - this._times[0] > 200 && (this._positions.shift(), this._times.shift())
                }
                this._map.fire("move").fire("drag")
            },
            _onViewReset: function() {
                var t = this._map.getSize()._divideBy(2),
                    e = this._map.latLngToLayerPoint([0, 0]);
                this._initialWorldOffset = e.subtract(t).x, this._worldWidth = this._map.project([0, 180]).x
            },
            _onPreDrag: function() {
                var t = this._worldWidth,
                    e = Math.round(t / 2),
                    n = this._initialWorldOffset,
                    i = this._draggable._newPos.x,
                    o = (i - e + n) % t + e - n,
                    r = (i + e + n) % t - e - n,
                    s = Math.abs(o + n) < Math.abs(r + n) ? o : r;
                this._draggable._newPos.x = s
            },
            _onDragEnd: function(t) {
                var e = this._map,
                    n = e.options,
                    i = +new Date - this._lastTime,
                    r = !n.inertia || i > n.inertiaThreshold || !this._positions[0];
                if (e.fire("dragend", t), r) e.fire("moveend");
                else {
                    var s = this._lastPos.subtract(this._positions[0]),
                        a = (this._lastTime + i - this._times[0]) / 1e3,
                        u = n.easeLinearity,
                        l = s.multiplyBy(u / a),
                        c = l.distanceTo([0, 0]),
                        h = Math.min(n.inertiaMaxSpeed, c),
                        f = l.multiplyBy(h / c),
                        d = h / (n.inertiaDeceleration * u),
                        p = f.multiplyBy(-d / 2).round();
                    p.x && p.y ? (p = e._limitOffset(p, e.options.maxBounds), o.Util.requestAnimFrame(function() {
                        e.panBy(p, {
                            duration: d,
                            easeLinearity: u,
                            noMoveStart: !0
                        })
                    })) : e.fire("moveend")
                }
            }
        }), o.Map.addInitHook("addHandler", "dragging", o.Map.Drag), o.Map.mergeOptions({
            doubleClickZoom: !0
        }), o.Map.DoubleClickZoom = o.Handler.extend({
            addHooks: function() {
                this._map.on("dblclick", this._onDoubleClick, this)
            },
            removeHooks: function() {
                this._map.off("dblclick", this._onDoubleClick, this)
            },
            _onDoubleClick: function(t) {
                var e = this._map,
                    n = e.getZoom() + (t.originalEvent.shiftKey ? -1 : 1);
                "center" === e.options.doubleClickZoom ? e.setZoom(n) : e.setZoomAround(t.containerPoint, n)
            }
        }), 
        o.Map.addInitHook("addHandler", "doubleClickZoom", o.Map.DoubleClickZoom), o.Map.mergeOptions({
            scrollWheelZoom: !0
        }), 
        
        o.Map.ScrollWheelZoom = o.Handler.extend({
            addHooks: function() {
                o.DomEvent.on(this._map._container, "mousewheel", this._onWheelScroll, this), o.DomEvent.on(this._map._container, "MozMousePixelScroll", o.DomEvent.preventDefault), this._delta = 0
            },
            removeHooks: function() {
                o.DomEvent.off(this._map._container, "mousewheel", this._onWheelScroll), o.DomEvent.off(this._map._container, "MozMousePixelScroll", o.DomEvent.preventDefault)
            },
            _onWheelScroll: function(t) {
                var e = o.DomEvent.getWheelDelta(t);
                this._delta += e, this._lastMousePos = this._map.mouseEventToContainerPoint(t), this._startTime || (this._startTime = +new Date);
                var n = Math.max(40 - (+new Date - this._startTime), 0);
                clearTimeout(this._timer), this._timer = setTimeout(o.bind(this._performZoom, this), n), o.DomEvent.preventDefault(t), o.DomEvent.stopPropagation(t)
            },
            _performZoom: function() {
                var t = this._map,
                    e = this._delta,
                    n = t.getZoom();
                e = e > 0 ? Math.ceil(e) : Math.floor(e), e = Math.max(Math.min(e, 4), -4), e = t._limitZoom(n + e) - n, this._delta = 0, this._startTime = null, e && ("center" === t.options.scrollWheelZoom ? t.setZoom(n + e) : t.setZoomAround(this._lastMousePos, n + e))
            }
        }), 
        
        o.Map.addInitHook("addHandler", "scrollWheelZoom", o.Map.ScrollWheelZoom), 
        
        o.extend(o.DomEvent, {
            _touchstart: o.Browser.msPointer ? "MSPointerDown" : o.Browser.pointer ? "pointerdown" : "touchstart",
            _touchend: o.Browser.msPointer ? "MSPointerUp" : o.Browser.pointer ? "pointerup" : "touchend",
            addDoubleTapListener: function(t, n, i) {
                function r(t) {
                    var e;
                    if (o.Browser.pointer ? (p.push(t.pointerId), e = p.length) : e = t.touches.length, !(e > 1)) {
                        var n = Date.now(),
                            i = n - (a || n);
                        u = t.touches ? t.touches[0] : t, l = i > 0 && c >= i, a = n
                    }
                }

                function s(t) {
                    if (o.Browser.pointer) {
                        var e = p.indexOf(t.pointerId);
                        if (-1 === e) return;
                        p.splice(e, 1)
                    }
                    if (l) {
                        if (o.Browser.pointer) {
                            var i, r = {};
                            for (var s in u) i = u[s], r[s] = "function" == typeof i ? i.bind(u) : i;
                            u = r
                        }
                        u.type = "dblclick", n(u), a = null
                    }
                }
                var a, u, l = !1,
                    c = 250,
                    h = "_leaflet_",
                    f = this._touchstart,
                    d = this._touchend,
                    p = [];
                t[h + f + i] = r, t[h + d + i] = s;
                var m = o.Browser.pointer ? e.documentElement : t;
                return t.addEventListener(f, r, !1), m.addEventListener(d, s, !1), o.Browser.pointer && m.addEventListener(o.DomEvent.POINTER_CANCEL, s, !1), this
            },
            removeDoubleTapListener: function(t, n) {
                var i = "_leaflet_";
                return t.removeEventListener(this._touchstart, t[i + this._touchstart + n], !1), (o.Browser.pointer ? e.documentElement : t).removeEventListener(this._touchend, t[i + this._touchend + n], !1), o.Browser.pointer && e.documentElement.removeEventListener(o.DomEvent.POINTER_CANCEL, t[i + this._touchend + n], !1), this
            }
        }), 
        o.extend(o.DomEvent, {
            POINTER_DOWN: o.Browser.msPointer ? "MSPointerDown" : "pointerdown",
            POINTER_MOVE: o.Browser.msPointer ? "MSPointerMove" : "pointermove",
            POINTER_UP: o.Browser.msPointer ? "MSPointerUp" : "pointerup",
            POINTER_CANCEL: o.Browser.msPointer ? "MSPointerCancel" : "pointercancel",
            _pointers: [],
            _pointerDocumentListener: !1,
            addPointerListener: function(t, e, n, i) {
                switch (e) {
                    case "touchstart":
                        return this.addPointerListenerStart(t, e, n, i);
                    case "touchend":
                        return this.addPointerListenerEnd(t, e, n, i);
                    case "touchmove":
                        return this.addPointerListenerMove(t, e, n, i);
                    default:
                        throw "Unknown touch event type"
                }
            },
            addPointerListenerStart: function(t, n, i, r) {
                var s = "_leaflet_",
                    a = this._pointers,
                    u = function(t) {
                        "mouse" !== t.pointerType && t.pointerType !== t.MSPOINTER_TYPE_MOUSE && o.DomEvent.preventDefault(t);
                        for (var e = !1, n = 0; n < a.length; n++)
                            if (a[n].pointerId === t.pointerId) {
                                e = !0;
                                break
                            } e || a.push(t), t.touches = a.slice(), t.changedTouches = [t], i(t)
                    };
                if (t[s + "touchstart" + r] = u, t.addEventListener(this.POINTER_DOWN, u, !1), !this._pointerDocumentListener) {
                    var l = function(t) {
                        for (var e = 0; e < a.length; e++)
                            if (a[e].pointerId === t.pointerId) {
                                a.splice(e, 1);
                                break
                            }
                    };
                    e.documentElement.addEventListener(this.POINTER_UP, l, !1), e.documentElement.addEventListener(this.POINTER_CANCEL, l, !1), this._pointerDocumentListener = !0
                }
                return this
            },
            addPointerListenerMove: function(t, e, n, i) {
                function o(t) {
                    if (t.pointerType !== t.MSPOINTER_TYPE_MOUSE && "mouse" !== t.pointerType || 0 !== t.buttons) {
                        for (var e = 0; e < s.length; e++)
                            if (s[e].pointerId === t.pointerId) {
                                s[e] = t;
                                break
                            } t.touches = s.slice(), t.changedTouches = [t], n(t)
                    }
                }
                var r = "_leaflet_",
                    s = this._pointers;
                return t[r + "touchmove" + i] = o, t.addEventListener(this.POINTER_MOVE, o, !1), this
            },
            addPointerListenerEnd: function(t, e, n, i) {
                var o = "_leaflet_",
                    r = this._pointers,
                    s = function(t) {
                        for (var e = 0; e < r.length; e++)
                            if (r[e].pointerId === t.pointerId) {
                                r.splice(e, 1);
                                break
                            } t.touches = r.slice(), t.changedTouches = [t], n(t)
                    };
                return t[o + "touchend" + i] = s, t.addEventListener(this.POINTER_UP, s, !1), t.addEventListener(this.POINTER_CANCEL, s, !1), this
            },
            removePointerListener: function(t, e, n) {
                var i = "_leaflet_",
                    o = t[i + e + n];
                switch (e) {
                    case "touchstart":
                        t.removeEventListener(this.POINTER_DOWN, o, !1);
                        break;
                    case "touchmove":
                        t.removeEventListener(this.POINTER_MOVE, o, !1);
                        break;
                    case "touchend":
                        t.removeEventListener(this.POINTER_UP, o, !1), t.removeEventListener(this.POINTER_CANCEL, o, !1)
                }
                return this
            }
        }), o.Map.mergeOptions({
            touchZoom: o.Browser.touch && !o.Browser.android23,
            bounceAtZoomLimits: !0
        }), o.Map.TouchZoom = o.Handler.extend({
            addHooks: function() {
                o.DomEvent.on(this._map._container, "touchstart", this._onTouchStart, this)
            },
            removeHooks: function() {
                o.DomEvent.off(this._map._container, "touchstart", this._onTouchStart, this)
            },
            _onTouchStart: function(t) {
                var n = this._map;
                if (t.touches && 2 === t.touches.length && !n._animatingZoom && !this._zooming) {
                    var i = n.mouseEventToLayerPoint(t.touches[0]),
                        r = n.mouseEventToLayerPoint(t.touches[1]),
                        s = n._getCenterLayerPoint();
                    this._startCenter = i.add(r)._divideBy(2), this._startDist = i.distanceTo(r), this._moved = !1, this._zooming = !0, this._centerOffset = s.subtract(this._startCenter), n._panAnim && n._panAnim.stop(), o.DomEvent.on(e, "touchmove", this._onTouchMove, this).on(e, "touchend", this._onTouchEnd, this), o.DomEvent.preventDefault(t)
                }
            },
            _onTouchMove: function(t) {
                var e = this._map;
                if (t.touches && 2 === t.touches.length && this._zooming) {
                    var n = e.mouseEventToLayerPoint(t.touches[0]),
                        i = e.mouseEventToLayerPoint(t.touches[1]);
                    this._scale = n.distanceTo(i) / this._startDist, this._delta = n._add(i)._divideBy(2)._subtract(this._startCenter), 1 !== this._scale && (e.options.bounceAtZoomLimits || !(e.getZoom() === e.getMinZoom() && this._scale < 1 || e.getZoom() === e.getMaxZoom() && this._scale > 1)) && (this._moved || (o.DomUtil.addClass(e._mapPane, "leaflet-touching"), e.fire("movestart").fire("zoomstart"), this._moved = !0), o.Util.cancelAnimFrame(this._animRequest), this._animRequest = o.Util.requestAnimFrame(this._updateOnMove, this, !0, this._map._container), o.DomEvent.preventDefault(t))
                }
            },
            _updateOnMove: function() {
                var t = this._map,
                    e = this._getScaleOrigin(),
                    n = t.layerPointToLatLng(e),
                    i = t.getScaleZoom(this._scale);
                t._animateZoom(n, i, this._startCenter, this._scale, this._delta, !1, !0)
            },
            _onTouchEnd: function() {
                if (!this._moved || !this._zooming) return void(this._zooming = !1);
                var t = this._map;
                this._zooming = !1, o.DomUtil.removeClass(t._mapPane, "leaflet-touching"), o.Util.cancelAnimFrame(this._animRequest), o.DomEvent.off(e, "touchmove", this._onTouchMove).off(e, "touchend", this._onTouchEnd);
                var n = this._getScaleOrigin(),
                    i = t.layerPointToLatLng(n),
                    r = t.getZoom(),
                    s = t.getScaleZoom(this._scale) - r,
                    a = s > 0 ? Math.ceil(s) : Math.floor(s),
                    u = t._limitZoom(r + a),
                    l = t.getZoomScale(u) / this._scale;
                t._animateZoom(i, u, n, l)
            },
            _getScaleOrigin: function() {
                var t = this._centerOffset.subtract(this._delta).divideBy(this._scale);
                return this._startCenter.add(t)
            }
        }), o.Map.addInitHook("addHandler", "touchZoom", o.Map.TouchZoom), o.Map.mergeOptions({
            tap: !0,
            tapTolerance: 15
        }), o.Map.Tap = o.Handler.extend({
            addHooks: function() {
                o.DomEvent.on(this._map._container, "touchstart", this._onDown, this)
            },
            removeHooks: function() {
                o.DomEvent.off(this._map._container, "touchstart", this._onDown, this)
            },
            _onDown: function(t) {
                if (t.touches) {
                    if (o.DomEvent.preventDefault(t), this._fireClick = !0, t.touches.length > 1) return this._fireClick = !1, void clearTimeout(this._holdTimeout);
                    var n = t.touches[0],
                        i = n.target;
                    this._startPos = this._newPos = new o.Point(n.clientX, n.clientY), i.tagName && "a" === i.tagName.toLowerCase() && o.DomUtil.addClass(i, "leaflet-active"), this._holdTimeout = setTimeout(o.bind(function() {
                        this._isTapValid() && (this._fireClick = !1, this._onUp(), this._simulateEvent("contextmenu", n))
                    }, this), 1e3), o.DomEvent.on(e, "touchmove", this._onMove, this).on(e, "touchend", this._onUp, this)
                }
            },
            _onUp: function(t) {
                if (clearTimeout(this._holdTimeout), o.DomEvent.off(e, "touchmove", this._onMove, this).off(e, "touchend", this._onUp, this), this._fireClick && t && t.changedTouches) {
                    var n = t.changedTouches[0],
                        i = n.target;
                    i && i.tagName && "a" === i.tagName.toLowerCase() && o.DomUtil.removeClass(i, "leaflet-active"), this._isTapValid() && this._simulateEvent("click", n)
                }
            },
            _isTapValid: function() {
                return this._newPos.distanceTo(this._startPos) <= this._map.options.tapTolerance
            },
            _onMove: function(t) {
                var e = t.touches[0];
                this._newPos = new o.Point(e.clientX, e.clientY)
            },
            _simulateEvent: function(n, i) {
                var o = e.createEvent("MouseEvents");
                o._simulated = !0, i.target._simulatedClick = !0, o.initMouseEvent(n, !0, !0, t, 1, i.screenX, i.screenY, i.clientX, i.clientY, !1, !1, !1, !1, 0, null), i.target.dispatchEvent(o)
            }
        }), o.Browser.touch && !o.Browser.pointer && o.Map.addInitHook("addHandler", "tap", o.Map.Tap), o.Map.mergeOptions({
            boxZoom: !0
        }), o.Map.BoxZoom = o.Handler.extend({
            initialize: function(t) {
                this._map = t, this._container = t._container, this._pane = t._panes.overlayPane, this._moved = !1
            },
            addHooks: function() {
                o.DomEvent.on(this._container, "mousedown", this._onMouseDown, this)
            },
            removeHooks: function() {
                o.DomEvent.off(this._container, "mousedown", this._onMouseDown), this._moved = !1
            },
            moved: function() {
                return this._moved
            },
            _onMouseDown: function(t) {
                return this._moved = !1, !t.shiftKey || 1 !== t.which && 1 !== t.button ? !1 : (o.DomUtil.disableTextSelection(), o.DomUtil.disableImageDrag(), this._startLayerPoint = this._map.mouseEventToLayerPoint(t), void o.DomEvent.on(e, "mousemove", this._onMouseMove, this).on(e, "mouseup", this._onMouseUp, this).on(e, "keydown", this._onKeyDown, this))
            },
            _onMouseMove: function(t) {
                this._moved || (this._box = o.DomUtil.create("div", "leaflet-zoom-box", this._pane), o.DomUtil.setPosition(this._box, this._startLayerPoint), this._container.style.cursor = "crosshair", this._map.fire("boxzoomstart"));
                var e = this._startLayerPoint,
                    n = this._box,
                    i = this._map.mouseEventToLayerPoint(t),
                    r = i.subtract(e),
                    s = new o.Point(Math.min(i.x, e.x), Math.min(i.y, e.y));
                o.DomUtil.setPosition(n, s), this._moved = !0, n.style.width = Math.max(0, Math.abs(r.x) - 4) + "px", n.style.height = Math.max(0, Math.abs(r.y) - 4) + "px"
            },
            _finish: function() {
                this._moved && (this._pane.removeChild(this._box), this._container.style.cursor = ""), o.DomUtil.enableTextSelection(), o.DomUtil.enableImageDrag(), o.DomEvent.off(e, "mousemove", this._onMouseMove).off(e, "mouseup", this._onMouseUp).off(e, "keydown", this._onKeyDown)
            },
            _onMouseUp: function(t) {
                this._finish();
                var e = this._map,
                    n = e.mouseEventToLayerPoint(t);
                if (!this._startLayerPoint.equals(n)) {
                    var i = new o.LatLngBounds(e.layerPointToLatLng(this._startLayerPoint), e.layerPointToLatLng(n));
                    e.fitBounds(i), e.fire("boxzoomend", {
                        boxZoomBounds: i
                    })
                }
            },
            _onKeyDown: function(t) {
                27 === t.keyCode && this._finish()
            }
        }), o.Map.addInitHook("addHandler", "boxZoom", o.Map.BoxZoom), o.Map.mergeOptions({
            keyboard: !0,
            keyboardPanOffset: 80,
            keyboardZoomOffset: 1
        }), o.Map.Keyboard = o.Handler.extend({
            keyCodes: {
                left: [37],
                right: [39],
                down: [40],
                up: [38],
                zoomIn: [187, 107, 61, 171],
                zoomOut: [189, 109, 173]
            },
            initialize: function(t) {
                this._map = t, this._setPanOffset(t.options.keyboardPanOffset), this._setZoomOffset(t.options.keyboardZoomOffset)
            },
            addHooks: function() {
                var t = this._map._container; - 1 === t.tabIndex && (t.tabIndex = "0"), o.DomEvent.on(t, "focus", this._onFocus, this).on(t, "blur", this._onBlur, this).on(t, "mousedown", this._onMouseDown, this), this._map.on("focus", this._addHooks, this).on("blur", this._removeHooks, this)
            },
            removeHooks: function() {
                this._removeHooks();
                var t = this._map._container;
                o.DomEvent.off(t, "focus", this._onFocus, this).off(t, "blur", this._onBlur, this).off(t, "mousedown", this._onMouseDown, this), this._map.off("focus", this._addHooks, this).off("blur", this._removeHooks, this)
            },
            _onMouseDown: function() {
                if (!this._focused) {
                    var n = e.body,
                        i = e.documentElement,
                        o = n.scrollTop || i.scrollTop,
                        r = n.scrollLeft || i.scrollLeft;
                    this._map._container.focus(), t.scrollTo(r, o)
                }
            },
            _onFocus: function() {
                this._focused = !0, this._map.fire("focus")
            },
            _onBlur: function() {
                this._focused = !1, this._map.fire("blur")
            },
            _setPanOffset: function(t) {
                var e, n, i = this._panKeys = {},
                    o = this.keyCodes;
                for (e = 0, n = o.left.length; n > e; e++) i[o.left[e]] = [-1 * t, 0];
                for (e = 0, n = o.right.length; n > e; e++) i[o.right[e]] = [t, 0];
                for (e = 0, n = o.down.length; n > e; e++) i[o.down[e]] = [0, t];
                for (e = 0, n = o.up.length; n > e; e++) i[o.up[e]] = [0, -1 * t]
            },
            _setZoomOffset: function(t) {
                var e, n, i = this._zoomKeys = {},
                    o = this.keyCodes;
                for (e = 0, n = o.zoomIn.length; n > e; e++) i[o.zoomIn[e]] = t;
                for (e = 0, n = o.zoomOut.length; n > e; e++) i[o.zoomOut[e]] = -t
            },
            _addHooks: function() {
                o.DomEvent.on(e, "keydown", this._onKeyDown, this)
            },
            _removeHooks: function() {
                o.DomEvent.off(e, "keydown", this._onKeyDown, this)
            },
            _onKeyDown: function(t) {
                var e = t.keyCode,
                    n = this._map;
                if (e in this._panKeys) {
                    if (n._panAnim && n._panAnim._inProgress) return;
                    n.panBy(this._panKeys[e]), n.options.maxBounds && n.panInsideBounds(n.options.maxBounds)
                } else {
                    if (!(e in this._zoomKeys)) return;
                    n.setZoom(n.getZoom() + this._zoomKeys[e])
                }
                o.DomEvent.stop(t)
            }
        }), o.Map.addInitHook("addHandler", "keyboard", o.Map.Keyboard), o.Handler.MarkerDrag = o.Handler.extend({
            initialize: function(t) {
                this._marker = t
            },
            addHooks: function() {
                var t = this._marker._icon;
                this._draggable || (this._draggable = new o.Draggable(t, t)), this._draggable.on("dragstart", this._onDragStart, this).on("drag", this._onDrag, this).on("dragend", this._onDragEnd, this), this._draggable.enable(), o.DomUtil.addClass(this._marker._icon, "leaflet-marker-draggable")
            },
            removeHooks: function() {
                this._draggable.off("dragstart", this._onDragStart, this).off("drag", this._onDrag, this).off("dragend", this._onDragEnd, this), this._draggable.disable(), o.DomUtil.removeClass(this._marker._icon, "leaflet-marker-draggable")
            },
            moved: function() {
                return this._draggable && this._draggable._moved
            },
            _onDragStart: function() {
                this._marker.closePopup().fire("movestart").fire("dragstart")
            },
            _onDrag: function() {
                var t = this._marker,
                    e = t._shadow,
                    n = o.DomUtil.getPosition(t._icon),
                    i = t._map.layerPointToLatLng(n);
                e && o.DomUtil.setPosition(e, n), t._latlng = i, t.fire("move", {
                    latlng: i
                }).fire("drag")
            },
            _onDragEnd: function(t) {
                this._marker.fire("moveend").fire("dragend", t)
            }
        }), o.Control = o.Class.extend({
            options: {
                position: "topright"
            },
            initialize: function(t) {
                o.setOptions(this, t)
            },
            getPosition: function() {
                return this.options.position
            },
            setPosition: function(t) {
                var e = this._map;
                return e && e.removeControl(this), this.options.position = t, e && e.addControl(this), this
            },
            getContainer: function() {
                return this._container
            },
            addTo: function(t) {
                this._map = t;
                var e = this._container = this.onAdd(t),
                    n = this.getPosition(),
                    i = t._controlCorners[n];
                return o.DomUtil.addClass(e, "leaflet-control"), -1 !== n.indexOf("bottom") ? i.insertBefore(e, i.firstChild) : i.appendChild(e), this
            },
            removeFrom: function(t) {
                var e = this.getPosition(),
                    n = t._controlCorners[e];
                return n.removeChild(this._container), this._map = null, this.onRemove && this.onRemove(t), this
            },
            _refocusOnMap: function() {
                this._map && this._map.getContainer().focus()
            }
        }), o.control = function(t) {
            return new o.Control(t)
        }, o.Map.include({
            addControl: function(t) {
                return t.addTo(this), this
            },
            removeControl: function(t) {
                return t.removeFrom(this), this
            },
            _initControlPos: function() {
                function t(t, r) {
                    var s = n + t + " " + n + r;
                    e[t + r] = o.DomUtil.create("div", s, i)
                }
                var e = this._controlCorners = {},
                    n = "leaflet-",
                    i = this._controlContainer = o.DomUtil.create("div", n + "control-container", this._container);
                t("top", "left"), t("top", "right"), t("bottom", "left"), t("bottom", "right")
            },
            _clearControlPos: function() {
                this._container.removeChild(this._controlContainer)
            }
        }), o.Control.Zoom = o.Control.extend({
            options: {
                position: "topleft",
                zoomInText: "+",
                zoomInTitle: "Zoom in",
                zoomOutText: "-",
                zoomOutTitle: "Zoom out"
            },
            onAdd: function(t) {
                var e = "leaflet-control-zoom",
                    n = o.DomUtil.create("div", e + " leaflet-bar");
                return this._map = t, this._zoomInButton = this._createButton(this.options.zoomInText, this.options.zoomInTitle, e + "-in", n, this._zoomIn, this), this._zoomOutButton = this._createButton(this.options.zoomOutText, this.options.zoomOutTitle, e + "-out", n, this._zoomOut, this), this._updateDisabled(), t.on("zoomend zoomlevelschange", this._updateDisabled, this), n
            },
            onRemove: function(t) {
                t.off("zoomend zoomlevelschange", this._updateDisabled, this)
            },
            _zoomIn: function(t) {
                this._map.zoomIn(t.shiftKey ? 3 : 1)
            },
            _zoomOut: function(t) {
                this._map.zoomOut(t.shiftKey ? 3 : 1)
            },
            _createButton: function(t, e, n, i, r, s) {
                var a = o.DomUtil.create("a", n, i);
                a.innerHTML = t, a.href = "#", a.title = e;
                var u = o.DomEvent.stopPropagation;
                return o.DomEvent.on(a, "click", u).on(a, "mousedown", u).on(a, "dblclick", u).on(a, "click", o.DomEvent.preventDefault).on(a, "click", r, s).on(a, "click", this._refocusOnMap, s), a
            },
            _updateDisabled: function() {
                var t = this._map,
                    e = "leaflet-disabled";
                o.DomUtil.removeClass(this._zoomInButton, e), o.DomUtil.removeClass(this._zoomOutButton, e), t._zoom === t.getMinZoom() && o.DomUtil.addClass(this._zoomOutButton, e), t._zoom === t.getMaxZoom() && o.DomUtil.addClass(this._zoomInButton, e)
            }
        }), o.Map.mergeOptions({
            zoomControl: !0
        }), o.Map.addInitHook(function() {
            this.options.zoomControl && (this.zoomControl = new o.Control.Zoom, this.addControl(this.zoomControl))
        }), o.control.zoom = function(t) {
            return new o.Control.Zoom(t)
        }, o.Control.Attribution = o.Control.extend({
            options: {
                position: "bottomright",
                prefix: '<a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a>'
            },
            initialize: function(t) {
                o.setOptions(this, t), this._attributions = {}
            },
            onAdd: function(t) {
                this._container = o.DomUtil.create("div", "leaflet-control-attribution"), o.DomEvent.disableClickPropagation(this._container);
                for (var e in t._layers) t._layers[e].getAttribution && this.addAttribution(t._layers[e].getAttribution());
                return t.on("layeradd", this._onLayerAdd, this).on("layerremove", this._onLayerRemove, this), this._update(), this._container
            },
            onRemove: function(t) {
                t.off("layeradd", this._onLayerAdd).off("layerremove", this._onLayerRemove)
            },
            setPrefix: function(t) {
                return this.options.prefix = t, this._update(), this
            },
            addAttribution: function(t) {
                return t ? (this._attributions[t] || (this._attributions[t] = 0), this._attributions[t]++, this._update(), this) : void 0
            },
            removeAttribution: function(t) {
                return t ? (this._attributions[t] && (this._attributions[t]--, this._update()), this) : void 0
            },
            _update: function() {
                if (this._map) {
                    var t = [];
                    for (var e in this._attributions) this._attributions[e] && t.push(e);
                    var n = [];
                    this.options.prefix && n.push(this.options.prefix), t.length && n.push(t.join(", ")), this._container.innerHTML = n.join(" | ")
                }
            },
            _onLayerAdd: function(t) {
                t.layer.getAttribution && this.addAttribution(t.layer.getAttribution())
            },
            _onLayerRemove: function(t) {
                t.layer.getAttribution && this.removeAttribution(t.layer.getAttribution())
            }
        }), o.Map.mergeOptions({
            attributionControl: !0
        }), o.Map.addInitHook(function() {
            this.options.attributionControl && (this.attributionControl = (new o.Control.Attribution).addTo(this))
        }), o.control.attribution = function(t) {
            return new o.Control.Attribution(t)
        }, o.Control.Scale = o.Control.extend({
            options: {
                position: "bottomleft",
                maxWidth: 100,
                metric: !0,
                imperial: !0,
                updateWhenIdle: !1
            },
            onAdd: function(t) {
                this._map = t;
                var e = "leaflet-control-scale",
                    n = o.DomUtil.create("div", e),
                    i = this.options;
                return this._addScales(i, e, n), t.on(i.updateWhenIdle ? "moveend" : "move", this._update, this), t.whenReady(this._update, this), n
            },
            onRemove: function(t) {
                t.off(this.options.updateWhenIdle ? "moveend" : "move", this._update, this)
            },
            _addScales: function(t, e, n) {
                t.metric && (this._mScale = o.DomUtil.create("div", e + "-line", n)), t.imperial && (this._iScale = o.DomUtil.create("div", e + "-line", n))
            },
            _update: function() {
                var t = this._map.getBounds(),
                    e = t.getCenter().lat,
                    n = 6378137 * Math.PI * Math.cos(e * Math.PI / 180),
                    i = n * (t.getNorthEast().lng - t.getSouthWest().lng) / 180,
                    o = this._map.getSize(),
                    r = this.options,
                    s = 0;
                o.x > 0 && (s = i * (r.maxWidth / o.x)), this._updateScales(r, s)
            },
            _updateScales: function(t, e) {
                t.metric && e && this._updateMetric(e), t.imperial && e && this._updateImperial(e)
            },
            _updateMetric: function(t) {
                var e = this._getRoundNum(t);
                this._mScale.style.width = this._getScaleWidth(e / t) + "px", this._mScale.innerHTML = 1e3 > e ? e + " m" : e / 1e3 + " km"
            },
            _updateImperial: function(t) {
                var e, n, i, o = 3.2808399 * t,
                    r = this._iScale;
                o > 5280 ? (e = o / 5280, n = this._getRoundNum(e), r.style.width = this._getScaleWidth(n / e) + "px", r.innerHTML = n + " mi") : (i = this._getRoundNum(o), r.style.width = this._getScaleWidth(i / o) + "px", r.innerHTML = i + " ft")
            },
            _getScaleWidth: function(t) {
                return Math.round(this.options.maxWidth * t) - 10
            },
            _getRoundNum: function(t) {
                var e = Math.pow(10, (Math.floor(t) + "").length - 1),
                    n = t / e;
                return n = n >= 10 ? 10 : n >= 5 ? 5 : n >= 3 ? 3 : n >= 2 ? 2 : 1, e * n
            }
        }), o.control.scale = function(t) {
            return new o.Control.Scale(t)
        }, o.Control.Layers = o.Control.extend({
            options: {
                collapsed: !0,
                position: "topright",
                autoZIndex: !0
            },
            initialize: function(t, e, n) {
                o.setOptions(this, n), this._layers = {}, this._lastZIndex = 0, this._handlingClick = !1;
                for (var i in t) this._addLayer(t[i], i);
                for (i in e) this._addLayer(e[i], i, !0)
            },
            onAdd: function(t) {
                return this._initLayout(), this._update(), t.on("layeradd", this._onLayerChange, this).on("layerremove", this._onLayerChange, this), this._container
            },
            onRemove: function(t) {
                t.off("layeradd", this._onLayerChange, this).off("layerremove", this._onLayerChange, this)
            },
            addBaseLayer: function(t, e) {
                return this._addLayer(t, e), this._update(), this
            },
            addOverlay: function(t, e) {
                return this._addLayer(t, e, !0), this._update(), this
            },
            removeLayer: function(t) {
                var e = o.stamp(t);
                return delete this._layers[e], this._update(), this
            },
            _initLayout: function() {
                var t = "leaflet-control-layers",
                    e = this._container = o.DomUtil.create("div", t);
                e.setAttribute("aria-haspopup", !0), o.Browser.touch ? o.DomEvent.on(e, "click", o.DomEvent.stopPropagation) : o.DomEvent.disableClickPropagation(e).disableScrollPropagation(e);
                var n = this._form = o.DomUtil.create("form", t + "-list");
                if (this.options.collapsed) {
                    o.Browser.android || o.DomEvent.on(e, "mouseover", this._expand, this).on(e, "mouseout", this._collapse, this);
                    var i = this._layersLink = o.DomUtil.create("a", t + "-toggle", e);
                    i.href = "#", i.title = "Layers", o.Browser.touch ? o.DomEvent.on(i, "click", o.DomEvent.stop).on(i, "click", this._expand, this) : o.DomEvent.on(i, "focus", this._expand, this), o.DomEvent.on(n, "click", function() {
                        setTimeout(o.bind(this._onInputClick, this), 0)
                    }, this), this._map.on("click", this._collapse, this)
                } else this._expand();
                this._baseLayersList = o.DomUtil.create("div", t + "-base", n), this._separator = o.DomUtil.create("div", t + "-separator", n), this._overlaysList = o.DomUtil.create("div", t + "-overlays", n), e.appendChild(n)
            },
            _addLayer: function(t, e, n) {
                var i = o.stamp(t);
                this._layers[i] = {
                    layer: t,
                    name: e,
                    overlay: n
                }, this.options.autoZIndex && t.setZIndex && (this._lastZIndex++, t.setZIndex(this._lastZIndex))
            },
            _update: function() {
                if (this._container) {
                    this._baseLayersList.innerHTML = "", this._overlaysList.innerHTML = "";
                    var t, e, n = !1,
                        i = !1;
                    for (t in this._layers) e = this._layers[t], this._addItem(e), i = i || e.overlay, n = n || !e.overlay;
                    this._separator.style.display = i && n ? "" : "none"
                }
            },
            _onLayerChange: function(t) {
                var e = this._layers[o.stamp(t.layer)];
                if (e) {
                    this._handlingClick || this._update();
                    var n = e.overlay ? "layeradd" === t.type ? "overlayadd" : "overlayremove" : "layeradd" === t.type ? "baselayerchange" : null;
                    n && this._map.fire(n, e)
                }
            },
            _createRadioElement: function(t, n) {
                var i = '<input type="radio" class="leaflet-control-layers-selector" name="' + t + '"';
                n && (i += ' checked="checked"'), i += "/>";
                var o = e.createElement("div");
                return o.innerHTML = i, o.firstChild
            },
            _addItem: function(t) {
                var n, i = e.createElement("label"),
                    r = this._map.hasLayer(t.layer);
                t.overlay ? (n = e.createElement("input"), n.type = "checkbox", n.className = "leaflet-control-layers-selector", n.defaultChecked = r) : n = this._createRadioElement("leaflet-base-layers", r), n.layerId = o.stamp(t.layer), o.DomEvent.on(n, "click", this._onInputClick, this);
                var s = e.createElement("span");
                s.innerHTML = " " + t.name, i.appendChild(n), i.appendChild(s);
                var a = t.overlay ? this._overlaysList : this._baseLayersList;
                return a.appendChild(i), i
            },
            _onInputClick: function() {
                var t, e, n, i = this._form.getElementsByTagName("input"),
                    o = i.length;
                for (this._handlingClick = !0, t = 0; o > t; t++) e = i[t], n = this._layers[e.layerId], e.checked && !this._map.hasLayer(n.layer) ? this._map.addLayer(n.layer) : !e.checked && this._map.hasLayer(n.layer) && this._map.removeLayer(n.layer);
                this._handlingClick = !1, this._refocusOnMap()
            },
            _expand: function() {
                o.DomUtil.addClass(this._container, "leaflet-control-layers-expanded")
            },
            _collapse: function() {
                this._container.className = this._container.className.replace(" leaflet-control-layers-expanded", "")
            }
        }), o.control.layers = function(t, e, n) {
            return new o.Control.Layers(t, e, n)
        }, o.PosAnimation = o.Class.extend({
            includes: o.Mixin.Events,
            run: function(t, e, n, i) {
                this.stop(), this._el = t, this._inProgress = !0, this._newPos = e, this.fire("start"), t.style[o.DomUtil.TRANSITION] = "all " + (n || .25) + "s cubic-bezier(0,0," + (i || .5) + ",1)", o.DomEvent.on(t, o.DomUtil.TRANSITION_END, this._onTransitionEnd, this), o.DomUtil.setPosition(t, e), o.Util.falseFn(t.offsetWidth), this._stepTimer = setInterval(o.bind(this._onStep, this), 50)
            },
            stop: function() {
                this._inProgress && (o.DomUtil.setPosition(this._el, this._getPos()), this._onTransitionEnd(), o.Util.falseFn(this._el.offsetWidth))
            },
            _onStep: function() {
                var t = this._getPos();
                return t ? (this._el._leaflet_pos = t, void this.fire("step")) : void this._onTransitionEnd()
            },
            _transformRe: /([-+]?(?:\d*\.)?\d+)\D*, ([-+]?(?:\d*\.)?\d+)\D*\)/,
            _getPos: function() {
                var e, n, i, r = this._el,
                    s = t.getComputedStyle(r);
                if (o.Browser.any3d) {
                    if (i = s[o.DomUtil.TRANSFORM].match(this._transformRe), !i) return;
                    e = parseFloat(i[1]), n = parseFloat(i[2])
                } else e = parseFloat(s.left), n = parseFloat(s.top);
                return new o.Point(e, n, !0)
            },
            _onTransitionEnd: function() {
                o.DomEvent.off(this._el, o.DomUtil.TRANSITION_END, this._onTransitionEnd, this), this._inProgress && (this._inProgress = !1, this._el.style[o.DomUtil.TRANSITION] = "", this._el._leaflet_pos = this._newPos, clearInterval(this._stepTimer), this.fire("step").fire("end"))
            }
        }), o.Map.include({
            setView: function(t, e, i) {
                if (e = e === n ? this._zoom : this._limitZoom(e), t = this._limitCenter(o.latLng(t), e, this.options.maxBounds), i = i || {}, this._panAnim && this._panAnim.stop(), this._loaded && !i.reset && i !== !0) {
                    i.animate !== n && (i.zoom = o.extend({
                        animate: i.animate
                    }, i.zoom), i.pan = o.extend({
                        animate: i.animate
                    }, i.pan));
                    var r = this._zoom !== e ? this._tryAnimatedZoom && this._tryAnimatedZoom(t, e, i.zoom) : this._tryAnimatedPan(t, i.pan);
                    if (r) return clearTimeout(this._sizeTimer), this
                }
                return this._resetView(t, e), this
            },
            panBy: function(t, e) {
                if (t = o.point(t).round(), e = e || {}, !t.x && !t.y) return this;
                if (this._panAnim || (this._panAnim = new o.PosAnimation, this._panAnim.on({
                        step: this._onPanTransitionStep,
                        end: this._onPanTransitionEnd
                    }, this)), e.noMoveStart || this.fire("movestart"), e.animate !== !1) {
                    o.DomUtil.addClass(this._mapPane, "leaflet-pan-anim");
                    var n = this._getMapPanePos().subtract(t);
                    this._panAnim.run(this._mapPane, n, e.duration || .25, e.easeLinearity)
                } else this._rawPanBy(t), this.fire("move").fire("moveend");
                return this
            },
            _onPanTransitionStep: function() {
                this.fire("move")
            },
            _onPanTransitionEnd: function() {
                o.DomUtil.removeClass(this._mapPane, "leaflet-pan-anim"), this.fire("moveend")
            },
            _tryAnimatedPan: function(t, e) {
                var n = this._getCenterOffset(t)._floor();
                return (e && e.animate) === !0 || this.getSize().contains(n) ? (this.panBy(n, e), !0) : !1
            }
        }), o.PosAnimation = o.DomUtil.TRANSITION ? o.PosAnimation : o.PosAnimation.extend({
            run: function(t, e, n, i) {
                this.stop(), this._el = t, this._inProgress = !0, this._duration = n || .25, this._easeOutPower = 1 / Math.max(i || .5, .2), this._startPos = o.DomUtil.getPosition(t), this._offset = e.subtract(this._startPos), this._startTime = +new Date, this.fire("start"), this._animate()
            },
            stop: function() {
                this._inProgress && (this._step(), this._complete())
            },
            _animate: function() {
                this._animId = o.Util.requestAnimFrame(this._animate, this), this._step()
            },
            _step: function() {
                var t = +new Date - this._startTime,
                    e = 1e3 * this._duration;
                e > t ? this._runFrame(this._easeOut(t / e)) : (this._runFrame(1), this._complete())
            },
            _runFrame: function(t) {
                var e = this._startPos.add(this._offset.multiplyBy(t));
                o.DomUtil.setPosition(this._el, e), this.fire("step")
            },
            _complete: function() {
                o.Util.cancelAnimFrame(this._animId), this._inProgress = !1, this.fire("end")
            },
            _easeOut: function(t) {
                return 1 - Math.pow(1 - t, this._easeOutPower)
            }
        }), o.Map.mergeOptions({
            zoomAnimation: !0,
            zoomAnimationThreshold: 4
        }), o.DomUtil.TRANSITION && o.Map.addInitHook(function() {
            this._zoomAnimated = this.options.zoomAnimation && o.DomUtil.TRANSITION && o.Browser.any3d && !o.Browser.android23 && !o.Browser.mobileOpera, this._zoomAnimated && o.DomEvent.on(this._mapPane, o.DomUtil.TRANSITION_END, this._catchTransitionEnd, this)
        }), o.Map.include(o.DomUtil.TRANSITION ? {
            _catchTransitionEnd: function(t) {
                this._animatingZoom && t.propertyName.indexOf("transform") >= 0 && this._onZoomTransitionEnd()
            },
            _nothingToAnimate: function() {
                return !this._container.getElementsByClassName("leaflet-zoom-animated").length
            },
            _tryAnimatedZoom: function(t, e, n) {
                if (this._animatingZoom) return !0;
                if (n = n || {}, !this._zoomAnimated || n.animate === !1 || this._nothingToAnimate() || Math.abs(e - this._zoom) > this.options.zoomAnimationThreshold) return !1;
                var i = this.getZoomScale(e),
                    o = this._getCenterOffset(t)._divideBy(1 - 1 / i),
                    r = this._getCenterLayerPoint()._add(o);
                return n.animate === !0 || this.getSize().contains(o) ? (this.fire("movestart").fire("zoomstart"), this._animateZoom(t, e, r, i, null, !0), !0) : !1
            },
            _animateZoom: function(t, e, n, i, r, s, a) {
                a || (this._animatingZoom = !0), o.DomUtil.addClass(this._mapPane, "leaflet-zoom-anim"), this._animateToCenter = t, this._animateToZoom = e, o.Draggable && (o.Draggable._disabled = !0), o.Util.requestAnimFrame(function() {
                    this.fire("zoomanim", {
                        center: t,
                        zoom: e,
                        origin: n,
                        scale: i,
                        delta: r,
                        backwards: s
                    }), setTimeout(o.bind(this._onZoomTransitionEnd, this), 250)
                }, this)
            },
            _onZoomTransitionEnd: function() {
                this._animatingZoom && (this._animatingZoom = !1, o.DomUtil.removeClass(this._mapPane, "leaflet-zoom-anim"), o.Util.requestAnimFrame(function() {
                    this._resetView(this._animateToCenter, this._animateToZoom, !0, !0), o.Draggable && (o.Draggable._disabled = !1)
                }, this))
            }
        } : {}), o.TileLayer.include({
            _animateZoom: function(t) {
                this._animating || (this._animating = !0, this._prepareBgBuffer());
                var e = this._bgBuffer,
                    n = o.DomUtil.TRANSFORM,
                    i = t.delta ? o.DomUtil.getTranslateString(t.delta) : e.style[n],
                    r = o.DomUtil.getScaleString(t.scale, t.origin);
                e.style[n] = t.backwards ? r + " " + i : i + " " + r
            },
            _endZoomAnim: function() {
                var t = this._tileContainer,
                    e = this._bgBuffer;
                t.style.visibility = "", t.parentNode.appendChild(t), o.Util.falseFn(e.offsetWidth);
                var n = this._map.getZoom();
                (n > this.options.maxZoom || n < this.options.minZoom) && this._clearBgBuffer(), this._animating = !1
            },
            _clearBgBuffer: function() {
                var t = this._map;
                !t || t._animatingZoom || t.touchZoom._zooming || (this._bgBuffer.innerHTML = "", this._bgBuffer.style[o.DomUtil.TRANSFORM] = "")
            },
            _prepareBgBuffer: function() {
                var t = this._tileContainer,
                    e = this._bgBuffer,
                    n = this._getLoadedTilesPercentage(e),
                    i = this._getLoadedTilesPercentage(t);
                return e && n > .5 && .5 > i ? (t.style.visibility = "hidden", void this._stopLoadingImages(t)) : (e.style.visibility = "hidden", e.style[o.DomUtil.TRANSFORM] = "", this._tileContainer = e, e = this._bgBuffer = t, this._stopLoadingImages(e), void clearTimeout(this._clearBgBufferTimer))
            },
            _getLoadedTilesPercentage: function(t) {
                var e, n, i = t.getElementsByTagName("img"),
                    o = 0;
                for (e = 0, n = i.length; n > e; e++) i[e].complete && o++;
                return o / n
            },
            _stopLoadingImages: function(t) {
                var e, n, i, r = Array.prototype.slice.call(t.getElementsByTagName("img"));
                for (e = 0, n = r.length; n > e; e++) i = r[e], i.complete || (i.onload = o.Util.falseFn, i.onerror = o.Util.falseFn, i.src = o.Util.emptyImageUrl, i.parentNode.removeChild(i))
            }
        }), o.Map.include({
            _defaultLocateOptions: {
                watch: !1,
                setView: !1,
                maxZoom: 1 / 0,
                timeout: 1e4,
                maximumAge: 0,
                enableHighAccuracy: !1
            },
            locate: function(t) {
                if (t = this._locateOptions = o.extend(this._defaultLocateOptions, t), !navigator.geolocation) return this._handleGeolocationError({
                    code: 0,
                    message: "Geolocation not supported."
                }), this;
                var e = o.bind(this._handleGeolocationResponse, this),
                    n = o.bind(this._handleGeolocationError, this);
                return t.watch ? this._locationWatchId = navigator.geolocation.watchPosition(e, n, t) : navigator.geolocation.getCurrentPosition(e, n, t), this
            },
            stopLocate: function() {
                return navigator.geolocation && navigator.geolocation.clearWatch(this._locationWatchId), this._locateOptions && (this._locateOptions.setView = !1), this
            },
            _handleGeolocationError: function(t) {
                var e = t.code,
                    n = t.message || (1 === e ? "permission denied" : 2 === e ? "position unavailable" : "timeout");
                this._locateOptions.setView && !this._loaded && this.fitWorld(), this.fire("locationerror", {
                    code: e,
                    message: "Geolocation error: " + n + "."
                })
            },
            _handleGeolocationResponse: function(t) {
                var e = t.coords.latitude,
                    n = t.coords.longitude,
                    i = new o.LatLng(e, n),
                    r = 180 * t.coords.accuracy / 40075017,
                    s = r / Math.cos(o.LatLng.DEG_TO_RAD * e),
                    a = o.latLngBounds([e - r, n - s], [e + r, n + s]),
                    u = this._locateOptions;
                if (u.setView) {
                    var l = Math.min(this.getBoundsZoom(a), u.maxZoom);
                    this.setView(i, l)
                }
                var c = {
                    latlng: i,
                    bounds: a,
                    timestamp: t.timestamp
                };
                for (var h in t.coords) "number" == typeof t.coords[h] && (c[h] = t.coords[h]);
                this.fire("locationfound", c)
            }
        })
}(window, document);
