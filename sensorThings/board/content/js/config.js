function config(t, e, a, r) {
    a.otherwise("/home"), r.setPrefix("stdash"), e.state("home", {
        url: "/home",
        templateUrl: "views/home.html",
        controller: "HomeCtrl",
        data: {
            pageTitle: "Home"
        }
    }).state("link", {
        url: "/link",
        templateUrl: "views/link.html",
        controller: "LinkCtrl",
        data: {
            pageTitle: "Link"
        }
    }).state("things", {
        url: "/things",
        templateUrl: "views/things.html",
        controller: "ThingsCtrl",
        data: {
            pageTitle: "Things"
        }
    }).state("thing", {
        url: "/thing/{thingId}",
        templateUrl: "views/thing.html",
        controller: "ThingCtrl",
        data: {
            pageTitle: "Thing"
        }
    }).state("locations", {
        url: "/locations",
        templateUrl: "views/locations.html",
        controller: "LocationsCtrl",
        data: {
            pageTitle: "Locations"
        }
    }).state("location", {
        url: "/location/{locationId}",
        templateUrl: "views/location.html",
        controller: "LocationCtrl",
        data: {
            pageTitle: "Location"
        }
    }).state("sensors", {
        url: "/sensors",
        templateUrl: "views/sensors.html",
        controller: "SensorsCtrl",
        data: {
            pageTitle: "Sensors"
        }
    }).state("sensor", {
        url: "/sensor/{sensorId}",
        templateUrl: "views/sensor.html",
        controller: "SensorCtrl",
        data: {
            pageTitle: "Sensor"
        }
    }).state("observations", {
        url: "/observations",
        templateUrl: "views/observations.html",
        controller: "ObservationsCtrl",
        data: {
            pageTitle: "Observations"
        }
    }).state("observation", {
        url: "/observation/{observationId}",
        templateUrl: "views/observation.html",
        controller: "ObservationCtrl",
        data: {
            pageTitle: "Observation"
        }
    }).state("observedProperties", {
        url: "/observedProperties",
        templateUrl: "views/observed-properties.html",
        controller: "ObservedPropertiesCtrl",
        data: {
            pageTitle: "Observed Properties"
        }
    }).state("observedProperty", {
        url: "/observedProperty/{observedPropertyId}",
        templateUrl: "views/observed-property.html",
        controller: "ObservedPropertyCtrl",
        data: {
            pageTitle: "Observed Property"
        }
    }).state("fois", {
        url: "/fois",
        templateUrl: "views/fois.html",
        controller: "FoisCtrl",
        data: {
            pageTitle: "Features of Interest"
        }
    }).state("foi", {
        url: "/foi/{foiId}",
        templateUrl: "views/foi.html",
        controller: "FoiCtrl",
        data: {
            pageTitle: "Feature of Interest"
        }
    }).state("datastreams", {
        url: "/datastreams",
        templateUrl: "views/datastreams.html",
        controller: "DatastreamsCtrl",
        data: {
            pageTitle: "Datastreams"
        }
    }).state("datastream", {
        url: "/datastream/{datastreamId}",
        templateUrl: "views/datastream.html",
        controller: "DatastreamCtrl",
        data: {
            pageTitle: "Datastream"
        }
    }).state("historicalLocations", {
        url: "/historicalLocations",
        templateUrl: "views/historical-locations.html",
        controller: "HistoricalLocationsCtrl",
        data: {
            pageTitle: "Historical Locations"
        }
    }).state("historicalLocation", {
        url: "/historicalLocation/{historicalLocationId}",
        templateUrl: "views/historical-location.html",
        controller: "HistoricalLocationCtrl",
        data: {
            pageTitle: "Historical Location"
        }
    }).state("settings", {
        url: "/settings",
        templateUrl: "views/settings.html",
        controller: "SettingsCtrl",
        data: {
            pageTitle: "Settings"
        }
    }).state("sessionStart", {
        url: "/sessionStart?t",
        templateUrl: "views/session-start.html",
        controller: "SessionStartCtrl",
        data: {
            pageTitle: "Session Start"
        }
    }).state("signInFailed", {
        url: "/signInFailed",
        templateUrl: "views/sign-in-failed.html",
        controller: "SignInFailedCtrl",
        data: {
            pageTitle: "Sign In Failed"
        }
    }).state("generators", {
        url: "/generators",
        templateUrl: "views/generators.html",
        controller: "GeneratorsCtrl",
        data: {
            pageTitle: "Generators"
        }
    })
}

var currentUrl            = location.protocol+'//' + location.hostname             ;  // Rac021
var currentUrlIncludePort = currentUrl + (location.port ? ':' + location.port: '') ;  // Rac021

angular.module( "sensorBoard" )
       .config(["$locationProvider"        , "$stateProvider", "$urlRouterProvider", "localStorageServiceProvider", config] )
       .value("initialStBaseUrl"           , currentUrl + ":8181/FROST-Server/v1.1") 
       // .value("initialStMqttUri"        , "ws://"  + location.hostname + ":9001/" ) // Rac021
       .value("initialStMqttUri"           , "" )                                      // Rac021
       .value("platformAuthBaseUrl"        , currentUrlIncludePort )
       .value("initialMopBaseUrl"          , "http://" + location.hostname  + "-mop-service")
       .value("shareableLinkBaseUrl"       , currentUrlIncludePort )
       .value("autoloadDatastreamPreviews" , "true").value("leafletBaseLayers", {
    
    openstreetmap_hot: {
        name: "OpenStreetMap HOT",
        url: "http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
        type: "xyz",
        layerOptions: {
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>'
        }
    },
    openstreetmap: {
        name: "OpenStreetMap",
        url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        type: "xyz",
        layerOptions: {
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>'
        }
    }
}
    
).run(["$anchorScroll", function(t) {
    t.yOffset = 50
}]).run(["$rootScope", "$state", function(t, e) {
    t.$state = e, t.$on("$stateChangeSuccess", function() {
        document.body.scrollTop = document.documentElement.scrollTop = 0
    })
}]);
