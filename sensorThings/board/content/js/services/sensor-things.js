angular.module("sensorBoard").factory("sensorThingsService", ["$http", "$q", "$rootScope", "initialStBaseUrl", "initialStMqttUri", "localStorageService", function(t, e, n, i, r, o) {
    var a = i,
        s = o.get("stBaseUrl");
    s && (a = s), n.rsGlobal = n.rsGlobal || {}, n.rsGlobal.stBaseUrl = a;
    var l = r,
        u = o.get("stMqttUri");
    u && (l = u);
    var c = o.get("stBaseUrlHistory") || [],
        h = o.get("basicAuthUsername") || "",
        d = o.get("basicAuthPassword") || "",
        f = o.get("xApiKey") || "",
        p = {},
        m = {};
    return m.setAuthorizationHeader = function() {
        h || d ? t.defaults.headers.common.Authorization = "Basic " + btoa(h + ":" + d) : delete t.defaults.headers.common.Authorization
    }, m.setAuthorizationHeader(), m.setXApiKeyHeader = function() {
        f ? t.defaults.headers.common["X-API-KEY"] = f : delete t.defaults.headers.common["X-API-KEY"]
    }, m.setXApiKeyHeader(), p.setStBasicAuthUsername = function(t) {
        h = t, o.set("basicAuthUsername", t), m.setAuthorizationHeader()
    }, p.getStBasicAuthUsername = function() {
        return h
    }, p.setStBasicAuthPassword = function(t) {
        d = t, o.set("basicAuthPassword", t), m.setAuthorizationHeader()
    }, p.getStBasicAuthPassword = function() {
        return d
    }, p.setStXApiKey = function(t) {
        f = t, o.set("xApiKey", t), m.setXApiKeyHeader()
    }, p.getStXApiKey = function() {
        return f
    }, p.setStMqttUri = function(t) {
        l = t, o.set("stMqttUri", l)
    }, p.getStMqttUri = function() {
        return l
    }, p.setStBaseUrl = function(t) {
        t !== a && (c.push({
            url: a
        }), c = c.length > 8 ? c.slice(1) : c, o.set("stBaseUrlHistory", c)), o.set("stBaseUrl", t), a = t, n.rsGlobal.stBaseUrl = a
    }, p.getStBaseUrl = function() {
        return a
    }, p.getStBaseUrlHistory = function() {
        return c
    }, p.getThings = function(e) {
        return t.get(a + "/Things", {
            params: e
        })
    }, p.getLocations = function(e) {
        return t.get(a + "/Locations", {
            params: e
        })
    }, p.getHistoricalLocations = function(e) {
        return t.get(a + "/HistoricalLocations", {
            params: e
        })
    }, p.getDatastreams = function(e) {
        return t.get(a + "/Datastreams", {
            params: e
        })
    }, p.getSensors = function(e) {
        return t.get(a + "/Sensors", {
            params: e
        })
    }, p.getObservations = function(e) {
        return t.get(a + "/Observations", {
            params: e
        })
    }, p.getObservedProperties = function(e) {
        return t.get(a + "/ObservedProperties", {
            params: e
        })
    }, p.getFeaturesOfInterest = function(e) {
        return t.get(a + "/FeaturesOfInterest", {
            params: e
        })
    }, p.getNextLink = function(e) {
        return t.get(e)
    }, p.getThing = function(e) {
        return t.get(a + "/Things(" + e + ")")
    }, p.getLocation = function(e) {
        return t.get(a + "/Locations(" + e + ")")
    }, p.getHistoricalLocation = function(e) {
        return t.get(a + "/HistoricalLocations(" + e + ")")
    }, p.getDatastream = function(e, n) {
        return t.get(a + "/Datastreams(" + e + ")", {
            params: n
        })
    }, p.getSensor = function(e) {
        return t.get(a + "/Sensors(" + e + ")")
    }, p.getObservation = function(e) {
        return t.get(a + "/Observations(" + e + ")")
    }, p.getObservedProperty = function(e) {
        return t.get(a + "/ObservedProperties(" + e + ")")
    }, p.getFeatureOfInterest = function(e) {
        return t.get(a + "/FeaturesOfInterest(" + e + ")")
    }, p.postThing = function(e) {
        return t.post(a + "/Things", e)
    }, p.postLocation = function(e) {
        return t.post(a + "/Locations", e)
    }, p.postSensor = function(e) {
        return t.post(a + "/Sensors", e)
    }, p.postObservation = function(e) {
        return t.post(a + "/Observations", e)
    }, p.postObservedProperty = function(e) {
        return t.post(a + "/ObservedProperties", e)
    }, p.postFeatureOfInterest = function(e) {
        return t.post(a + "/FeaturesOfInterest", e)
    }, p.postDatastream = function(e) {
        return t.post(a + "/Datastreams", e)
    }, p.postHistoricalLocation = function(e) {
        return t.post(a + "/HistoricalLocations", e)
    }, p.patchThing = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Things('%s')" : "%s/Things(%s)" // Rac021
        return t.patch(sprintf( url , a, e["@iot.id"]), e)
    }, p.patchLocation = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Locations('%s')" : "%s/Locations(%s)" // Rac021
        return t.patch(sprintf( url , a, e["@iot.id"]), e)
    }, p.patchHistoricalLocation = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/HistoricalLocations('%s')" : "%s/HistoricalLocations(%s)" // Rac021
        return t.patch(sprintf( url , a, e["@iot.id"]), e)
    }, p.patchObservation = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Observations('%s')" : "%s/Observations(%s)" // Rac021
        return t.patch(sprintf( url , a, e["@iot.id"]), e)
    }, p.patchObservedProperty = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/ObservedProperties('%s')" : "%s/ObservedProperties(%s)" // Rac021
        return t.patch(sprintf( url , a, e["@iot.id"]), e)
    }, p.patchDatastream = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Datastreams('%s')" : "%s/Datastreams(%s)" // Rac021
        return t.patch(sprintf( url , a, e["@iot.id"]), e)
    }, p.patchSensor = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Sensors('%s')" : "%s/Sensors(%s)" // Rac021
        return t.patch(sprintf(url , a, e["@iot.id"]), e)
    }, p.patchFoi = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/FeaturesOfInterest('%s')" : "%s/FeaturesOfInterest(%s)" // Rac021
        return t.patch(sprintf( url , a, e["@iot.id"]), e)
    }, p.deleteThing = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/Things('" + e + "')" : "/Things(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.deleteLocation = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/Locations('" + e + "')" : "/Locations(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.deleteSensor = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/Sensors('" + e + "')" : "/Sensors(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.deleteObservation = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/Observations('" + e + "')" : "/Observations(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.deleteObservedProperty = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/ObservedProperties('" + e + "')" : "/ObservedProperties(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.deleteFeatureOfInterest = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/FeaturesOfInterest('" + e + "')" : "/FeaturesOfInterest(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.deleteDatastream = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/Datastreams('" + e + "')" : "/Datastreams(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.deleteHistoricalLocation = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/HistoricalLocations('" + e + "')" : "/HistoricalLocations(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.deleteFeatureOfInterest = function(e) {
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "/FeaturesOfInterest('" + e + "')" : "/FeaturesOfInterest(" + e + ")" // Rac021
        return t["delete"](a + url )
    }, p.getCompleteThing = function(n) {
       
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Things('%s')" : "%s/Things(%s)"

        return e.all([t.get(sprintf( url , a, n)), t.get(sprintf( url + "/Datastreams", a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        }), t.get(sprintf( url + "/HistoricalLocations?$expand=Locations", a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        }), t.get(sprintf( url + "/Locations", a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        })]).then(function(t) {
            var e = t[0].data;
            return e.Datastreams = t[1].data.value, e.HistoricalLocations = t[2].data.value, e.Locations = t[3].data.value, e
        })
    }, p.getCompleteLocation = function(n) {
        
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Locations('%s')" : "%s/Locations(%s)"

        return e.all([t.get(sprintf( url, a, n)), t.get(sprintf( url + "/Things", a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        }), t.get(sprintf( url + "/HistoricalLocations?$expand=Things", a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        })]).then(function(t) {
            var e = t[0].data;
            return e.Things = t[1].data.value, e.HistoricalLocations = t[2].data.value, e
        })
    }, p.getCompleteHistoricalLocation = function(n) {
        
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/HistoricalLocations('%s')" : "%s/HistoricalLocations(%s)"
        
        return e.all([t.get(sprintf( url , a, n)), t.get(sprintf( url +"/Thing", a, n)), t.get(sprintf( url +"/Locations", a, n))]).then(function(t) {
            var e = t[0].data;
            return e.Thing = t[1].data, e.Locations = t[2].data.value, e
        })
    }, p.getCompleteObservation = function(n) {
        
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Observations('%s')" : "%s/Observations(%s)"
        
        return e.all([t.get(sprintf( url , a, n)), t.get(sprintf( url + "/Datastream", a, n)), t.get(sprintf( url + "/FeatureOfInterest", a, n))]).then(function(t) {
            var e = t[0].data;
            return e.Datastream = t[1].data, e.FeatureOfInterest = t[2].data, e
        })
    }, p.getCompleteObservedProperty = function(n) {
        
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/ObservedProperties('%s')" : "%s/ObservedProperties(%s)"
        
        return e.all([t.get(sprintf( url , a, n)), t.get(sprintf(  url + "/Datastreams" , a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        })]).then(function(t) {
            var e = t[0].data;
            return e.Datastreams = t[1].data.value, e
        })
    }, p.getCompleteDatastream = function(n) {
        
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Datastreams('%s')" : "%s/Datastreams(%s)" 
        
        return e.all([t.get(sprintf( url , a, n)), t.get(sprintf( url + "/Observations?$orderby=phenomenonTime desc" , a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        }), t.get(sprintf( url + "/Sensor", a, n)), t.get(sprintf( url + "/ObservedProperty", a, n)), t.get(sprintf( url + "/Thing", a, n))]).then(function(t) {
            var e = t[0].data;
            return e.Observations = t[1].data, e.Sensor = t[2].data, e.ObservedProperty = t[3].data, e.Thing = t[4].data, e
        })
    }, p.getCompleteSensor = function(n) {
        
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/Sensors('%s')" : "%s/Sensors(%s)"
        
        return e.all([t.get(sprintf( url , a, n)), t.get(sprintf( url + "/Datastreams" , a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        })]).then(function(t) {
            var e = t[0].data;
            return e.Datastreams = t[1].data.value, e
        })
    }, p.getCompleteFoi = function(n) {
        
        url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "%s/FeaturesOfInterest('%s')" : "%s/FeaturesOfInterest(%s)"
        
        return e.all([t.get(sprintf( url , a, n)), t.get(sprintf( url + "/Observations" , a, n)).then(function(t) {
            return t
        }, function() {
            return {
                data: {
                    value: []
                }
            }
        })]).then(function(t) {
            var e = t[0].data;
            return e.Observations = t[1].data, e
        })
    }, p.deleteAllEntities = function() {
        var e = a.substr(0, a.lastIndexOf("/v1.1"));
        return t.post(e + "/admin/clearAll", {}, {
            transformResponse: void 0
        })
    }, p
}]);
