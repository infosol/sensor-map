angular.module("sensorBoard").factory("mopService", ["$http", "$q", "$rootScope", "initialMopBaseUrl", "localStorageService", function(t, e, n, i, r) {
    var o = i,
        a = r.get("mopBaseUrl");
    a && (o = a);
    var s = {};
    return s.autocomplete = function(e) {
        return t.get(o + "/autocomplete", {
            params: {
                q: e
            }
        })
    }, s
}]);
