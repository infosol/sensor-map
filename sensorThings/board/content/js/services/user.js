angular.module("sensorBoard").factory("userService", ["$http", "$q", "initialStBaseUrl", "platformAuthBaseUrl", "localStorageService", function(t, e, n, i, r) {
    var o = {};
    return o.storeSessionToken = function(t) {
        r.set("sessionToken", t)
    }, o.getSessionToken = function() {
        return r.get("sessionToken")
    }, o.getProfile = function() {
        var e = o.getSessionToken();
        return t.get(i + "/profile", {
            headers: {
                "Su-Session-Token": e
            }
        })
    }, o
}]);
