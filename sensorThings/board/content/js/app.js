! function() {
    angular.module("sensorBoard", ["ui.router", "ui.bootstrap", "LocalStorageModule", "nvd3", "angularMoment", "angular-uuid", "angular-clipboard", "nemLogging", "ui-leaflet", "angulartics", "angulartics.google.analytics", "toastr"])
}();
