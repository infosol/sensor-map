angular.module("sensorBoard").directive("linkDirective", ["sensorThingsService", "shareableLinkBaseUrl", "$uibModal", function(t, e, n) {
    return {
        restrict: "E",
        scope: {
            asLink: "@asLink",
            page: "@page",
            anchor: "@anchor",
            stateParams: "=stateParams"
        },
        templateUrl: "./views/link-directive.html",
        controller: ["$scope", function(r) {
            var i = _.pickBy({
                    stBaseUrl: t.getStBaseUrl(),
                    stMqttUri: t.getStMqttUri(),
                    page: r.page,
                    anchor: r.anchor,
                    params: JSON.stringify(r.stateParams)
                }, function(t) {
                    return t
                }),
                o = _.map(i, function(t, e) {
                    return e + "=" + encodeURIComponent(t)
                }).join("&");
            r.openModal = function() {
                n.open({
                    animation: !0,
                    templateUrl: "link-directive-modal-template.html",
                    controller: ["$scope", "$uibModalInstance", function(t, n) {
                        t.shareableLink = e + "/#/link?" + o, t.close = function() {
                            n.dismiss()
                        }
                    }]
                })
            }
        }]
    }
}]);
