angular.module("sensorBoard").controller("FoiCtrl", ["$scope", "$state", "$stateParams", "$timeout", "sensorThingsService", "leafletData", "leafletBaseLayers", function(t, e, n, r, i, o, a) {
    t.stateParams = n, t.getFoiState = {
        foiId: n.foiId,
        lastRequestFailed: !1
    }, t.editFoiState = {
        name: "",
        description: "",
        encodingType: "",
        feature: "",
        lastRequestFailed: !1
    }, angular.extend(t, {
        markers: {},
        layers: {
            baselayers: a,
            overlays: {
                primary: {
                    name: "Points",
                    type: "markercluster",
                    visible: !0
                },
                complex: {
                    name: "Shapes",
                    type: "geoJSONShape",
                    visible: "true",
                    layerOptions: {
                        style: {
                            color: "#00D",
                            fillColor: "red",
                            weight: 2,
                            opacity: .6,
                            fillOpacity: .2
                        }
                    },
                    data: {
                        type: "FeatureCollection",
                        features: [{}]
                    }
                }
            }
        },
        defaults: {
            scrollWheelZoom: !1
        }
    }), t.processNextLink = function(t) {
        i.getNextLink(t["@iot.nextLink"]).then(function(e) {
            t.value = t.value.concat(e.data.value), t["@iot.nextLink"] = e.data["@iot.nextLink"] || null
        })
    }, t.getCompleteFoi = function(e) {
        i.getCompleteFoi(e).then(function(e) {
            if (t.completeFoi = e, t.prepareEditState(), t.getFoiState.lastRequestFailed = !1, t.markers = {}, t.layers.overlays.complex.data.features = [], "Point" == e.feature.type) t.markers[location["@iot.id"]] = {
                layer: "primary",
                lat: e.feature.coordinates[1],
                lng: e.feature.coordinates[0]
            };
            else if ("Polygon" == e.feature.type) {
                var n = {
                    type: "Feature",
                    geometry: {
                        type: "Polygon",
                        coordinates: e.feature.coordinates
                    }
                };
                t.layers.overlays.complex.data.features.push(n)
            }
            t.layers.overlays.complex.doRefresh = !0, r(function() {
                o.getLayers().then(function(t) {
                    var n = t.overlays.primary.getLayers(),
                        r = new L.featureGroup(n).getBounds(),
                        i = t.overlays.complex.getLayers(),
                        a = new L.featureGroup(i).getBounds(),
                        s = r.extend(a);
                    o.getMap().then(function(t) {
                        t.fitBounds(s, {
                            maxZoom: "Point" == e.feature.type ? 8 : 17
                        })
                    })
                }), t.refreshMap()
            }, 500)
        }, function() {
            t.getFoiState.lastRequestFailed = !0
        })
    }, t.prepareEditState = function() {
        t.editFoiState.name = t.completeFoi.name, t.editFoiState.description = t.completeFoi.description, t.editFoiState.encodingType = t.completeFoi.encodingType, t.editFoiState.feature = JSON.stringify(t.completeFoi.feature)
    }, t.prepareEditRequestObject = function() {
        var e = {
            "@iot.id": t.completeFoi["@iot.id"],
            name: t.editFoiState.name,
            description: t.editFoiState.description,
            encodingType: t.editFoiState.encodingType,
            feature: JSON.parse(t.editFoiState.feature)
        };
        return e
    }, t.sendEditRequest = function() {
        try {
            t.sendEditRequestInternal()
        } catch (e) {
            t.editFoiState.lastRequestFailed = !0
        }
    }, t.sendEditRequestInternal = function() {
        i.patchFoi(t.prepareEditRequestObject()).then(function() {
            e.reload()
        }, function() {
            t.editFoiState.lastRequestFailed = !0
        })
    }, t.loadFoiState = function(t) {
        e.go("foi", {
            foiId: t
        })
    }, /*isNaN(n.foiId) || */ t.getCompleteFoi(t.getFoiState.foiId), t.refreshMap = function() {
        r(function() {
            o.getMap().then(function(t) {
                t.invalidateSize()
            })
        }, 300)
    }
}]);
