angular.module("sensorBoard").controller("ObservationCtrl", ["$scope", "$state", "$stateParams", "sensorThingsService", function(t, e, n, r) {
    t.stateParams = n, t.getObservationState = {
        observationId: n.observationId,
        lastRequestFailed: !1
    }, t.editObservationState = {
        phenomenonTime: "",
        result: "",
        resultQuality: "",
        lastRequestFailed: !1
    }, t.getCompleteObservation = function(e) {
        r.getCompleteObservation(e).then(function(e) {
            t.completeObservation = e, t.prepareEditState(), t.getObservationState.lastRequestFailed = !1
        }, function() {
            t.getObservationState.lastRequestFailed = !0
        })
    }, t.prepareEditState = function() {
        t.editObservationState.phenomenonTime = t.completeObservation.phenomenonTime, t.editObservationState.result = t.completeObservation.result, t.editObservationState.resultQuality = t.completeObservation.resultQuality
    }, t.prepareEditRequestObject = function() {
        var e = {
            "@iot.id": t.completeObservation["@iot.id"],
            phenomenonTime: t.editObservationState.phenomenonTime,
            result: t.editObservationState.result,
            resultQuality: t.editObservationState.resultQuality
        };
        return e
    }, t.sendEditRequest = function() {
        r.patchObservation(t.prepareEditRequestObject()).then(function() {
            e.reload()
        }, function() {
            t.editObservationState.lastRequestFailed = !0
        })
    }, t.loadObservationState = function(t) {
        e.go("observation", {
            observationId: t
        })
    }, /*isNaN(n.observationId) || */ t.getCompleteObservation(t.getObservationState.observationId)
}]);
