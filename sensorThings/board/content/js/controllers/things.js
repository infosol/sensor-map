angular.module("sensorBoard").controller("ThingsCtrl", ["$scope", "$state", "$timeout", "sensorThingsService", "helpers", function(t, e, n, r, i) {
    t.thingsNextLink = "", t.createThingState = {
        name: "",
        description: "",
        properties: "",
        locationId: "",
        locationIdValid: "",
        lastRequestFailed: !1
    }, t.queryState = {
        filterParam: "",
        orderbyParam: "id desc"
    }, t.loadMainListInProgress = !1, t.loadMainList = function() {
        t.loadMainListInProgress = !0;
        var e = {
            $count: "true"
        };
        t.queryState.filterParam && (e.$filter = t.queryState.filterParam), t.queryState.orderbyParam && (e.$orderby = t.queryState.orderbyParam), r.getThings(e).then(function(e) {
            t.thingsResponse = e.data, t.things = e.data.value, t.thingsNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1
             localStorage.setItem("IOT_ID_TYPE", typeof (e.data.value[0]["@iot.id"]) );
        }, function() {
            t.thingsResponse = null, t.things = [], t.thingsNextLink = null, t.loadMainListInProgress = !1
        })
    }, t.getNextLink = function(e) {
        r.getNextLink(e).then(function(e) {
            t.thingsResponse = e.data, t.things = t.things.concat(e.data.value), t.thingsNextLink = e.data["@iot.nextLink"] || null
        })
    }, t.createThing = function() {
        try {
            t.createThingInternal()
        } catch (e) {
            t.createThingState.lastRequestFailed = !0
        }
    }, t.createThingInternal = function() {
        var n = {
            name: t.createThingState.name,
            description: t.createThingState.description
        };
        t.createThingState.properties && (n.properties = JSON.parse(t.createThingState.properties)), t.createThingState.locationId && (n.Locations = [{
            "@iot.id": t.createThingState.locationId // "@iot.id": Number(t.createThingState.locationId) // Rac021
        }]), r.postThing(n).then(function() {
            e.reload()
        }, function() {
            t.createThingState.lastRequestFailed = !0
        })
    }, t.deleteThing = function(t) {
        r.deleteThing(t).then(function() {
            e.reload()
        }, function() {})
    }, t.locationIdChanged = function() {
        i.entityIdChanged(r.getLocation, t.createThingState.locationId, function(e) {
            t.createThingState.locationIdValid = e.data["@iot.id"]
        }, function() {
            t.createThingState.locationIdValid = null
        })
    }, t.loadMainList()
}]);
