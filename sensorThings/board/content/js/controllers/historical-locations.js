angular.module("sensorBoard").controller("HistoricalLocationsCtrl", ["$scope", "$state", "sensorThingsService", "helpers", function(t, e, n, r) {
    t.historicalLocationsNextLink = "", t.createHistoricalLocationState = {
        time: "",
        locationId: "",
        thingId: "",
        locationIdValid: null,
        thingIdValid: null,
        lastRequestFailed: !1
    }, t.loadMainListInProgress = !1, t.queryState = {
        filterParam: "",
        orderbyParam: "id desc"
    }, t.loadMainList = function() {
        t.loadMainListInProgress = !0;
        var e = {
            $count: "true"
        };
        t.queryState.filterParam && (e.$filter = t.queryState.filterParam), t.queryState.orderbyParam && (e.$orderby = t.queryState.orderbyParam), n.getHistoricalLocations(e).then(function(e) {
            t.historicalLocationsResponse = e.data, t.historicalLocations = e.data.value, t.historicalLocationsNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1
            localStorage.setItem("IOT_ID_TYPE", typeof (e.data.value[0]["@iot.id"]) ); // RAC021
        }, function() {
            t.historicalLocationsResponse = null, t.historicalLocations = [], t.historicalLocationsNextLink = null, t.loadMainListInProgress = !1
        })
    }, t.getNextLink = function(e) {
        n.getNextLink(e).then(function(e) {
            t.historicalLocationsResponse = e.data, t.historicalLocations = t.historicalLocations.concat(e.data.value), t.historicalLocationsNextLink = e.data["@iot.nextLink"] || null
        })
    }, t.createHistoricalLocation = function() {
        n.postHistoricalLocation({
            time: t.createHistoricalLocationState.time,
            Location: {
                "@iot.id": t.createHistoricalLocationState.locationId
            },
            Thing: {
                "@iot.id": t.createHistoricalLocationState.thingId
            }
        }).then(function() {
            e.reload()
        }, function() {
            t.createHistoricalLocationState.lastRequestFailed = !0
        })
    }, t.deleteHistoricalLocation = function(t) {
        n.deleteHistoricalLocation(t).then(function() {
            e.reload()
        }, function() {})
    }, t.locationIdChanged = function() {
        r.entityIdChanged(n.getLocation, t.createHistoricalLocationState.locationId, function(e) {
            t.createHistoricalLocationState.locationIdValid = e.data["@iot.id"]
        }, function() {
            t.createHistoricalLocationState.locationIdValid = null
        })
    }, t.thingIdChanged = function() {
        r.entityIdChanged(n.getThing, t.createHistoricalLocationState.thingId, function(e) {
            t.createHistoricalLocationState.thingIdValid = e.data["@iot.id"]
        }, function() {
            t.createHistoricalLocationState.thingIdValid = null
        })
    }, t.loadMainList()
}]);
