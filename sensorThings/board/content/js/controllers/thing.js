angular.module("sensorBoard").controller("ThingCtrl", ["$scope", "$state", "$stateParams", "$timeout", "$location", "sensorThingsService", "helpers", "autoloadDatastreamPreviews", "leafletData", "leafletBaseLayers", function(t, e, n, r, i, o, a, s, u, c) {
    function l() {
        r(function() { 
            u.getLayers().then(function(t) {
                var e = t.overlays.primary.getLayers(),
                    n = new L.featureGroup(e).getBounds(),
                    r = t.overlays.complex.getLayers(),
                    i = new L.featureGroup(r).getBounds(),
                    o = n.extend(i);
                u.getMap().then(function(t) {
                    t.fitBounds(o, {
                        maxZoom: 12
                    })
                })
            })
        }, 500)
    }
    t.stateParams = n, t.getThingState = {
        thingId: n.thingId,
        lastRequestFailed: !1
    }, t.editThingState = {
        name: "",
        description: "",
        properties: "",
        locationId: "",
        locationIdValid: null,
        lastRequestFailed: !1
    }, t.leftAxisLoaded = !1, t.rightAxisLoaded = !1, t.previewMargin = !1, t.leftDatastreamId = null, t.rightDatastreamId = null, t.activateTab = {}, t.nvd3 = {}, angular.extend(t, {
        markers: {},
        layers: {
            baselayers: c,
            overlays: {
                primary: {
                    name: "Points",
                    type: "markercluster",
                    visible: !0
                },
                complex: {
                    name: "Shapes",
                    type: "geoJSONShape",
                    visible: "true",
                    layerOptions: {
                        style: {
                            color: "#00D",
                            fillColor: "red",
                            weight: 2,
                            opacity: .6,
                            fillOpacity: .2
                        }
                    },
                    data: {
                        type: "FeatureCollection",
                        features: [{}]
                    }
                }
            }
        },
        defaults: {
            scrollWheelZoom: !1
        }
    }), t.getCompleteThing = function(e) {
        o.getCompleteThing(e).then(function(e) {
            t.completeThing = e, t.prepareEditState(), t.getThingState.lastRequestFailed = !1, t.markers = {}, t.layers.overlays.complex.data.features = [], e.Locations.forEach(function(e) {
                if ("Point" == e.location.type) try {
                    t.markers[e["@iot.id"]] = {
                        layer: "primary",
                        lat: e.location.coordinates[1],
                        lng: e.location.coordinates[0],
                        message: '<a href="#/location/' + e["@iot.id"] + '">' + _.escape(e.description) + "</a>"
                    }
                } catch (n) {} else if ("Polygon" == e.location.type) try {
                    var r = {
                        type: "Feature",
                        geometry: {
                            type: "Polygon",
                            coordinates: e.location.coordinates
                        }
                    };
                    t.layers.overlays.complex.data.features.push(r)
                } catch (n) {}
            }), t.layers.overlays.complex.doRefresh = !0, l(), i.hash() && (t.activateTab[i.hash()] = !0)
        }, function() {
            t.getThingState.lastRequestFailed = !0
        })
    }, t.prepareEditState = function() {
        t.editThingState.name = t.completeThing.name, t.editThingState.description = t.completeThing.description, t.editThingState.properties = JSON.stringify(t.completeThing.properties), t.editThingState.locationId = ""
    }, t.prepareEditRequestObject = function() {
        var e = {
            "@iot.id": t.completeThing["@iot.id"],
            name: t.editThingState.name,
            description: t.editThingState.description
        };
        return t.editThingState.properties && (e.properties = JSON.parse(t.editThingState.properties)), t.editThingState.locationId && (e.Locations = [{
            "@iot.id": t.editThingState.locationId /* Number(t.editThingState.locationId) -  Rac021 */
        }]), e
    }, t.sendEditRequest = function() {
        try {
            t.sendEditRequestInternal()
        } catch (e) {
            t.editThingState.lastRequestFailed = !0
        }
    }, t.sendEditRequestInternal = function() {
        o.patchThing(t.prepareEditRequestObject()).then(function() {
            e.reload()
        }, function() {
            t.editThingState.lastRequestFailed = !0
        })
    }, t.loadThingState = function(t) {
        e.go("thing", {
            thingId: t
        })
    }, t.locationIdChanged = function() {
        a.entityIdChanged(o.getLocation, t.editThingState.locationId, function(e) {
            t.editThingState.locationIdValid = e.data["@iot.id"]
        }, function() {
            t.editThingState.locationIdValid = null
        })
    }, /*isNaN(n.thingId) || Rac021 */ t.getCompleteThing(t.getThingState.thingId), t.loadPreview = function() {
        t.previewMargin = !0, t.previewLoading = !0;
        var e = t.completeThing.Datastreams.length;
        if( e == null || e === 0 ) return // Rac021
        t.completeThing.Datastreams.forEach(function(n) {
            o.getCompleteDatastream(n["@iot.id"]).then(function(n) {
                0 == --e && (t.previewLoading = !1);
                var r = [];
                if( n.Observations.value == null || n.Observations.value.length === 0 ) return // Rac021
                n.Observations.value.forEach(function(t) {
                    var e = parseFloat(t.result);
                    isNaN(e) || r.push([moment(t.phenomenonTime).valueOf(), e])
                }), $(".sparkline-" + n["@iot.id"] ).sparkline( r, {
                    type: "line",                    
                    //width: "10em",
                    //lineColor: "#1c84c6",
                    //highlightLineColor: null,
                    //fillColor: !1,
                    //spotColor: !1,
                    //minSpotColor: !1,
                    //maxSpotColor: !1,
                    //spotRadius: 0,
                    //tooltipFormat: ""
                    // Rac021 
                    backgroundColor: null,
                    borderWidth: 0,
                    margin: [2, 0, 2, 0],
                    width: 120,
                    height: 20,
                    style: {
                        overflow: 'visible'
                    },
                    // small optimalization, saves 1-2 ms each sparkline
                    skipClone: true
                })
            }, function() {
                0 == --e && (t.previewLoading = !1)
            })
        })
    }, t.loadAxis = function(e, n) {

        o.getCompleteDatastream(e).then(function(e) {
            
            iot_id_type_in_ls = localStorage.getItem("IOT_ID_TYPE") 
            url1 = ( iot_id_type_in_ls == "string" ) ? "v1.1/Datastreams('" + e["@iot.id"] + "')/Observations" : "v1.1/Datastreams(" + e["@iot.id"] + ")/Observations"
            url2 = ( iot_id_type_in_ls == "string" ) ? "v1.1/Datastreams('" + t.leftDatastreamId + "')/Observations" : "v1.1/Datastreams(" + t.leftDatastreamId + ")/Observations"
            url3 = ( iot_id_type_in_ls == "string" ) ? "v1.1/Datastreams('" + t.rightDatastreamId + "')/Observations" : "v1.1/Datastreams(" + t.rightDatastreamId + ")/Observations"
            
            try { // Rac021
                 1 == n && f && f.isConnected() ? (f.subscribe( url1 ), t.leftDatastreamId && f.unsubscribe( url2 ), t.leftDatastreamId = e["@iot.id"]) : 2 == n && p && p.isConnected &&
                (p.subscribe( url1 ), t.rightDatastreamId && p.unsubscribe( url3 ), t.rightDatastreamId = e["@iot.id"]);
            } catch (error) {
               console.error(error);
            }
            
            var i = [];
            if( e.Observations.value == null || e.Observations.value.length === 0 ) return // Rac021
            e.Observations.value.slice().reverse().forEach(function(t) {
                var e = parseFloat(t.result);
                /* isNaN(e) || */ i.push({
                    x: moment(t.phenomenonTime),
                    y: e
                })
            });
            var o = e.ObservedProperty.description + " (" + (e.unitOfMeasurement.symbol || "") + ")";
            t.nvd3Options.chart["yAxis" + n].axisLabel = o, t.nvd3Data = t.nvd3Data || [], t.nvd3Data[n - 1] = {
                key: o,
                values: i,
                type: "line",
                yAxis: n
            }, r(function() {
                $(".nvtooltip").remove(), t.nvd3.api.update()
            }, 500)
        })
    }, t.loadLeftAxis = function(e) {
        t.loadAxis(e, 1), t.leftAxisLoaded = !0
    }, t.loadRightAxis = function(e) {
        t.loadAxis(e, 2), t.rightAxisLoaded = !0
    }, t.nvd3Options = {
        chart: {
            type: "multiChart",
            height: 500,
            margin: {
                top: 20,
                right: 90,
                bottom: 40,
                left: 60
            },
            color: d3.scale.category10().range(),
            x: function(t) {
                return t.x
            },
            y: function(t) {
                return t.y
            },
            interpolate: "linear",
            refreshDataOnly: !0,
            deepWatchData: !0,
            lines1: {
                duration: 0
            },
            lines2: {
                duration: 0
            },
            xAxis: {
                tickFormat: function() {
                    var e = null,
                        n = null;
                    return function(r, i) {
                        if ("undefined" == typeof i) return moment(r).toISOString();
                        var o = null,
                            a = t.nvd3.api.getScope().chart.lines1.xScale().domain(),
                            s = d3.time.format.utc("%Y")(moment(r).toDate()),
                            u = d3.time.format.utc("%Y-%m-%d")(moment(r).toDate());
                        return o = moment.duration(a[1] - a[0]).asDays() < 30 ? 0 !== i ? u !== n ? moment(r).utc().format("MMM D") : d3.time.format.utc("%H:%M:%S")(moment(r).toDate()) : moment(r).utc().format("MMM D") : moment(r).utc().format(moment.duration(a[1] - a[0]).asDays() < 500 ? 0 !== i ? s !== lastYear ? "MMM D, YYYY" : "MMM D" : "MMM D, YYYY" : "MMM YYYY"), e = a.slice(), lastYear = s, n = u, o
                    }
                }(),
                ticks: 7,
                showMaxMin: !1
            },
            yAxis1: {
                showMaxMin: !0,
                tickFormat: function(t) {
                    return d3.format(".4s")(t)
                }
            },
            yAxis2: {
                showMaxMin: !0,
                tickFormat: function(t) {
                    return d3.format(".4s")(t)
                }
            },
            showLegend: !0
        }
    }, t.nvd3Data = [], t.displaySparklines = function() {
        r(function() {
            $.sparkline_display_visible()
        }, 100)
    };
    try {
        var f = new Paho.MQTT.Client(o.getStMqttUri(), "c" + Math.random());
        ! function() {
            function e() {
                f.isConnected = !0
            }

            function n(e) {
                f.isConnected = !1, 0 !== e.errorCode && t.$apply(function() {
                    t.leftDatastreamId = null
                })
            }

            function r(e) {
                var n = JSON.parse(e.payloadString);
                if (n["@iot.id"]) {
                    var r = parseFloat(n.result);
                    /*isNaN(r) || */ t.$apply(function() {
                        t.nvd3Data[0].values.push({
                            x: moment(n.phenomenonTime),
                            y: r
                        }), t.nvd3Data[0].values.length > 100 && t.nvd3Data[0].values.shift()
                    })
                }
            }
            f.onConnectionLost = n, f.onMessageArrived = r, f.connect({
                onSuccess: e
            })
        }()
    } catch (d) {}
    try {
        var p = new Paho.MQTT.Client(o.getStMqttUri(), "c" + Math.random());
        ! function() {
            function e() {
                p.isConnected = !0
            }

            function n(e) {
                p.isConnected = !1, 0 !== e.errorCode && t.$apply(function() {
                    t.rightDatastreamId = null
                })
            }

            function r(e) {
                var n = JSON.parse(e.payloadString);
                if (n["@iot.id"]) {
                    var r = parseFloat(n.result);
                    /*isNaN(r) || */ t.$apply(function() {
                        t.nvd3Data[1].values.push({
                            x: moment(n.phenomenonTime),
                            y: r
                        }), t.nvd3Data[1].values.length > 100 && t.nvd3Data[1].values.shift()
                    })
                }
            }
            p.onConnectionLost = n, p.onMessageArrived = r, p.connect({
                onSuccess: e
            })
        }()
    } catch (d) {}
    t.$on("$destroy", function() {
        try {
            f.disconnect()
        } catch (t) {}
        try {
            p.disconnect()
        } catch (t) {}
    }), t.refreshMap = function() {
        r(function() {
            u.getMap().then(function(t) {
                t.invalidateSize()
            })
        }, 300)
    }, t.selectDatastreams = function() {
        s && t.previewMargin === !1 && t.loadPreview()
    }, t.selectTab = function(t) {
        i.hash(t), "location" === t && l()
    }
}]);
