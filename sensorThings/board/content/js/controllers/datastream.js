angular.module("sensorBoard").controller("DatastreamCtrl", ["$scope", "$state", "$stateParams", "$timeout", "$anchorScroll", "$location", "$interval", "helpers", "moment", "sensorThingsService", function(t, e, n, r, i, o, a, s, u, c) {
    t.stateParams = n, t.getDatastreamState = {
        datastreamId: n.datastreamId,
        lastRequestFailed: !1
    }, t.editDatastreamState = {
        name: "",
        description: "",
        observationType: "",
        unitOfMeasurement: "",
        lastRequestFailed: !1
    }, t.nvd3 = {
        api: {}
    }, t.loadMoreInProgress = !1, t.showingSamplesOnly = !1, t.connectedDatastreamId = null, t.showPollingOption = !1, t.currentlyPolling = !1, t.activateTab = {}, t.loadMore = function(e) {
        t.loadMoreInProgress = !0, c.getNextLink(e["@iot.nextLink"]).then(function(n) {
            e.value = e.value.concat(n.data.value), e["@iot.nextLink"] = n.data["@iot.nextLink"] || null, t.updateObservationsVisualization(e.value)
        })
    }, t.updateObservationsVisualization = function(e) {
        var n = [];
        e.forEach(function(t) {
            var e = parseFloat(t.result);
            /*isNaN(e) || */ n.push([u(t.phenomenonTime), e, t["@iot.id"]])
        });
        var o = _.sortBy(s.sampleValues(n), function(t) {
            return -t[0].valueOf()
        });
        o.length !== n.length && (t.showingSamplesOnly = !0);
        var a = t.completeDatastream.ObservedProperty.description + " (" + (t.completeDatastream.unitOfMeasurement.symbol || "") + ")";
        t.nvd3Options.chart.yAxis.axisLabel = a, t.nvd3Data[0] = {
            key: a,
            values: o
        }, r(function() {
            
            if (typeof t.nvd3.api.getScope !== 'function') { // Rac021
                return
            }
            
            $(".nvtooltip").remove(), t.nvd3.api.getScope().chart.lines.dispatch.on("elementClick", function(e) {
                i("observation-" + e.point[2]), t.$apply(function() {
                    t.selectedObservationId = e.point[2]
                })
            }), t.loadMoreInProgress = !1
        }, 500)
    }, t.getCompleteDatastream = function(e) {
        c.getCompleteDatastream(e).then(function(e) {
            t.completeDatastream = e, t.prepareEditState(), t.updateObservationsVisualization(t.completeDatastream.Observations.value), t.getDatastreamState.lastRequestFailed = !1, r(function() {
                url = ( localStorage.getItem("IOT_ID_TYPE") == "string" ) ? "v1.1/Datastreams('" + e["@iot.id"] + "')/Observations" : "v1.1/Datastreams(" + e["@iot.id"] + ")/Observations"
                t.showPollingOption = !0, f && f.isConnected && (f.subscribe( url ), t.connectedDatastreamId = e["@iot.id"])
            }, 1e3)
        }, function() {
            t.getDatastreamState.lastRequestFailed = !0
        })
    }, t.prepareEditState = function() {
        t.editDatastreamState.name = t.completeDatastream.name, t.editDatastreamState.description = t.completeDatastream.description, t.editDatastreamState.observationType = t.completeDatastream.observationType, t.editDatastreamState.unitOfMeasurement = JSON.stringify(t.completeDatastream.unitOfMeasurement)
    }, t.prepareEditRequestObject = function() {
        var e = {
            "@iot.id": t.completeDatastream["@iot.id"],
            name: t.editDatastreamState.name,
            description: t.editDatastreamState.description,
            observationType: t.editDatastreamState.observationType,
            unitOfMeasurement: JSON.parse(t.editDatastreamState.unitOfMeasurement)
        };
        return e
    }, t.sendEditRequest = function() {
        c.patchDatastream(t.prepareEditRequestObject()).then(function() {
            e.reload()
        }, function() {
            t.editDatastreamState.lastRequestFailed = !0
        })
    }, t.loadDatastreamState = function(t) {
        e.go("datastream", {
            datastreamId: t
        })
    }, t.deleteObservation = function(t) {
        c.deleteObservation(t).then(function() {
            e.reload()
        })
    }, t.clearDisplayedObservations = function() {
        t.completeDatastream.Observations.value.length = 0
    };
    var l = function() {
        var e = null;
        return function(n) {
            null != e && a.cancel(e), "start" == n && (e = a(function() {
                c.getDatastream(t.getDatastreamState.datastreamId, {
                    $expand: "Observations"
                }).then(function(e) {
                    t.completeDatastream.Observations = {
                        value: e.data.Observations
                    }, t.updateObservationsVisualization(t.completeDatastream.Observations.value)
                })
            }, 5e3))
        }
    }();
    t.startPolling = function() {
        l("start"), t.currentlyPolling = !0
    }, t.stopPolling = function() {
        l("stop"), t.currentlyPolling = !1
    }, t.$on("$destroy", function() {
        t.stopPolling()
    }), /*isNaN(n.datastreamId) || */ t.getCompleteDatastream(t.getDatastreamState.datastreamId), t.nvd3Options = {
        chart: {
            type: "lineWithFocusChart",
            height: 500,
            margin: {
                top: 20,
                right: 20,
                bottom: 40,
                left: 60
            },
            x: function(t) {
                return t[0]
            },
            y: function(t) {
                return t[1]
            },
            xAxis: {
                tickFormat: function() {
                    var e = null,
                        n = null;
                    return function(r, i) {
                        if ("undefined" == typeof i) return u(r).toISOString();
                        var o = null,
                            a = t.nvd3.api.getScope().chart.xScale().domain(),
                            s = d3.time.format.utc("%Y")(u(r).toDate()),
                            c = d3.time.format.utc("%Y-%m-%d")(u(r).toDate());
                        return o = u.duration(a[1] - a[0]).asDays() < 30 ? 0 !== i ? c !== n ? u(r).utc().format("MMM D") : d3.time.format.utc("%H:%M:%S")(u(r).toDate()) : u(r).utc().format("MMM D") : u(r).utc().format(u.duration(a[1] - a[0]).asDays() < 500 ? 0 !== i ? s !== lastYear ? "MMM D, YYYY" : "MMM D" : "MMM D, YYYY" : "MMM YYYY"), e = a.slice(), lastYear = s, n = c, o
                    }
                }(),
                ticks: 7,
                showMaxMin: !1
            },
            yAxis: {
                showMaxMin: !0,
                tickFormat: function(t) {
                    return d3.format(".4s")(t)
                }
            },
            y2Axis: {
                tickValues: [],
                showMaxMin: !1
            },
            x2Axis: {
                ticks: 5,
                tickFormat: function() {
                    var e = null,
                        n = null;
                    return function(r) {
                        var i = null,
                            o = t.nvd3.api.getScope().chart.xScale().domain(),
                            a = d3.time.format.utc("%Y-%m-%d")(u(r).toDate());
                        return i = u.duration(o[1] - o[0]).asDays() < 30 ? _.isEqual(o, e) ? a !== n ? u(r).utc().format("MMM D") : d3.time.format.utc("%H:%M:%S")(u(r).toDate()) : u(r).utc().format("MMM D") : u(r).utc().format("MMM 'YY"), e = o.slice(), n = a, i
                    }
                }(),
                showMaxMin: !1
            },
            showLegend: !1
        }
    }, t.nvd3Data = [], r(function() {
        i()
    }, 1e3);
    try {
        var f = new Paho.MQTT.Client(c.getStMqttUri(), "c" + Math.random());
        ! function() {
            function e() {
                f.isConnected = !0
            }

            function n(e) {
                f.isConnected = !1, 0 !== e.errorCode && t.$apply(function() {
                    t.connectedDatastreamId = null
                })
            }

            function r(e) {
                var n = JSON.parse(e.payloadString);
                n["@iot.id"] && t.$apply(function() {
                    t.completeDatastream.Observations.value.unshift(n), t.completeDatastream.Observations.value.length > 100 && t.completeDatastream.Observations.value.pop(), t.updateObservationsVisualization(t.completeDatastream.Observations.value)
                })
            }
            f.onConnectionLost = n, f.onMessageArrived = r, f.connect({
                onSuccess: e
            })
        }()
    } catch (h) {}
    t.$on("$destroy", function() {
        try {
            f.disconnect()
        } catch (t) {}
    }), t.disconnectMqtt = function() {
        try {
            f.disconnect()
        } catch (e) {}
        t.connectedDatastreamId = null
    }, t.selectTab = function(t) {
        o.hash(t)
    }, t.selectObservations = function() {
        t.selectTab("observations"), r(function() {
            
            if (typeof t.nvd3.api.update !== 'function') { // Rac021
               return
            }
            
            $(".nvtooltip").remove(), t.nvd3.api.update()
        }, 200)
    }, o.hash() && (t.activateTab[o.hash()] = !0)
}]);
