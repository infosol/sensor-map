angular.module("sensorBoard").controller("ObservedPropertiesCtrl", ["$scope", "$state", "sensorThingsService", "mopService", function(t, e, n, r) {
    t.sensorsNextLink = "", t.createObservedPropertyState = {
        name: "",
        definition: "",
        description: "",
        lastRequestFailed: !1
    }, t.autocompleteQ = "", t.loadMainListInProgress = !1, t.queryState = {
        filterParam: "",
        orderbyParam: "id desc"
    }, t.loadMainList = function() {
        t.loadMainListInProgress = !0;
        var e = {
            $count: "true"
        };
        t.queryState.filterParam && (e.$filter = t.queryState.filterParam), t.queryState.orderbyParam && (e.$orderby = t.queryState.orderbyParam), n.getObservedProperties(e).then(function(e) {
            t.observedPropertiesResponse = e.data, t.observedProperties = e.data.value, t.observedPropertiesNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1
        }, function() {
            t.observedPropertiesResponse = null, t.observedProperties = [], t.observedPropertiesNextLink = null, t.loadMainListInProgress = !1
        })
    }, t.getNextLink = function(e) {
        n.getNextLink(e).then(function(e) {
            t.observedPropertiesResponse = e.data, t.observedProperties = t.observedProperties.concat(e.data.value), t.observedPropertiesNextLink = e.data["@iot.nextLink"] || null
        })
    }, t.createObservedProperty = function() {
        n.postObservedProperty({
            name: t.createObservedPropertyState.name,
            definition: t.createObservedPropertyState.definition,
            description: t.createObservedPropertyState.description
        }).then(function() {
            e.reload()
        }, function() {
            t.createObservedPropertyState.lastRequestFailed = !0
        })
    }, t.deleteObservedProperty = function(t) {
        n.deleteObservedProperty(t).then(function() {
            e.reload()
        }, function() {})
    }, t.autocomplete = function(t) {
        return r.autocomplete(t).then(function(t) {
            return t.data
        })
    }, t.typeaheadOnSelect = function(e, n) {
        t.createObservedPropertyState.name = n.name, t.createObservedPropertyState.definition = n.definition, t.createObservedPropertyState.description = n.description
    }, t.loadMainList()
}]);
