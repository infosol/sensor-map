angular.module("sensorBoard").controller("SensorsCtrl", ["$scope", "$state", "sensorThingsService", function(t, e, n) {
    t.sensorsNextLink = "", t.createSensorState = {
        name: "",
        description: "",
        encodingType: "",
        metadata: "",
        lastRequestFailed: !1
    }, t.loadMainListInProgress = !1, t.queryState = {
        filterParam: "",
        orderbyParam: "id desc"
    }, t.loadMainList = function() {
        t.loadMainListInProgress = !0;
        var e = {
            $count: "true"
        };
        t.queryState.filterParam && (e.$filter = t.queryState.filterParam), t.queryState.orderbyParam && (e.$orderby = t.queryState.orderbyParam), n.getSensors(e).then(function(e) {
            t.sensorsResponse = e.data, t.sensors = e.data.value, t.sensorsNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1
            localStorage.setItem("IOT_ID_TYPE", typeof (e.data.value[0]["@iot.id"]) ); // RAC021 
        }, function() {
            t.sensorsResponse = null, t.sensors = [], t.sensorsNextLink = null, t.loadMainListInProgress = !1
        })
    }, t.getNextLink = function(e) {
        n.getNextLink(e).then(function(e) {
            t.sensorsResponse = e.data, t.sensors = t.sensors.concat(e.data.value), t.sensorsNextLink = e.data["@iot.nextLink"] || null
        })
    }, t.createSensor = function() {
        n.postSensor({
            name: t.createSensorState.name,
            description: t.createSensorState.description,
            encodingType: t.createSensorState.encodingType,
            metadata: t.createSensorState.metadata
        }).then(function() {
            e.reload()
        }, function() {
            t.createSensorState.lastRequestFailed = !0
        })
    }, t.deleteSensor = function(t) {
        n.deleteSensor(t).then(function() {
            e.reload()
        }, function() {})
    }, t.loadMainList()
}]);
