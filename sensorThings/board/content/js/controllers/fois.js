angular.module("sensorBoard").controller("FoisCtrl", ["$scope", "$state", "$timeout", "sensorThingsService", "leafletData", "leafletBaseLayers", function(t, e, n, r, i, o) {
    t.foisNextLink = "", t.createFoiState = {
        name: "",
        description: "",
        encodingType: "",
        feature: "",
        lastRequestFailed: !1
    }, t.loadMainListInProgress = !1, t.queryState = {
        filterParam: "",
        orderbyParam: "id desc"
    }, angular.extend(t, {
        markers: {},
        layers: {
            baselayers: o,
            overlays: {
                primary: {
                    name: "Points",
                    type: "markercluster",
                    visible: !0
                },
                complex: {
                    name: "Shapes",
                    type: "geoJSONShape",
                    visible: "true",
                    layerOptions: {
                        style: {
                            color: "#00D",
                            fillColor: "red",
                            weight: 2,
                            opacity: .6,
                            fillOpacity: .2
                        }
                    },
                    data: {
                        type: "FeatureCollection",
                        features: [{}]
                    }
                }
            }
        },
        defaults: {
            scrollWheelZoom: !1
        }
    }), t.loadMainList = function() {
        t.loadMainListInProgress = !0;
        var e = {
            $count: "true"
        };
        t.queryState.filterParam && (e.$filter = t.queryState.filterParam), t.queryState.orderbyParam && (e.$orderby = t.queryState.orderbyParam), r.getFeaturesOfInterest(e).then(function(e) {
            t.foisResponse = e.data, t.fois = e.data.value, t.foisNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1, t.visualizeMap()
            localStorage.setItem("IOT_ID_TYPE", typeof (e.data.value[0]["@iot.id"]) ); // RAC021
        }, function() {
            t.foisResponse = null, t.fois = [], t.foisNextLink = null, t.loadMainListInProgress = !1
        })
    }, t.visualizeMap = function() {
        t.markers = {}, t.layers.overlays.complex.data.features = [], t.fois.forEach(function(e) {
            if ("Point" == e.feature.type) try {
                t.markers[e["@iot.id"]] = {
                    layer: "primary",
                    lat: e.feature.coordinates[1],
                    lng: e.feature.coordinates[0],
                    message: '<a href="#/foi/' + e["@iot.id"] + '">' + _.escape(e.description) + "</a>"
                }
            } catch (n) {} else if ("Polygon" == e.feature.type) try {
                var r = {
                    type: "Feature",
                    geometry: {
                        type: "Polygon",
                        coordinates: e.feature.coordinates
                    }
                };
                t.layers.overlays.complex.data.features.push(r)
            } catch (n) {}
        }), t.layers.overlays.complex.doRefresh = !0, n(function() {
            i.getLayers().then(function(t) {
                var e = t.overlays.primary.getLayers(),
                    n = new L.featureGroup(e).getBounds(),
                    r = t.overlays.complex.getLayers(),
                    o = new L.featureGroup(r).getBounds(),
                    a = n.extend(o);
                i.getMap().then(function(t) {
                    t.fitBounds(a, {
                        maxZoom: 10
                    })
                })
            })
        }, 500)
    }, t.getNextLink = function(e) {
        t.loadMainListInProgress = !0, r.getNextLink(e).then(function(e) {
            t.foisResponse = e.data, t.fois = t.fois.concat(e.data.value), t.foisNextLink = e.data["@iot.nextLink"] || null, t.visualizeMap(), t.loadMainListInProgress = !1
        }, function() {
            t.loadMainListInProgress = !1
        })
    }, t.createFoi = function() {
        try {
            t.createFoiInternal()
        } catch (e) {
            t.createFoiState.lastRequestFailed = !0
        }
    }, t.createFoiInternal = function() {
        r.postFeatureOfInterest({
            name: t.createFoiState.name,
            description: t.createFoiState.description,
            encodingType: t.createFoiState.encodingType,
            feature: JSON.parse(t.createFoiState.feature)
        }).then(function() {
            e.reload()
        }, function() {
            t.createFoiState.lastRequestFailed = !0
        })
    }, t.deleteFoi = function(t) {
        r.deleteFeatureOfInterest(t).then(function() {
            e.reload()
        }, function() {})
    }, t.loadMainList(), t.refreshMap = function() {
        n(function() {
            i.getMap().then(function(t) {
                t.invalidateSize()
            })
        }, 300)
    }
}]);
