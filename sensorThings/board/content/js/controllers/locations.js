angular.module("sensorBoard").controller("LocationsCtrl", ["$scope", "$state", "$timeout", "sensorThingsService", "leafletData", "leafletBaseLayers", "leafletDrawEvents", function(t, e, n, r, i, o, a) {
    t.locationsNextLink = "", t.createLocationState = {
        name: "",
        description: "",
        encodingType: "",
        location: "",
        lastRequestFailed: !1
    }, t.loadMainListInProgress = !1, t.queryState = {
        filterParam: "",
        orderbyParam: "id desc"
    };
    var s = new L.FeatureGroup;
    angular.extend(t, {
        markers: {},
        layers: {
            baselayers: o,
            overlays: {
                primary: {
                    name: "Points",
                    type: "markercluster",
                    visible: !0
                },
                complex: {
                    name: "Shapes",
                    type: "geoJSONShape",
                    visible: "true",
                    layerOptions: {
                        style: {
                            color: "#00D",
                            fillColor: "red",
                            weight: 2,
                            opacity: .6,
                            fillOpacity: .2
                        }
                    },
                    data: {
                        type: "FeatureCollection",
                        features: [{}]
                    }
                }
            }
        },
        defaults: {
            scrollWheelZoom: !1
        },
        drawOptions: {
            position: "bottomright",
            draw: {
                polyline: !1,
                polygon: {
                    metric: !0,
                    showArea: !0,
                    drawError: {
                        color: "red",
                        timeout: 1e3
                    },
                    shapeOptions: {
                        color: "#223341"
                    }
                },
                rectangle: {
                    metric: !0,
                    showArea: !0,
                    shapeOptions: {
                        color: "#223341"
                    }
                },
                circle: {
                    showArea: !0,
                    metric: !0,
                    shapeOptions: {
                        color: "#223341"
                    }
                },
                marker: !1
            },
            edit: {
                featureGroup: s,
                remove: !0
            }
        }
    });
    var u = {
            "draw:created": function(e, n) {
                s.addLayer(n.layer), t.sendLocationQueryFromShapes()
            },
            "draw:edited": function() {
                t.sendLocationQueryFromShapes()
            },
            "draw:deleted": function(e) {
                s.removeLayer(e.layers), t.sendLocationQueryFromShapes()
            }
        },
        c = a.getAvailableEvents();
    c.forEach(function(e) {
        t.$on("leafletDirectiveDraw." + e, function(t, n) {
            u[e] && u[e](t, n.leafletEvent, n.leafletObject, n.model, n.modelName)
        })
    }), t.loadMainList = function() {
        t.loadMainListInProgress = !0;
        var e = {
            $count: "true"
        };
        t.queryState.filterParam && (e.$filter = t.queryState.filterParam), t.queryState.orderbyParam && (e.$orderby = t.queryState.orderbyParam), r.getLocations(e).then(function(e) {
            t.locationsResponse = e.data, t.locations = e.data.value, t.locationsNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1, t.visualizeMap()
            localStorage.setItem("IOT_ID_TYPE", typeof (e.data.value[0]["@iot.id"]) ); // RAC021
        }, function() {
            t.locationsResponse = null, t.locations = [], t.locationsNextLink = null, t.loadMainListInProgress = !1
        })
    }, t.visualizeMap = function() {
        t.markers = {}, t.layers.overlays.complex.data.features = [], t.locations.forEach(function(e) {
            if ("Point" == e.location.type) try {
                t.markers[e["@iot.id"]] = {
                    layer: "primary",
                    lat: e.location.coordinates[1],
                    lng: e.location.coordinates[0],
                    message: '<a href="#/location/' + e["@iot.id"] + '">' + _.escape(e.description) + "</a>"
                }
            } catch (n) {} else if ("Polygon" == e.location.type) try {
                var r = {
                    type: "Feature",
                    geometry: {
                        type: "Polygon",
                        coordinates: e.location.coordinates
                    }
                };
                t.layers.overlays.complex.data.features.push(r)
            } catch (n) {}
        }), t.layers.overlays.complex.doRefresh = !0, n(function() {
            i.getLayers().then(function(t) {
                var e = t.overlays.primary.getLayers(),
                    n = new L.featureGroup(e).getBounds(),
                    r = t.overlays.complex.getLayers(),
                    o = new L.featureGroup(r).getBounds(),
                    a = n.extend(o);
                i.getMap().then(function(t) {
                    t.fitBounds(a, {
                        maxZoom: 12
                    })
                })
            })
        }, 500)
    }, t.getNextLink = function(e) {
        t.loadMainListInProgress = !0, r.getNextLink(e).then(function(e) {
            t.locationsResponse = e.data, t.locations = t.locations.concat(e.data.value), t.locationsNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1, t.visualizeMap()
        }, function() {
            t.loadMainListInProgress = !1
        })
    }, t.createLocation = function() {
        var n = {
            name: t.createLocationState.name,
            description: t.createLocationState.description,
            encodingType: t.createLocationState.encodingType
        };
        t.createLocationState.location && (n.location = JSON.parse(t.createLocationState.location)), r.postLocation(n).then(function() {
            e.reload()
        }, function() {
            t.createLocationState.lastRequestFailed = !0
        })
    }, t.deleteLocation = function(t) {
        r.deleteLocation(t).then(function() {
            e.reload()
        }, function() {})
    }, t.loadMainList(), t.refreshMap = function() {
        n(function() {
            i.getMap().then(function(t) {
                t.invalidateSize()
            })
        }, 300)
    }, t.sendLocationQueryFromShapes = function() {
        var e = [],
            n = s.getLayers().filter(function(t) {
                return t.getRadius
            }),
            r = s.toGeoJSON().features.filter(function(t) {
                return "Point" === t.geometry.type
            });
        _.zip(n, r).forEach(function(t) {
            e.push("(geo.distance(location, geography'" + wellknown.stringify(t[1]) + "') lt " + t[0].getRadius().toFixed(1) + ")")
        });
        var i = s.toGeoJSON().features.filter(function(t) {
            return "Point" !== t.geometry.type
        });
        i.forEach(function(t) {
            e.push("st_within(location, geography'" + wellknown.stringify(t) + "')")
        }), t.queryState.filterParam = e.join(" or "), t.loadMainList()
    }
}]);
