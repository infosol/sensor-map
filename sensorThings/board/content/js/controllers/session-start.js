angular.module("sensorBoard").controller("SessionStartCtrl", ["$scope", "$rootScope", "$state", "$stateParams", "userService", function(t, e, n, r, i) {
    var o = r.t;
    i.storeSessionToken(o), i.getProfile().then(function(t) {
        e.rUser.sessionToken = o, e.rUser.profile = t.data, n.go("home", {}, {
            location: "replace"
        })
    })
}]);
