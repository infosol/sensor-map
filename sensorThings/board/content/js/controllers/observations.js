angular.module("sensorBoard").controller("ObservationsCtrl", ["$scope", "$state", "sensorThingsService", "helpers", function(t, e, n, r) {
    t.observationsNextLink = "", t.createObservationState = {
        phenomenonTime: "",
        result: "",
        resultQuality: "",
        datastreamId: "",
        foiId: "",
        datastreamIdValid: null,
        foiIdValid: null,
        lastRequestFailed: !1
    }, t.loadMainListInProgress = !1, t.queryState = {
        filterParam: "",
        orderbyParam: "id desc"
    }, t.loadMainList = function() {
        t.loadMainListInProgress = !0;
        var e = {
            $count: "true"
        };
        t.queryState.filterParam && (e.$filter = t.queryState.filterParam), t.queryState.orderbyParam && (e.$orderby = t.queryState.orderbyParam), n.getObservations(e).then(function(e) {
            t.observationsResponse = e.data, t.observations = e.data.value, t.observationsNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1
            localStorage.setItem("IOT_ID_TYPE", typeof (e.data.value[0]["@iot.id"]) ); // RAC021
        }, function() {
            t.observationsResponse = null, t.observations = [], t.observationsNextLink = null, t.loadMainListInProgress = !1
        })
    }, t.getNextLink = function(e) {
        n.getNextLink(e).then(function(e) {
            t.observationsResponse = e.data, t.observations = t.observations.concat(e.data.value), t.observationsNextLink = e.data["@iot.nextLink"] || null
        })
    }, t.createObservation = function() {
        n.postObservation({
            phenomenonTime: "" != t.createObservationState.phenomenonTime ? t.createObservationState.phenomenonTime : void 0,
            result: t.createObservationState.result,
            resultQuality: "" != t.createObservationState.resultQuality ? t.createObservationState.resultQuality : void 0,
            Datastream: "" != t.createObservationState.datastreamId ? {
                "@iot.id": t.createObservationState.datastreamId
            } : void 0,
            FeatureOfInterest: "" != t.createObservationState.foiId ? {
                "@iot.id": t.createObservationState.foiId
            } : void 0
        }).then(function() {
            e.reload()
        }, function() {
            t.createObservationState.lastRequestFailed = !0
        })
    }, t.deleteObservation = function(t) {
        n.deleteObservation(t).then(function() {
            e.reload()
        }, function() {})
    }, t.datastreamIdChanged = function() {
        r.entityIdChanged(n.getDatastream, t.createObservationState.datastreamId, function(e) {
            t.createObservationState.datastreamIdValid = e.data["@iot.id"]
        }, function() {
            t.createObservationState.datastreamIdValid = null
        })
    }, t.foiIdChanged = function() {
        r.entityIdChanged(n.getFeatureOfInterest, t.createObservationState.foiId, function(e) {
            t.createObservationState.foiIdValid = e.data["@iot.id"]
        }, function() {
            t.createObservationState.foiIdValid = null
        })
    }, t.loadMainList()
}]);
