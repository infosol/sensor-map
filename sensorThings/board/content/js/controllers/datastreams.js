angular.module("sensorBoard").controller("DatastreamsCtrl", ["$scope", "$state", "sensorThingsService", "helpers", function(t, e, n, r) {
    t.datastreamsNextLink = "", t.createDatastreamState = {
        name: "",
        description: "",
        observationType: "",
        unitOfMeasurement: "",
        sensorId: "",
        observedPropertyId: "",
        thingId: "",
        sensorIdValid: null,
        observedPropertyIdValid: null,
        thingIdValid: null,
        lastRequestFailed: !1
    }, t.loadMainListInProgress = !1, t.queryState = {
        filterParam: "",
        orderbyParam: "id desc"
    }, t.loadDatastreamsWithRecentObservationsInProgress = !1, t.activateTab = {}, t.loadMainList = function() {
        t.loadMainListInProgress = !0;
        var e = {
            $count: "true"
        };
        t.queryState.filterParam && (e.$filter = t.queryState.filterParam), t.queryState.orderbyParam && (e.$orderby = t.queryState.orderbyParam), n.getDatastreams(e).then(function(e) {
            t.datastreamsResponse = e.data, t.datastreams = e.data.value, t.datastreamsNextLink = e.data["@iot.nextLink"] || null, t.loadMainListInProgress = !1
            localStorage.setItem("IOT_ID_TYPE", typeof (e.data.value[0]["@iot.id"]) ); // RAC021
            
        }, function() {
            t.datastreamsResponse = null, t.datastreams = [], t.datastreamsNextLink = null, t.loadMainListInProgress = !1
        })
    }, t.getNextLink = function(e) {
        n.getNextLink(e).then(function(e) {
            t.datastreamsResponse = e.data, t.datastreams = t.datastreams.concat(e.data.value), t.datastreamsNextLink = e.data["@iot.nextLink"] || null
        })
    }, t.createDatastream = function() {
        n.postDatastream({
            name: t.createDatastreamState.name,
            description: t.createDatastreamState.description,
            observationType: t.createDatastreamState.observationType,
            unitOfMeasurement: JSON.parse(t.createDatastreamState.unitOfMeasurement),
            Sensor: {
                "@iot.id": t.createDatastreamState.sensorId
            },
            ObservedProperty: {
                "@iot.id": t.createDatastreamState.observedPropertyId
            },
            Thing: {
                "@iot.id": t.createDatastreamState.thingId
            }
        }).then(function() {
            e.reload()
        }, function() {
            t.createDatastreamState.lastRequestFailed = !0
        })
    }, t.deleteDatastream = function(t) {
        n.deleteDatastream(t).then(function() {
            e.reload()
        }, function() {})
    }, t.sensorIdChanged = function() {
        r.entityIdChanged(n.getSensor, t.createDatastreamState.sensorId, function(e) {
            t.createDatastreamState.sensorIdValid = e.data["@iot.id"]
        }, function() {
            t.createDatastreamState.sensorIdValid = null
        })
    }, t.observedPropertyIdChanged = function() {
        r.entityIdChanged(n.getObservedProperty, t.createDatastreamState.observedPropertyId, function(e) {
            t.createDatastreamState.observedPropertyIdValid = e.data["@iot.id"]
        }, function() {
            t.createDatastreamState.observedPropertyIdValid = null
        })
    }, t.thingIdChanged = function() {
        r.entityIdChanged(n.getThing, t.createDatastreamState.thingId, function(e) {
            t.createDatastreamState.thingIdValid = e.data["@iot.id"]
        }, function() {
            t.createDatastreamState.thingIdValid = null
        })
    }, t.selectDatastreamsWithRecentObservations = function() {
        t.datastreamsWithRecentObservations || (t.loadDatastreamsWithRecentObservationsInProgress = !0, n.getObservations({
            $orderby: "phenomenonTime desc",
            $expand: "Datastream/Thing"
        }).then(function(e) {
            var n = e.data.value,
                r = n.reduce(function(t, e) {
                    return t.has(e.Datastream["@iot.id"]) || (e.Datastream.mostRecentObservation = e, t.set(e.Datastream["@iot.id"], e.Datastream)), t
                }, new Map);
            t.datastreamsWithRecentObservations = Array.from(r.values()), t.loadDatastreamsWithRecentObservationsInProgress = !1
        }, function() {
            t.loadDatastreamsWithRecentObservationsInProgress = !1
        }))
    }, t.createWithTemplate = function(e) {
        n.getDatastream(e["@iot.id"], {
            $expand: "Sensor,ObservedProperty,Thing"
        }).then(function(e) {
            var n = e.data;
            t.createDatastreamState.name = n.name || "", t.createDatastreamState.description = n.description || "", t.createDatastreamState.observationType = n.observationType || "", t.createDatastreamState.unitOfMeasurement = JSON.stringify(n.unitOfMeasurement) || "", t.createDatastreamState.sensorId = n.Sensor["@iot.id"] || "", t.createDatastreamState.observedPropertyId = n.ObservedProperty["@iot.id"] || "", t.createDatastreamState.thingId = n.Thing["@iot.id"] || "", t.activateTab.create = !0
        })
    }, t.loadMainList()
}]);
