
angular.module("sensorBoard").controller("HomeCtrl", ["$scope", "$rootScope", "sensorThingsService", function(t, e, n) {
    t.counts = {
        things: null,
        locations: null,
        historicalLocations: null,
        datastreams: null,
        sensors: null,
        observations: null,
        observedProperties: null,
        featuresOfInterest: null
    }, t.spotClickCount = 0, n.getThings({
        $count: "true"
    }).then(function(e) {
        t.counts.things = e.data["@iot.count"]
    }), n.getLocations({
        $count: "true"
    }).then(function(e) {
        t.counts.locations = e.data["@iot.count"]
    }), n.getHistoricalLocations({
        $count: "true"
    }).then(function(e) {
        t.counts.historicalLocations = e.data["@iot.count"]
    }), n.getDatastreams({
        $count: "true"
    }).then(function(e) {
        t.counts.datastreams = e.data["@iot.count"]
    }), n.getSensors({
        $count: "true"
    }).then(function(e) {
        t.counts.sensors = e.data["@iot.count"]
    }), n.getObservations({
        $count: "true"
    }).then(function(e) {
        t.counts.observations = e.data["@iot.count"]
    }), n.getObservedProperties({
        $count: "true"
    }).then(function(e) {
        t.counts.observedProperties = e.data["@iot.count"]
    }), n.getFeaturesOfInterest({
        $count: "true"
    }).then(function(e) {
        t.counts.featuresOfInterest = e.data["@iot.count"]
    }), t.spotClick = function() {
        t.spotClickCount++, 8 === t.spotClickCount && (e.rsGlobal.navigationShowGenerators = !0)
    }
}]);
