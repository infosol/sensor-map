angular.module("sensorBoard").controller("SensorCtrl", ["$scope", "$state", "$stateParams", "$timeout", "sensorThingsService", "autoloadDatastreamPreviews", function(t, e, n, r, i, o) {
    t.stateParams = n, t.getSensorState = {
        sensorId: n.sensorId,
        lastRequestFailed: !1
    }, t.editSensorState = {
        name: "",
        description: "",
        encodingType: "",
        metadata: "",
        lastRequestFailed: !1
    }, t.nvd3 = {}, t.leftAxisLoaded = !1, t.rightAxisLoaded = !1, t.previewMargin = !1, t.leftDatastreamId = null, t.rightDatastreamId = null, t.processNextLink = function(t) {
        i.getNextLink(t["@iot.nextLink"]).then(function(e) {
            t.value = t.value.concat(e.data.value), t["@iot.nextLink"] = e.data["@iot.nextLink"] || null
        })
    }, t.getCompleteSensor = function(e) {
        i.getCompleteSensor(e).then(function(e) {
            t.completeSensor = e, t.prepareEditState(), t.getSensorState.lastRequestFailed = !1
        }, function() {
            t.getSensorState.lastRequestFailed = !0
        })
    }, t.prepareEditState = function() {
        t.editSensorState.name = t.completeSensor.name, t.editSensorState.description = t.completeSensor.description, t.editSensorState.encodingType = t.completeSensor.encodingType, t.editSensorState.metadata = t.completeSensor.metadata
    }, t.prepareEditRequestObject = function() {
        var e = {
            "@iot.id": t.completeSensor["@iot.id"],
            name: t.editSensorState.name,
            description: t.editSensorState.description,
            encodingType: t.editSensorState.encodingType,
            metadata: t.editSensorState.metadata
        };
        return e
    }, t.sendEditRequest = function() {
        i.patchSensor(t.prepareEditRequestObject()).then(function() {
            e.reload()
        }, function() {
            t.editSensorState.lastRequestFailed = !0
        })
    }, t.loadSensorState = function(t) {
        e.go("sensor", {
            sensorId: t
        })
    }, /* isNaN(n.sensorId) || */ t.getCompleteSensor(t.getSensorState.sensorId), t.loadPreview = function() {
        t.previewMargin = !0, t.previewLoading = !0;
        var e = t.completeSensor.Datastreams.length;
        t.completeSensor.Datastreams.forEach(function(n) {
            i.getCompleteDatastream(n["@iot.id"]).then(function(n) {
                0 == --e && (t.previewLoading = !1);
                var r = [];
                n.Observations.value.forEach(function(t) {
                    var e = parseFloat(t.result);
                    /*isNaN(e) || */ r.push([moment(t.phenomenonTime).valueOf(), e])
                }), $(".sparkline-" + n["@iot.id"]).sparkline(r, {
                    type: "line",
                    //width: "100px",
                    //lineColor: "#1c84c6",
                    //highlightLineColor: null,
                    //fillColor: !1,
                    //spotColor: !1,
                    //minSpotColor: !1,
                    //maxSpotColor: !1,
                    //spotRadius: 0,
                    //tooltipFormat: ""
                    // Rac021
                    backgroundColor: null,
                    borderWidth: 0,
                    margin: [2, 0, 2, 0],
                    width: 120,
                    height: 20,
                    style: {
                        overflow: 'visible'
                    },
                    // small optimalization, saves 1-2 ms each sparkline
                    skipClone: true
                })
            }, function() {
                0 == --e && (t.previewLoading = !1)
            })
        })
    }, t.loadAxis = function(e, n) {
        i.getCompleteDatastream(e).then(function(e) {
            1 == n && a && a.isConnected ? (a.subscribe("v1.0/Datastreams(" + e["@iot.id"] + ")/Observations"), t.leftDatastreamId && a.unsubscribe("v1.0/Datastreams(" + t.leftDatastreamId + ")/Observations"), t.leftDatastreamId = e["@iot.id"]) : 2 == n && u && u.isConnected && (u.subscribe("v1.0/Datastreams(" + e["@iot.id"] + ")/Observations"), t.rightDatastreamId && u.unsubscribe("v1.0/Datastreams(" + t.rightDatastreamId + ")/Observations"), t.rightDatastreamId = e["@iot.id"]);
            var i = [];
            e.Observations.value.slice().reverse().forEach(function(t) {
                var e = parseFloat(t.result);
                /*isNaN(e) || */ i.push({
                    x: moment(t.phenomenonTime),
                    y: e
                })
            });
            var o = e.ObservedProperty.description + " (" + (e.unitOfMeasurement.symbol || "") + ")";
            t.nvd3Options.chart["yAxis" + n].axisLabel = o, t.nvd3Data = t.nvd3Data || [], t.nvd3Data[n - 1] = {
                key: o,
                values: i,
                type: "line",
                yAxis: n
            }, r(function() {
                $(".nvtooltip").remove(), t.nvd3.api.update()
            }, 500)
        })
    }, t.loadLeftAxis = function(e) {
        t.loadAxis(e, 1), t.leftAxisLoaded = !0
    }, t.loadRightAxis = function(e) {
        t.loadAxis(e, 2), t.rightAxisLoaded = !0
    }, t.nvd3Options = {
        chart: {
            type: "multiChart",
            height: 500,
            margin: {
                top: 20,
                right: 90,
                bottom: 40,
                left: 60
            },
            color: d3.scale.category10().range(),
            x: function(t) {
                return t.x
            },
            y: function(t) {
                return t.y
            },
            interpolate: "linear",
            xAxis: {
                tickFormat: function() {
                    var e = null,
                        n = null;
                    return function(r, i) {
                        if ("undefined" == typeof i) return moment(r).toISOString();
                        var o = null,
                            a = t.nvd3.api.getScope().chart.lines1.xScale().domain(),
                            s = d3.time.format.utc("%Y")(moment(r).toDate()),
                            u = d3.time.format.utc("%Y-%m-%d")(moment(r).toDate());
                        return o = moment.duration(a[1] - a[0]).asDays() < 30 ? 0 !== i ? u !== n ? moment(r).utc().format("MMM D") : d3.time.format.utc("%H:%M:%S")(moment(r).toDate()) : moment(r).utc().format("MMM D") : moment(r).utc().format(moment.duration(a[1] - a[0]).asDays() < 500 ? 0 !== i ? s !== lastYear ? "MMM D, YYYY" : "MMM D" : "MMM D, YYYY" : "MMM YYYY"), e = a.slice(), lastYear = s, n = u, o
                    }
                }(),
                ticks: 7,
                showMaxMin: !1
            },
            yAxis1: {
                showMaxMin: !0,
                tickFormat: function(t) {
                    return d3.format(".4s")(t)
                }
            },
            yAxis2: {
                showMaxMin: !0,
                tickFormat: function(t) {
                    return d3.format(".4s")(t)
                }
            },
            showLegend: !0
        }
    }, t.nvd3Data = [], t.displaySparklines = function() {
        r(function() {
            $.sparkline_display_visible()
        }, 100)
    };
    try {
        var a = new Paho.MQTT.Client(i.getStMqttUri(), "c" + Math.random());
        ! function() {
            function e() {
                a.isConnected = !0
            }

            function n(e) {
                a.isConnected = !1, 0 !== e.errorCode && t.$apply(function() {
                    t.leftDatastreamId = null
                })
            }

            function r(e) {
                var n = JSON.parse(e.payloadString),
                    r = parseFloat(n.result);
                /*isNaN(r) || */ t.$apply(function() {
                    t.nvd3Data[0].values.push({
                        x: moment(n.phenomenonTime),
                        y: r
                    })
                })
            }
            a.onConnectionLost = n, a.onMessageArrived = r, a.connect({
                onSuccess: e
            })
        }()
    } catch (s) {}
    try {
        var u = new Paho.MQTT.Client(i.getStMqttUri(), "c" + Math.random());
        ! function() {
            function e() {
                u.isConnected = !0
            }

            function n(e) {
                u.isConnected = !1, 0 !== e.errorCode && t.$apply(function() {
                    t.rightDatastreamId = null
                })
            }

            function r(e) {
                var n = JSON.parse(e.payloadString),
                    r = parseFloat(n.result);
                /* isNaN(r) || */ t.$apply(function() {
                    t.nvd3Data[1].values.push({
                        x: moment(n.phenomenonTime),
                        y: r
                    })
                })
            }
            u.onConnectionLost = n, u.onMessageArrived = r, u.connect({
                onSuccess: e
            })
        }()
    } catch (s) {}
    t.$on("$destroy", function() {
        try {
            a.disconnect()
        } catch (t) {}
        try {
            u.disconnect()
        } catch (t) {}
    }), t.selectDatastreams = function() {
        o && t.previewMargin === !1 && t.loadPreview()
    }
}]);
