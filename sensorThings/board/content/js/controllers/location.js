angular.module("sensorBoard").controller("LocationCtrl", ["$scope", "$state", "$stateParams", "$timeout", "sensorThingsService", "leafletData", "leafletBaseLayers", function(t, e, n, r, i, o, a) {
    t.stateParams = n, t.getLocationState = {
        locationId: n.locationId,
        lastRequestFailed: !1
    }, t.editLocationState = {
        name: "",
        description: "",
        encodingType: "",
        location: "",
        lastRequestFailed: !1
    }, angular.extend(t, {
        markers: {},
        layers: {
            baselayers: a,
            overlays: {
                primary: {
                    name: "Points",
                    type: "markercluster",
                    visible: !0
                },
                complex: {
                    name: "Shapes",
                    type: "geoJSONShape",
                    visible: "true",
                    layerOptions: {
                        style: {
                            color: "#00D",
                            fillColor: "red",
                            weight: 2,
                            opacity: .6,
                            fillOpacity: .2
                        }
                    },
                    data: {
                        type: "FeatureCollection",
                        features: [{}]
                    }
                }
            }
        },
        defaults: {
            scrollWheelZoom: !1
        }
    }), t.getCompleteLocation = function(e) {
        i.getCompleteLocation(e).then(function(e) {
            if (t.completeLocation = e, t.prepareEditState(), t.getLocationState.lastRequestFailed = !1, t.markers = {}, t.layers.overlays.complex.data.features = [], "Point" == e.location.type) try {
                t.markers[location["@iot.id"]] = {
                    layer: "primary",
                    lat: e.location.coordinates[1],
                    lng: e.location.coordinates[0]
                }
            } catch (n) {} else if ("Polygon" == e.location.type) try {
                var i = {
                    type: "Feature",
                    geometry: {
                        type: "Polygon",
                        coordinates: e.location.coordinates
                    }
                };
                t.layers.overlays.complex.data.features.push(i)
            } catch (n) {}
            t.layers.overlays.complex.doRefresh = !0, t.refreshMap(), r(function() {
                o.getLayers().then(function(t) {
                    var n = t.overlays.primary.getLayers(),
                        r = new L.featureGroup(n).getBounds(),
                        i = t.overlays.complex.getLayers(),
                        a = new L.featureGroup(i).getBounds(),
                        s = r.extend(a);
                    o.getMap().then(function(t) {
                        t.fitBounds(s, {
                            maxZoom: "Point" == e.location.type ? 8 : 17
                        })
                    })
                })
            }, 500)
        }, function() {
            t.getLocationState.lastRequestFailed = !0
        })
    }, t.prepareEditState = function() {
        t.editLocationState.name = t.completeLocation.name, t.editLocationState.description = t.completeLocation.description, t.editLocationState.encodingType = t.completeLocation.encodingType, t.editLocationState.location = JSON.stringify(t.completeLocation.location), t.editLocationState.thingId = ""
    }, t.prepareEditRequestObject = function() {
        var e = {
            "@iot.id": t.completeLocation["@iot.id"],
            name: t.editLocationState.name,
            description: t.editLocationState.description,
            encodingType: t.editLocationState.encodingType,
            location: JSON.parse(t.editLocationState.location)
        };
        return e
    }, t.sendEditRequest = function() {
        i.patchLocation(t.prepareEditRequestObject()).then(function() {
            e.reload()
        }, function() {
            t.editLocationState.lastRequestFailed = !0
        })
    }, t.loadLocationState = function(t) {
        e.go("location", {
            locationId: t
        })
    }, /*isNaN(n.locationId) || */ t.getCompleteLocation(t.getLocationState.locationId), t.refreshMap = function() {
        r(function() {
            o.getMap().then(function(t) {
                t.invalidateSize()
            })
        }, 300)
    }
}]);
