angular.module("sensorBoard").controller("LinkCtrl", ["$scope", "$rootScope", "$state", "$stateParams", "$location", "sensorThingsService", function(t, e, n, r, i, o) {
    var a = i.search(),
        s = a.stBaseUrl,
        u = a.stMqttUri,
        c = a.anchor,
        l = a.params ? JSON.parse(a.params) : {};
    c && (l["#"] = c);
    var f = a.page;
    s && o.setStBaseUrl(s), o.setStMqttUri(u || ""), o.setStBasicAuthUsername(""), o.setStBasicAuthPassword(""), o.setStXApiKey(""), "datastream" === f ? n.go("datastream", l) : f && f.length ? n.go(f, l) : n.go("home", {}, {
        location: "replace"
    })
}]);
