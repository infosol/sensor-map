angular.module("sensorBoard").controller("HistoricalLocationCtrl", ["$scope", "$state", "$stateParams", "sensorThingsService", function(t, e, n, r) {
    t.stateParams = n, t.getHistoricalLocationState = {
        historicalLocationId: n.historicalLocationId,
        lastRequestFailed: !1
    }, t.editHistoricalLocationState = {
        time: "",
        lastRequestFailed: !1
    }, t.getCompleteHistoricalLocation = function(e) {
        r.getCompleteHistoricalLocation(e).then(function(e) {
            t.completeHistoricalLocation = e, t.prepareEditState(), t.getHistoricalLocationState.lastRequestFailed = !1
        }, function() {
            t.getHistoricalLocationState.lastRequestFailed = !0
        })
    }, t.prepareEditState = function() {
        t.editHistoricalLocationState.time = t.completeHistoricalLocation.time
    }, t.prepareEditRequestObject = function() {
        var e = {
            "@iot.id": t.completeHistoricalLocation["@iot.id"],
            time: t.editHistoricalLocationState.time
        };
        return e
    }, t.sendEditRequest = function() {
        r.patchHistoricalLocation(t.prepareEditRequestObject()).then(function() {
            e.reload()
        }, function() {
            t.editHistoricalLocationState.lastRequestFailed = !0
        })
    }, t.loadHistoricalLocationState = function(t) {
        e.go("historicalLocation", {
            historicalLocationId: t
        })
    }, /* isNaN(n.historicalLocationId) || */ t.getCompleteHistoricalLocation(t.getHistoricalLocationState.historicalLocationId)
}]);
