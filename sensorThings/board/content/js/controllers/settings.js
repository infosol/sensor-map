angular.module("sensorBoard").controller("SettingsCtrl", ["$scope", "$state", "sensorThingsService", "$analytics", function(t, e, n, r) {
    var baseURL = n.getStBaseUrl().trim().endsWith('/') ? n.getStBaseUrl().trim().slice( 0, -1 ) :  n.getStBaseUrl().trim() // Rac021
    t.stBaseUrl = baseURL , t.stBaseUrlHistory = n.getStBaseUrlHistory(), t.stBasicAuthUsername = n.getStBasicAuthUsername(), t.stBasicAuthPassword = n.getStBasicAuthPassword(), t.stXApiKey = n.getStXApiKey(), t.stMqttUri = n.getStMqttUri(), t.stBaseUrlForClear = baseURL , t.stBaseUrlForClearConfirm = "", t.save = function() {
        n.setStBaseUrl(t.stBaseUrl), n.setStBasicAuthUsername(t.stBasicAuthUsername), n.setStBasicAuthPassword(t.stBasicAuthPassword), n.setStXApiKey(t.stXApiKey), n.setStMqttUri(t.stMqttUri), r.eventTrack("StBaseUrl", {
            category: "Settings",
            label: t.stBaseUrl
        }), e.go("home")
    }, t.matchBaseUrl = function() {
        var e = URI(t.stBaseUrl);
        e.hostname() && (t.stMqttUri = "ws://" + e.hostname() + ":9001/mqtt")
    }, t.deleteAllEntities = function() {
        n.deleteAllEntities().then(function() {
            e.go("home")
        }, function(t) {
            iziToast.error(403 == t.status ? {
                title: "Forbidden",
                message: "You do not have permission to perform this operation."
            } : {
                title: "Error",
                message: "An error has occurred."
            })
        })
    }
}]);
