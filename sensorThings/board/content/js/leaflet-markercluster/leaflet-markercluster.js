! function(t, e, n) {
    L.MarkerClusterGroup = L.FeatureGroup.extend({
            options: {
                maxClusterRadius: 80,
                iconCreateFunction: null,
                spiderfyOnMaxZoom: !0,
                showCoverageOnHover: !0,
                zoomToBoundsOnClick: !0,
                singleMarkerMode: !1,
                disableClusteringAtZoom: null,
                removeOutsideVisibleBounds: !0,
                animateAddingMarkers: !1,
                spiderfyDistanceMultiplier: 1,
                spiderLegPolylineOptions: {
                    weight: 1.5,
                    color: "#222"
                },
                chunkedLoading: !1,
                chunkInterval: 200,
                chunkDelay: 50,
                chunkProgress: null,
                polygonOptions: {}
            },
            initialize: function(t) {
                L.Util.setOptions(this, t), this.options.iconCreateFunction || (this.options.iconCreateFunction = this._defaultIconCreateFunction), this._featureGroup = L.featureGroup(), this._featureGroup.on(L.FeatureGroup.EVENTS, this._propagateEvent, this), this._nonPointGroup = L.featureGroup(), this._nonPointGroup.on(L.FeatureGroup.EVENTS, this._propagateEvent, this), this._inZoomAnimation = 0, this._needsClustering = [], this._needsRemoving = [], this._currentShownBounds = null, this._queue = []
            },
            addLayer: function(t) {
                if (t instanceof L.LayerGroup) {
                    var e = [];
                    for (var n in t._layers) e.push(t._layers[n]);
                    return this.addLayers(e)
                }
                if (!t.getLatLng) return this._nonPointGroup.addLayer(t), this;
                if (!this._map) return this._needsClustering.push(t), this;
                if (this.hasLayer(t)) return this;
                this._unspiderfy && this._unspiderfy(), this._addLayer(t, this._maxZoom);
                var i = t,
                    r = this._map.getZoom();
                if (t.__parent)
                    for (; i.__parent._zoom >= r;) i = i.__parent;
                return this._currentShownBounds.contains(i.getLatLng()) && (this.options.animateAddingMarkers ? this._animationAddLayer(t, i) : this._animationAddLayerNonAnimated(t, i)), this
            },
            removeLayer: function(t) {
                if (t instanceof L.LayerGroup) {
                    var e = [];
                    for (var n in t._layers) e.push(t._layers[n]);
                    return this.removeLayers(e)
                }
                return t.getLatLng ? this._map ? t.__parent ? (this._unspiderfy && (this._unspiderfy(), this._unspiderfyLayer(t)), this._removeLayer(t, !0), this._featureGroup.hasLayer(t) && (this._featureGroup.removeLayer(t), t.clusterShow && t.clusterShow()), this) : this : (!this._arraySplice(this._needsClustering, t) && this.hasLayer(t) && this._needsRemoving.push(t), this) : (this._nonPointGroup.removeLayer(t), this)
            },
            addLayers: function(t) {
                var e, n, i, r, o = this._featureGroup,
                    a = this._nonPointGroup,
                    s = this.options.chunkedLoading,
                    l = this.options.chunkInterval,
                    u = this.options.chunkProgress;
                if (this._map) {
                    var c = 0,
                        h = (new Date).getTime(),
                        d = L.bind(function() {
                            for (var e = (new Date).getTime(); c < t.length; c++) {
                                if (s && 0 === c % 200) {
                                    var n = (new Date).getTime() - e;
                                    if (n > l) break
                                }
                                if (r = t[c], r.getLatLng) {
                                    if (!this.hasLayer(r) && (this._addLayer(r, this._maxZoom), r.__parent && 2 === r.__parent.getChildCount())) {
                                        var i = r.__parent.getAllChildMarkers(),
                                            f = i[0] === r ? i[1] : i[0];
                                        o.removeLayer(f)
                                    }
                                } else a.addLayer(r)
                            }
                            u && u(c, t.length, (new Date).getTime() - h), c === t.length ? (this._featureGroup.eachLayer(function(t) {
                                t instanceof L.MarkerCluster && t._iconNeedsUpdate && t._updateIcon()
                            }), this._topClusterLevel._recursivelyAddChildrenToMap(null, this._zoom, this._currentShownBounds)) : setTimeout(d, this.options.chunkDelay)
                        }, this);
                    d()
                } else {
                    for (e = [], n = 0, i = t.length; i > n; n++) r = t[n], r.getLatLng ? this.hasLayer(r) || e.push(r) : a.addLayer(r);
                    this._needsClustering = this._needsClustering.concat(e)
                }
                return this
            },
            removeLayers: function(t) {
                var e, n, i, r = this._featureGroup,
                    o = this._nonPointGroup;
                if (this._unspiderfy && this._unspiderfy(), !this._map) {
                    for (e = 0, n = t.length; n > e; e++) i = t[e], this._arraySplice(this._needsClustering, i), o.removeLayer(i);
                    return this
                }
                for (e = 0, n = t.length; n > e; e++) i = t[e], i.__parent ? (this._removeLayer(i, !0, !0), r.hasLayer(i) && (r.removeLayer(i), i.clusterShow && i.clusterShow())) : o.removeLayer(i);
                return this._topClusterLevel._recursivelyAddChildrenToMap(null, this._zoom, this._currentShownBounds), r.eachLayer(function(t) {
                    t instanceof L.MarkerCluster && t._updateIcon()
                }), this
            },
            clearLayers: function() {
                return this._map || (this._needsClustering = [], delete this._gridClusters, delete this._gridUnclustered), this._noanimationUnspiderfy && this._noanimationUnspiderfy(), this._featureGroup.clearLayers(), this._nonPointGroup.clearLayers(), this.eachLayer(function(t) {
                    delete t.__parent
                }), this._map && this._generateInitialClusters(), this
            },
            getBounds: function() {
                var t = new L.LatLngBounds;
                this._topClusterLevel && t.extend(this._topClusterLevel._bounds);
                for (var e = this._needsClustering.length - 1; e >= 0; e--) t.extend(this._needsClustering[e].getLatLng());
                return t.extend(this._nonPointGroup.getBounds()), t
            },
            eachLayer: function(t, e) {
                var n, i = this._needsClustering.slice();
                for (this._topClusterLevel && this._topClusterLevel.getAllChildMarkers(i), n = i.length - 1; n >= 0; n--) t.call(e, i[n]);
                this._nonPointGroup.eachLayer(t, e)
            },
            getLayers: function() {
                var t = [];
                return this.eachLayer(function(e) {
                    t.push(e)
                }), t
            },
            getLayer: function(t) {
                var e = null;
                return this.eachLayer(function(n) {
                    L.stamp(n) === t && (e = n)
                }), e
            },
            hasLayer: function(t) {
                if (!t) return !1;
                var e, n = this._needsClustering;
                for (e = n.length - 1; e >= 0; e--)
                    if (n[e] === t) return !0;
                for (n = this._needsRemoving, e = n.length - 1; e >= 0; e--)
                    if (n[e] === t) return !1;
                return !(!t.__parent || t.__parent._group !== this) || this._nonPointGroup.hasLayer(t)
            },
            zoomToShowLayer: function(t, e) {
                var n = function() {
                    if ((t._icon || t.__parent._icon) && !this._inZoomAnimation)
                        if (this._map.off("moveend", n, this), this.off("animationend", n, this), t._icon) e();
                        else if (t.__parent._icon) {
                        var i = function() {
                            this.off("spiderfied", i, this), e()
                        };
                        this.on("spiderfied", i, this), t.__parent.spiderfy()
                    }
                };
                if (t._icon && this._map.getBounds().contains(t.getLatLng())) e();
                else if (t.__parent._zoom < this._map.getZoom()) this._map.on("moveend", n, this), this._map.panTo(t.getLatLng());
                else {
                    var i = function() {
                        this._map.off("movestart", i, this), i = null
                    };
                    this._map.on("movestart", i, this), this._map.on("moveend", n, this), this.on("animationend", n, this), t.__parent.zoomToBounds(), i && n.call(this)
                }
            },
            onAdd: function(t) {
                this._map = t;
                var e, n, i;
                if (!isFinite(this._map.getMaxZoom())) throw "Map has no maxZoom specified";
                for (this._featureGroup.onAdd(t), this._nonPointGroup.onAdd(t), this._gridClusters || this._generateInitialClusters(), e = 0, n = this._needsRemoving.length; n > e; e++) i = this._needsRemoving[e], this._removeLayer(i, !0);
                this._needsRemoving = [], this._zoom = this._map.getZoom(), this._currentShownBounds = this._getExpandedVisibleBounds(), this._map.on("zoomend", this._zoomEnd, this), this._map.on("moveend", this._moveEnd, this), this._spiderfierOnAdd && this._spiderfierOnAdd(), this._bindEvents(), n = this._needsClustering, this._needsClustering = [], this.addLayers(n)
            },
            onRemove: function(t) {
                t.off("zoomend", this._zoomEnd, this), t.off("moveend", this._moveEnd, this), this._unbindEvents(), this._map._mapPane.className = this._map._mapPane.className.replace(" leaflet-cluster-anim", ""), this._spiderfierOnRemove && this._spiderfierOnRemove(), this._hideCoverage(), this._featureGroup.onRemove(t), this._nonPointGroup.onRemove(t), this._featureGroup.clearLayers(), this._map = null
            },
            getVisibleParent: function(t) {
                for (var e = t; e && !e._icon;) e = e.__parent;
                return e || null
            },
            _arraySplice: function(t, e) {
                for (var n = t.length - 1; n >= 0; n--)
                    if (t[n] === e) return t.splice(n, 1), !0
            },
            _removeLayer: function(t, e, n) {
                var i = this._gridClusters,
                    r = this._gridUnclustered,
                    o = this._featureGroup,
                    a = this._map;
                if (e)
                    for (var s = this._maxZoom; s >= 0 && r[s].removeObject(t, a.project(t.getLatLng(), s)); s--);
                var l, u = t.__parent,
                    c = u._markers;
                for (this._arraySplice(c, t); u && (u._childCount--, !(u._zoom < 0));) e && u._childCount <= 1 ? (l = u._markers[0] === t ? u._markers[1] : u._markers[0], i[u._zoom].removeObject(u, a.project(u._cLatLng, u._zoom)), r[u._zoom].addObject(l, a.project(l.getLatLng(), u._zoom)), this._arraySplice(u.__parent._childClusters, u), u.__parent._markers.push(l), l.__parent = u.__parent, u._icon && (o.removeLayer(u), n || o.addLayer(l))) : (u._recalculateBounds(), n && u._icon || u._updateIcon()), u = u.__parent;
                delete t.__parent
            },
            _isOrIsParent: function(t, e) {
                for (; e;) {
                    if (t === e) return !0;
                    e = e.parentNode
                }
                return !1
            },
            _propagateEvent: function(t) {
                if (t.layer instanceof L.MarkerCluster) {
                    if (t.originalEvent && this._isOrIsParent(t.layer._icon, t.originalEvent.relatedTarget)) return;
                    t.type = "cluster" + t.type
                }
                this.fire(t.type, t)
            },
            _defaultIconCreateFunction: function(t) {
                var e = t.getChildCount(),
                    n = " marker-cluster-";
                return n += 10 > e ? "small" : 100 > e ? "medium" : "large", new L.DivIcon({
                    html: "<div><span>" + e + "</span></div>",
                    className: "marker-cluster" + n,
                    iconSize: new L.Point(40, 40)
                })
            },
            _bindEvents: function() {
                var t = this._map,
                    e = this.options.spiderfyOnMaxZoom,
                    n = this.options.showCoverageOnHover,
                    i = this.options.zoomToBoundsOnClick;
                (e || i) && this.on("clusterclick", this._zoomOrSpiderfy, this), n && (this.on("clustermouseover", this._showCoverage, this), this.on("clustermouseout", this._hideCoverage, this), t.on("zoomend", this._hideCoverage, this))
            },
            _zoomOrSpiderfy: function(t) {
                var e = this._map;
                t.layer._bounds._northEast.equals(t.layer._bounds._southWest) ? this.options.spiderfyOnMaxZoom && t.layer.spiderfy() : e.getMaxZoom() === e.getZoom() ? this.options.spiderfyOnMaxZoom && t.layer.spiderfy() : this.options.zoomToBoundsOnClick && t.layer.zoomToBounds(), t.originalEvent && 13 === t.originalEvent.keyCode && e._container.focus()
            },
            _showCoverage: function(t) {
                var e = this._map;
                this._inZoomAnimation || (this._shownPolygon && e.removeLayer(this._shownPolygon), t.layer.getChildCount() > 2 && t.layer !== this._spiderfied && (this._shownPolygon = new L.Polygon(t.layer.getConvexHull(), this.options.polygonOptions), e.addLayer(this._shownPolygon)))
            },
            _hideCoverage: function() {
                this._shownPolygon && (this._map.removeLayer(this._shownPolygon), this._shownPolygon = null)
            },
            _unbindEvents: function() {
                var t = this.options.spiderfyOnMaxZoom,
                    e = this.options.showCoverageOnHover,
                    n = this.options.zoomToBoundsOnClick,
                    i = this._map;
                (t || n) && this.off("clusterclick", this._zoomOrSpiderfy, this), e && (this.off("clustermouseover", this._showCoverage, this), this.off("clustermouseout", this._hideCoverage, this), i.off("zoomend", this._hideCoverage, this))
            },
            _zoomEnd: function() {
                this._map && (this._mergeSplitClusters(), this._zoom = this._map._zoom, this._currentShownBounds = this._getExpandedVisibleBounds())
            },
            _moveEnd: function() {
                if (!this._inZoomAnimation) {
                    var t = this._getExpandedVisibleBounds();
                    this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, this._zoom, t), this._topClusterLevel._recursivelyAddChildrenToMap(null, this._map._zoom, t), this._currentShownBounds = t
                }
            },
            _generateInitialClusters: function() {
                var t = this._map.getMaxZoom(),
                    e = this.options.maxClusterRadius,
                    n = e;
                "function" != typeof e && (n = function() {
                    return e
                }), this.options.disableClusteringAtZoom && (t = this.options.disableClusteringAtZoom - 1), this._maxZoom = t, this._gridClusters = {}, this._gridUnclustered = {};
                for (var i = t; i >= 0; i--) this._gridClusters[i] = new L.DistanceGrid(n(i)), this._gridUnclustered[i] = new L.DistanceGrid(n(i));
                this._topClusterLevel = new L.MarkerCluster(this, -1)
            },
            _addLayer: function(t, e) {
                var n, i, r = this._gridClusters,
                    o = this._gridUnclustered;
                for (this.options.singleMarkerMode && (t.options.icon = this.options.iconCreateFunction({
                        getChildCount: function() {
                            return 1
                        },
                        getAllChildMarkers: function() {
                            return [t]
                        }
                    })); e >= 0; e--) {
                    n = this._map.project(t.getLatLng(), e);
                    var a = r[e].getNearObject(n);
                    if (a) return a._addChild(t), void(t.__parent = a);
                    if (a = o[e].getNearObject(n)) {
                        var s = a.__parent;
                        s && this._removeLayer(a, !1);
                        var l = new L.MarkerCluster(this, e, a, t);
                        r[e].addObject(l, this._map.project(l._cLatLng, e)), a.__parent = l, t.__parent = l;
                        var u = l;
                        for (i = e - 1; i > s._zoom; i--) u = new L.MarkerCluster(this, i, u), r[i].addObject(u, this._map.project(a.getLatLng(), i));
                        for (s._addChild(u), i = e; i >= 0 && o[i].removeObject(a, this._map.project(a.getLatLng(), i)); i--);
                        return
                    }
                    o[e].addObject(t, n)
                }
                this._topClusterLevel._addChild(t), t.__parent = this._topClusterLevel
            },
            _enqueue: function(t) {
                this._queue.push(t), this._queueTimeout || (this._queueTimeout = setTimeout(L.bind(this._processQueue, this), 300))
            },
            _processQueue: function() {
                for (var t = 0; t < this._queue.length; t++) this._queue[t].call(this);
                this._queue.length = 0, clearTimeout(this._queueTimeout), this._queueTimeout = null
            },
            _mergeSplitClusters: function() {
                this._processQueue(), this._zoom < this._map._zoom && this._currentShownBounds.intersects(this._getExpandedVisibleBounds()) ? (this._animationStart(), this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, this._zoom, this._getExpandedVisibleBounds()), this._animationZoomIn(this._zoom, this._map._zoom)) : this._zoom > this._map._zoom ? (this._animationStart(), this._animationZoomOut(this._zoom, this._map._zoom)) : this._moveEnd()
            },
            _getExpandedVisibleBounds: function() {
                if (!this.options.removeOutsideVisibleBounds) return this._map.getBounds();
                var t = this._map,
                    e = t.getBounds(),
                    n = e._southWest,
                    i = e._northEast,
                    r = L.Browser.mobile ? 0 : Math.abs(n.lat - i.lat),
                    o = L.Browser.mobile ? 0 : Math.abs(n.lng - i.lng);
                return new L.LatLngBounds(new L.LatLng(n.lat - r, n.lng - o, !0), new L.LatLng(i.lat + r, i.lng + o, !0))
            },
            _animationAddLayerNonAnimated: function(t, e) {
                if (e === t) this._featureGroup.addLayer(t);
                else if (2 === e._childCount) {
                    e._addToMap();
                    var n = e.getAllChildMarkers();
                    this._featureGroup.removeLayer(n[0]), this._featureGroup.removeLayer(n[1])
                } else e._updateIcon()
            }
        }), L.MarkerClusterGroup.include(L.DomUtil.TRANSITION ? {
            _animationStart: function() {
                this._map._mapPane.className += " leaflet-cluster-anim", this._inZoomAnimation++
            },
            _animationEnd: function() {
                this._map && (this._map._mapPane.className = this._map._mapPane.className.replace(" leaflet-cluster-anim", "")), this._inZoomAnimation--, this.fire("animationend")
            },
            _animationZoomIn: function(t, e) {
                var n, i = this._getExpandedVisibleBounds(),
                    r = this._featureGroup;
                this._topClusterLevel._recursively(i, t, 0, function(o) {
                    var a, s = o._latlng,
                        l = o._markers;
                    for (i.contains(s) || (s = null), o._isSingleParent() && t + 1 === e ? (r.removeLayer(o), o._recursivelyAddChildrenToMap(null, e, i)) : (o.clusterHide(), o._recursivelyAddChildrenToMap(s, e, i)), n = l.length - 1; n >= 0; n--) a = l[n], i.contains(a._latlng) || r.removeLayer(a)
                }), this._forceLayout(), this._topClusterLevel._recursivelyBecomeVisible(i, e), r.eachLayer(function(t) {
                    t instanceof L.MarkerCluster || !t._icon || t.clusterShow()
                }), this._topClusterLevel._recursively(i, t, e, function(t) {
                    t._recursivelyRestoreChildPositions(e)
                }), this._enqueue(function() {
                    this._topClusterLevel._recursively(i, t, 0, function(t) {
                        r.removeLayer(t), t.clusterShow()
                    }), this._animationEnd()
                })
            },
            _animationZoomOut: function(t, e) {
                this._animationZoomOutSingle(this._topClusterLevel, t - 1, e), this._topClusterLevel._recursivelyAddChildrenToMap(null, e, this._getExpandedVisibleBounds()), this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, t, this._getExpandedVisibleBounds())
            },
            _animationZoomOutSingle: function(t, e, n) {
                var i = this._getExpandedVisibleBounds();
                t._recursivelyAnimateChildrenInAndAddSelfToMap(i, e + 1, n);
                var r = this;
                this._forceLayout(), t._recursivelyBecomeVisible(i, n), this._enqueue(function() {
                    if (1 === t._childCount) {
                        var o = t._markers[0];
                        o.setLatLng(o.getLatLng()), o.clusterShow && o.clusterShow()
                    } else t._recursively(i, n, 0, function(t) {
                        t._recursivelyRemoveChildrenFromMap(i, e + 1)
                    });
                    r._animationEnd()
                })
            },
            _animationAddLayer: function(t, e) {
                var n = this,
                    i = this._featureGroup;
                i.addLayer(t), e !== t && (e._childCount > 2 ? (e._updateIcon(), this._forceLayout(), this._animationStart(), t._setPos(this._map.latLngToLayerPoint(e.getLatLng())), t.clusterHide(), this._enqueue(function() {
                    i.removeLayer(t), t.clusterShow(), n._animationEnd()
                })) : (this._forceLayout(), n._animationStart(), n._animationZoomOutSingle(e, this._map.getMaxZoom(), this._map.getZoom())))
            },
            _forceLayout: function() {
                L.Util.falseFn(e.body.offsetWidth)
            }
        } : {
            _animationStart: function() {},
            _animationZoomIn: function(t, e) {
                this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, t), this._topClusterLevel._recursivelyAddChildrenToMap(null, e, this._getExpandedVisibleBounds()), this.fire("animationend")
            },
            _animationZoomOut: function(t, e) {
                this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, t), this._topClusterLevel._recursivelyAddChildrenToMap(null, e, this._getExpandedVisibleBounds()), this.fire("animationend")
            },
            _animationAddLayer: function(t, e) {
                this._animationAddLayerNonAnimated(t, e)
            }
        }), L.markerClusterGroup = function(t) {
            return new L.MarkerClusterGroup(t)
        }, L.MarkerCluster = L.Marker.extend({
            initialize: function(t, e, n, i) {
                L.Marker.prototype.initialize.call(this, n ? n._cLatLng || n.getLatLng() : new L.LatLng(0, 0), {
                    icon: this
                }), this._group = t, this._zoom = e, this._markers = [], this._childClusters = [], this._childCount = 0, this._iconNeedsUpdate = !0, this._bounds = new L.LatLngBounds, n && this._addChild(n), i && this._addChild(i)
            },
            getAllChildMarkers: function(t) {
                t = t || [];
                for (var e = this._childClusters.length - 1; e >= 0; e--) this._childClusters[e].getAllChildMarkers(t);
                for (var n = this._markers.length - 1; n >= 0; n--) t.push(this._markers[n]);
                return t
            },
            getChildCount: function() {
                return this._childCount
            },
            zoomToBounds: function() {
                for (var t, e = this._childClusters.slice(), n = this._group._map, i = n.getBoundsZoom(this._bounds), r = this._zoom + 1, o = n.getZoom(); e.length > 0 && i > r;) {
                    r++;
                    var a = [];
                    for (t = 0; t < e.length; t++) a = a.concat(e[t]._childClusters);
                    e = a
                }
                i > r ? this._group._map.setView(this._latlng, r) : o >= i ? this._group._map.setView(this._latlng, o + 1) : this._group._map.fitBounds(this._bounds)
            },
            getBounds: function() {
                var t = new L.LatLngBounds;
                return t.extend(this._bounds), t
            },
            _updateIcon: function() {
                this._iconNeedsUpdate = !0, this._icon && this.setIcon(this)
            },
            createIcon: function() {
                return this._iconNeedsUpdate && (this._iconObj = this._group.options.iconCreateFunction(this), this._iconNeedsUpdate = !1), this._iconObj.createIcon()
            },
            createShadow: function() {
                return this._iconObj.createShadow()
            },
            _addChild: function(t, e) {
                this._iconNeedsUpdate = !0, this._expandBounds(t), t instanceof L.MarkerCluster ? (e || (this._childClusters.push(t), t.__parent = this), this._childCount += t._childCount) : (e || this._markers.push(t), this._childCount++), this.__parent && this.__parent._addChild(t, !0)
            },
            _expandBounds: function(t) {
                var e, n = t._wLatLng || t._latlng;
                t instanceof L.MarkerCluster ? (this._bounds.extend(t._bounds), e = t._childCount) : (this._bounds.extend(n), e = 1), this._cLatLng || (this._cLatLng = t._cLatLng || n);
                var i = this._childCount + e;
                this._wLatLng ? (this._wLatLng.lat = (n.lat * e + this._wLatLng.lat * this._childCount) / i, this._wLatLng.lng = (n.lng * e + this._wLatLng.lng * this._childCount) / i) : this._latlng = this._wLatLng = new L.LatLng(n.lat, n.lng)
            },
            _addToMap: function(t) {
                t && (this._backupLatlng = this._latlng, this.setLatLng(t)), this._group._featureGroup.addLayer(this)
            },
            _recursivelyAnimateChildrenIn: function(t, e, n) {
                this._recursively(t, 0, n - 1, function(t) {
                    var n, i, r = t._markers;
                    for (n = r.length - 1; n >= 0; n--) i = r[n], i._icon && (i._setPos(e), i.clusterHide())
                }, function(t) {
                    var n, i, r = t._childClusters;
                    for (n = r.length - 1; n >= 0; n--) i = r[n], i._icon && (i._setPos(e), i.clusterHide())
                })
            },
            _recursivelyAnimateChildrenInAndAddSelfToMap: function(t, e, n) {
                this._recursively(t, n, 0, function(i) {
                    i._recursivelyAnimateChildrenIn(t, i._group._map.latLngToLayerPoint(i.getLatLng()).round(), e), i._isSingleParent() && e - 1 === n ? (i.clusterShow(), i._recursivelyRemoveChildrenFromMap(t, e)) : i.clusterHide(), i._addToMap()
                })
            },
            _recursivelyBecomeVisible: function(t, e) {
                this._recursively(t, 0, e, null, function(t) {
                    t.clusterShow()
                })
            },
            _recursivelyAddChildrenToMap: function(t, e, n) {
                this._recursively(n, -1, e, function(i) {
                    if (e !== i._zoom)
                        for (var r = i._markers.length - 1; r >= 0; r--) {
                            var o = i._markers[r];
                            n.contains(o._latlng) && (t && (o._backupLatlng = o.getLatLng(), o.setLatLng(t), o.clusterHide && o.clusterHide()), i._group._featureGroup.addLayer(o))
                        }
                }, function(e) {
                    e._addToMap(t)
                })
            },
            _recursivelyRestoreChildPositions: function(t) {
                for (var e = this._markers.length - 1; e >= 0; e--) {
                    var n = this._markers[e];
                    n._backupLatlng && (n.setLatLng(n._backupLatlng), delete n._backupLatlng)
                }
                if (t - 1 === this._zoom)
                    for (var i = this._childClusters.length - 1; i >= 0; i--) this._childClusters[i]._restorePosition();
                else
                    for (var r = this._childClusters.length - 1; r >= 0; r--) this._childClusters[r]._recursivelyRestoreChildPositions(t)
            },
            _restorePosition: function() {
                this._backupLatlng && (this.setLatLng(this._backupLatlng), delete this._backupLatlng)
            },
            _recursivelyRemoveChildrenFromMap: function(t, e, n) {
                var i, r;
                this._recursively(t, -1, e - 1, function(t) {
                    for (r = t._markers.length - 1; r >= 0; r--) i = t._markers[r], n && n.contains(i._latlng) || (t._group._featureGroup.removeLayer(i), i.clusterShow && i.clusterShow())
                }, function(t) {
                    for (r = t._childClusters.length - 1; r >= 0; r--) i = t._childClusters[r], n && n.contains(i._latlng) || (t._group._featureGroup.removeLayer(i), i.clusterShow && i.clusterShow())
                })
            },
            _recursively: function(t, e, n, i, r) {
                var o, a, s = this._childClusters,
                    l = this._zoom;
                if (e > l)
                    for (o = s.length - 1; o >= 0; o--) a = s[o], t.intersects(a._bounds) && a._recursively(t, e, n, i, r);
                else if (i && i(this), r && this._zoom === n && r(this), n > l)
                    for (o = s.length - 1; o >= 0; o--) a = s[o], t.intersects(a._bounds) && a._recursively(t, e, n, i, r)
            },
            _recalculateBounds: function() {
                var t, e = this._markers,
                    n = this._childClusters;
                for (this._bounds = new L.LatLngBounds, delete this._wLatLng, t = e.length - 1; t >= 0; t--) this._expandBounds(e[t]);
                for (t = n.length - 1; t >= 0; t--) this._expandBounds(n[t])
            },
            _isSingleParent: function() {
                return this._childClusters.length > 0 && this._childClusters[0]._childCount === this._childCount
            }
        }), L.Marker.include({
            clusterHide: function() {
                return this.options.opacityWhenUnclustered = this.options.opacity || 1, this.setOpacity(0)
            },
            clusterShow: function() {
                var t = this.setOpacity(this.options.opacity || this.options.opacityWhenUnclustered);
                return delete this.options.opacityWhenUnclustered, t
            }
        }), L.DistanceGrid = function(t) {
            this._cellSize = t, this._sqCellSize = t * t, this._grid = {}, this._objectPoint = {}
        }, L.DistanceGrid.prototype = {
            addObject: function(t, e) {
                var n = this._getCoord(e.x),
                    i = this._getCoord(e.y),
                    r = this._grid,
                    o = r[i] = r[i] || {},
                    a = o[n] = o[n] || [],
                    s = L.Util.stamp(t);
                this._objectPoint[s] = e, a.push(t)
            },
            updateObject: function(t, e) {
                this.removeObject(t), this.addObject(t, e)
            },
            removeObject: function(t, e) {
                var n, i, r = this._getCoord(e.x),
                    o = this._getCoord(e.y),
                    a = this._grid,
                    s = a[o] = a[o] || {},
                    l = s[r] = s[r] || [];
                for (delete this._objectPoint[L.Util.stamp(t)], n = 0, i = l.length; i > n; n++)
                    if (l[n] === t) return l.splice(n, 1), 1 === i && delete s[r], !0
            },
            eachObject: function(t, e) {
                var n, i, r, o, a, s, l, u = this._grid;
                for (n in u) {
                    a = u[n];
                    for (i in a)
                        for (s = a[i], r = 0, o = s.length; o > r; r++) l = t.call(e, s[r]), l && (r--, o--)
                }
            },
            getNearObject: function(t) {
                var e, n, i, r, o, a, s, l, u = this._getCoord(t.x),
                    c = this._getCoord(t.y),
                    h = this._objectPoint,
                    d = this._sqCellSize,
                    f = null;
                for (e = c - 1; c + 1 >= e; e++)
                    if (r = this._grid[e])
                        for (n = u - 1; u + 1 >= n; n++)
                            if (o = r[n])
                                for (i = 0, a = o.length; a > i; i++) s = o[i], l = this._sqDist(h[L.Util.stamp(s)], t), d > l && (d = l, f = s);
                return f
            },
            _getCoord: function(t) {
                return Math.floor(t / this._cellSize)
            },
            _sqDist: function(t, e) {
                var n = e.x - t.x,
                    i = e.y - t.y;
                return n * n + i * i
            }
        },
        function() {
            L.QuickHull = {
                getDistant: function(t, e) {
                    var n = e[1].lat - e[0].lat,
                        i = e[0].lng - e[1].lng;
                    return i * (t.lat - e[0].lat) + n * (t.lng - e[0].lng)
                },
                findMostDistantPointFromBaseLine: function(t, e) {
                    var n, i, r, o = 0,
                        a = null,
                        s = [];
                    for (n = e.length - 1; n >= 0; n--) i = e[n], r = this.getDistant(i, t), r > 0 && (s.push(i), r > o && (o = r, a = i));
                    return {
                        maxPoint: a,
                        newPoints: s
                    }
                },
                buildConvexHull: function(t, e) {
                    var n = [],
                        i = this.findMostDistantPointFromBaseLine(t, e);
                    return i.maxPoint ? (n = n.concat(this.buildConvexHull([t[0], i.maxPoint], i.newPoints)), n = n.concat(this.buildConvexHull([i.maxPoint, t[1]], i.newPoints))) : [t[0]]
                },
                getConvexHull: function(t) {
                    var e, n = !1,
                        i = !1,
                        r = null,
                        o = null;
                    for (e = t.length - 1; e >= 0; e--) {
                        var a = t[e];
                        (n === !1 || a.lat > n) && (r = a, n = a.lat), (i === !1 || a.lat < i) && (o = a, i = a.lat)
                    }
                    var s = [].concat(this.buildConvexHull([o, r], t), this.buildConvexHull([r, o], t));
                    return s
                }
            }
        }(), L.MarkerCluster.include({
            getConvexHull: function() {
                var t, e, n = this.getAllChildMarkers(),
                    i = [];
                for (e = n.length - 1; e >= 0; e--) t = n[e].getLatLng(), i.push(t);
                return L.QuickHull.getConvexHull(i)
            }
        }), L.MarkerCluster.include({
            _2PI: 2 * Math.PI,
            _circleFootSeparation: 25,
            _circleStartAngle: Math.PI / 6,
            _spiralFootSeparation: 28,
            _spiralLengthStart: 11,
            _spiralLengthFactor: 5,
            _circleSpiralSwitchover: 9,
            spiderfy: function() {
                if (this._group._spiderfied !== this && !this._group._inZoomAnimation) {
                    var t, e = this.getAllChildMarkers(),
                        n = this._group,
                        i = n._map,
                        r = i.latLngToLayerPoint(this._latlng);
                    this._group._unspiderfy(), this._group._spiderfied = this, e.length >= this._circleSpiralSwitchover ? t = this._generatePointsSpiral(e.length, r) : (r.y += 10, t = this._generatePointsCircle(e.length, r)), this._animationSpiderfy(e, t)
                }
            },
            unspiderfy: function(t) {
                this._group._inZoomAnimation || (this._animationUnspiderfy(t), this._group._spiderfied = null)
            },
            _generatePointsCircle: function(t, e) {
                var n, i, r = this._group.options.spiderfyDistanceMultiplier * this._circleFootSeparation * (2 + t),
                    o = r / this._2PI,
                    a = this._2PI / t,
                    s = [];
                for (s.length = t, n = t - 1; n >= 0; n--) i = this._circleStartAngle + n * a, s[n] = new L.Point(e.x + o * Math.cos(i), e.y + o * Math.sin(i))._round();
                return s
            },
            _generatePointsSpiral: function(t, e) {
                var n, i = this._group.options.spiderfyDistanceMultiplier * this._spiralLengthStart,
                    r = this._group.options.spiderfyDistanceMultiplier * this._spiralFootSeparation,
                    o = this._group.options.spiderfyDistanceMultiplier * this._spiralLengthFactor,
                    a = 0,
                    s = [];
                for (s.length = t, n = t - 1; n >= 0; n--) a += r / i + 5e-4 * n, s[n] = new L.Point(e.x + i * Math.cos(a), e.y + i * Math.sin(a))._round(), i += this._2PI * o / a;
                return s
            },
            _noanimationUnspiderfy: function() {
                var t, e, n = this._group,
                    i = n._map,
                    r = n._featureGroup,
                    o = this.getAllChildMarkers();
                for (this.setOpacity(1), e = o.length - 1; e >= 0; e--) t = o[e], r.removeLayer(t), t._preSpiderfyLatlng && (t.setLatLng(t._preSpiderfyLatlng), delete t._preSpiderfyLatlng), t.setZIndexOffset && t.setZIndexOffset(0), t._spiderLeg && (i.removeLayer(t._spiderLeg), delete t._spiderLeg);
                n._spiderfied = null
            }
        }), L.MarkerCluster.include(L.DomUtil.TRANSITION ? {
            SVG_ANIMATION: function() {
                return e.createElementNS("http://www.w3.org/2000/svg", "animate").toString().indexOf("SVGAnimate") > -1
            }(),
            _animationSpiderfy: function(t, i) {
                var r, o, a, s, l = this,
                    u = this._group,
                    c = u._map,
                    h = u._featureGroup,
                    d = c.latLngToLayerPoint(this._latlng);
                for (r = t.length - 1; r >= 0; r--) o = t[r], o.setOpacity ? (o.setZIndexOffset(1e6), o.clusterHide(), h.addLayer(o), o._setPos(d)) : h.addLayer(o);
                u._forceLayout(), u._animationStart();
                var f = L.Path.SVG ? 0 : .3,
                    p = L.Path.SVG_NS;
                for (r = t.length - 1; r >= 0; r--) {
                    s = c.layerPointToLatLng(i[r]), o = t[r], o._preSpiderfyLatlng = o._latlng, o.setLatLng(s), o.setOpacity && o.clusterShow();
                    var m = this._group.options.spiderLegPolylineOptions;
                    if (m.opacity === n && (m.opacity = f), a = new L.Polyline([l._latlng, s], m), c.addLayer(a), o._spiderLeg = a, L.Path.SVG && this.SVG_ANIMATION) {
                        var g = a._path.getTotalLength();
                        a._path.setAttribute("stroke-dasharray", g + "," + g);
                        var v = e.createElementNS(p, "animate");
                        v.setAttribute("attributeName", "stroke-dashoffset"), v.setAttribute("begin", "indefinite"), v.setAttribute("from", g), v.setAttribute("to", 0), v.setAttribute("dur", .25), a._path.appendChild(v), v.beginElement(), v = e.createElementNS(p, "animate"), v.setAttribute("attributeName", "stroke-opacity"), v.setAttribute("attributeName", "stroke-opacity"), v.setAttribute("begin", "indefinite"), v.setAttribute("from", 0), v.setAttribute("to", .5), v.setAttribute("dur", .25), a._path.appendChild(v), v.beginElement()
                    }
                }
                if (l.setOpacity(.3), L.Path.SVG)
                    for (this._group._forceLayout(), r = t.length - 1; r >= 0; r--) o = t[r]._spiderLeg, o.options.opacity = .5, o._path.setAttribute("stroke-opacity", .5);
                setTimeout(function() {
                    u._animationEnd(), u.fire("spiderfied")
                }, 200)
            },
            _animationUnspiderfy: function(t) {
                var e, n, i, r = this._group,
                    o = r._map,
                    a = r._featureGroup,
                    s = t ? o._latLngToNewLayerPoint(this._latlng, t.zoom, t.center) : o.latLngToLayerPoint(this._latlng),
                    l = this.getAllChildMarkers(),
                    u = L.Path.SVG && this.SVG_ANIMATION;
                for (r._animationStart(), this.setOpacity(1), n = l.length - 1; n >= 0; n--) e = l[n], e._preSpiderfyLatlng && (e.setLatLng(e._preSpiderfyLatlng), delete e._preSpiderfyLatlng, e.setOpacity ? (e._setPos(s), e.clusterHide()) : a.removeLayer(e), u && (i = e._spiderLeg._path.childNodes[0], i.setAttribute("to", i.getAttribute("from")), i.setAttribute("from", 0), i.beginElement(), i = e._spiderLeg._path.childNodes[1], i.setAttribute("from", .5), i.setAttribute("to", 0), i.setAttribute("stroke-opacity", 0), i.beginElement(), e._spiderLeg._path.setAttribute("stroke-opacity", 0)));
                setTimeout(function() {
                    var t = 0;
                    for (n = l.length - 1; n >= 0; n--) e = l[n], e._spiderLeg && t++;
                    for (n = l.length - 1; n >= 0; n--) e = l[n], e._spiderLeg && (e.setOpacity && (e.clusterShow(), e.setZIndexOffset(0)), t > 1 && a.removeLayer(e), o.removeLayer(e._spiderLeg), delete e._spiderLeg);
                    r._animationEnd()
                }, 200)
            }
        } : {
            _animationSpiderfy: function(t, e) {
                var n, i, r, o, a = this._group,
                    s = a._map,
                    l = a._featureGroup;
                for (n = t.length - 1; n >= 0; n--) {
                    o = s.layerPointToLatLng(e[n]), i = t[n], i._preSpiderfyLatlng = i._latlng, i.setLatLng(o), i.setZIndexOffset && i.setZIndexOffset(1e6), l.addLayer(i);
                    var u = this._group.options.spiderLegPolylineOptions;
                    r = new L.Polyline([this._latlng, o], u), s.addLayer(r), i._spiderLeg = r
                }
                this.setOpacity(.3), a.fire("spiderfied")
            },
            _animationUnspiderfy: function() {
                this._noanimationUnspiderfy()
            }
        }), L.MarkerClusterGroup.include({
            _spiderfied: null,
            _spiderfierOnAdd: function() {
                this._map.on("click", this._unspiderfyWrapper, this), this._map.options.zoomAnimation && this._map.on("zoomstart", this._unspiderfyZoomStart, this), this._map.on("zoomend", this._noanimationUnspiderfy, this), L.Path.SVG && !L.Browser.touch && this._map._initPathRoot()
            },
            _spiderfierOnRemove: function() {
                this._map.off("click", this._unspiderfyWrapper, this), this._map.off("zoomstart", this._unspiderfyZoomStart, this), this._map.off("zoomanim", this._unspiderfyZoomAnim, this), this._unspiderfy()
            },
            _unspiderfyZoomStart: function() {
                this._map && this._map.on("zoomanim", this._unspiderfyZoomAnim, this)
            },
            _unspiderfyZoomAnim: function(t) {
                L.DomUtil.hasClass(this._map._mapPane, "leaflet-touching") || (this._map.off("zoomanim", this._unspiderfyZoomAnim, this), this._unspiderfy(t))
            },
            _unspiderfyWrapper: function() {
                this._unspiderfy()
            },
            _unspiderfy: function(t) {
                this._spiderfied && this._spiderfied.unspiderfy(t)
            },
            _noanimationUnspiderfy: function() {
                this._spiderfied && this._spiderfied._noanimationUnspiderfy()
            },
            _unspiderfyLayer: function(t) {
                t._spiderLeg && (this._featureGroup.removeLayer(t), t.setOpacity(1), t.setZIndexOffset(0), this._map.removeLayer(t._spiderLeg), delete t._spiderLeg)
            }
        })
}(window, document);
