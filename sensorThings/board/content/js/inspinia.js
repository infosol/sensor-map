$(document).ready(function() {
    function t() {
        var t = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", t + "px");
        var e = $(window).height();
        $("#page-wrapper").css("min-height", e + "px")
    }
    $(window).bind("load resize click scroll", function() {
        $("body").hasClass("body-small") || t()
    }), t()
}), $(function() {
    $(window).bind("load resize", function() {
        $(this).width() < 769 ? ($("body").addClass("mini-navbar"), $("body").addClass("body-small")) : ($("body").removeClass("body-small"), $("body").removeClass("mini-navbar"))
    })
});
