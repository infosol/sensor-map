function pageTitle(t, e) {
    return {
        link: function(a, r) {
            var o = function(t, a) {
                var o = "sensorBoard";
                a.data && a.data.pageTitle && (o = "SensorBoard | " + a.data.pageTitle), e(function() {
                    r.text(o)
                })
            };
            t.$on("$stateChangeStart", o)
        }
    }
}

function sideNavigation() {
    return {
        restrict: "A",
        link: function(t, e) {
            e.metisMenu()
        }
    }
}

function iboxTools(t) {
    return {
        restrict: "A",
        scope: !0,
        templateUrl: "views/common/ibox_tools.html",
        controller: function(e, a) {
            e.showhide = function() {
                var e = a.closest("div.ibox"),
                    r = a.find("i:first"),
                    o = e.find("div.ibox-content");
                o.slideToggle(200), r.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down"), e.toggleClass("").toggleClass("border-bottom"), t(function() {
                    e.resize(), e.find("[id^=map-]").resize()
                }, 50)
            }, e.closebox = function() {
                var t = a.closest("div.ibox");
                t.remove()
            }
        }
    }
}

function minimalizaSidebar(t) {
    return {
        restrict: "A",
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: ["$scope", "$element", function(e) {
            e.minimalize = function() {
                $("body").toggleClass("mini-navbar"), !$("body").hasClass("mini-navbar") || $("body").hasClass("body-small") ? ($("#side-menu").hide(), t(function() {
                    $("#side-menu").fadeIn(500)
                }, 100)) : $("#side-menu").removeAttr("style")
            }
        }]
    }
}
angular.module("sensorBoard").directive("pageTitle", ["$rootScope", "$timeout", pageTitle]).directive("sideNavigation", [sideNavigation]).directive("iboxTools", ["$timeout", iboxTools]).directive("minimalizaSidebar", ["$timeout", minimalizaSidebar]);
